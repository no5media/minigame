length_num=0;
var GuiderLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        Guider_sp=[];
        NUM=0;
        NUM_X=0;//被移动的精灵的x值
        NUM_Y=0;//被移动的精灵的y值
        Block_Array=[];
        Array_Sprite=[];
        Array_SpriteRole=[];
        MAX_ROW=5
        IS_MOVE=false;
        Is_Can=true;
        var winsize = cc.winSize;

        bg = new cc.Sprite(res.bg);
        bg.setPosition(winsize.width / 2, winsize.height / 2);
        bg.setAnchorPoint(0.5, 0.5)
        this.addChild(bg);

        var leylabel = new cc.Sprite("res/gameStart/guider.png");
        leylabel.x = winsize.width/2;
        leylabel.setAnchorPoint(0.5,0.5);
        leylabel.y = 1000;
        this.addChild(leylabel,1);

        bg1 = new cc.Sprite(res.bg1);
        bg1.setPosition(winsize.width / 2, 650);
        bg1.setAnchorPoint(0.5, 0.5)
        this.addChild(bg1);
        for(var a=0;a<5;a++){
            for(var b=0;b<5;b++) {
                bg2 = new cc.Sprite(res.bg2);
                bg2.setPosition(184 + b * 100, 450 + a * 100);
                bg2.setAnchorPoint(0.5, 0.5);
                this.addChild(bg2);
                var sp={
                    x:184 + b * 100,
                    y: 450 + a * 100,
                    row:a,
                    col:b,
                }
                Block_Array.push(sp);
            }
        }
        for(var a=0;a<levelData[level-1].length;a++){
            var m=levelData[level-1][a][2];
            //var SpriteRole=flax.assetsManager.createDisplay(res.All_plist,"b"+m,{parent:this,x:185+100*levelData[level-1][a][0],y:430+99*levelData[level-1][a][1]});
            var SpriteRole=new cc.Sprite(g_resources[m+4]);
            SpriteRole.setPosition(184+100*levelData[level-1][a][0],450+100*levelData[level-1][a][1])
            SpriteRole.setAnchorPoint(0.5,0.5);
            this.addChild(SpriteRole);
            SpriteRole.tag=m;
            SpriteRole.row=levelData[level-1][a][1];
            SpriteRole.col=levelData[level-1][a][0];
            Array_SpriteRole[SpriteRole.row*MAX_ROW+SpriteRole.col]=SpriteRole;
        }
        for(var a=0;a<levelData1[level-1].length;a++){
            var m=levelData1[level-1][a][1];
            var Sp_x=levelData1[level-1][a][2];
            //var SpriteRole=flax.assetsManager.createDisplay(res.All_plist,"b"+m,{parent:this,x:(winSize.width-Sp_x),y:310});
            var SpriteRole=new cc.Sprite(g_resources[m-1]);
            SpriteRole.setPosition(winsize.width-Sp_x,310)
            SpriteRole.setAnchorPoint(0.5,0.5);
            this.addChild(SpriteRole);
            SpriteRole.row=0;
            SpriteRole.col=0;
            SpriteRole.tag=m;
            SpriteRole.bool=false;
            SpriteRole.setAnchorPoint(0.5,0.5);
            Array_Sprite.push(SpriteRole);
        }

        var self = this;
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                for(var a=0;a<Array_Sprite.length;a++){
                    if(cc.rectIntersectsRect(Array_Sprite[a].getBoundingBox(),rect)){
                        NUM_X=Array_Sprite[a].x;
                        NUM_Y=Array_Sprite[a].y;
                        NUM=a;

                        IS_MOVE=true;
                    }
                }
                    return true;
            },
            onTouchMoved: function (touch, event) {
                if(IS_MOVE){
                    if(NUM==0){
                        Array_Sprite[0].stopAllActions();
                        Array_Sprite[0].setOpacity(200);
                    }
                    Array_Sprite[NUM].setLocalZOrder(2)
                    Array_Sprite[NUM].x=touch.getLocation().x;
                    Array_Sprite[NUM].y=touch.getLocation().y;
                }
            },
            onTouchEnded: function (touch,event) {
                if((touch.getLocation().x>=134&&touch.getLocation().x<=632)&&(touch.getLocation().y>=380&&touch.getLocation().y<=878)){
                    if(IS_MOVE){
                        var Min=Math.sqrt((Array_Sprite[NUM].x-Block_Array[0].x)*(Array_Sprite[NUM].x-Block_Array[0].x)+(Array_Sprite[NUM].y-Block_Array[0].y)*(Array_Sprite[NUM].y-Block_Array[0].y));
                        var Min_a=0
                        var Min_n=0
                        for(var a=1;a<Block_Array.length;a++){
                            if(Min>Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))){
                                Min=Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))
                                Min_a=a;
                            }else if(Min==Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))){
                                Min_n++;
                            }
                        }
                        if(!Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]&&Min_n==0){
                             if(NUM==0){
                                 var Min_tag= Array_Sprite[NUM].tag;
                                 var Remov=cc.callFunc(function () {
                                     self.CheckRowSprite( Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col],Min_tag);
                                 },this)
                                 Array_Sprite[NUM].runAction(cc.sequence(cc.moveTo(0.2,Block_Array[Min_a].x,Block_Array[Min_a].y),Remov));
                                 if(Is_MUSIC){
                                     //cc.audioEngine.playEffect("res/move.wav",false);
                                 }
                                 Array_Sprite[NUM].setLocalZOrder(1);
                                 Array_Sprite[NUM].row=Block_Array[Min_a].row;
                                 Array_Sprite[NUM].col=Block_Array[Min_a].col;
                                 Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]= Array_Sprite[NUM];
                                 Array_Sprite.splice(NUM,1);
                             }else {
                                 Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y));
                             }
                        }else{
                            Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y));
                        }
                        IS_MOVE=false;
                    }
                }else{
                    if(IS_MOVE){
                        IS_MOVE=false;
                        Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y))
                    }
                }
            }
        });
        cc.eventManager.addListener(this.touchListener, this);
        this.CreateBink();
        this.SpriteRun();
    },
    CheckRowSprite: function (object,M,posX,posY) {
        var Array_CheckColSprite=[];
        Array_CheckColSprite.push(object);
        var ChekRole_col=object.col-1;
        while(ChekRole_col>=0){
            var CheckRole=Array_SpriteRole[object.row*MAX_ROW+ChekRole_col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckColSprite.push(CheckRole);
                ChekRole_col--;
            }else{
                break;
            }
        }
        var ChekRole_col=object.col+1;
        while(ChekRole_col<5){
            var CheckRole=Array_SpriteRole[object.row*MAX_ROW+ChekRole_col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckColSprite.push(CheckRole);
                ChekRole_col++;
            }else{
                break;
            }
        }

        var Array_CheckRowSprite=[];
        Array_CheckRowSprite.push(object);
        var ChekRoleRight_row=object.row+1
        while(ChekRoleRight_row<5){
            var CheckRole=Array_SpriteRole[ChekRoleRight_row*MAX_ROW+object.col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckRowSprite.push(CheckRole);
                ChekRoleRight_row++;
            }else{
                break;
            }
        };
        var ChekRole_row=object.row-1
        while(ChekRole_row>=0){
            var CheckRole=Array_SpriteRole[ChekRole_row*MAX_ROW+object.col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckRowSprite.push(CheckRole);
                ChekRole_row--;
            }else{
                break;
            }
        }
        if(Array_CheckRowSprite.length==5||Array_CheckColSprite.length==5){
            if(Array_CheckRowSprite.length==5&&Array_CheckColSprite.length==5){
                for(var a=0;a<Array_CheckColSprite.length;a++){
                    if(Array_CheckRowSprite.indexOf(Array_CheckColSprite[a])==-1){
                        Array_CheckRowSprite.push(Array_CheckColSprite[a]);
                    }
                }
                for(var a=0;a<Array_CheckColSprite.length;a++){
                    Array_CheckColSprite.splice(a,1);
                    a--;
                }
                this.RemoveSprite(Array_CheckRowSprite,M);
            }else if(Array_CheckRowSprite.length==5){
                this.RemoveSprite(Array_CheckRowSprite,M);
            }else if(Array_CheckColSprite.length==5){
                this.RemoveSprite(Array_CheckColSprite,M);
            }

        }else{
            cc.director.runScene(new GuiderScene());
        }
    },
    RemoveSprite: function (Array_SP,M) {
        if(Array_SP.length==5) {
            for (var a = 0; a < Array_SP.length; a++) {
                Array_SpriteRole[Array_SP[a].row * MAX_ROW + Array_SP[a].col] = null;
                var fadout = new cc.fadeOut(0.6);
                //var blink =new cc.Blink(0.2,2);//闪烁函数
                var destory = new cc.RemoveSelf(true);
                var fun=new cc.callFunc(function () {
                    level=2;
                    cc.director.runScene(new GuiderTwoScene())
                })
                var number=0;
                for(var b=0;b<Array_SpriteRole.length;b++){
                    if(Array_SpriteRole[b]){
                        number++;
                    }
                }
                if(number==0){
                    Array_SP[a].runAction(cc.sequence(fadout, destory,fun));
                }else {
                    Array_SP[a].runAction(cc.sequence(fadout, destory));
                }
            }
        }
        Guider_sp[0].removeFromParent();
        Guider_sp.splice(0,1);
        if(length_num<4){
            length_num++;
            this.CreateBink();
            this.SpriteRun();
        }

    },
    SpriteRun: function () {
        var fadein1=new cc.fadeIn(0.3,180);
        var fadein2=new cc.fadeOut(0.3,0);
        Array_Sprite[0].runAction(cc.repeatForever(cc.sequence(fadein1,fadein2)));
    },
    CreateBink: function () {
        //var guiderSp=flax.assetsManager.createDisplay(res.All1_plist,"guider",{parent:this,x:GuiderData[level-1][length_num][0],y:GuiderData[level-1][length_num][1]});
        var guiderSp=new cc.Sprite(res.bink);
        guiderSp.setPosition(GuiderData[level-1][length_num][0],GuiderData[level-1][length_num][1])
        guiderSp.setAnchorPoint(0.5,0.5);
        guiderSp.setOpacity(0);
        this.addChild(guiderSp)
        guiderSp.setLocalZOrder(4);
        var fadein1=new cc.fadeIn(0.3,180);
        var fadein2=new cc.fadeOut(0.3,0);
        guiderSp.runAction(cc.repeatForever(cc.sequence(fadein1,fadein2)));
        Guider_sp.push(guiderSp);
    },
});
var GuiderScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GuiderLayer();
        this.addChild(layer);

    }
})