Record_Data=[];
var loadLayer=cc.LayerColor.extend({
    ctor: function () {
        this._super(cc.color(0,0,0));
        time=0;
        start_time=Math.floor(Math.random()*5+5);
        end_time=0;
        var winSize=cc.winSize;
        var bg=new cc.Sprite("res/Newwork/loading.png");
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);
        var str="倒计时：   "+"秒";
        var str_name=new cc.LabelTTF(str,"Arial",30);
        str_name.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(str_name)
        str_time=new cc.LabelTTF("0","Arial",30);
        str_time.setPosition(winSize.width/2+35,winSize.height/2);
        str_time.setColor(0,0,0)
        this.addChild(str_time)
        str_time.setString(start_time)

        this.CreteXML1();
        var btn=new cc.MenuItemImage(
            "res/Newwork/load_btn.png",
            "res/Newwork/load_btn.png",
            function () {
                cc.director.runScene(new GameStartScene())

            }
        )
        btn.setPosition(winSize.width/2,winSize.height/2-70);
        var menu=new cc.Menu();
        var menu = new cc.Menu(btn);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu);
        this.scheduleUpdate();
    },
    update: function (dt) {
        time+=dt;
        if(time>=1){
            time=0;
            start_time--;
            if(start_time<=0){
                this.unscheduleAllCallbacks();
                cc.director.runScene(new NetWorkScene())
            }
            str_time.setString(start_time);
            if(start_time==end_time){
                this.unscheduleAllCallbacks();
                cc.director.runScene(new NetWorkScene())
            }
        }
    },
    CreteXML1: function () {
        var xhr = cc.loader.getXMLHttpRequest();
        //var  url="/openapi/game/interactionGame/getGameLog.json?appId="+appId
        var  url="/openapi/game/interactionGame/matchGameLog.json?appId="+appId+"&gameType="+1
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        var self=this;
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status <= 207){
                    var resw = xhr.responseText;
                    var jsondata = JSON.parse(resw);
                    self.data1 = jsondata["data"];
                    var Data = jsondata["data"];
                    if(self.data1.interActionPlayGameLog){
                        Record_Data=self.data1.interActionPlayGameLog.gameLog;
                        if(typeof Record_Data === 'string'){
                            Record_Data = JSON.parse(Record_Data);
                        }
                    }else{
                        Record_Data=null;
                    }
                    if(Record_Data){
                        cc.log("从服务上下载有数据",Record_Data)
                        RecordData=Record_Data[0];
                        SpRecord.downblock=Record_Data[2];
                        SpRecord.upblock=Record_Data[1];
                        end_time=start_time-self.data1.interActionPlayGameLog.waitTime;
                        Record_Data=self.data1.interActionPlayGameLog.gameLog;
                        record_Id=self.data1.interActionPlayGameLog.id
                    }else{
                        RecordData=Data_RecordData;
                        SpRecord.downblock=Data_SpRecord.downblock;
                        SpRecord.upblock=Data_SpRecord.upblock;
                        end_time=Math.floor(Math.random()*2);
                        cc.log("服务上没有数据，读取本地的数据",Record_Data)
                    }
                }
            }
        }
        xhr.send();
    },
})

var loadScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new loadLayer();
        this.addChild(layer);
    }
});