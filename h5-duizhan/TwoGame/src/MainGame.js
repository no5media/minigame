IQ_level=1;
IQ_level1=1;
Score_Num=0;
CoinNum_num=0;//奖学金
BestScore_num=0;
SuperRubberNumber=1
ChangeRubberNumber=0;
Best_level=1;
num_i=0;
Is_first=0;
Object1=null;
Is_use=0;
Sp_Record_Array=[];//录制存储数组
tag_Record_Array=[]
tag_Record_Array.upblock=[]
tag_Record_Array.downblock=[]
Record_time=0;//录制数据的时间点
Record_getScore=0;//录制数据的最高分
Is_Record=false//录制开始
Main_layer=null
Is_Record_end=false//录制开始
Is_Angin=false;
Is_overOrplay=false

var MaingameLayer  = cc.Layer.extend({
    CoinNum:null,
    Score:null,
    BestScore:null,
    NUM:null,
    Run_Sprite:null,
    SuperRubber_number:null,
    Rubber_number:null,
    bg:null,
    Change_Rubber_number:null,
    _paySuccess:null,
    _payError:null,
    _payClose:null,
    //cocos compile -p web -m release

    ctor: function () {
        this._super();
        Array_test = [];
        var winsize = cc.visibleRect;
        Block_Array=[];
        Array_Sprite = [];
        Array_SpriteRole = [];
        Array_Sprite1 = [];
        Array_SpriteColorRole = [];
        Array_SpriteNumberRole = [];
        Main_layer=this
        Is_Angin=false;
        Is_overOrplay=false
        Score=0



        IS_MOVE = false;
        IS_CREATE = false;
        IS_BEGIN = true;
        IS_CLICK = false;
        IS_REDBTN_CLICK = true;
        IS_BTN_CLICK = true;
        IS_CLEAR = false;
        IS_USER_RUBBER = false;
        MAX_ROW = 5;
        this.time = 0;

        NUM = 0;//被移动精灵的的下标
        NUM_X = 0;//被移动的精灵的x值
        NUM_Y = 0;//被移动的精灵的y值
        //左上角进度条
        COLOR_row = 0;
        COLOR_col = 0;
        Begain_COLOR_row = 0;
        Begain_COLOR_col = 0;
        Begain_M = 1;
        BUSHU = 32;
        Object1 = this;
        Is_Start = false;
        IS_Superrun = false;
        Is_ru = false
        Is_xhr = false;
        //橡皮的数量
        buy_num = 60000;
        Guider_num = 0;//判断是几连消
        RubberNumber = 2;
        bg = new cc.Sprite(res.bg);
        bg.setPosition(winsize.width / 2, winsize.height / 2);
        bg.setAnchorPoint(0.5, 0.5);
        this.addChild(bg);
        bg1 = new cc.Sprite(res.bg1);
        bg1.setPosition(winsize.width / 2, 650);
        bg1.setAnchorPoint(0.5, 0.5);
        this.addChild(bg1);

        var btn1 = new cc.MenuItemImage(
            "res/mainGame/SuperRubber.png",
            "res/mainGame/SuperRubber.png",
            function () {
                if(Is_Start){
                    self.RedOneFun();
                }
            }, this
        )
        btn1.setPosition(210, 200);
        var btn2 = new cc.MenuItemImage(
            "res/mainGame/Rubber.png",
            "res/mainGame/Rubber.png",
            function () {
                if(Is_Start){
                    self.RedTwoFun();
                }
            }, this
        )
        btn2.setPosition(420, 200);
        var btn3 = new cc.MenuItemImage(
            res.angin,
            res.angin,
            function () {
                cc.director.runScene(new MaingameScene());
            }, this
        )
        btn3.setPosition(350,950)
        var menu = new cc.Menu(btn1,btn2,btn3);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu);

        var ChangeRubber=new cc.Sprite("res/mainGame/ChangeRubber.png");
        ChangeRubber.setPosition(560,200)
        this.addChild(ChangeRubber)

        SuperRubber_number=new cc.LabelAtlas(SuperRubberNumber, "res/1.png", 54, 76,"0");
        SuperRubber_number.x=150;
        SuperRubber_number.y=200;
        SuperRubber_number.setScale(0.3);
        SuperRubber_number.setAnchorPoint(0.5,0.5);
        this.addChild(SuperRubber_number,1);

        Rubber_number=new cc.LabelAtlas(RubberNumber, "res/1.png", 54, 76,"0");
        Rubber_number.x=360;
        Rubber_number.y=200;
        Rubber_number.setScale(0.3);
        Rubber_number.setAnchorPoint(0.5,0.5);
        this.addChild(Rubber_number,1);

        Change_Rubber_number=new cc.LabelAtlas("0", "res/1.png", 54, 76,"0");
        Change_Rubber_number.x=592;
        Change_Rubber_number.y=200;
        Change_Rubber_number.setScale(0.26);
        Change_Rubber_number.setAnchorPoint(0.5,0.5);
        this.addChild(Change_Rubber_number,1);

        var Rubber= "/5";
        var leylabel = new cc.LabelTTF(Rubber,"Arial",30);
        leylabel.x = 615;
        leylabel.y = 200;
        this.addChild(leylabel,1);
        var bushu=new cc.Sprite(res.bushu);
        bushu.setPosition(210,985);
        this.addChild(bushu,1);
        var level=new cc.Sprite(res.level);
        level.setPosition(530,1020);
        this.addChild(level,1);
        var anginlevel=new cc.Sprite(res.anginlevel);
        anginlevel.setPosition(530,950)
        this.addChild(anginlevel,1)
        var score=new cc.Sprite(res.score);
        score.setPosition(350,1020);
        score.setAnchorPoint(0.5,0.5)
        this.addChild(score,1);
        this.bushu_num=new cc.LabelAtlas("32", "res/1.png", 54, 76,"0");
        this.bushu_num.setPosition(210,985);
        this.bushu_num.setAnchorPoint(0.5,0.5)
        //this.bushu_num.setScale(0.3)
        this.addChild( this.bushu_num,1)
        this.level_num=new cc.LabelAtlas(IQ_level, "res/1.png", 54, 76,"0");
        this.level_num.setPosition(530,990)
        this.level_num.setScale(0.3)
        this.addChild( this.level_num,1)
        this.anginlevel_num=new cc.LabelAtlas(Best_level, "res/1.png", 54, 76,"0");
        this.anginlevel_num.setPosition(530,920)
        this.anginlevel_num.setScale(0.3)
        this.addChild(this.anginlevel_num,1)
        this.score_num=new cc.LabelAtlas(CoinNum_num, "res/1.png", 54, 76,"0");
        this.score_num.setPosition(350,990)
        this.score_num.setScale(0.3)
        this.addChild(this.score_num,1);
        for(var a=0;a<5;a++){
            for(var b=0;b<5;b++) {
                bg2 = new cc.Sprite(res.bg2);
                bg2.setPosition(184 + b * 100, 450 + a * 100);
                bg2.setAnchorPoint(0.5, 0.5);
                this.addChild(bg2);
                var sp={
                    x:184 + b * 100,
                    y: 450 + a * 100,
                    row:a,
                    col:b,
                }
                Block_Array.push(sp);
            }
        }
        var self = this;
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                if(IS_CLICK){
                    var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                    for(var a=0;a<Array_Sprite.length;a++){
                        if(cc.rectIntersectsRect(Array_Sprite[a].getBoundingBox(),rect)){
                            NUM_X=Array_Sprite[a].x;
                            NUM_Y=Array_Sprite[a].y;
                            NUM=a;
                            IS_MOVE=true;
                        }
                    }
                    return true;
                }
            },
            onTouchMoved: function (touch, event) {
                if(IS_MOVE){
                    Array_Sprite[NUM].setLocalZOrder(2)
                    Array_Sprite[NUM].x=touch.getLocation().x;
                    Array_Sprite[NUM].y=touch.getLocation().y;
                }
            },
            onTouchEnded: function (touch,event) {
                if((touch.getLocation().x>=134&&touch.getLocation().x<=632)&&(touch.getLocation().y>=380&&touch.getLocation().y<=878)){
                    if(IS_MOVE){
                        var Min=Math.sqrt((Array_Sprite[NUM].x-Block_Array[0].x)*(Array_Sprite[NUM].x-Block_Array[0].x)+(Array_Sprite[NUM].y-Block_Array[0].y)*(Array_Sprite[NUM].y-Block_Array[0].y));
                        var Min_a=0
                        var Min_n=0
                        for(var a=1;a<Block_Array.length;a++){
                            if(Min>Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))){
                                Min=Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))
                                Min_a=a;
                            }else if(Min==Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))){
                                Min_n++;
                            }
                        }
                        if(!Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]&&Min_n==0){
                            var Min_tag= Array_Sprite[NUM].tag;
                            var Remov=cc.callFunc(function () {
                                self.CheckRowSprite( Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col],Min_tag);
                            },this)
                            Array_Sprite[NUM].runAction(cc.sequence(cc.moveTo(0.2,Block_Array[Min_a].x,Block_Array[Min_a].y),Remov));
                            if(Is_MUSIC){
                                cc.audioEngine.playEffect("res/move.wav",false);
                                cc.audioEngine.playEffect("res/move.ogg",false);
                            }
                            Array_Sprite[NUM].setLocalZOrder(1);
                            var Record_tag=Array_Sprite[NUM].tag
                            Array_Sprite[NUM].row=Block_Array[Min_a].row;
                            Array_Sprite[NUM].col=Block_Array[Min_a].col;
                            Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]= Array_Sprite[NUM];
                            Array_Sprite.splice(NUM,1);
                            var time1=Record_time*100
                            var time2=Math.round(time1)
                            var time3=time2/100
                            cc.log(time3)
                            if(!Is_Record_end){
                                Sp_Record_Array[Sp_Record_Array.length]={
                                    row:Block_Array[Min_a].row,
                                    col:Block_Array[Min_a].col,
                                    tag:Record_tag,
                                    time:time3
                                }
                            }

                        }else{
                            Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y));
                        }
                        IS_MOVE=false;
                    }
                }else{
                    if(IS_MOVE){
                        IS_MOVE=false;
                        Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y))
                    }
                }
            }
        });
        cc.eventManager.addListener(this.touchListener, this);
        this.CreateIQSprite();
        this.scheduleUpdate()
        //var layer=new GameOverLayer();
        //this.addChild(layer,10)
    },
    update: function (dt) {
        if(Is_Record&&!Is_Record_end){
            Record_time+=dt
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false
            //加步数
            BUSHU+=5
            this.bushu_num.setString(BUSHU);
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            Is_overOrplay=false
            var transition=new cc.TransitionMoveInT(1,new GameOverScene());
            cc.director.runScene(transition);
        }
    },
    CheckRowSprite: function (object,M) {
        var Array_CheckColSprite=[];
        Array_CheckColSprite.push(object);
        var ChekRole_col=object.col-1;
        while(ChekRole_col>=0){
            var CheckRole=Array_SpriteRole[object.row*MAX_ROW+ChekRole_col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckColSprite.push(CheckRole);
                ChekRole_col--;
            }else{
                break;
            }
        }
        var ChekRole_col=object.col+1;
        while(ChekRole_col<5){
            var CheckRole=Array_SpriteRole[object.row*MAX_ROW+ChekRole_col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckColSprite.push(CheckRole);
                ChekRole_col++;
            }else{
                break;
            }
        }
        var Array_CheckRowSprite=[];
        Array_CheckRowSprite.push(object);
        var ChekRoleRight_row=object.row+1
        while(ChekRoleRight_row<5){
            var CheckRole=Array_SpriteRole[ChekRoleRight_row*MAX_ROW+object.col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckRowSprite.push(CheckRole);
                ChekRoleRight_row++;
            }else{
                break;
            }
        };
        var ChekRole_row=object.row-1
        while(ChekRole_row>=0){
            var CheckRole=Array_SpriteRole[ChekRole_row*MAX_ROW+object.col];
            if(CheckRole&&CheckRole.tag==object.tag){
                Array_CheckRowSprite.push(CheckRole);
                ChekRole_row--;
            }else{
                break;
            }
        }
        BUSHU--;
        //if(BUSHU>0&&BUSHU<=3){
        //    var move1=new cc.RotateBy(0.1,5);
        //    var move2=new cc.RotateBy(0.2,-10);
        //    var move3=new cc.RotateBy(0.1,5);
        //    var cal=cc.callFunc(function () {
        //        var fd = new cc.scaleTo(0.15,1.5);
        //        var sx = new cc.scaleTo(0.25,1);
        //        this.this.bushu_num.runAction(cc.sequence(fd,sx));
        //    },this)
        //    this.bushu_num.runAction(cc.spawn(cc.sequence(move1,move2,move3),cal));
        //}
           if(BUSHU<1){
            // GAMEOVER
            if(Array_CheckRowSprite.length==5||Array_CheckColSprite.length==5){
            }else{
                var angin_num=cc.sys.localStorage.getItem("hsdzzAngin")
                if(angin_num<3){
                    var layer = new AnginLayer(1);
                    this.addChild(layer,10);
                }else{
                    Is_overOrplay=true;
                }
            }
        }else{
            this.bushu_num.setString(BUSHU);
        }

        if(Array_CheckRowSprite.length==5||Array_CheckColSprite.length==5){
            if(Array_CheckRowSprite.length==5&&Array_CheckColSprite.length==5){
                for(var a=0;a<Array_CheckColSprite.length;a++){
                    if(Array_CheckRowSprite.indexOf(Array_CheckColSprite[a])==-1){
                        Array_CheckRowSprite.push(Array_CheckColSprite[a]);
                    }
                };
                for(var a=0;a<Array_CheckColSprite.length;a++){
                    Array_CheckColSprite.splice(a,1);
                    a--;
                }
                this.RemoveSprite(Array_CheckRowSprite,M);
            }else if(Array_CheckRowSprite.length==5){
                this.RemoveSprite(Array_CheckRowSprite,M);
            }else if(Array_CheckColSprite.length==5){
                this.RemoveSprite(Array_CheckColSprite,M);
            }
        }else{
            if( Array_Sprite.length==0){
                this.CreateSprite();
            }
            var a_NUM = 0;
            for (var a = 0; a < 25; a++) {
                if (!Array_SpriteRole[a]) {
                    a_NUM++
                }
            }
            if (Array_SpriteRole.length!=1&&a_NUM == 0) {
                this.unscheduleAllCallbacks();
                var transition=new cc.TransitionMoveInT(1,new GameOverScene());
                cc.director.runScene(transition);

            }
        }
        /*
        if(BUSHU==10){
            var m=0;
            for(a=0;a<Array_SpriteRole.length;a++){
                if(!Array_SpriteRole[a]){
                    m++
                }
            }
            if(m!=0){
                this.CreateGuiderTwo(9)
            }
        }else
         */
        if(BUSHU==3){
            var m=0;
            for(a=0;a<Array_SpriteRole.length;a++){
                if(!Array_SpriteRole[a]){
                    m++
                }
            }
            if(m!=0){
                this.CreateGuider()
            }
        }
    },
    CreateGuider: function () {
        var winsize=cc.visibleRect;
        var sp=new cc.Sprite("res/mainGame/n.png")
        sp.setPosition(winsize.width*1.5,winsize.height/2);
        //sp.setPosition(winsize.width/2,winsize.height/2);
        this.addChild(sp,100);
        //var sp_num=new cc.LabelAtlas("3", "res/1.png", 54, 76,"0");
        //sp_num.setPosition(35,30);
        //sp_num.setScale(0.8)
        //sp.addChild(sp_num)
        var move1=cc.moveBy(0.2,-winsize.width,0);
        var dely=cc.delayTime(1.5);
        var destory = new cc.RemoveSelf(true);
        sp.runAction(cc.sequence(move1,dely,move1,destory));
    },
    RemoveSprite: function (Array_SP,M) {
      if(Array_SP.length==5){
            ChangeRubberNumber++;
            if(ChangeRubberNumber==5){
                RubberNumber++;
                Rubber_number.setString(RubberNumber);
                ChangeRubberNumber=0;
                Change_Rubber_number.setString(ChangeRubberNumber);
            }else{
                Change_Rubber_number.setString(ChangeRubberNumber);
            }
        }else if(Array_SP.length==9){
            ChangeRubberNumber+=2;
            if(ChangeRubberNumber>=5){
                RubberNumber++;
                Rubber_number.setString(RubberNumber);
                ChangeRubberNumber=ChangeRubberNumber-5;
                Change_Rubber_number.setString(ChangeRubberNumber);
            }else{
                Change_Rubber_number.setString(ChangeRubberNumber);
            }
        }
        CoinNum_num+=Math.pow(2,M);
        this.score_num.setString(CoinNum_num);

        for(var a=0;a<Array_SP.length;a++){
            Array_SpriteRole[Array_SP[a].row*MAX_ROW+Array_SP[a].col]=null;
            //var fadout=new cc.fadeOut(0.6);
            var fd = new cc.scaleTo(0.15,1.2);
            var sx = new cc.scaleTo(0.25,0);
            //var blink =new cc.Blink(0.2,2);//闪烁函数
            var destory = new cc.RemoveSelf(true);
            if(Is_MUSIC){
                cc.audioEngine.playEffect("res/xiao.wav",false);
                cc.audioEngine.playEffect("res/xiao.ogg",false);
            }
            Array_SP[a].runAction(cc.sequence(fd,sx,destory));
        }

            var IQ_num=0;
            for(var a=0;a<Array_SpriteRole.length;a++){
                if(Array_SpriteRole[a]&&Array_SpriteRole[a].IQ_IS_Check){
                    IQ_num++;
                }
            }
            if(IQ_num==0){
                IQ_level++;
                if(IQ_level>22){
                    IQ_level=22;
                }
                IQ_level1++;
                if(Best_level<IQ_level1){
                    Best_level=IQ_level1;
                }
                //if(!IS_USER_RUBBER){
                //    var winsize=cc.visibleRect;
                //    var guider=flax.assetsManager.createDisplay(res.All1_plist,"g"+7,{parent:this,x:winsize.width/2,y:winsize.height/2});
                //    guider.setAnchorPoint(0.5,0.5);
                //    guider.setLocalZOrder(4);
                //    var fade=cc.fadeOut(1);
                //    guider.runAction(fade);
                //    SuperRubberNumber++;
                //}else{
                //    this.CreateGuider(6)
                //}
                this.scheduleOnce(function () {
                    if(Is_use==0){
                        SuperRubberNumber++
                    }else {
                        Is_use=0;
                    }
                    Record_getScore=BUSHU;
                    cc.director.runScene(new MaingameScene());
                },0.8);
            }
            if( Array_Sprite.length==0){
                this.CreateSprite();
            }
    },
    CreateSprite: function () {
        var winsize=cc.visibleRect;
        var arary=[]
        for(var a=0;a<5;a++){
            var m=Math.floor(cc.rand()%5+1);
            //var SpriteRole=flax.assetsManager.createDisplay(res.All_plist,"b"+m,{parent:this,x:winsize.width,y:310});
            var SpriteRole=new cc.Sprite(g_resources[m-1]);
            SpriteRole.setPosition(winsize.width,300);
            this.addChild(SpriteRole)
            SpriteRole.row=0;
            SpriteRole.col=0;
            SpriteRole.tag=m;
            SpriteRole.bool=false;
            SpriteRole.IQ_IS_Check=false;
            SpriteRole.setAnchorPoint(0.5,0.5);
            Array_Sprite.push(SpriteRole);
            arary.push(SpriteRole.tag)
        }
        if(!Is_Record_end){
            tag_Record_Array.downblock[ tag_Record_Array.downblock.length]=arary
            //cc.log("tag_Record_Array.downblock",tag_Record_Array.downblock)
        }
        Array_Sprite[0].runAction(cc.sequence(cc.moveBy(0.2,-180,0)));
        Array_Sprite[1].runAction(cc.sequence(cc.moveBy(0.3,-285,0)));
        Array_Sprite[2].runAction(cc.sequence(cc.moveBy(0.4,-387,0)));
        Array_Sprite[3].runAction(cc.sequence(cc.moveBy(0.5,-488,0)));
        var Click=cc.callFunc(function () {
            IS_CLICK=true;
            Is_Start=true;
        },this)
        Array_Sprite[4].runAction(cc.sequence(cc.moveBy(0.6,-590,0),Click));

    },
    CreateIQSprite: function () {
        var Array_NUM=[];
        var Len=(IQ_level+2);
        if(Len>3){
            if(Is_use==0){
                this.CreteXML();
            }
            Is_Record_end=true

        }
        for(var a=0;a<Len;a++){
            var nn=Math.floor(cc.rand()%25);
            if(Array_NUM.indexOf(nn)==-1){
                Array_NUM.push(nn);
            }else{
                Len++;
            }
        }
        var time=0;
        var array=[]
        for(var a=0;a<Array_NUM.length;a++){
            var num_X=Block_Array[Array_NUM[a]].x;
            var num_Y=Block_Array[Array_NUM[a]].y;
            var m=Math.floor(cc.rand()%5+1);
            var SpriteRole=new cc.Sprite(g_resources[4+m]);
            SpriteRole.setPosition(num_X,num_Y);
            this.addChild(SpriteRole);
            //var SpriteRole=flax.assetsManager.createDisplay(res.All1_plist,"m"+m,{parent:this,x:num_X,y:num_Y});
            SpriteRole.row=Block_Array[Array_NUM[a]].row;
            SpriteRole.col=Block_Array[Array_NUM[a]].col;
            SpriteRole.tag=m;
            SpriteRole.bool=false;
            SpriteRole.start=false;
            SpriteRole.IQ_IS_Check=true;
            var Fadein=new cc.fadeIn(0.5,255)
            SpriteRole.setOpacity(0);
            var dely=cc.delayTime(0.4*a);
            if(time<0.4*a){
                time=0.4*a;
            }
            var call=cc.callFunc(function () {
                SpriteRole.start=true;
            },this)
            SpriteRole.runAction(cc.sequence(dely,Fadein,call));
            Array_SpriteRole[SpriteRole.row*MAX_ROW+SpriteRole.col]=SpriteRole;
            if(!Is_Record_end){
              array[array.length]={
                    row: SpriteRole.row,
                    col: SpriteRole.col,
                    tag: SpriteRole.tag,
                }
            }
        }
        if(!Is_Record_end){
            tag_Record_Array.upblock[tag_Record_Array.upblock.length]=array
        }
        this.scheduleOnce(function () {
            this.CreateSprite();
            Is_Record=true;
        },time)
    },

    run: function () {
        //IS_CLICK_ONE=true;
        for(var a =0;a<Array_SpriteRole.length;a++){
            if(Array_SpriteRole[a]){
                var move1=new cc.RotateBy(0.08,4);
                var move2=new cc.RotateBy(0.08,-8);
                var move3=new cc.RotateBy(0.08,4);

                Array_SpriteRole[a].runAction(cc.repeatForever(cc.sequence(move1,move2,move3)));
            }
        }
    },
    RedOneFun: function () {
        var num=0;
        for(var a=0;a<Array_SpriteRole.length;a++){
            if(Array_SpriteRole[a]){
                num++
            }
        }
        if(num!=0&&SuperRubberNumber>0){
            IS_REDBTN_CLICK=false;
            for(var a =0;a<Array_SpriteRole.length;a++){
                if(Array_SpriteRole[a]){
                    Array_SpriteRole[a].stopAllActions();
                }
            }
            this.run();
            cc.eventManager.removeListener(this.touchListener);
            cc.eventManager.removeListener(this.touchListener1);
            cc.eventManager.removeListener(this.touchListener2);
            var self=this;
            this.touchListener1 = cc.EventListener.create({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                swallowTouches: true,
                onTouchBegan: function (touch, event) {
                    var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                    var  Array_MoveSprite=[];
                    if(Array_SpriteRole.length>0){
                        for(var a=0;a<Array_SpriteRole.length;a++) {
                            if (Array_SpriteRole[a]&&cc.rectIntersectsRect(Array_SpriteRole[a].getBoundingBox(), rect)) {
                                Array_MoveSprite.push(Array_SpriteRole[a]);
                                Array_SpriteRole[Array_SpriteRole[a].row*MAX_ROW+Array_SpriteRole[a].col]=null;
                            }
                        }
                        for(var b=0;b<Array_SpriteRole.length;b++){
                            if (Array_SpriteRole[b]&&Array_MoveSprite[0]&&Array_MoveSprite[0].tag==Array_SpriteRole[b].tag) {
                                Array_MoveSprite.push(Array_SpriteRole[b]);
                                Array_SpriteRole[Array_SpriteRole[b].row*MAX_ROW+Array_SpriteRole[b].col]=null;
                            }
                        }
                        if(Array_MoveSprite.length==0){
                            cc.eventManager.removeListener(self.touchListener1);
                            cc.eventManager.addListener(self.touchListener, self);
                            for(var a =0;a<Array_SpriteRole.length;a++){
                                if(Array_SpriteRole[a]){
                                    Array_SpriteRole[a].stopAllActions();
                                    Array_SpriteRole[a].rotation=0
                                }
                            }
                            IS_REDBTN_CLICK=true;
                        }else {
                            SuperRubberNumber--;
                            Is_use++;
                            SuperRubber_number.setString(SuperRubberNumber);
                            for(var a=0;a<Array_MoveSprite.length;a++){
                                var fd = new cc.scaleTo(0.15,1.2);
                                var sx = new cc.scaleTo(0.25,0);
                                Array_MoveSprite[a].runAction(cc.sequence(fd,sx));
                                Array_MoveSprite.splice(a,1);
                                a--;
                                IS_USER_RUBBER=true;
                            }
                            cc.eventManager.removeListener(self.touchListener1);
                            cc.eventManager.addListener(self.touchListener, self);
                            for(var a =0;a<Array_SpriteRole.length;a++){
                                if(Array_SpriteRole[a]){
                                    Array_SpriteRole[a].stopAllActions();
                                    Array_SpriteRole[a].rotation=0
                                }
                            }
                                var IQ_num = 0;
                                for (var a = 0; a < Array_SpriteRole.length; a++) {
                                    if (Array_SpriteRole[a] && Array_SpriteRole[a].IQ_IS_Check) {
                                        IQ_num++;
                                    }
                                }
                                if (IQ_num == 0) {
                                    IQ_level++;
                                    if(IQ_level>22){
                                        IQ_level=22;
                                    }
                                    IQ_level1++;
                                    if(Best_level<IQ_level1){
                                        Best_level=IQ_level1;
                                    }
                                    self.scheduleOnce(function () {
                                        if(Is_use==0){
                                            SuperRubberNumber++
                                        }else {
                                            Is_use=0;
                                        }
                                        cc.director.runScene(new MaingameScene());
                                    },0.6)
                                }
                            IS_REDBTN_CLICK=true;
                        }
                    }else{
                        cc.eventManager.removeListener(self.touchListener1);
                        cc.eventManager.addListener(self.touchListener, self);
                        for(var a =0;a<Array_SpriteRole.length;a++){
                            if(Array_SpriteRole[a]){
                                Array_SpriteRole[a].stopAllActions();
                                Array_SpriteRole[a].rotation=0
                            }
                        }
                        IS_REDBTN_CLICK=true;
                    }
                    return true;
                },
                onTouchMoved: function (touch, event) {

                },
                onTouchEnded: function (touch, event) {
                }
            });
            cc.eventManager.addListener(this.touchListener1, this);
        }else if(SuperRubberNumber<=0) {
        }
    },
    RedTwoFun: function () {
        var num=0;
        for(var a=0;a<Array_SpriteRole.length;a++){
            if(Array_SpriteRole[a]){
                num++
            }
        }
        if(num!=0&&RubberNumber>0){
            IS_BTN_CLICK=false;
            for(var a =0;a<Array_SpriteRole.length;a++){
                if(Array_SpriteRole[a]){
                    Array_SpriteRole[a].stopAllActions();
                }
            }
            for(var a =0;a<Array_SpriteRole.length;a++){
                if(Array_SpriteRole[a]){
                    var move1=new cc.RotateBy(0.08,4);
                    var move2=new cc.RotateBy(0.08,-8);
                    var move3=new cc.RotateBy(0.08,4);
                    Array_SpriteRole[a].runAction(cc.repeatForever(cc.sequence(move1,move2,move3)));
                }
            }
            cc.eventManager.removeListener(this.touchListener);
            cc.eventManager.removeListener(this.touchListener1);
            cc.eventManager.removeListener(this.touchListener2);
            var self=this;
            this.touchListener2 = cc.EventListener.create({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                swallowTouches: true,
                onTouchBegan: function (touch, event) {
                    var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                    var  Array_MoveSprite=[];
                    if(Array_SpriteRole.length>0){
                        for(var a=0;a<Array_SpriteRole.length;a++) {
                            if (Array_SpriteRole[a]&&cc.rectIntersectsRect(Array_SpriteRole[a].getBoundingBox(), rect)) {
                                Array_MoveSprite.push(Array_SpriteRole[a]);
                                Array_SpriteRole[Array_SpriteRole[a].row*MAX_ROW+Array_SpriteRole[a].col]=null;
                            }
                        }
                        if(Array_MoveSprite.length==0){
                            cc.eventManager.removeListener(self.touchListener2);
                            cc.eventManager.addListener(self.touchListener, self);
                            for(var a =0;a<Array_SpriteRole.length;a++){
                                if(Array_SpriteRole[a]){
                                    Array_SpriteRole[a].stopAllActions();
                                    Array_SpriteRole[a].rotation=0
                                }
                            }
                            IS_BTN_CLICK=true;
                        }else {
                            RubberNumber--;
                            Is_use++;
                            Rubber_number.setString(RubberNumber);
                            for(var a=0;a<Array_MoveSprite.length;a++){
                                var fd = new cc.scaleTo(0.15,1.2);
                                var sx = new cc.scaleTo(0.25,0);
                                Array_MoveSprite[a].runAction(cc.sequence(fd,sx));
                                //var fadout=new cc.fadeOut(0.6);
                                //Array_MoveSprite[a].runAction(fadout);
                                Array_MoveSprite.splice(a,1);
                                a--;
                                IS_USER_RUBBER=true;
                            }
                            cc.eventManager.removeListener(self.touchListener2);
                            cc.eventManager.addListener(self.touchListener, self);
                            for(var a =0;a<Array_SpriteRole.length;a++){
                                if(Array_SpriteRole[a]){
                                    Array_SpriteRole[a].stopAllActions();
                                    Array_SpriteRole[a].rotation=0
                                }
                            }

                                var IQ_num = 0;
                                for (var a = 0; a < Array_SpriteRole.length; a++) {
                                    if (Array_SpriteRole[a] && Array_SpriteRole[a].IQ_IS_Check) {
                                        IQ_num++;
                                    }
                                }
                                if (IQ_num == 0) {
                                    IQ_level++;
                                    if(IQ_level>22){
                                        IQ_level=22;
                                    }
                                    IQ_level1++;
                                    if(Best_level<IQ_level1){
                                        Best_level=IQ_level1;
                                    }
                                    //if(!IS_USER_RUBBER){
                                    //    self.CreateGuider(7)
                                    //}else{
                                    //    self.CreateGuider(6)
                                    //}
                                    self.scheduleOnce(function () {
                                        if(Is_use==0){
                                            SuperRubberNumber++
                                        }else {
                                            Is_use=0;
                                        }


                                        cc.director.runScene(new MaingameScene());
                                    },0.6)
                                }

                            IS_BTN_CLICK=true;
                        }
                    }else{
                        cc.eventManager.removeListener(self.touchListener2);
                        cc.eventManager.addListener(self.touchListener, self);
                        for(var a =0;a<Array_SpriteRole.length;a++){
                            if(Array_SpriteRole[a]){
                                Array_SpriteRole[a].stopAllActions();
                                Array_SpriteRole[a].rotation=0;
                            }
                        }
                        IS_BTN_CLICK=true;
                    }
                    return true;
                },
                onTouchMoved: function (touch, event) {

                },
                onTouchEnded: function (touch, event) {
                }
            });
            cc.eventManager.addListener(this.touchListener2, this);
        }else if(RubberNumber<=0){}

    },
    CreteXML: function () {
        var xhr = cc.loader.getXMLHttpRequest();
        //var jsondata = JSON.parse(Sp_Record_Array);
        //cc.log("867",jsondata)
        var allData=[];
        cc.log("Sp_Record_Array",Sp_Record_Array)
        allData.push(Sp_Record_Array);
        allData.push(tag_Record_Array.upblock)
        allData.push(tag_Record_Array.downblock)
        var jsondata1 = JSON.stringify(allData)
        var game_score=Sp_Record_Array[Sp_Record_Array.length-1].time;
        var game_type=1;
        var availabity=1
        var type_score=Sp_Record_Array[Sp_Record_Array.length-1].time;
        cc.log("869",jsondata1);
        //var  url="/openapi/game/interactionGame/postGameLog.json?appId="+appId+"&gameScore="+100+"&gameLog="+jsondata1
        var  url="/openapi/game/interactionGame/postGameLog.json?availabity="+availabity+"&appId="+appId+"&gameScore="+game_score+"&gameLog="+jsondata1+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+0
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        var self=this;
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status <= 207){
                    var resw = xhr.responseText;
                    var jsondata = JSON.parse(resw);
                }
            }
        }
        //xhr.send();
    },



})

var MaingameScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new MaingameLayer();
        this.addChild(layer);
    }
});