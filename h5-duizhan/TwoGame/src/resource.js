var res = {
    sp1:"res/mainGame/sp1.png",
    sp2:"res/mainGame/sp2.png",
    sp3:"res/mainGame/sp3.png",
    sp4:"res/mainGame/sp4.png",
    sp5:"res/mainGame/sp5.png",
    white1:"res/mainGame/white1.png",
    white2:"res/mainGame/white2.png",
    white3:"res/mainGame/white3.png",
    white4:"res/mainGame/white4.png",
    white5:"res/mainGame/white5.png",
    btn:"res/gameStart/btn.png",
    startbg:"res/start/start_bg.png",
    help:"res/gameStart/help.png",
    bg:"res/mainGame/bg.png",
    bg1:"res/mainGame/bg1.png",
    bg2:"res/mainGame/bg2.png",
    bushu:"res/mainGame/bushu.png",
    angin:"res/mainGame/angin.png",
    anginlevel:"res/mainGame/anginlevel.png",
    level:"res/mainGame/level.png",
    score:"res/mainGame/score.png",
    bink:"res/bink.png",


    Score: "res/start/Score.png",
    start_bg1:"res/start/start_bg1.png",
    start_btn1: "res/start/start_btn1.png",
    start_btn2:  "res/start/start_btn2.png",
    follow:"res/start/follow.png",
    moregame:  "res/start/moregame.png",
    invite:"res/start/invite.png",


};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
