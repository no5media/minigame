var GuiderTwoLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        Array_Sprite=[];
        Array_All_Sprite=[];
        NUM=0;
        NUM_X=0;//被移动的精灵的x值
        NUM_Y=0;//被移动的精灵的y值
        Guider_num=0;
        MAX_ROW=5;
        COLOR_row=0;
        COLOR_col=0;
        Array_SpriteRole=[];
        Guider_sp=[];
        var winSize = cc.visibleRect;
        IS_CLICK_ONE=false;
        IS_CLICK_TWO=false;
        Is_Yes=false;
        bg = new cc.Sprite(res.bg);
        bg.setPosition(winSize.width / 2, winSize.height / 2);
        bg.setAnchorPoint(0.5, 0.5)
        this.addChild(bg);
        var leylabel = new cc.Sprite("res/gameStart/guider.png");
        leylabel.x = winSize.width/2;
        leylabel.setAnchorPoint(0.5,0.5);
        leylabel.y = 1000;
        this.addChild(leylabel,1);
        bg1 = new cc.Sprite(res.bg1);
        bg1.setPosition(winSize.width / 2, 650);
        bg1.setAnchorPoint(0.5, 0.5)
        this.addChild(bg1);
        for(var a=0;a<5;a++){
            for(var b=0;b<5;b++) {
                bg2 = new cc.Sprite(res.bg2);
                bg2.setPosition(184 + b * 100, 450 + a * 100);
                bg2.setAnchorPoint(0.5, 0.5);
                this.addChild(bg2);
                var sp={
                    x:184 + b * 100,
                    y: 450 + a * 100,
                    row:a,
                    col:b,
                }
                Block_Array.push(sp);
            }
        }
        var self=this
        rubber= new cc.MenuItemImage(
            "res/mainGame/Rubber.png",
            "res/mainGame/Rubber.png",

            function () {
               self.RedOneFun();
            }, this
        )
        rubber.setPosition(381, 240);
        var menu = new cc.Menu(rubber);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu);
        var move1=new cc.moveBy(0.1,0,8);
        var move2=new cc.moveBy(0.1,0,-16);
        var move3=new cc.moveBy(0.1,0,8);
        rubber.runAction(cc.repeatForever(cc.sequence(move1,move2,move3)));

        var Rubber= "普通炸弹：\n"+"使用一次会将格子盘上的任意一块花色方块消掉。";
        this.le1 = new cc.LabelTTF(Rubber,"Arial",30);
        this.le1.x = 400;
        this.le1.y = 100;
        this.le1.setColor(cc.color(122,109,103));
        this.addChild(this.le1,1);



        for(var a=0;a<levelData[level-1].length;a++){
            var m=levelData[level-1][a][2];
            //var SpriteRole=flax.assetsManager.createDisplay(res.All_plist,"b"+m,{parent:this,x:185+100*levelData[level-1][a][0],y:430+99*levelData[level-1][a][1]});
            var SpriteRole=new cc.Sprite(g_resources[4+m]);
            SpriteRole.setPosition(184+100*levelData[level-1][a][0],450+100*levelData[level-1][a][1])
            SpriteRole.setAnchorPoint(0.5,0.5);
            this.addChild(SpriteRole);
            SpriteRole.tag=m;
            SpriteRole.row=levelData[level-1][a][1];
            SpriteRole.col=levelData[level-1][a][0];
            Array_SpriteRole[SpriteRole.row*MAX_ROW+SpriteRole.col]=SpriteRole;
        }

        var self = this;
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                var  Array_MoveSprite=[];
                if(level==2&&IS_CLICK_ONE){
                    if(Array_SpriteRole.length>0){
                        for(var a=0;a<Array_SpriteRole.length;a++) {
                            if (Array_SpriteRole[a]&&cc.rectIntersectsRect(Array_SpriteRole[a].getBoundingBox(), rect)) {
                                Array_MoveSprite.push(Array_SpriteRole[a]);
                                Array_SpriteRole[Array_SpriteRole[a].row*MAX_ROW+Array_SpriteRole[a].col]=null;
                            }
                        }
                        if(Array_MoveSprite.length==0){
                            for(var a =0;a<Array_SpriteRole.length;a++){
                                if(Array_SpriteRole[a]){
                                    Array_SpriteRole[a].stopAllActions();
                                    Array_SpriteRole[a].rotation=0
                                }
                            }
                        }else {
                            for(var a=0;a<Array_MoveSprite.length;a++){
                                var fadout=new cc.fadeOut(0.6);
                                var FUN=cc.callFunc(function () {
                                    rubber.removeFromParent();
                                    self.le1.removeFromParent();
                                    rubber1= new cc.MenuItemImage(
                                        "res/mainGame/SuperRubber.png",
                                        "res/mainGame/SuperRubber.png",
                                        function () {
                                            self.RedTwoFun();
                                        }, this
                                    )
                                    rubber1.setPosition(381, 240);
                                    var menu = new cc.Menu(rubber1);
                                    menu.x = 0;
                                    menu.y = 0;
                                    self.addChild(menu);

                                    var Rubber= "超级炸弹:\n"+"使用一次会将格子盘上所有同一花色的方块一起消除。";
                                    self.le2 = new cc.LabelTTF(Rubber,"Arial",30);
                                    self.le2.x = 400;
                                    self.le2.y = 100;
                                    self.le2.setColor(cc.color(122,109,103));
                                    self.addChild(self.le2,1);

                                    var move1=new cc.moveBy(0.1,0,8);
                                    var move2=new cc.moveBy(0.1,0,-16);
                                    var move3=new cc.moveBy(0.1,0,8);
                                    rubber1.runAction(cc.repeatForever(cc.sequence(move1,move2,move3)));
                                    level++
                                },this)
                                Array_MoveSprite[a].runAction(cc.sequence(fadout,FUN));
                                Array_MoveSprite.splice(a,1);
                                a--;
                            }
                            for(var a =0;a<Array_SpriteRole.length;a++){
                                if(Array_SpriteRole[a]){
                                    Array_SpriteRole[a].stopAllActions();
                                    Array_SpriteRole[a].rotation=0
                                }
                            }
                        }

                    }else{
                        for(var a =0;a<Array_SpriteRole.length;a++){
                            if(Array_SpriteRole[a]){
                                Array_SpriteRole[a].stopAllActions();
                                Array_SpriteRole[a].rotation=0;;
                            }
                        }
                    }
                }
                if(level==3&&IS_CLICK_ONE){
                    if(Array_SpriteRole.length>0){
                        for(var a=0;a<Array_SpriteRole.length;a++) {
                            if (Array_SpriteRole[a]&&cc.rectIntersectsRect(Array_SpriteRole[a].getBoundingBox(), rect)) {
                                Array_MoveSprite.push(Array_SpriteRole[a]);
                                Array_SpriteRole[Array_SpriteRole[a].row*MAX_ROW+Array_SpriteRole[a].col]=null;
                                Is_Yes=true;
                            }
                        }
                        if(Is_Yes){
                            for(var b=0;b<Array_SpriteRole.length;b++){
                                if (Array_SpriteRole[b]&&Array_MoveSprite[0]&&Array_MoveSprite[0].tag==Array_SpriteRole[b].tag) {
                                    Array_MoveSprite.push(Array_SpriteRole[b]);
                                    Array_SpriteRole[Array_SpriteRole[b].row*MAX_ROW+Array_SpriteRole[b].col]=null;
                                }
                            }
                            for(var a=0;a<Array_MoveSprite.length;a++){
                                var fadout=new cc.fadeOut(0.6);
                                Array_MoveSprite[a].runAction(fadout);
                                Array_MoveSprite.splice(a,1);
                                a--;
                            }
                            var dely=cc.delayTime(0.4)
                            var call=cc.callFunc(function () {
                                cc.director.runScene(new GameStartScene())
                                cc.sys.localStorage.setItem("zzw","50")

                            },this)
                            self.runAction(cc.sequence(dely,call))
                        }else {
                            for(var b=0;b<Array_SpriteRole.length;b++){
                                if(Array_SpriteRole[b]){
                                    Array_SpriteRole[b].stopAllActions();
                                    Array_SpriteRole[b].rotation=0;
                                }

                            }
                        }
                    }else{
                        for(var a =0;a<Array_SpriteRole.length;a++){
                            if(Array_SpriteRole[a]){
                                Array_SpriteRole[a].stopAllActions();
                                Array_SpriteRole[a].rotation=0
                            }
                        }
                    }

                }

                return true;
            },
            onTouchMoved: function (touch, event) {
            },
            onTouchEnded: function () {
                IS_CLICK_ONE=false;
            }
        });
        cc.eventManager.addListener(this.touchListener,this);

    },
    RedOneFun: function () {
        IS_CLICK_ONE=true;
        for(var a =0;a<Array_SpriteRole.length;a++){
            if(Array_SpriteRole[a]){
                var move1=new cc.RotateBy(0.08,4);
                var move2=new cc.RotateBy(0.08,-8);
                var move3=new cc.RotateBy(0.08,4);
                Array_SpriteRole[a].runAction(cc.repeatForever(cc.sequence(move1,move2,move3)));
            }
        }
    },
    RedTwoFun: function () {
        IS_CLICK_ONE=true;
        for(var a =0;a<Array_SpriteRole.length;a++){
            if(Array_SpriteRole[a]){
                var move1=new cc.RotateBy(0.08,4);
                var move2=new cc.RotateBy(0.08,-8);
                var move3=new cc.RotateBy(0.08,4);
                Array_SpriteRole[a].runAction(cc.repeatForever(cc.sequence(move1,move2,move3)));
            }
        }
    }
});
var GuiderTwoScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GuiderTwoLayer();
        this.addChild(layer);

    }
})