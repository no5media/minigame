var GuiderData=[
    [[584,450,4,0],[484, 550,3,1],[384,650,2,2],[284,750,1,3],[184,850,0,4]],
    [585,430],
    [285,628]
];
var levelData=[
    [[0,0,5],[0,1,5],[0,2,5],[0,3,5],[1,0,4],[1,1,4],[1,2,4],[1,4,4],[2,0,3],[2,1,3],[2,3,3],[2,4,3],[3,0,2],[3,2,2],[3,3,2],[3,4,2],[4,4,1],[4,1,1],[4,2,1],[4,3,1]],
    [[0,1,3],[0,2,3],[0,3,3],[0,4,3],[1,2,3],[1,3,3],[1,4,3],[2,3,3],[2,4,3],[3,4,3]],
    [[0,1,3],[0,2,3],[0,3,3],[0,4,3],[1,2,3],[1,3,3],[1,4,3],[2,3,3],[2,4,3],[3,4,3]],
];
var levelData1=[
    [[0,1,180],[1,2,285],[2,3,387],[3,4,488],[3,5,590]],
];
var level=1;
var gamelevel=1;
//var RecordData=Record_Data[0]
var RecordData;
var SpRecord=[];
SpRecord.downblock=[];
SpRecord.upblock=[];
var Data_RecordData= [
    {col:0,row:3,tag:3,time:4.13},
    {col:1,row:0,tag:4,time:5.45},
    {col:3,row:2,tag:2,time:6.77},
    {col:0,row:1,tag:1,time:8.1},
    {col:1,row:1,tag:1,time:9.25},

    {col:1,row:3,tag:3,time:11.22},
    {col:2,row:0,tag:4,time:12.56},
    {col:3,row:0,tag:4,time:13.85},
    {col:2,row:2,tag:2,time:15.37},
    {col:2,row:1,tag:1,time:17.12},

    {col:4,row:0,tag:4,time:19.32},
    {col:1,row:2,tag:2,time:20.64},
    {col:3,row:1,tag:1,time:23.36},
    {col:1,row:4,tag:5,time:24.62},
    {col:2,row:4,tag:5,time:25.96},

    {col:0,row:2,tag:2,time:29.06},
    {col:2,row:3,tag:1,time:30.12},
    {col:0,row:0,tag:4,time:32.49},
    {col:4,row:1,tag:1,time:33.61},
    {col:3,row:4,tag:5,time:36.44},

    {col:1,row:0,tag:4,time:38.84},
    {col:2,row:0,tag:4,time:39.71},
    {col:0,row:1,tag:2,time:41.58},
    {col:1,row:1,tag:4,time:42.86},
    {col:1,row:2,tag:2,time:47.16},

    {col:4,row:4,tag:5,time:48.2},
    {col:3,row:0,tag:4,time:49.21},
    {col:4,row:0,tag:4,time:50.58},
    {col:0,row:0,tag:4,time:52.42},
    {col:1,row:2,tag:2,time:52.42},


]
var Data_SpRecord={
    downblock : [
        [1,2,1,4,3],
        [3,1,4,4,2],
        [5,1,4,5,2],
        [5,1,4,2,1],
        [2,4,4,4,2],
        [4,4,4,2,5]
    ],
    upblock:[
        [
            {col:0,row:0,tag:4},
            {col:4,row:2,tag:2},
            {col:0,row:4,tag:5}
        ]
    ]

}
//SpRecord.downblock=Record_Data[2]
//SpRecord.upblock=Record_Data[1]
var fun= function (num,object) {
    object.setString(num)
}
//  目标对象， Array_SpriteRole，BESHU剩余步数，将步数重新渲染到界面（this.bushu_num），M(计算得分时候用),fun1表示移除函数，fun2表示创建函数
var CheckRowSprite=function (object,str_bushu,fenshu,str_fenshu,num,M) {
    var Array_CheckColSprite=[];
    Array_CheckColSprite.push(object);
    var ChekRole_col=object.col-1;
    while(ChekRole_col>=0){
        var CheckRole
        if(num==1){
            CheckRole=Array_SpriteRole[object.row*MAX_ROW+ChekRole_col];
        }else{
            CheckRole=Array_SpriteRole1[object.row*MAX_ROW+ChekRole_col];
        }
        if(CheckRole&&CheckRole.tag==object.tag){
            Array_CheckColSprite.push(CheckRole);
            ChekRole_col--;
        }else{
            break;
        }
    }
    var ChekRole_col=object.col+1;
    while(ChekRole_col<5){
        var CheckRole
        if(num==1){
            CheckRole=Array_SpriteRole[object.row*MAX_ROW+ChekRole_col];
        }else{
            CheckRole=Array_SpriteRole1[object.row*MAX_ROW+ChekRole_col];
        }
        if(CheckRole&&CheckRole.tag==object.tag){
            Array_CheckColSprite.push(CheckRole);
            ChekRole_col++;
        }else{
            break;
        }
    }
    var Array_CheckRowSprite=[];
    Array_CheckRowSprite.push(object);
    var ChekRoleRight_row=object.row+1
    while(ChekRoleRight_row<5){
        var CheckRole
        if(num==1){
            CheckRole=Array_SpriteRole[ChekRoleRight_row*MAX_ROW+object.col];
        }else{
            CheckRole=Array_SpriteRole1[ChekRoleRight_row*MAX_ROW+object.col];
        }
        if(CheckRole&&CheckRole.tag==object.tag){
            Array_CheckRowSprite.push(CheckRole);
            ChekRoleRight_row++;
        }else{
            break;
        }
    };
    var ChekRole_row=object.row-1
    while(ChekRole_row>=0){
        var CheckRole
        if(num==1){
            CheckRole=Array_SpriteRole[ChekRole_row*MAX_ROW+object.col];
        }else{
            CheckRole=Array_SpriteRole1[ChekRole_row*MAX_ROW+object.col];
        }
        if(CheckRole&&CheckRole.tag==object.tag){
            Array_CheckRowSprite.push(CheckRole);
            ChekRole_row--;
        }else{
            break;
        }
    }
    if(num==1){
        BUSHU--
        if(BUSHU<1){
            // GAMEOVER
            if(Array_CheckRowSprite.length==5||Array_CheckColSprite.length==5){
            }else{
                var layer = new GameOverLayer();
                this.addChild(layer,3);
            }
        }else{
            str_bushu.setString(BUSHU);
        }
    }
    if(num==2){
        Record_BUSHU--;
        if(Record_BUSHU<1){
            // GAMEOVER
            if(Array_CheckRowSprite.length==5||Array_CheckColSprite.length==5){
            }else{
                var layer = new GameOverLayer();
                this.addChild(layer,3);
            }
        }else{
            str_bushu.setString(Record_BUSHU);
        }
    }

    if(Array_CheckRowSprite.length==5||Array_CheckColSprite.length==5){
        if(Array_CheckRowSprite.length==5&&Array_CheckColSprite.length==5){
            for(var a=0;a<Array_CheckColSprite.length;a++){
                if(Array_CheckRowSprite.indexOf(Array_CheckColSprite[a])==-1){
                    Array_CheckRowSprite.push(Array_CheckColSprite[a]);
                }
            }
            for(var a=0;a<Array_CheckColSprite.length;a++){
                Array_CheckColSprite.splice(a,1);
                a--;
            }
            RemoveSprite(Array_CheckRowSprite,fenshu,str_fenshu,num,M)
        }else if(Array_CheckRowSprite.length==5){
            RemoveSprite(Array_CheckRowSprite,fenshu,str_fenshu,num,M)
        }else if(Array_CheckColSprite.length==5){
            RemoveSprite(Array_CheckColSprite,fenshu,str_fenshu,num,M)
        }
    }else{
        if(num==1){
            if(Array_Sprite.length==0){
                index++
                diaoCreateSprite(num)

            }
            var a_NUM = 0;
            for (var a = 0; a < 25; a++) {
                if (!Array_SpriteRole[a]) {
                    a_NUM++
                }
            }
        }
       if(num==2){
           if(Array_Sprite1.length==0){
               Record_level++
               diaoCreateSprite(num)
           }
            var a_NUM1 = 0;
            for (var a = 0; a < 25; a++) {
                if (!Array_SpriteRole1[a]) {
                    a_NUM1++
                }
            }
        }

        //if (SpriteArray.length!=1&&a_NUM == 0) {
        //    this.unscheduleAllCallbacks();
        //    var layer = new GameOverLayer();
        //    this.addChild(layer,10);
        //}
    }
}
//  分数，分数重置，数组（Array_SpriteRole），num(判断是录制的还是玩家在玩)，index（控制ai创建sprite）
var RemoveSprite= function (Array_SP,fenshu,str_fenshu,num,M) {
    if(num==1){
        fenshu+=Math.pow(2,M);
        str_fenshu.setString(fenshu);
    }
    for(var a=0;a<Array_SP.length;a++){
        if(num==1){
            Array_SpriteRole[Array_SP[a].row*MAX_ROW+Array_SP[a].col]=null;
        }
        if(num==2){
            Array_SpriteRole1[Array_SP[a].row*MAX_ROW+Array_SP[a].col]=null;
        }
        //var fadout=new cc.fadeOut(0.6);
        var fd = new cc.scaleTo(0.15,1.2);
        var sx = new cc.scaleTo(0.25,0);
        //var blink =new cc.Blink(0.2,2);//闪烁函数
        var destory = new cc.RemoveSelf(true);
        var fun=new cc.callFunc(function () {
            var IQ_num=0;
            var Record_num=0;
            for(var a=0;a<Array_SpriteRole.length;a++){
                if(Array_SpriteRole[a]&&Array_SpriteRole[a].IQ_IS_Check){
                    IQ_num++;
                }
            }
            for(var a=0;a<Array_SpriteRole1.length;a++){
                if(Array_SpriteRole1[a]&&Array_SpriteRole1[a].IQ_IS_Check){
                    Record_num++;
                }
            }

            if(Record_num==0&&IQ_num!=0){
                gameXML()
                cc.director.runScene(new GameStartScene())
                Record_layer.unscheduleAllCallbacks();
                var Fadeout=new cc.fadeIn(0.5,0)
                RecordLayer.runAction(Fadeout)
            }else if(IQ_num==0&&Record_num!=0){
                gameXML()
                cc.director.runScene(new GameStartScene())
                Record_layer.unscheduleAllCallbacks();
                var Fadeout=new cc.fadeIn(0.5,0)
                RecordLayer.runAction(Fadeout)
            }
        })
        if(num==1){
            if(Is_MUSIC){
                cc.audioEngine.playEffect("res/xiao.wav",false);
                cc.audioEngine.playEffect("res/xiao.ogg",false);
            }
        }
        if(a==Array_SP.length-1){
            Array_SP[a].runAction(cc.sequence(fd,sx,destory,fun));
        }else{
            Array_SP[a].runAction(cc.sequence(fd,sx,destory));
        }
    }

    if(num==1){
        if( Array_Sprite.length==0){
            index++
            cc.log("index",index)
            diaoCreateSprite(num)
        }
    }
    if(num==2){
        if( Array_Sprite1.length==0){
            Record_level++
            //cc.log("Record_level",Record_level)
            diaoCreateSprite(num)
        }
    }
}
var  gameXML=function () {
    var xhr = cc.loader.getXMLHttpRequest();
    var game_score=mapping_time;
    var game_type=1;
    var availabity=0
    var type_score=mapping_time;
    var jsondata1=null
    var  url="/openapi/game/interactionGame/postGameLog.json?availabity="+availabity+"&appId="+appId+"&gameScore="+game_score+"&gameLog="+jsondata1+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+record_Id
    xhr.open("POST", url,true);
    xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
    var self=this;
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4){
            if(xhr.status >= 200 && xhr.status <= 207){
                var resw = xhr.responseText;
                var jsondata = JSON.parse(resw);
                var  num=jsondata.fightResult
                if(num==1){
                    alert("你赢了");
                }else if(num==0){
                    alert("你输了");
                }else if(!num){
                    var IQ_num=0;
                    var Record_num=0;
                    for(var a=0;a<Array_SpriteRole.length;a++){
                        if(Array_SpriteRole[a]&&Array_SpriteRole[a].IQ_IS_Check){
                            IQ_num++;
                        }
                    }
                    for(var a=0;a<Array_SpriteRole1.length;a++){
                        if(Array_SpriteRole1[a]&&Array_SpriteRole1[a].IQ_IS_Check){
                            Record_num++;
                        }
                    }
                    if(Record_num==0&&IQ_num!=0){
                        alert("你输了");
                    }else if(IQ_num==0&&Record_num!=0){
                        alert("你赢了");
                    }
                }
            }
        }
    }
    xhr.send();
}
//tag_m(创建的哪种精灵)，sp_x,sp_y（精灵的x，y值），father（精灵的父类），sp_Array（精灵添加的数组），posArray（存储精灵移动的的位置）
var CreateSprite=function (sp_x,sp_y,father,sp_Array,posArray,num) {
    var winsize=cc.visibleRect;
    var sp_index
    if(num==1){
        sp_index=index
        //cc.log("index",index)
    }
    if(num==2){
        sp_index=Record_level
        //cc.log("Record_level",Record_level)
    }
    for(var a=0;a<5;a++){
        var m=SpRecord.downblock[sp_index][a]
        var url;
        if(num==1){
            url=g_resources[m-1]
        }
        if(num==2){
            url="res/Newwork/sp"+m+".png"
        }
        var SpriteRole=new cc.Sprite(url);
        SpriteRole.setPosition(sp_x,sp_y);
        father.addChild(SpriteRole)
        SpriteRole.row=0;
        SpriteRole.col=0;
        SpriteRole.tag=m;
        SpriteRole.bool=false;
        SpriteRole.IQ_IS_Check=false;
        SpriteRole.setAnchorPoint(0.5,0.5);
        if(num==1){
            Array_Sprite.push(SpriteRole);
        }
        if(num==2){
            Array_Sprite1.push(SpriteRole);
        }
    }
    for(var a=0;a<4;a++){
        if(num==1){
            Array_Sprite[a].runAction(cc.sequence(cc.moveBy(0.2+a*0.1,-(posArray[a]),0)))
        }
        if(num==2){
            Array_Sprite1[a].runAction(cc.sequence(cc.moveBy(0.2+a*0.1,-(posArray[a]),0)))
        }
    }
    var Click=cc.callFunc(function () {
        IS_CLICK=true;
        Is_Start=true;
    },this)
    if(num==1){
        Array_Sprite[4].runAction(cc.sequence(cc.moveBy(0.6,-(posArray[4]),0),Click));
    }
    if(num==2){
        Array_Sprite1[4].runAction(cc.sequence(cc.moveBy(0.6,-(posArray[4]),0),Click));
    }
}
var CreateIQSprite=function (down_index1,father,sp_Array,num) {
    var winSize=cc.winSize
    var time=0;
    for(var a=0;a<SpRecord.upblock[0].length;a++){
        var m=SpRecord.upblock[0][a].tag;
        var num_X;
        var num_Y;
        var url;
        if(num==1){
            num_X=SpRecord.upblock[0][a].col*100+184;
            num_Y=SpRecord.upblock[0][a].row*100+450;
            url=g_resources[4+m]
        }
        if(num==2){
            num_X=38 + SpRecord.upblock[0][a].col * 22;
            num_Y=95+SpRecord.upblock[0][a].row * 22;
            url="res/Newwork/white"+m+".png"
        }
        var SpriteRole=new cc.Sprite(url);
        SpriteRole.setPosition(num_X,num_Y);
        father.addChild(SpriteRole);
        SpriteRole.row=SpRecord.upblock[0][a].row
        SpriteRole.col=SpRecord.upblock[0][a].col;
        SpriteRole.tag=m;
        SpriteRole.bool=false;
        SpriteRole.start=false;
        SpriteRole.IQ_IS_Check=true;
        var Fadein=new cc.fadeIn(0.5,255)
        SpriteRole.setOpacity(0);
        var dely=cc.delayTime(0.4*a);
        if(time<0.4*a){
            time=0.4*a;
        }
        var call=cc.callFunc(function () {
            SpriteRole.start=true;
        },this)
        SpriteRole.runAction(cc.sequence(dely,Fadein,call));
        if(num==1){
            Array_SpriteRole[SpriteRole.row*MAX_ROW+SpriteRole.col]=SpriteRole;
        }
        if(num==2){
            Array_SpriteRole1[SpriteRole.row*MAX_ROW+SpriteRole.col]=SpriteRole;
        }
    }
    father.scheduleOnce(function () {
        if(num==1){
            diaoCreateSprite(num)
        }
        if(num==2){
            diaoCreateSprite(num)
            Is_Record=true
        }


    },time)
}
var diaoCreateSprite= function (num) {
    var winSize=cc.winSize
    var posArray
    if(num==1){
        posArray=[180,285,387,488,590]
        CreateSprite(winSize.width,300,this_layer,Array_Sprite,posArray,num)
    }
    if(num==2){
        posArray=[11,33,55,77,99]
        CreateSprite(137,60,Record_layer,Array_Sprite1,posArray,num)
    }

}
var mapping_time=0;
var record_Id=0//表示记录的id
var CreateBtn=function(url,fun,x,y,Anchorx,Anchory,father){
    var btn = new cc.MenuItemImage(
        url,
        url,
        function () {
            fun()

        }, this
    )
    btn.setPosition(x,y);
    btn.setAnchorPoint(Anchorx,Anchory)
    var menu = new cc.Menu(btn);
    menu.x = 0;
    menu.y = 0;
    father.addChild(menu,5)
    //7580
}
var  CreateFun=function (width,hight,radius) {
    var sp_Array=[];
    var radius=radius;
    var num=100;
    var start_x=0
    var start_y=0
    //左下角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=sp_x-width/2;
        var jiaodu=Math.acos((radius-sp_x)/radius)
        var y=radius-(Math.sin(jiaodu)*radius)-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }

    //右下角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=sp_x+width-radius-width/2;
        var jiaodu=Math.asin((sp_x)/radius)
        var y=radius-(Math.cos(jiaodu)*radius)-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    //右上角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=width-sp_x-width/2;
        var jiaodu=Math.acos((radius-sp_x)/radius)
        var y=Math.sin(jiaodu)*radius+width-radius-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    //左上角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=radius-sp_x-width/2;
        var jiaodu=Math.asin((sp_x)/radius)
        var y=Math.cos(jiaodu)*radius+width-radius-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    return sp_Array;
};
var CreateClipperImage= function (url,x,y,radius,father) {
    var shape =new cc.DrawNode();
    var vertices=CreateFun(radius,radius,radius/2);
    shape.drawPoly(vertices, cc.color(0, 255, 255, 80), 2, cc.color(255, 0, 0, 255));
    var clipper = new cc.ClippingNode();
    clipper.x = x;
    clipper.y = y;
    clipper.stencil = shape;  // 把刚刚创建的圆形模板放入
    father.addChild(clipper,10);
    var sp  = new cc.Sprite(url);
    var SceleX=radius/sp.getContentSize().width;
    var sceleY=radius/sp.getBoundingBox().height;
    sp.setScale(SceleX,sceleY);
    sp.setAnchorPoint(0.5,0.5);
    clipper.addChild(sp);   // 在这个clippingnode中只显示圆形模板的部分.
};
var appId=113648;
var scoreId=179;
var getBytesLength= function (str,len) {
    // 在GBK编码里，除了ASCII字符，其它都占两个字符宽
    //return str.replace(/[^\x00-\xff]/g, 'xx').length;
    var str_length = 0;
    var str_len = 0;
    var  str_cut = new String();
    str_len = str.length;
    for(var i = 0;i<str_len;i++)
    {
        a = str.charAt(i);
        str_length++;
        if(escape(a).length > 4) {
            //中文字符的长度经编码之后大于4
            str_length++;
        }
        str_cut = str_cut.concat(a);
        if(str_length>=len) {
            str_cut = str_cut.concat("");
            return str_cut;
        }
    }
    //如果给定字符串小于指定长度，则返回源字符串；
    if(str_length<len){
        return  str;
    }
}
