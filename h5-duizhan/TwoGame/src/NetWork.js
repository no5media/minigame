IQ_level=1;
IQ_level1=1;
Score_Num=0;
CoinNum_num=0;//奖学金
BestScore_num=0;
BUSHU = 32;
Record_BUSHU=32
SuperRubberNumber=1
ChangeRubberNumber=0;
Best_level=1;

Is_first=0;
Object1=null;
Is_use=0;
Record_layer=null;
this_layer=null;
Array_SpriteRole = [];
Array_SpriteRole1 = [];
Array_Sprite = [];
Array_Sprite1 = [];
index=0;
Record_index=0
Record_level=0
Is_Record=false
RecordLayer=null;
var NetWorkLayer  = cc.Layer.extend({
    CoinNum:null,
    Score:null,
    BestScore:null,
    NUM:null,
    Run_Sprite:null,
    SuperRubber_number:null,
    Rubber_number:null,
    bg:null,
    Change_Rubber_number:null,
    _paySuccess:null,
    _payError:null,
    _payClose:null,
    //cocos compile -p web -m release
    ctor: function () {
        this._super();
        var winsize = cc.visibleRect;
        Block_Array=[];
        IS_MOVE = false;
        IS_CREATE = false;
        IS_BEGIN = true;
        IS_CLICK = false;
        IS_REDBTN_CLICK = true;
        IS_BTN_CLICK = true;
        IS_CLEAR = false;
        IS_USER_RUBBER = false;
        MAX_ROW = 5;
        this.time = 0;
        this_layer=this;
        NUM = 0;//被移动精灵的的下标
        NUM_X = 0;//被移动的精灵的x值
        NUM_Y = 0;//被移动的精灵的y值
        //左上角进度条
        COLOR_row = 0;
        COLOR_col = 0;
        Begain_COLOR_row = 0;
        Begain_COLOR_col = 0;
        Begain_M = 1;
        mapping_time=0
        Object1 = this;
        Is_Start = false;
        IS_Superrun = false;
        Is_ru = false
        Is_xhr = false;
        //橡皮的数量
        buy_num = 60000;
        Guider_num = 0;//判断是几连消
        RubberNumber = 2;

        bg = new cc.Sprite(res.bg);
        bg.setPosition(winsize.width / 2, winsize.height / 2);
        bg.setAnchorPoint(0.5, 0.5)
        this.addChild(bg);
        bg1 = new cc.Sprite(res.bg1);
        bg1.setPosition(winsize.width / 2, 650);
        bg1.setAnchorPoint(0.5, 0.5)
        this.addChild(bg1);
        var btn1 = new cc.MenuItemImage(
            "res/mainGame/SuperRubber.png",
            "res/mainGame/SuperRubber.png",
            function () {
                if(Is_Start){
                    self.RedOneFun()
                }

            }, this
        )
        btn1.setPosition(250, 200);
        var btn2 = new cc.MenuItemImage(
            "res/mainGame/Rubber.png",
            "res/mainGame/Rubber.png",
            function () {
                if(Is_Start){
                    self.RedTwoFun();
                }
            }, this
        )
        btn2.setPosition(450, 200);
        var btn3 = new cc.MenuItemImage(
            res.angin,
            res.angin,
            function () {
                cc.director.runScene(new MaingameScene());
            }, this
        )
        btn3.setPosition(350,950)
        var menu = new cc.Menu(btn1,btn2,btn3);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu);

        var ChangeRubber=new cc.Sprite("res/mainGame/ChangeRubber.png");
        ChangeRubber.setPosition(560,200)
        this.addChild(ChangeRubber)

        SuperRubber_number=new cc.LabelAtlas(SuperRubberNumber, "res/1.png", 54, 76,"0");
        SuperRubber_number.x=195;
        SuperRubber_number.y=190;
        SuperRubber_number.setScale(0.3);
        SuperRubber_number.setAnchorPoint(0.5,0.5);
        this.addChild(SuperRubber_number,1);

        Rubber_number=new cc.LabelAtlas(RubberNumber, "res/1.png", 54, 76,"0");
        Rubber_number.x=390;
        Rubber_number.y=190;
        Rubber_number.setScale(0.3);
        Rubber_number.setAnchorPoint(0.5,0.5);
        this.addChild(Rubber_number,1);

        Change_Rubber_number=new cc.LabelAtlas("0", "res/1.png", 54, 76,"0");
        Change_Rubber_number.x=592;
        Change_Rubber_number.y=190;
        Change_Rubber_number.setScale(0.26);
        Change_Rubber_number.setAnchorPoint(0.5,0.5);
        this.addChild(Change_Rubber_number,1);

        var Rubber= "/5";
        var leylabel = new cc.LabelTTF(Rubber,"Arial",30);
        leylabel.x = 615;
        leylabel.y = 190;
        this.addChild(leylabel,1);
        var bushu=new cc.Sprite(res.bushu);
        bushu.setPosition(210,985)
        this.addChild(bushu,1)
        var level=new cc.Sprite(res.level);
        level.setPosition(530,1020)
        this.addChild(level,1)
        var anginlevel=new cc.Sprite(res.anginlevel);
        anginlevel.setPosition(530,950)
        this.addChild(anginlevel,1)
        var score=new cc.Sprite(res.score);
        score.setPosition(350,1020);
        score.setAnchorPoint(0.5,0.5)
        this.addChild(score,1);
        this.bushu_num=new cc.LabelAtlas("32", "res/1.png", 54, 76,"0");
        this.bushu_num.setPosition(210,985);
        this.bushu_num.setAnchorPoint(0.5,0.5)
        //this.bushu_num.setScale(0.3)
        this.addChild( this.bushu_num,1)
        this.level_num=new cc.LabelAtlas(IQ_level, "res/1.png", 54, 76,"0");
        this.level_num.setPosition(530,990)
        this.level_num.setScale(0.3)
        this.addChild( this.level_num,1)
        this.anginlevel_num=new cc.LabelAtlas(Best_level, "res/1.png", 54, 76,"0");
        this.anginlevel_num.setPosition(530,920)
        this.anginlevel_num.setScale(0.3)
        this.addChild(this.anginlevel_num,1)
        this.score_num=new cc.LabelAtlas(CoinNum_num, "res/1.png", 54, 76,"0");
        this.score_num.setPosition(350,990)
        this.score_num.setScale(0.3)
        this.addChild(this.score_num,1);
        for(var a=0;a<5;a++){
            for(var b=0;b<5;b++) {
                bg2 = new cc.Sprite(res.bg2);
                bg2.setPosition(184 + b * 100, 450 + a * 100);
                bg2.setAnchorPoint(0.5, 0.5);
                this.addChild(bg2);
                var sp={
                    x:184 + b * 100,
                    y: 450 + a * 100,
                    row:a,
                    col:b,
                }
                Block_Array.push(sp);
            }
        }
        var self = this;
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                if(IS_CLICK){
                    var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                    for(var a=0;a<Array_Sprite.length;a++){
                        if(cc.rectIntersectsRect(Array_Sprite[a].getBoundingBox(),rect)){
                            NUM_X=Array_Sprite[a].x;
                            NUM_Y=Array_Sprite[a].y;
                            NUM=a;
                            IS_MOVE=true;
                        }
                    }
                    return true;
                }
            },
            onTouchMoved: function (touch, event) {
                if(IS_MOVE){
                    Array_Sprite[NUM].setLocalZOrder(2)
                    Array_Sprite[NUM].x=touch.getLocation().x;
                    Array_Sprite[NUM].y=touch.getLocation().y;
                }
            },
            onTouchEnded: function (touch,event) {
                if((touch.getLocation().x>=134&&touch.getLocation().x<=632)&&(touch.getLocation().y>=380&&touch.getLocation().y<=878)){
                    if(IS_MOVE){
                        var Min=Math.sqrt((Array_Sprite[NUM].x-Block_Array[0].x)*(Array_Sprite[NUM].x-Block_Array[0].x)+(Array_Sprite[NUM].y-Block_Array[0].y)*(Array_Sprite[NUM].y-Block_Array[0].y));
                        var Min_a=0
                        var Min_n=0
                        for(var a=1;a<Block_Array.length;a++){
                            if(Min>Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))){
                                Min=Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))
                                Min_a=a;
                            }else if(Min==Math.sqrt((Array_Sprite[NUM].x-Block_Array[a].x)*(Array_Sprite[NUM].x-Block_Array[a].x)+(Array_Sprite[NUM].y-Block_Array[a].y)*(Array_Sprite[NUM].y-Block_Array[a].y))){
                                Min_n++;
                            }
                        }
                        if(!Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]&&Min_n==0){
                            var Min_tag= Array_Sprite[NUM].tag;
                            var Remov=cc.callFunc(function () {
                                var sp_arr= Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]
                                CheckRowSprite(sp_arr,this_layer.bushu_num,CoinNum_num, this_layer.score_num,1,Min_tag)
                                //self.CheckRowSprite(sp_arr,Min_tag);
                            },this)
                            Array_Sprite[NUM].runAction(cc.sequence(cc.moveTo(0.2,Block_Array[Min_a].x,Block_Array[Min_a].y),Remov));
                            if(Is_MUSIC){
                                cc.audioEngine.playEffect("res/move.wav",false);
                                cc.audioEngine.playEffect("res/move.ogg",false);
                            }
                            Array_Sprite[NUM].setLocalZOrder(1);
                            Array_Sprite[NUM].row=Block_Array[Min_a].row;
                            Array_Sprite[NUM].col=Block_Array[Min_a].col;
                            Array_SpriteRole[Block_Array[Min_a].row*MAX_ROW+Block_Array[Min_a].col]= Array_Sprite[NUM];
                            Array_Sprite.splice(NUM,1);
                        }else{
                            Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y));
                        }
                        IS_MOVE=false;
                    }
                }else{
                    if(IS_MOVE){
                        IS_MOVE=false;
                        Array_Sprite[NUM].runAction(cc.moveTo(0.3,NUM_X,NUM_Y))
                    }
                }
            }
        });
        cc.eventManager.addListener(this.touchListener, this);
        //this.CreateIQSprite();
        CreateIQSprite(0,this_layer,Array_SpriteRole,1);
        RecordLayer = new NetRecordLayer();
        this.addChild(RecordLayer,10);
        cc.log("aa",Array_Sprite)

    },
})
var NetRecordLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        Record_time=0;
        Record_layer=this;
        this.CreateRecord()
        CreateIQSprite(0,Record_layer,Array_Sprite1,2);
        this.scheduleUpdate();
        return true;
    },
    update: function (dt) {
        mapping_time+=dt
        if(Is_Record){
            Record_time+=dt
            if(RecordData[Record_index]){
                if(Record_time>=RecordData[Record_index].time){
                    var sp_row=RecordData[Record_index].row
                    var sp_col=RecordData[Record_index].col
                    var sp_tag=RecordData[Record_index].tag
                    this.RecordSprite(sp_row,sp_col,sp_tag);
                    Record_index++
                    cc.log("Record_index",Record_index)
                }
            }
        }
    },
    RecordSprite: function (row,col,tag) {
        var posx=38+col * 22;
        var posy=95+row * 22;
        var sp_in
        for(var a=0;a<Array_Sprite1.length;a++){
            if(Array_Sprite1[a].tag==tag){
                sp_in=a
            }
        }
        cc.log(sp_in)
        var fun=new cc.callFunc(function () {
            Array_Sprite1[sp_in].row=row;
            Array_Sprite1[sp_in].col=col;
            Array_SpriteRole1[row*MAX_ROW+col]=Array_Sprite1[sp_in];
            Array_Sprite1.splice(sp_in,1);
            var sp_arr= Array_SpriteRole1[row*MAX_ROW+col]
            CheckRowSprite(sp_arr,Record_layer.Network_bushu_num,null,null,2)
            //cc.log(Array_Sprite1.length);

        })
        Array_Sprite1[sp_in].runAction(new cc.sequence(fun,new cc.moveTo(0.2,posx,posy)));
    },
    CreateRecord: function () {
        var winSize=cc.winSize
        var bg2 = new cc.Sprite("res/Newwork/bg.png");
        bg2.setAnchorPoint(0,0)
        bg2.setPosition(0,0);
        this.addChild(bg2);
        this.Network_bushu_num=new cc.LabelAtlas("32", "res/1.png", 54, 76,"0");
        this.Network_bushu_num.setPosition(45,205);
        this.Network_bushu_num.setAnchorPoint(0.5,0.5)
        this.Network_bushu_num.setScale(0.2)
        this.addChild( this.Network_bushu_num,1)
    }
})
var NetWorkScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new NetWorkLayer();
        this.addChild(layer);
    }
});