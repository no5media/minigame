Score=0
Main_layer=null;
Is_Angin=false;
Is_overOrplay=false
var GameMainLayer=cc.LayerColor.extend({
    Sprite_Array:null,
    tishi1:null,
    sex_num1:null,
    ctor: function () {
        this._super(cc.color(231,237,239));
        Score=0;
        time=0;
        clock_time=40;
        Main_layer=this;
        Sprite_Array=[];
        Postion_Array=[];
        Block_Array=[];
        sp_index=0;
        var self=this;
        Is_Click=true;
        Is_Click1=true;
        Is_Angin=false;
        Is_overOrplay=false;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=new cc.Rect(touch.getLocation().x,touch.getLocation().y,1,1)
                if(Is_Click&&Is_Click1){
                    Is_Click1=false
                    for(var a=0;a<Block_Array.length;a++){
                        if(cc.rectIntersectsRect(rect,Block_Array[a].getBoundingBox())){
                            if(sex_num1==1){//大
                                if(a==Block_Array.length-1){
                                    Score+=24;
                                    getScore.setString(Score);
                                    Block_Array[a].removeFromParent();
                                    Block_Array.splice(a,1);
                                    if(Block_Array.length==0){
                                        self.AddSprite();
                                    }
                                }else{
                                    //错误
                                    Score-=40;
                                    if(Score<=0){
                                        Score=0;
                                    }
                                    getScore.setString(Score);
                                    self.CreateError(sex_num1+1,Block_Array[a].x,Block_Array[a].y);
                                }
                            }else{
                                if(a==0){
                                    Score+=24;
                                    getScore.setString(Score);
                                    Block_Array[a].removeFromParent();
                                    Block_Array.splice(a,1);
                                    if(Block_Array.length==0){
                                        self.AddSprite();
                                    }
                                }else{
                                    //错误
                                    Score-=40;
                                    if(Score<=0){
                                        Score=0
                                    }
                                    getScore.setString(Score);
                                    self.CreateError(sex_num1-1,Block_Array[a].x,Block_Array[a].y)
                                }
                            }
                        }
                    }
                }
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click1=true
            }
        });
        cc.eventManager.addListener(listener, this);
        this.CreateBg();

        this.CreatePeopleInformation();
        var winSize=cc.winSize;
        this.DrawBg();
        for(var row=0;row<4;row++){
            for(var col=0;col<3;col++){
                var posx=115+col*205;
                var posy=200+row*200;
                Postion_Array.push({
                    x:posx,
                    y:posy
                })
            }
        }
        Index_array=[];
        for(var a=0;a<30;a++){
            Index_array.push(1)
        }
        for(var a=0;a<30;a++){
            Index_array.push(2)
        }
        for(var a=0;a<10;a++){
            Index_array.shuffle();
        }
        this.AddSprite();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt){
        time+=dt;
        if(time>=1){
            time=0;
            clock_time--;
            timeclock.setString(clock_time);
            if(clock_time==0){
                var angin_num=cc.sys.localStorage.getItem("axdjAngin")
                if(angin_num<3){
                    cc.director.pause();
                    var layer=new AnginLayer(1);
                    this.addChild(layer,10);
                }else{
                    Is_overOrplay=true
                }
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false
            clock_time+=5;
            timeclock.setString(clock_time);
            PeppleScore.setString(window.select.userPoint)
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return
        }
    },

    AddSprite: function () {
        //var num1=Math.floor(Math.random()*2)//判断数字的消除顺序；
        var sex=sp_index%Index_array.length;
         sex_num1=Index_array[sex];
        if(sex_num1==1){
            //按小到大的顺序消除
            var tishi_str1="从大到小按序点击";
            tishi1.setString(tishi_str1);
            this.getSprite(1);
            sp_index++;
        }else{
            //按大到小的顺序消除
            var tishi_str1="从小到大按序点击";
            tishi1.setString(tishi_str1);
            this.getSprite(2);
            sp_index++;
        }
    },
    getSprite: function (m) {
        var sp_num1//方块的数量
        var sp_num2//方块的初始值
        var spindex//方块的位置

        if(sp_index<2){
            sp_num1=3;
        }else if(sp_index<10){
            sp_num1=Math.floor(Math.random()*4+3);
        }else {
            sp_num1=Math.floor(Math.random()*6+3);
        }
        sp_num2=Math.floor(Math.random()*(9-sp_num1)+1);
        var Sp_Array=[]
        for(var a=0;a<Sp_Array.length;a++){
            Sp_Array.splice(a,1);
            a--;
        }
        for(var a=0;a<Postion_Array.length;a++){
            Sp_Array.push(Postion_Array[a]);
        }
        for(var a=0;a<sp_num1;a++){
            spindex=Math.floor((Math.random()*Sp_Array.length));
            this.CreateSprite(m,Sp_Array[spindex].x,Sp_Array[spindex].y,sp_num2+a,a,sp_num1-1);
            Sp_Array.splice(spindex,1);

        }
    },
    CreateSprite: function (color,x,y,m,num1,num) {
        var sp=new cc.Sprite(g_resources[color-1]);
        sp.setPosition(x,y);
        sp.tag=m;
        this.addChild(sp);
        var sp_num=new cc.LabelAtlas(m, "res/num1.png", 72, 72,"0");
        sp_num.setPosition(sp.getBoundingBox().width/2,sp.getBoundingBox().height/2);
        sp_num.setAnchorPoint(0.5,0.5);
        sp_num.setScale(1.5);
        sp.addChild(sp_num,10);
        sp.scale=0.1;
        var move=new cc.ScaleTo(0.2,1);
        var call1=cc.callFunc(function () {
            if(num1==0){
                Is_Click=false;
            }
        },this);
        var call2=cc.callFunc(function () {
            if(num1==num){
                Is_Click=true;
            }
        },this);
        sp.runAction(cc.sequence(call1,move,call2));
        Block_Array.push(sp);
    },
    CreateError: function (m,x,y) {
        var winSize=cc.winSize;
        var layer=new cc.LayerColor(cc.color(225,50,120,60));
        this.addChild(layer);
        var blink =new cc.Blink(0.3,2);//闪烁函数
        var sp=new cc.Sprite(g_resources[m-1]);
        sp.setAnchorPoint(0.5,0.5);
        sp.setPosition(x,y);
        this.addChild(sp,5);
        var no_sp=new cc.Sprite(res.no_sp);
        no_sp.setPosition(sp.getBoundingBox().width/2,sp.getBoundingBox().height/2);
        sp.addChild(no_sp,10);
        var destory = new cc.RemoveSelf(true);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
        });
        //layer.runAction(cc.repeatForever(cc.sequence(blink,blink,destory,fun)))
        layer.runAction(cc.repeatForever(cc.sequence(blink,fun,destory)))
    },
    DrawBg: function () {
        var winSize=cc.winSize;
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,5);
        drawNode.drawRect(
            cc.p(0,10), // 起点
            cc.p(winSize.width,80), // 起点的对角点
            cc.color(255,255, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(winSize.width,winSize.height), // 起点
            cc.p(0,winSize.height-160), // 起点的对角点
            cc.color(6,183, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(6,183, 255, 255) // 线颜色
        );
    },
    CreateBg: function () {
        var winSize=cc.winSize;
        var tishi_str="提示 : ";
        var tishi = new cc.LabelTTF(tishi_str, "Arial", 30);
        tishi.x = 25;
        tishi.y = 40;
        tishi.setAnchorPoint(0,0.5);
        tishi.setColor(cc.color(225,50,120));
        this.addChild(tishi, 6);
        var tishi_str1="从小到大按序点击";
        //var tishi_str1="从大到小按序点击"
        tishi1 = new cc.LabelTTF(tishi_str1, "Arial", 30);
        tishi1.x = 120;
        tishi1.y = 40;
        tishi1.setAnchorPoint(0,0.5);
        tishi1.setColor(cc.color(0,0,0));
        this.addChild(tishi1, 6);
        var time_str="倒计时:";
        var timeName = new cc.LabelTTF(time_str, "Arial", 20);
        timeName.x = 170;
        timeName.y = winSize.height-130;
        timeName.setColor(cc.color(255,255,255));
        this.addChild(timeName, 6);
        timeclock=new cc.LabelAtlas("40", "res/num.png", 20, 29,"0");
        timeclock.setPosition(210, winSize.height-140);
        this.addChild(timeclock,10);
        getScore=new cc.LabelAtlas("0", "res/num.png", 20, 29,"0");
        getScore.setPosition(winSize.width-80, winSize.height-130);
        getScore.setAnchorPoint(0.5,0.5);
        this.addChild(getScore,10);

    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-70);
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score,10);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12)
        //var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,255,255))
        this.addChild(record_name, 10);
        var get_score=window.select.userPoint;
        //var get_score=10
        PeppleScore = new cc.LabelTTF("0", "Arial", 30);
        PeppleScore.x = winSize.width-70;
        PeppleScore.y =winSize.height-70;
        PeppleScore.setAnchorPoint(0.5,0.5)
        PeppleScore.setColor(cc.color(254,202,75));
        PeppleScore.setString(get_score)
        this.addChild(PeppleScore, 10);

    },
    GameOver: function () {
        var winSize=cc.winSize
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners()
        var dely=cc.delayTime(1.5);
        var sp=new cc.Sprite(res.bg);
        sp.setPosition(winSize.width/2,winSize.height/2);
        sp.setScale(0.1);
        var fun=cc.callFunc(function () {
            sp.removeFromParent()
            var transition=new cc.TransitionProgressRadialCW(1,new GameOverScene());
            cc.director.runScene(transition);
        })
        var fd = new cc.scaleTo(0.1,1);
        this.addChild(sp,12)
        sp.runAction(cc.sequence(fd,dely,fun));
    }
})

var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})
Array.prototype.shuffle = function() {
    var input = this;
    for (var i = input.length-1; i >=0; i--) {
        var randomIndex = Math.floor(Math.random()*(i+1));
        var itemAtIndex = input[randomIndex];
        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
}