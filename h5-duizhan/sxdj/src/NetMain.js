var NetMainLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        var winSize=cc.winSize;
        var Net_bg=new cc.Sprite("res/fristBg.png");
        Net_bg.setPosition(winSize.width/2,winSize.height/2);
        Net_bg.setScale(0.9);
        this.addChild(Net_bg);
        var sp_str="敬请期待";
        var sp_name = new cc.LabelTTF(sp_str, "Arial", 40);
        sp_name.x = winSize.width/2;
        sp_name.y =winSize.height/2+50;
        this.addChild(sp_name, 5);
        var self=this;
        CreateBtn("res/fristbtn1.png", function () {
            //经典模式
           self.removeFromParent();
        },winSize.width/2,winSize.height/2-80,0.5,0.5,this);
        var buman="确  定";
        var  buman_name = new cc.LabelTTF(buman, "Arial", 30);
        buman_name.x =winSize.width/2;
        buman_name.y =winSize.height/2-80;
        this.addChild(buman_name, 5);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {

                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
})
var NetMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new NetMainLayer();
        this.addChild(layer,10)
    }
})