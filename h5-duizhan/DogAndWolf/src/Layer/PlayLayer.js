/**
 * Created by Yasir on 15/10/7.
 */
score=null
Is_Angin=false;
Is_overOrplay=false
Main_layer=null
var PlayLayer  = cc.Layer.extend({
    SpriteArray:null,
    RingArray:null,
    TypeArray:null,
    WolfArray:null,
    DeleArray:null,
    ctor:function() {
        this._super();
        Is_Angin=false;
        Is_overOrplay=false
        //游戏层

        Main_layer=this
        this.playSymbol = flax.assetsManager.createDisplay(res.game, "playSymbol", {parent: this});
        //this.playSymbol.setLocalZOrder(1);
        this.rabbit= flax.assetsManager.createDisplay(res.game, "rabbit", {parent: this,x:60,y:50});
        var move = new cc.JumpBy(3,cc.p(360,0),20,10);
        this.rabbit.runAction(cc.repeatForever(cc.sequence(move,move.reverse())));
        this.rabbit.play();
        this.DeleArray = [];

        //精灵数组
        this.SpriteArray = [];

        //划线
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,1);

        //爆炸效果
        var Boom1 = flax.assetsManager.createDisplay(res.game, "boom1", {parent: this,x:-100,autoStopWhenOver:true});
        var Boom2 = flax.assetsManager.createDisplay(res.game, "boom2", {parent: this,x:-100,autoStopWhenOver:true});

        //下落速度
        this.speed = 5;
         score = 0;
        NowScore = 0;

        var scoreLabel = new cc.LabelAtlas(String(score),res.number1,114,123,"0");
        scoreLabel.setAnchorPoint(0.5,0.5);
        scoreLabel.setPosition(250,725);
        this.addChild(scoreLabel,10);
        scoreLabel.setScale(0.7);

        //添加触摸事件
        this.WolfArray = [];
        this.RingArray = [];
        this.TypeArray = [];
        var self = this;
        var listener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches:true,
            onTouchBegan:function (touch,event){
                for(var i in self.SpriteArray)
                {
                    for(var j in self.SpriteArray[i])
                    {
                        self.SpriteArray[i][j].isClick = false;
                    }
                }
                return true;
            },
            onTouchMoved:function (touch,event){
               for(var i in self.SpriteArray)
               {
                   for(var j in self.SpriteArray[i])
                   {
                       if(!self.SpriteArray[i][j].isClick)
                       {
                           if(flax.ifTouched(self.SpriteArray[i][j],touch.getLocation()))
                           {
                               self.SpriteArray[i][j].isClick = true;
                               var ring = flax.assetsManager.createDisplay(res.game, "huan", {parent: self});
                               ring.setPosition(self.SpriteArray[i][j].getPositionX()+6,self.SpriteArray[i][j].getPositionY()-23);
                               ring.row = self.SpriteArray[i][j].row;
                               ring.col = self.SpriteArray[i][j].col;
                               ring.setScale(1.25);
                               ring.setLocalZOrder(11)
                               self.RingArray.push(ring);
                               self.TypeArray.push(self.SpriteArray[i][j].num);
                               var x1 = self.RingArray[self.RingArray.length-1].getPositionX()-14;
                               var y1 = self.RingArray[self.RingArray.length-1].getPositionY()+16;
                               if(self.RingArray.length>1)
                               {
                                   var x2 = self.RingArray[self.RingArray.length-2].getPositionX()-14;
                                   var y2 = self.RingArray[self.RingArray.length-2].getPositionY()+16;
                                   drawNode.drawSegment(cc.p(x1,y1),cc.p(x2,y2),6)//cc.color(0,160,240,200));
                                   break;
                               }
                           }
                       }
                   }
               }
            },
            onTouchEnded:function (touch,event){
                drawNode.clear();
                for(var i=0;i<self.RingArray.length;i++)
                {
                    self.RingArray[i].removeFromParent();
                    self.RingArray.splice(i,1);
                    i--;
                }
                for(var i in self.WolfArray)
                {
                    if(self.WolfArray[i])
                    {
                        for(var j in TypeData[self.WolfArray[i].id-1])
                        {
                            if(TypeData[self.WolfArray[i].id-1] && TypeData[self.WolfArray[i].id-1][j].toString() == self.TypeArray.toString())
                            {
                                    self.DeleArray.push(self.WolfArray[i]);
                                    var pos = self.WolfArray[i].getPosition();
                                    Boom1.setPosition(pos);
                                    Boom1.gotoAndPlay(0);
                                    self.WolfArray[i].removeFromParent();
                                    self.WolfArray.splice(i--,1);
                                    var tmp = flax.assetsManager.createDisplay(res.game, "wolfdie", {parent:self});
                                    tmp.setPosition(pos);
                                    tmp.play();
                                    var down = new cc.MoveTo(tmp.y/450,cc.p(tmp.x,50));
                                    var back = new cc.EaseBounceOut(down);
                                    var callfunc = new cc.CallFunc(function(){
                                        for(var k in self.DeleArray)
                                        {
                                            if(self.DeleArray[k].parent)
                                            {
                                                self.DeleArray[k].removeFromParent();
                                                self.DeleArray.splice(k--,1)
                                            }
                                            self
                                            {
                                                self.DeleArray.splice(k--,1)
                                            }
                                        }
                                        Boom2.setPosition(tmp.getPosition());
                                        tmp.removeFromParent();
                                        Boom2.gotoAndPlay(0);
                                        NowScore = ++score;
                                        scoreLabel.setString(score);

                                        if(score>10 && score<20)
                                        {
                                            self.unscheduleAllCallbacks();
                                            self.schedule(self.CreateWolfUpdate,2.5);
                                            self.schedule(self.WolfDownUpdate,0.04);
                                        }
                                        else if(score>=20 && score<30)
                                        {
                                            self.unscheduleAllCallbacks();
                                            self.schedule(self.CreateWolfUpdate,2);
                                            self.schedule(self.WolfDownUpdate,0.03);
                                        }
                                        else if(score>=30 && score<50)
                                        {
                                            self.unscheduleAllCallbacks();
                                            self.schedule(self.CreateWolfUpdate,1.5);
                                            self.schedule(self.WolfDownUpdate,0.02);
                                        }
                                        else if(score>=50)
                                        {
                                            self.unscheduleAllCallbacks();
                                            self.schedule(self.CreateWolfUpdate,1);
                                            self.schedule(self.WolfDownUpdate,0.01);
                                        }
                                    },self);
                                    tmp.runAction(cc.sequence(back,callfunc));
                                    break;
                            }
                        }
                    }
                }
                self.TypeArray = [];
            }
        });
        cc.eventManager.addListener(listener,this);

        this.CreateWolfUpdate();
        for(var i=0;i<3;i++)
        {
            this.SpriteArray[i] = [];
            for(var j=0;j<3;j++) {
                this.CreateSprite(i,j);
            }
        }
        this.schedule(this.CreateWolfUpdate,3);
        this.schedule(this.WolfDownUpdate,0.05);

        return true;
    },
    CreateSprite:function(row,col){
        //cc.spriteFrameCache.addSpriteFrames("res/en/game.plist","res/en/game.png");
        //var  sp = new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_000060"));
        //sp.setPosition(390,600);
        //this.addChild(sp,10);
        var sp = this.playSymbol["sp"+(row*3+col+1)];
        sp.row = row;
        sp.col = col;
        sp.num = row*3 + col + 1;
        sp.isClick = false;
        sp.setScale(1.2);

        this.SpriteArray[row][col] = sp;
    },
    CreateWolfUpdate:function(dt){
        var num1 = Math.floor(cc.rand()%12+1);
        var num2 = Math.floor(cc.rand()%12+1);
        var num = num1>num2?num2:num1;
        var balltype = Math.floor(cc.rand()%10+1);
        var wolftype = Math.floor(cc.rand()%3+1);
        var wolf = flax.assetsManager.createDisplay(res.game, "wolfdown"+wolftype, {parent: this,x:cc.rand()%350+50,y:800});
        wolf.wolftype = wolftype;
        wolf.speed = this.speed;
        var posX;
        if(wolftype==2) {
            posX = 75;
        }
        else {
            posX = 40;
        }
        var ball = flax.assetsManager.createDisplay(res.game, "ball"+balltype, {parent: wolf,x:posX,y:260});
        var type = flax.assetsManager.createDisplay(res.game, "type"+num, {parent: ball,x:36,y:92});
        wolf.play();
        wolf.id = num;
        this.WolfArray.push(wolf);
    },
    WolfDownUpdate:function(dt){
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false
            //加步数
            //BUSHU+=5
            //this.bushu_num.setString(BUSHU);
            for(var a=0;a<this.WolfArray.length;a++){
                this.WolfArray[a].removeFromParent();
                this.WolfArray.splice(a,1);
                a--
            }
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            Is_overOrplay=false
            for(var i in this.WolfArray){
                this.WolfArray[i].removeFromParent();
                this.WolfArray.splice(i,1);
                var wolf = flax.assetsManager.createDisplay(res.game,"taizou", {parent: this});
                wolf.setPosition(this.rabbit.getPosition());
                wolf.play();
                this.rabbit.removeFromParent();
                var move = new cc.MoveBy(wolf.x/100,cc.p(-(wolf.x+120),0));
                var callfunc = new cc.CallFunc(function(){
                    var transition=new cc.TransitionMoveInT(1,new GameOverScene());
                    cc.director.runScene(transition);

                },this);
                wolf.runAction(cc.sequence(move,callfunc));
                return
            }

        }
        for(var i in this.WolfArray)
        {
            if(this.WolfArray[i].y>75){
                this.WolfArray[i].y -= this.WolfArray[i].speed;
            } else {
                var angin_num=cc.sys.localStorage.getItem("zjxyyAngin")
                if(angin_num<3){
                    cc.director.pause();
                    var Layer = new AnginLayer(1);
                    this.addChild(Layer,20);
                }else{
                    Is_overOrplay=true;
                }
            }
        }
    }
});