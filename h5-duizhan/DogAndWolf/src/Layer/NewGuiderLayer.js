/**
 * Created by Yasir on 15/10/8.
 */
var NewGuiderLayer = cc.LayerColor.extend({
    ctor:function() {
        this._super(cc.color(255,255,255,255));

        var listener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches:true,
            onTouchBegan:function (touch,event){
                return true;
            }});
        cc.eventManager.addListener(listener,this);

        var newSymbol = flax.assetsManager.createDisplay(res.over, "guiderSymbol", {parent: this});
        flax.inputManager.addListener(newSymbol.closeBut,this.close,null,this);
        flax.inputManager.addListener(newSymbol.selectBut,this.select,null,this);
        newSymbol.selectBut.setOpacity(0);
    },
    close:function(){
        var move = new cc.ScaleTo(0.3,0.01);
        var die = new cc.RemoveSelf(true);
        cc.director.resume();
        this.runAction(cc.sequence(move,die));
    },
    select:function(touch,event){
        if(firstplay) {
            firstplay = false;
            event.target.setOpacity(255);
            cc.sys.localStorage.setItem("zzw","50");
        }
        else {
            cc.sys.localStorage.setItem("zzw","0");
            firstplay = true;
            event.target.setOpacity(0);
        }
    }
});