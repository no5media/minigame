//语言选择
var g = g || {}
g.language = window.location.href.indexOf('bookor')==-1 ? 'en' : 'ch';
var icon_link = window.document.getElementById('gameico');
icon_link.href = 'res/'+g.language + "/favicon.ico";

//游戏资源
var res = {
    start:"res/"+ g.language+"/start.plist",
    start_png:"res/"+ g.language+"/start.png",
    game:"res/"+ g.language+"/game.plist",
    game_png:"res/"+ g.language+"/game.png",
    over:"res/"+ g.language+"/over.plist",
    over_png:"res/"+ g.language+"/over.png",
    number1:"res/number1.png",
    number2:"res/number2.png",

    startbg:"res/start/start_bg.jpg",
    Score: "res/start/Score.png",
    start_bg1:"res/start/start_bg1.png",
    start_btn1: "res/start/start_btn1.png",
    start_btn2:  "res/start/start_btn2.png",
    follow:"res/start/follow.png",
    moregame:  "res/start/moregame.png",
    invite:"res/start/invite.png"
};

//各个场景资源
var res_start = [
    res.start,
    res.start_png,
    res.startbg,
    res.Score,
    res.start_bg1,
    res.start_btn1,
    res.start_btn2,
    res.follow,
    res.moregame,
    res.invite


];
var res_play = [
    res.game,
    res.game_png,
    res.over,
    res.over_png,
    res.number1,
    res.number2,

];
