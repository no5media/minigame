Score=0;
Main_layer=null;
Is_Angin=false;
Is_overOrplay=false;
winer=null;
Block_Array=[];
bg_Array=[];
Is_Click=true;
var GameMainLayer=cc.LayerColor.extend({
    timeclock:null,
    PeppleScore:null,
    ctor: function () {
        this._super(cc.color(231,237,239));
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        Score=0;
        time=0;
        Is_Click=true;
        clock_time=40;
        winer=0;
        Main_layer=this;
        Is_Angin=false;
        Is_overOrplay=false;
        bg_num1=0;
        bg_num2=0;
        bg_Array=[];
        Block_Array=[];

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=new cc.Rect(touch.getLocation().x,touch.getLocation().y,1,1);
                //cc.rectIntersectsRect
                return true;
            },
            onTouchEnded:function(touch,event){

            }
        });
        cc.eventManager.addListener(listener, this);

        this.CreatePeopleInformation();
        this.DrawBg();
        this.CreateClock();
        this.CreateSp();
        this.AddSprite();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt){
        time+=dt;
        if(time>=1){
            time=0;
            clock_time--;
            timeclock.setString(clock_time);
            if(clock_time==0){
                if(angin_num<3){
                    cc.director.pause();
                    var layer=new AnginLayer(1);
                    this.addChild(layer,100);
                }else{
                    Is_overOrplay=true
                }
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false;
            clock_time+=5;
            timeclock.setString(clock_time);
            PeppleScore.setString(window.select.userPoint)
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return true;
        }
    },
    CreateSp: function () {
        var winSize=cc.winSize;
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("btn1.png"), function () {
            if(Is_Click){
                Is_Click=false;
                if(winer==1){
                    Score+=70;
                    SpgetScore.setString(Score);
                    Main_layer. CreateNewSprite();
                }else{
                    Score-=180;
                    if(Score<0){
                        Score=0
                    }
                    SpgetScore.setString(Score);
                    bg_Array[0].setVisible(false);
                    Main_layer. CreateError(22+92,100+114);
                }
            }
        },22+92,100+114,0.5,0.5,this);
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("btn2.png"), function () {
            if(Is_Click){
                Is_Click=false;
                if(winer==2){
                    Score+=70;
                    SpgetScore.setString(Score);
                    Main_layer. CreateNewSprite();
                }else{
                    Score-=180;
                    if(Score<0){
                        Score=0
                    }
                    SpgetScore.setString(Score);
                    bg_Array[1].setVisible(false);
                    Main_layer. CreateError(44+184+92,100+114);
                }
            }
        },44+184+92,100+114,0.5,0.5,this);
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("btn3.png"), function () {
            if(Is_Click){
                Is_Click=false;
                if(winer==3){
                    Score+=70;
                    SpgetScore.setString(Score);
                    Main_layer. CreateNewSprite();
                }else{
                    Score-=180;
                    if(Score<0){
                        Score=0
                    }
                    SpgetScore.setString(Score);
                    bg_Array[2].setVisible(false)
                    Main_layer. CreateError(66+184*2+92,100+114);
                }
            }

        },66+184*2+92,100+114,0.5,0.5,this);

        var bg1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("bg1.png"));
        bg1.setPosition(winSize.width/2-150,winSize.height/2+60);
        this.addChild(bg1,10);
        var bg2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("bg2.png"));
        bg2.setPosition(winSize.width/2+150,winSize.height/2+60);
        this.addChild(bg2,10);


        var s1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("b1.png"));
        s1.setPosition(22+92,100+114);
        this.addChild(s1,10);
        bg_Array.push(s1);
        var s2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("b2.png"));
        s2.setPosition(44+184+92,100+114);
        this.addChild(s2,10);
        bg_Array.push(s2);
        var s3=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("b1.png"));
        s3.setPosition(66+184*2+92,100+114);
        this.addChild(s3,10);
        bg_Array.push(s3);
    },
    CreateNewSprite: function () {
        for(var a=0;a<Block_Array.length;a++){
            Block_Array[a].removeFromParent();
            Block_Array.splice(a,1);
            a--;
        }
        Main_layer.AddSprite();
    },
    AddSprite: function () {
        var winSize=cc.winSize;
        for(var a=0;a<bg_Array.length;a++){
            bg_Array[a].setVisible(true);
        }
        var num1;
        var num2;
        var Is_true=true;
        if(bg_num1==0&&bg_num2==0){
             num1=Math.floor(Math.random()*3)+1;
             num2=Math.floor(Math.random()*3)+1;
            bg_num1=num1;
            bg_num2=num2;
        }else{
            do{
                num1=Math.floor(Math.random()*3)+1;
                num2=Math.floor(Math.random()*3)+1;
                if(num1==bg_num1&&bg_num2==num2){
                }else{
                    bg_num1=num1;
                    bg_num2=num2;
                    break;
                }
            }while(Is_true)
        }

        var s1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("s"+num1+".png"));
        s1.setPosition(winSize.width/2-150,winSize.height/2+60);
        this.addChild(s1,10);
        Block_Array.push(s1);

        var s2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("s"+num2+".png"));
        s2.setPosition(winSize.width/2+150,winSize.height/2+60);
        this.addChild(s2,10);
        Block_Array.push(s2);
        if(num1==1){
            if(num2==1){
                winer=2
            }else if(num2==2){
                winer=1
            }else if(num2==3){
                winer=3
            }
        }else if(num1==2){
            if(num2==1){
                winer=3
            }else if(num2==2){
                winer=2
            }else if(num2==3){
                winer=1
            }
        }else if(num1==3){
            if(num2==1){
                winer=1
            }else if(num2==2){
                winer=3
            }else if(num2==3){
                winer=2
            }
        }
        Is_Click=true;
    },
    CreateClock: function () {
        var winSize=cc.winSize;
        var time_str="倒计时:";
        var timeName = new cc.LabelTTF(time_str, "Arial", 20);
        timeName.x = 170;
        timeName.y = winSize.height-130;
        timeName.setColor(cc.color(255,255,255));
        this.addChild(timeName, 6);
        timeclock=new cc.LabelAtlas("40", "res/num.png", 20, 29,"0");
        timeclock.setPosition(210, winSize.height-140);
        this.addChild(timeclock,10);
        SpgetScore=new cc.LabelAtlas("0", "res/num.png", 20, 29,"0");
        SpgetScore.setPosition(winSize.width-80, winSize.height-130);
        SpgetScore.setAnchorPoint(0.5,0.5);
        this.addChild(SpgetScore,10);
    },
    CreateError: function (x,y) {
        var winSize=cc.winSize;
        var layer=new cc.LayerColor(cc.color(225,50,120,60));
        this.addChild(layer);
        var blink =new cc.Blink(0.3,2);//闪烁函数
        var no_sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("s4.png"));
        no_sp.setPosition(x,y);
        this.addChild(no_sp,10);
        var destory = new cc.RemoveSelf(true);
        var fun=cc.callFunc(function () {
            no_sp.removeFromParent();
            Main_layer.CreateNewSprite();

        });
        layer.runAction(cc.repeatForever(cc.sequence(blink,fun,destory)))
    },
    DrawBg: function () {
        var winSize=cc.winSize;
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,5);
        drawNode.drawRect(
            cc.p(0,0), // 起点
            cc.p(winSize.width,100), // 起点的对角点
            cc.color(0,0, 0, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(winSize.width,winSize.height), // 起点
            cc.p(0,winSize.height-160), // 起点的对角点
            cc.color(6,183, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(6,183, 255, 255) // 线颜色
        );
    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize;
        var start_score=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("Score.png"));
        start_score.setPosition(winSize.width,winSize.height-70)
        start_score.setAnchorPoint(1,0.5);
        this.addChild(start_score,10);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12)
        //var name_str="飞花飘絮";
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,255,255));
        this.addChild(record_name, 10);
        var get_score=window.select.userPoint;
        //var get_score=10;
        PeppleScore = new cc.LabelTTF("0", "Arial", 30);
        PeppleScore.x = winSize.width-70;
        PeppleScore.y =winSize.height-70;
        PeppleScore.setAnchorPoint(0.5,0.5);
        PeppleScore.setColor(cc.color(254,202,75));
        PeppleScore.setString(get_score);
        this.addChild(PeppleScore, 10);
    },
    GameOver: function () {
        var winSize=cc.winSize;
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners();
        var dely=cc.delayTime(1.5);
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("end_bg.png"));
        sp.setPosition(winSize.width/2,winSize.height/2);
        sp.setScale(0.1);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
            var transition=new cc.TransitionProgressRadialCW(1,new GameOverScene());
            cc.director.runScene(transition);
        })
        var fd = new cc.scaleTo(0.1,1);
        this.addChild(sp,12);
        sp.runAction(cc.sequence(fd,dely,fun));
    }
});

var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
});
Array.prototype.shuffle = function() {
    var input = this;
    for (var i = input.length-1; i >=0; i--) {
        var randomIndex = Math.floor(Math.random()*(i+1));
        var itemAtIndex = input[randomIndex];
        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
};