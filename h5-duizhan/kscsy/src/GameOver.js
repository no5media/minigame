var GameOverLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        var winSize = cc.winSize;

        var bg=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("start_bg.png"));
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg)
        hlmySetShare(Score,'jingdian')
        angin_num=0
        var bg1 = new cc.Sprite("res/start_bg1.png");
        bg1.setPosition(winSize.width / 2, winSize.height / 2+350);
        this.addChild(bg1);
        var type_score=Score;
        var game_score=Score;
        var game_type=0;
        var availabity=0;
        var url="/openapi/game/interactionGame/postGameLog.json?availabity="+availabity+"&aid="+appId+"&gameScore="+game_score+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+""+"&scoreId="+scoreId
        var xhr = cc.loader.getXMLHttpRequest();
        var url=url;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.send()
        //玩家的结束信息
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        //var name_str="飞花飘絮"
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-72;
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);
        var score_str="本局得分:"
        var score_str_name = new cc.LabelTTF(score_str, "Arial", 30);
        score_str_name.x = winSize.width/2;
        score_str_name.y =winSize.height-332;
        score_str_name.setAnchorPoint(0.5,0.5);
        score_str_name.setColor(cc.hexToColor("#ff8448"))
        this.addChild(score_str_name, 5);
        CoinNum_num=Score
        var game_Score=new cc.LabelTTF("0", "Arial", 50);
        game_Score.x = winSize.width / 2;
        game_Score.y = winSize.height-402;
        game_Score.setColor(cc.hexToColor("#fe5417"));
        game_Score.setScale(1.6);
        game_Score.setString(CoinNum_num);

        if(CoinNum_num>100000&&CoinNum_num<100000){
            game_Score.setScale(0.7);
        }else if(CoinNum_num>=100000){
            game_Score.setScale(0.6);
        }
        this.addChild(game_Score, 10);
        var self=this;
        var Best_Score_str= cc.sys.localStorage.getItem("kscsyBestScore");
        if(Best_Score_str){
            if(Best_Score_str<CoinNum_num){
                Best_Score_str=CoinNum_num;
            }
        }else{
            Best_Score_str=CoinNum_num;
        }
        cc.sys.localStorage.setItem("kscsyBestScore",Best_Score_str);
        var best_str="最高分:"
        var best_str_name = new cc.LabelTTF(best_str, "Arial", 30);
        best_str_name.x = winSize.width/2;
        best_str_name.y =winSize.height-472;
        best_str_name.setAnchorPoint(0.5,0.5);
        best_str_name.setColor(cc.color(170,170,170))
        this.addChild(best_str_name, 5);
        var Best_score= new cc.LabelTTF("0", "Arial", 35);
        Best_score.x = winSize.width/2;
        Best_score.y = winSize.height-522;
        Best_score.setAnchorPoint(0.5,0.5)
        Best_score.setColor(cc.color(41,175,235));
        Best_score.setString(Best_Score_str)
        this.addChild(Best_score,10);

        var start_score=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("Score.png"))
        start_score.setPosition(winSize.width,winSize.height-72)
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score);
        var xhr = cc.loader.getXMLHttpRequest();
        var  url="/openapi/game/interactionGame/index.json?aid="+appId;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status <= 207){
                    var resw = xhr.responseText;
                    var jsondata = JSON.parse(resw);
                    var data = jsondata["data"];
                    gameNum2=data.userPoint;//积分总数
                    getScore = new cc.LabelTTF("0", "Arial", 30);
                    getScore.x = winSize.width-70;
                    getScore.y =winSize.height-72;
                    getScore.setAnchorPoint(0.5,0.5)
                    getScore.setColor(cc.color(254,202,75));
                    getScore.setString(gameNum2);
                    self.addChild(getScore, 1);
                    if(gameNum2>=10000&&gameNum2<1000000){
                        getScore.setScale(0.8)
                    }else if(gameNum2>=1000000){
                        getScore.setScale(0.6)
                    }
                }
            }
        }
        xhr.send();
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("show.png"), function () {
            var share = document.getElementById('share-square');
            share.style.display = 'block';
        },winSize.width/2,490,0.5,0.5,this)
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("angin.png"), function () {
            cc.director.runScene(new GameMainScene());
            hlmySetShare(0,'jingdian')
        },winSize.width/2,320,0.5,0.5,this)
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("gohome.png"), function () {
            var transition=new cc.TransitionShrinkGrow(1,new GameStartScene());
            cc.director.runScene(transition);
            //cc.director.runScene(new GameStartScene());
        },winSize.width/2,150,0.5,0.5,this)
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("follow.png"), function () {
            //关注
            showEWM()
        },82.5,150,0.5,0.5,this)
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("invite.png"), function () {
            //邀请
            var share = document.getElementById('share-square');
            share.style.display = 'block';
        },winSize.width-82.5,150,0.5,0.5,this)
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;

            }
        });
        cc.eventManager.addListener(listener, this);
        var startlogo=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("start_logo.png"))
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
        return true;
    }
})
var GameOverScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameOverLayer();
        this.addChild(layer);
    }
})