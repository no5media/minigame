Score=0;
Main_layer=null;
Is_Angin=false;
Is_overOrplay=false;
var GameMainLayer=cc.LayerColor.extend({
    ctor: function () {
        this._super(cc.color(231,237,239));
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        Score=0;
        time=0;
        clock_time=40;
        Main_layer=this;
        Is_Angin=false;
        Is_overOrplay=false;
        Is_bool=false;
        this.CreatePeopleInformation();
        this.DrawBg();
        this.CreateClock();
        this.CreateBg();
        this.CreateNum();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt){
        time+=dt;
        if(time>=1){
            time=0;
            clock_time--;
            timeclock.setString(clock_time);
            if(clock_time==0){
                if(angin_num<3){
                    cc.director.pause();
                    var layer=new AnginLayer(1);
                    this.addChild(layer,12);
                }else{
                    Is_overOrplay=true
                }
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false;
            clock_time+=5;
            timeclock.setString(clock_time);
            PeppleScore.setString(window.select.userPoint);
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return true;
        }

    },
    CreateBg: function () {
        var winSize=cc.winSize;
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_btn1.png"), function () {
            if(Is_bool){
                Score+=44;
                SpgetScore.setString(Score);
                Main_layer.CreateNum();
            }else{
                Score-=150;
                if(Score<0){
                    Score=0;
                }
                SpgetScore.setString(Score);
                Main_layer.CreateNum();
                Main_layer.CreateError(1,165,205)
            }
        },165 ,205,0.5,0.5,this);
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_btn2.png"), function () {
            if(!Is_bool){
                Score+=44;
                SpgetScore.setString(Score);
                Main_layer.CreateNum();
            }else{
                Score-=150;
                if(Score<0){
                    Score=0;
                }
                SpgetScore.setString(Score);
                Main_layer.CreateNum();
                Main_layer.CreateError(2,475,205)
            }
        },475,205,0.5,0.5,this) ;
        bg_num=new cc.LabelTTF("97", "Arial", 100);
        bg_num.x=winSize.width/2;
        bg_num.y=winSize.height/2+110;
        this.addChild(bg_num,6);
        var sp=new cc.LabelTTF("7", "Arial", 100);
        sp.x=160;
        sp.y=200;
        this.addChild(sp,5);
        var sp1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg3.png"));
        sp1.x=475;
        sp1.y=205;
        this.addChild(sp1,5);
        var sp2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg.png"));
        sp2.x=winSize.width/2;
        sp2.y=winSize.height/2+110;
        this.addChild(sp2,5);
        var tishi_str="提示 : ";
        var tishi = new cc.LabelTTF(tishi_str, "Arial", 30);
        tishi.x = 50;
        tishi.y = 350;
        tishi.setAnchorPoint(0,0.5);
        tishi.setColor(cc.color(225,50,120));
        this.addChild(tishi, 6);
        var tishi_str1="包含7或者7的倍数请点击7按" +
            "钮，\n不然请点击跳过按钮";
        //var tishi_str1="从大到小按序点击"
        tishi1 = new cc.LabelTTF(tishi_str1, "Arial", 30);
        tishi1.x = 150;
        tishi1.y = 350;
        tishi1.setAnchorPoint(0,0.5);
        tishi1.setColor(cc.color(0,0,0));
        this.addChild(tishi1, 6);
    },
    CreateClock: function () {
        var winSize=cc.winSize;
        var time_str="倒计时:";
        var timeName = new cc.LabelTTF(time_str, "Arial", 20);
        timeName.x = 170;
        timeName.y = winSize.height-130;
        timeName.setColor(cc.color(255,255,255));
        this.addChild(timeName, 6);
        timeclock=new cc.LabelAtlas("40", "res/num.png", 20, 29,"0");
        timeclock.setPosition(210, winSize.height-140);
        this.addChild(timeclock,10);
        SpgetScore=new cc.LabelAtlas("0", "res/num.png", 20, 29,"0");
        SpgetScore.setPosition(winSize.width-80, winSize.height-130);
        SpgetScore.setAnchorPoint(0.5,0.5);
        this.addChild(SpgetScore,10);
    },
    CreateError: function (m,x,y) {
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        var winSize=cc.winSize;
        var layer=new cc.LayerColor(cc.color(225,50,120,60));
        this.addChild(layer);
        var blink =new cc.Blink(0.3,2);//闪烁函数
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_btn"+m+".png"));
        sp.setAnchorPoint(0.5,0.5);
        sp.setPosition(x,y);
        this.addChild(sp,10);
        var no_sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg2.png"));
        no_sp.setPosition(sp.getBoundingBox().width/2,sp.getBoundingBox().height/2);
        sp.addChild(no_sp,10);
        var destory = new cc.RemoveSelf(true);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
        });
        layer.runAction(cc.repeatForever(cc.sequence(blink,fun,destory)))
    },
    CreateNum: function () {
        var num=Math.floor(Math.random()*4);
        if(num==0){
            //是七或包含七
            Is_bool=true;
            this.CreateTrueNum();
        }else{
            Is_bool=false;
            this.CreateFalseNum()
        }
    },
    CreateTrueNum: function () {
        var end_num;
        var num=Math.floor(Math.random()*2);
        if(num==1){
            //包含七
            var true_num=Math.floor(Math.random()*2);
            if(true_num==1){
                //七在十位
                var num2=Math.floor(Math.random()*10);
                end_num=70+num2;
            }else{
                //七在个位
                var num2=Math.floor(Math.random()*10);
                end_num=num2*10+7;
            }
        }else{
            //七的倍数
            var num2=Math.floor(Math.random()*9+1);
            end_num=num2*7;
        }
        bg_num.setString(end_num);
    },
    CreateFalseNum: function () {
        var end_num;
        var  Is_true=false;
        do{
            var num1=Math.floor(Math.random()*100);
            var num_Array=[];
            if(num1>9){
                var num2=num1%10;
                num_Array.push(num2);
                var num3=num1/10;
                num3=Math.floor(num3);
                num_Array.push(num3);
            }else{
                num_Array.push(num1);
            }
            Is_true=true;
            for(var a=0;a<num_Array.length;a++){
                if(num_Array.indexOf(7)!=-1){
                    Is_true=false
                }
            }
            if(num1%7==0){
                Is_true=false
            }
            if(Is_true){
                end_num=num1;
                break;
            }
        } while(!Is_true);

        bg_num.setString(end_num);
    },
    DrawBg: function () {
        var winSize=cc.winSize;
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,5);
        drawNode.drawRect(
            cc.p(0,0), // 起点
            cc.p(winSize.width,100), // 起点的对角点
            cc.color(0,0, 0, 255), // 填充颜色
            1, // 线粗
            cc.color(0,0, 0, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(0,310), // 起点
            cc.p(winSize.width,390), // 起点的对角点
            cc.color(255,255, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(winSize.width,winSize.height), // 起点
            cc.p(0,winSize.height-160), // 起点的对角点
            cc.color(6,183, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(6,183, 255, 255) // 线颜色
        );
    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        cc.spriteFrameCache.getSpriteFrame("Score.png");
        var start_score=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("Score.png"));
        start_score.setPosition(winSize.width,winSize.height-70);
        start_score.setAnchorPoint(1,0.5);
        this.addChild(start_score,10);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        //var name_str="飞花飘絮";
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,255,255));
        this.addChild(record_name, 10);
        var get_score=window.select.userPoint;
        //var get_score=10;
        PeppleScore = new cc.LabelTTF("0", "Arial", 30);
        PeppleScore.x = winSize.width-70;
        PeppleScore.y =winSize.height-70;
        PeppleScore.setAnchorPoint(0.5,0.5);
        PeppleScore.setColor(cc.color(254,202,75));
        PeppleScore.setString(get_score);
        this.addChild(PeppleScore, 10);

    },
    GameOver: function () {
        var winSize=cc.winSize;
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners();
        var dely=cc.delayTime(1.5);
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("end_bg.png"));
        sp.setPosition(winSize.width/2,winSize.height/2);
        sp.setScale(0.1);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
            var transition=new cc.TransitionProgressRadialCW(1,new GameOverScene());
            cc.director.runScene(transition);
        });
        var fd = new cc.scaleTo(0.1,1);
        this.addChild(sp,12);
        sp.runAction(cc.sequence(fd,dely,fun));
    }
});
var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})
