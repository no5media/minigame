Tishi_layer=null;
var TishiLayer=cc.Layer.extend({
    ctor: function (str,gametype) {
        this._super();
        Tishi_layer=this;
        var winSize=cc.director.getVisibleSize();
        var self=this;
        var bg=new cc.Sprite("res/start/fristBg.png");
        bg.x=winSize.width/2;
        bg.y=winSize.height/2;
        //bg.setScaleX(0.8);
        this.addChild(bg);
        var str_str=str;
        var  str_name = new cc.LabelTTF(str_str, "Arial", 30);
        str_name.x = winSize.width/2;
        str_name.y =winSize.height/2+50;
        this.addChild(str_name, 5);
        CreateBtn(res.fristbtn, function () {
            self.removeFromParent();
            if(gametype){
                var layer=new fuchouLayer();
                Start_layer.addChild(layer,20);
            }
        },winSize.width/2,winSize.height/2-80,0.5,0.5,Tishi_layer);

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                self.removeFromParent();
                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
})
