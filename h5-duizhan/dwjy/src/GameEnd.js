end_layer=null;
var GameEndLayer=cc.LayerColor.extend({
    ctor: function (end_type) {
        this._super(cc.color(255,255,255,255));
        end_layer=this;
        var winSize=cc.winSize;
        var bg=new cc.Sprite(res.start_bg);
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);
        cc.log("游戏结束",end_type);
        var name_str1=window.get.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        switch (end_type){
            case 0:
                var PK_str;
                if(net_Score>=window.get.interActionPlayGameLog.typeScore){
                    PK_str="我在和"+ name_str+"的对战中完胜了TA，不服来战？？？";
                }else{
                    PK_str="我在和"+ name_str+"的对战中拜下阵来，有人帮我吗？？？";
                }
                var link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=pk_dwjy_share";
                share_object={
                    title:PK_str,
                    link:link
                };
                HLMY_PK.setShare(share_object);
                break;
            case 1:
                var FC_str;
                if(net_Score>=window.get.interActionPlayGameLog.typeScore){
                    FC_str="我在和"+ name_str+"的对战中复仇成功，已经玩了24个小时，根本停不下来~";
                }else{
                    FC_str="我在和"+ name_str+"的对战中复仇失败，失败是成功之母，继续干~";
                }
                var link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=fc_dwjy_share";
                share_object={
                    title:FC_str,
                    link:link
                };
                HLMY_PK.setShare(share_object);
                break;
            case 2:
                var TZ_str;
                if(net_Score>=window.get.interActionPlayGameLog.typeScore){
                    TZ_str="我在和"+ name_str+"的对战中完胜了TA，不服来战？？？";
                }else{
                    TZ_str="我在和"+ name_str+"的对战中拜下阵来，有人帮我吗？？？";
                }
                var link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=pk_dwjy_share";
                share_object={
                    title:TZ_str,
                    link:link
                };
                HLMY_PK.setShare(share_object);
                break;
            case 3:
                var TZ_str;
                if(net_Score>=window.get.interActionPlayGameLog.typeScore){
                    TZ_str="我在和"+ name_str+"的对战中完胜了TA，不服来战？？？";
                }else{
                    TZ_str="我在和"+ name_str+"的对战中拜下阵来，有人帮我吗？？？";
                }
                var link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=pk_dwjy_share";
                share_object={
                    title:TZ_str,
                    link:link
                };
                HLMY_PK.setShare(share_object);
                break;
        }
        var self=this;
        cc.sys.localStorage.setItem("dwjyAngin",0);
        //SetShare(Score,end_type);
        /*测试用例*/
        var allData=[];
        allData.push(Record_Array);
        cc.log(Record_Array.length,Record_fall_Array.length,Recordclick.length,Record_remove.length);
        allData.push(Record_fall_Array);
        allData.push(Recordclick);
        allData.push(Record_remove);
        allData.push(Record_Score);
        var type_score=net_Score;
        var min_Score=0;
        var min=0;
        for(var a=0;a<Record_Score.length;a++){
            if(Record_Score[a].time>=60){
                min_Score=Record_Score[a].Score;
                min=a;
                break;
            }
        }
        if(min==0){

        }else{
            if(Record_Score[min-1].Score==min_Score){
                type_score=min_Score;
            }else{
                type_score=Record_Score[min-1].Score
            }
        }
        allData[allData.length]=type_score;
        allData[allData.length]=net_Score;
        var jsondata1 = JSON.stringify(allData);
        var availabity=0;
        if(type_score!=0){
            //cc.sys.localStorage.removeItem("pipei");
            //cc.sys.localStorage.setItem("pipei",jsondata1);
            availabity=1;
        }else{
            availabity=0;
        }

        /*测试结束*/
        var game_score=0;
        var game_type=0;
        var type_score=net_Score;
        var player_GameLogId=window.get.interActionPlayGameLog.id;
        var rivalUid=window.get.userProfile.userId;
        cc.log("rivalUid",rivalUid);
        var rivalType=end_type;
        var rivalTypeScore=window.get.interActionPlayGameLog.typeScore;
        //var player_GameLogId=0
        var xhr1 = cc.loader.getXMLHttpRequest();
        var  url="/openapi/game/interactionGame/postGameLog.json?gameConfigId=5&rivalTypeScore="+rivalTypeScore +"&availabity="+availabity+"&aid="+appId+"&rivalUid="+rivalUid+"&rivalType="+rivalType+"&gameScore="+game_score+"&gameLog="+null+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+player_GameLogId+"&scoreId="+scoreId
        xhr1.open("POST", url,true);
        xhr1.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr1.onreadystatechange = function () {
            if (xhr1.readyState == 4){
                if(xhr1.status >= 200 && xhr1.status <= 207){
                    var resw = xhr1.responseText;
                    var jsondata = JSON.parse(resw);
                    var data = jsondata["data"];
                    var num=data.fightResult;
                    if(num==1){
                        var win=new cc.Sprite("res/start/win.png");
                        win.setPosition(winSize.width/2,winSize.height-180);
                        self.addChild(win);
                    }else if(num==0){
                        var nowin=new cc.Sprite("res/start/nowin.png");
                        nowin.setPosition(winSize.width/2,winSize.height-180);
                        self.addChild(nowin);
                    }
                    var newGame=new cc.Sprite("res/start/newGame.png");
                    newGame.setPosition(winSize.width/2-30,655);
                    self.addChild(newGame);
                    var jiangpai_num=data.medals;
                    var jiangpai_str;
                    var jiangpai_shuming_str;
                    if(jiangpai_num>=0){
                        jiangpai_str="+ "+jiangpai_num;
                        jiangpai_shuming_str="您本局得到 "+jiangpai_num+"枚奖牌"
                    }else{
                        jiangpai_str=+jiangpai_num;
                        jiangpai_shuming_str="您本局损失"+Math.abs(jiangpai_num)+"枚奖牌"
                    }
                    var jiangpai_shuming=new cc.LabelTTF(jiangpai_shuming_str, "Arial", 30);
                    jiangpai_shuming.x = winSize.width/2;
                    jiangpai_shuming.y =585;
                    jiangpai_shuming.setColor(cc.color(88,195,224));
                    self.addChild(jiangpai_shuming, 1);
                    var jiangpai_name=new cc.LabelTTF(jiangpai_str, "Arial", 40);
                    jiangpai_name.x = winSize.width/2+40;
                    jiangpai_name.y =650;
                    jiangpai_name.setColor(cc.color(255,0,0));
                    self.addChild(jiangpai_name, 1);
                }
            }
        };
        xhr1.send(jsondata1);
        var winSize=cc.winSize;
        var  startbg_x=winSize.width/2;
        var  startbg_y=winSize.height/2;
        var  startbg1_x=winSize.width/2;
        var  startbg1_y=winSize.height-220;
        var startbg=new cc.Sprite(res.start_bg);
        startbg.setPosition(startbg_x,startbg_y);
        startbg.setAnchorPoint(0.5,0.5);
        this.addChild(startbg);

        var record_headUrl;
        if(window.select.userProfile&&window.select.userProfile.headUrl){
             record_headUrl=window.select.userProfile.headUrl;
        }else{
             record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-40,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        //var name_str="李乾@1758"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-40;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);

        var url1=index_xhr;
        var xhr_fun= function (data) {
                window.select=[];
                window.select = data["data"];
                var start_score=new cc.Sprite(res.Score);
                start_score.setPosition(winSize.width,winSize.height-40);
                start_score.setAnchorPoint(1,0.5);
                self.addChild(start_score,1);
                Game_BestScore=window.select.userPoint;//积分总数
                getScore = new cc.LabelTTF("0", "Arial", 30);
                getScore.x = winSize.width-70;
                getScore.y =winSize.height-40;
                getScore.setColor(cc.color(88,195,224));
                getScore.setString(Game_BestScore);
                self.addChild(getScore, 1);
                if(gameNum2>=10000&&gameNum2<1000000){
                    getScore.setScale(0.8)
                }else if(gameNum2>=1000000){
                    getScore.setScale(0.6)
                }
        };
        var error_fun= function () {
        };
        CreateXhr(url1,xhr_fun,error_fun);


        var vs=new cc.Sprite("res/start/VS.png");
        vs.setPosition(winSize.width/2,winSize.height-280);
        this.addChild(vs);

        //玩家的结束信息
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl=window.select.userProfile.headUrl;
        var record_headUrl;
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,200,winSize.height-300,120,self);
        });
        //var name_str="李乾@1758"
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 200;
        record_name.y =winSize.height-400;
        record_name.setAnchorPoint(0.5,0.5);
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);

        var netScore = new cc.LabelTTF("0", "Arial", 40);
        netScore.x = 200;
        netScore.y =winSize.height-450;
        netScore.setAnchorPoint(0.5,0.5);
        netScore.setColor(cc.color(88,195,224));
        this.addChild(netScore, 1);
        netScore.setString(net_Score);
        if(net_Score>=10000&&net_Score<1000000){
            netScore.setScale(0.8)
        }else if(net_Score>=1000000){
            netScore.setScale(0.6)
        }
        //记录的结束信息

        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl=window.get.userProfile.headUrl;
        var record_headUrl;
        if(window.get.userProfile&&window.get.userProfile.headUrl){
             record_headUrl=window.get.userProfile.headUrl
        }else{
             record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,winSize.width-200,winSize.height-300,120,self);
        });
        //var name_str="李乾@1758";
        var name_str1=window.get.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = winSize.width-200;
        record_name.y =winSize.height-400;
        record_name.setAnchorPoint(0.5,0.5);
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);
        var recordScore = new cc.LabelTTF("0", "Arial", 40);
        recordScore.x = winSize.width-200;
        recordScore.y =winSize.height-450;
        recordScore.setAnchorPoint(0.5,0.5);
        recordScore.setColor(cc.color(88,195,224));

        //cc.log("window.get",window.get);
        var ss=window.get.interActionPlayGameLog.typeScore;
        //var ss=record_Score;
        recordScore.setString(ss);
        this.addChild(recordScore, 1);
        if(ss>=10000&&ss<1000000){
            recordScore.setScale(0.8)
        }else if(ss>=1000000){
            recordScore.setScale(0.6)
        }
        var show_X=winSize.width/2;
        var show_Y=500;
        if(end_type==0){
            show_X=winSize.width/2;
            show_Y=500;
        }else{
            show_X=winSize.width/2;
            show_Y=360;
        }

        CreateBtn("res/start/show.png", function () {
            HLMY_PK.shareTip();
        },show_X,show_Y,0.5,0.5,this);
        var self=this;
        if(end_type==0){
            CreateBtn("res/start/angin.png", function () {
                //匹配模式
                var layer=new NetLoadLayer();
                end_layer.addChild(layer,10);
                var start_time=new Date().getTime();
                var url=pk_xhr;
                var xhr_fun= function (data) {
                    var end_time=new Date().getTime();
                    if(end_time-start_time<=10*1000){
                        cc.log("start_time:",start_time,"end_time:",end_time,"end_time-start_time:",end_time-start_time);
                        window.get=[];
                        window.get= data["data"];
                        cc.director.runScene(new NetWorkScene(end_type));
                        cc.log("window.get",window.get)
                    }
                };
                var error_fun= function () {
                    self.removeFromParent();
                    var layer=new TishiLayer("网络异常...");
                    end_layer .addChild(layer,10);
                };
                CreateXhr(url,xhr_fun,error_fun);
            },winSize.width/2,330,0.5,0.5,this);
        }
        var self=this;

        CreateBtn(res.start_ranklist, function () {
            //排行榜
            //document.body.style.backgroundColor = '#9c9b9b';
            //document.documentElement.style.opacity = '0.5';
                var layer=new NetLoadLayer();
                end_layer .addChild(layer,10);
                var url=PK_zhouRnking;
                var fun= function (Array) {
                    if(Net_load_layer){
                        Net_load_layer.removeFromParent();
                    }
                    Gamedata2=[];
                    Gamedata2 = Array["data"];
                    cc.log("Gamedata2",Gamedata2);
                    var layer = new RankList(1);
                    layer.setScale(0.1);
                    end_layer.addChild(layer,10);
                    var scal=new cc.ScaleTo(0.3,1);
                    layer.runAction(scal);
                };
                var error_fun= function () {
                    if(Net_load_layer){
                        Net_load_layer.removeFromParent();
                    }
                    var layer=new TishiLayer("网络异常...");
                    end_layer .addChild(layer,10);
                };
                CreateXhr(url,fun,error_fun);

        },winSize.width/2-92,165,0.5,0.5,this);
        CreateBtn("res/start/gohome.png", function () {
            var url1=index_xhr;
            var xhr_fun= function (data) {
                window.select=[];
                window.select = data["data"];
                var transition=new cc.TransitionShrinkGrow(1,new GameStartScene());
                cc.director.runScene(transition);
            };
            var error_fun= function () {
            };
            CreateXhr(url1,xhr_fun,error_fun);

        },winSize.width/2+92,150,0.5,0.5,this);
        CreateBtn(res.follow, function () {
            //关注
            HLMY_PK.ewm()
        },100,150,0.5,0.5,this);
        CreateBtn(res.invite, function () {
            //邀请
            HLMY_PK.shareTip();
            //var share = document.getElementById('share-square');
            //share.style.display = 'block';
        },winSize.width-100,150,0.5,0.5,this);

        var startlogo=new cc.Sprite("res/start/start_logo.png");
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
            }
        });
        cc.eventManager.addListener(listener, this);
        return true

    }

});
var endScene=cc.Scene.extend({
    ctor: function (game_type) {
        this._super();
        cc.log("endScene",game_type);
        var layer=new GameEndLayer(game_type);
        this.addChild(layer);
    }
});