var ShuomingLayer=cc.LayerColor.extend({
    ctor: function () {
        this._super(cc.color(0,0,0,100));
        var winSize=cc.director.getVisibleSize();

        var self=this;
        CreateBtn("res/shuoming.png", function () {
            self.removeFromParent();
        },winSize.width/2,winSize.height/2,0.5,0.5,this);

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
})
