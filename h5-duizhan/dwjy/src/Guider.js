var GuiderLayer=cc.LayerColor.extend({
    ctor:function(game_type){
        this._super(cc.color(255,255,255,255));
        AllBlock=[];
        AllBlockPostion=[];
        Step_Array=[];
        hand=null;
        Is_Click=true;
        Max_row=3;
        var winSize=cc.director.getVisibleSize();
        var bg=new cc.Sprite("res/Guider/bg.png");
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);
        for(var a=0;a<6;a++){
            this.CreateSprite(GuiderData[0][a].tag,GuiderData[0][a].row,GuiderData[0][a].col)
        }
        this.CreateAction();
        this.CreateStep();
        hand=new cc.Sprite("res/Guider/bg1.png");
        hand.setPosition(winSize.width/2+66,winSize.height/2-33);
        this.addChild(hand,3);
        var self=this;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                //cc.log("aa",touch.getLocation())
                return true;
            },
            onTouchEnded:function(touch,event){
                var rect= cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                var Is_click_Check=false;
                if(Is_Click&&(level==1||level==2)){
                    Is_Click=false;
                    for(var a=0;a<AllBlock.length;a++){
                        if(AllBlock[a]&&cc.rectIntersectsRect(AllBlock[a].getBoundingBox(),rect)){
                            if(AllBlock[a].row==LevelData[level-1].row&&AllBlock[a].col==LevelData[level-1].col){
                                Is_click_Check=true;
                                var tag=AllBlock[a].tag+1;
                                var sp_col=AllBlock[a].col;
                                var sp_row=AllBlock[a].row;
                                var sp_x=AllBlock[a].x;
                                var sp_y=AllBlock[a].y;
                                Step_Array[0].removeFromParent();
                                Step_Array.splice(0,1.2);
                                hand.setVisible(false);
                                AllBlock[AllBlock[a].row*Max_row+AllBlock[a].col].removeFromParent();
                                AllBlock[AllBlock[a].row*Max_row+AllBlock[a].col]=null;
                                self.CreateSprite(tag,sp_row,sp_col,false);
                                var dely=cc.delayTime(0.8)
                                var fun=new cc.callFunc(function () {
                                    if(level==1){
                                        self.RemoveSprite(AllBlock[3],sp_x,sp_y,1);
                                        self.RemoveSprite(AllBlock[5],sp_x,sp_y,2);

                                    }else if(level==2){
                                        self.RemoveSprite(AllBlock[3],sp_x,sp_y,1);
                                        self.RemoveSprite(AllBlock[1],sp_x,sp_y,2);
                                    }
                                });
                                AllBlock[a].runAction(cc.sequence(dely,fun))

                            }
                        }
                    }
                    if(Is_click_Check){
                        Is_Click=false
                    }else{
                        Is_Click=true
                    }
                }else if(!Is_Click&&level>2){
                    //cc.director.runScene(new GameMainScene());
                    cc.director.runScene(new GameStartScene(game_type));
                }

            }
        });
        cc.eventManager.addListener(listener, this);
        var sp_Label = new cc.LabelTTF("新手引导", "Arial", 50);
        sp_Label.x = winSize.width/2;
        sp_Label.y =  winSize.height-100;
        sp_Label.setColor(cc.color(0,0,0));
        sp_Label.setScale(1.5);
        this.addChild(sp_Label, 5);
    },
    CreateSprite: function (tag,row,col,bool) {
        var winSize=cc.director.getVisibleSize();
        var sp=new cc.Sprite("res/sp"+tag+".png");
        sp.row=row;
        sp.col=col;
        sp.tag=tag;
        var posx=winSize.width/2-132+132*col;
        var posy=winSize.height/2-66+132*row;
        sp.setAnchorPoint(0.5,0.5);
        sp.setPosition(posx,posy);
        AllBlock[row*Max_row+col]=sp;
        AllBlockPostion[row*Max_row+col]={
            x:posx,
            y:posy
        };

        this.addChild(sp);
        var sp_num= new cc.LabelAtlas(tag, "res/num.png", 55, 71,"0");
        //sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
        sp_num.setPosition(63,66);
        sp_num.setAnchorPoint(0.5,0.5);
        sp.addChild(sp_num);
        sp.setScale(0.01);
        sp.runAction(new cc.ScaleTo(0.25,1));
        if(bool){
            Is_Click=true;
        }
    },
    CreateAction: function () {
        var sp=new cc.Sprite("res/Guider/bg2.png");
        sp.setPosition(AllBlock[1*Max_row+1].x,AllBlock[1*Max_row+1].y);
        sp.setScale(0.01);
        this.addChild(sp,1);
        var sp1=new cc.Sprite("res/Guider/bg2.png");
        sp1.setPosition(AllBlock[1*Max_row+1].x,AllBlock[1*Max_row+1].y);
        sp1.setScale(0.01);
        this.addChild(sp1,1);
        var time=0.5;
        var dely=cc.delayTime(time);
        var fun1=new cc.callFunc(function () {
            var scale1=new  cc.scaleTo(time,1.4);
            var scale2=new  cc.scaleTo(time,0.1);
            sp.runAction(cc.repeatForever(new cc.sequence(scale1,scale2)))
        });
        var fun2=new cc.callFunc(function () {
            var scale3=new  cc.scaleTo(time,1.4);
            var scale4=new  cc.scaleTo(time,0.1);
            sp1.runAction(cc.repeatForever(new cc.sequence(scale3,scale4)))
        });
        sp.runAction(new cc.sequence(fun1,dely,fun2))
    },
    CreateStep: function () {
        var winSize=cc.director.getVisibleSize();
        for(var a=0;a<5;a++){
            var sp=new cc.Sprite("res/Guider/step"+(a+1)+".png");
            sp.setPosition(winSize.width/2+66,winSize.height/2+220)
            this.addChild(sp,5-a);
            Step_Array.push(sp);
        }
    },
    RemoveSprite: function (object,x,y,num) {
        var self=this;
        var move=new cc.moveTo(0.2,x,y);
        var fun=cc.callFunc(function () {
            for(var a=0;a<GuiderData[level].length;a++){
                if(GuiderData[level][a].row==object.row&&object.col==GuiderData[level][a].col){
                    object.removeFromParent();
                    AllBlock[object.row*Max_row+object.col]=null;
                    self.FallSprite(GuiderData[level][a].tag,GuiderData[level][a].row,GuiderData[level][a].col)
                }
            }
            if(level==1&&num==2){
                level++;
                var tag=AllBlock[1*Max_row+1].tag+1;
                AllBlock[1*Max_row+1].removeFromParent();
                AllBlock[1*Max_row+1]=null;
                self.CreateSprite(tag,1,1,true);
                Step_Array[0].removeFromParent();
                Step_Array.splice(0,1);
                hand.setVisible(true);

            }else  if(level==2&&num==2){
                level++
                var tag=AllBlock[1*Max_row+1].tag+1
                AllBlock[1*Max_row+1].removeFromParent();
                AllBlock[1*Max_row+1]=null;
                self.FallSprite(2,1,1);
                self.CreateSprite(tag,1,1,false);
                Step_Array[0].removeFromParent();
                Step_Array.splice(0,1);
                var fun=new cc.callFunc(function () {
                    Is_Click=true;
                })
                AllBlock[4].runAction(new cc.sequence(new cc.moveTo(0.2,AllBlockPostion[1].x,AllBlockPostion[1].y,fun)))
                //hand.setVisible(true);
            }
        });
        object.runAction(new cc.sequence(move,fun))
    },
    FallSprite: function (tag,row,col) {
        var winSize=cc.director.getVisibleSize();
        var sp=new cc.Sprite("res/sp"+tag+".png");
        sp.row=row;
        sp.col=col;
        AllBlock[row*Max_row+col]=sp
        var posx=winSize.width/2-132+132*col;
        var posy=winSize.height/2-66+132*row
        sp.setPosition(posx,winSize.height/2+132*2);
        this.addChild(sp);
        sp.runAction(new cc.moveTo(0.2,posx,posy));
        var sp_num= new cc.LabelAtlas(tag, "res/num.png", 55, 71,"0");
        //sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
        sp_num.setPosition(63,66);
        sp_num.setAnchorPoint(0.5,0.5);
        sp.addChild(sp_num);
    }
});
var GuiderScene=cc.Scene.extend({
    ctor:function(num){
        this._super();
        var layer=new GuiderLayer(num);
        this.addChild(layer);

    }
})