net_Score=0;
Net_Main_layer=null;
Net_layer=null;
Max_row=5;
net_All_Array=[];
net_life_num=5;
net_life_num=5;
net_move_num=0;
net_move_num1=0;
net_remove_num=0;
net_Click=true;
net_click_Check=false;//判断某个数值方块是否是点击状态
net_Check=false;//是否能够进行检测
net_fall=false;//是否处于下落状态
net_move=false;//是否处于移动状态
speed=0.08;
net_Life_Array=[];
net_All_Array=[];
net_All_Block_Postion=[];
net_GetSprite_Array=[];
net_index=0;
net_index_index=0;
net_index_index=0;
Score_idnex=0;
Is_wait=true;
net_overOrplay=null;
Ceshi_time=0;
startRecord_time=0;
startRecordtime=0;
Record_end=true;
Record_Array=[];//存储初始化这个地图的数据，（用于对战时上传时用）
Record_fall_Array=[];//存储下落时的数字方块，（用于对战时上传时用）
Recordclick=[];//存储玩家点击的数字方块，（用于对战时上传时用）
Record_remove=[];
Record_Score=[];
Is_canGetAngin=true;
var NetWorkLayer=cc.LayerColor.extend({
    ctor: function (game_type) {
        this._super(cc.color(255,255,255,255));



        Net_Main_layer=this;
       gametype=game_type;
        Max_row=5;
        net_Life_Array=[];
        net_All_Array=[];
        net_All_Block_Postion=[];
        net_GetSprite_Array=[];

        Record_end=true;
        Record_Array=[];//存储初始化这个地图的数据，（用于对战时上传时用）
        Record_fall_Array=[];//存储下落时的数字方块，（用于对战时上传时用）
        Recordclick=[];//存储玩家点击的数字方块，（用于对战时上传时用）
        Record_remove=[];
        Record_Score=[];
        Score_idnex=0;
        net_x=48+132/2;//最左下角的的方块的坐标x值
        net_y=100;//最左下角的的方块的坐标y值
        net_xy=3;//设置方块方块的间隔
        Min_width=132;
        net_life_num=5;
        net_time=0;
        speed=0.08;
        net_move_num=0;
        net_move_num1=0;
        net_remove_num=0;
        net_index=0;
        clock_timeh=1;
        clock_timef=0;
        clock_timef1=0;
        clock_time=0;
        net_index_index=0;
        net_Score=0;
        startRecordtime=0;
        startRecord_time=0;
        Is_wait=true;
        Is_read=true;
        Is_go=true;
        net_Click=true;
        Layer_object=this;
        net_click_Check=false;//判断某个数值方块是否是点击状态
        net_Check=false;//是否能够进行检测
        net_fall=false;//是否处于下落状态
        net_move=false;//是否处于移动状态
        net_bianli=true;
        net_create=true;
        game_satrt=false;
        game_createBg=true;
        net_overOrplay=false;
        net_angin=true;
        guiderGuider=true;
        Is_game_over=true;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect= cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                if(net_Click){
                    net_Click=false;
                    for(var a=0;a<net_All_Array.length;a++){
                        if(net_All_Array[a]&&cc.rectIntersectsRect(net_All_Array[a].getBoundingBox(),rect)){
                            net_remove_num=0;
                            net_click_Check=true;
                            var tag=net_All_Array[a].tag+1;
                            var sp_x=net_All_Array[a].x;
                            var sp_y=net_All_Array[a].y;
                            var sp_col=net_All_Array[a].col;
                            var sp_row=net_All_Array[a].row;
                            net_All_Array[net_All_Array[a].row*Max_row+net_All_Array[a].col].removeFromParent();
                            net_All_Array[net_All_Array[a].row*Max_row+net_All_Array[a].col]=null;
                            /*开始记录*/
                            if(Record_end){
                                Recordclick.push({
                                    time:startRecord_time,
                                    tag:tag,
                                    row:sp_row,
                                    col:sp_col
                                });
                                //cc.log(Record_click)
                            }
                            /*记录结束*/
                            CreateSprite(tag,sp_x,sp_y,sp_row,sp_col,net_GetSprite_Array,net_All_Array,net_All_Block_Postion,Net_Main_layer,2);
                        }
                    }
                    return true;
                }
            },
            onTouchEnded:function(touch,event){
                net_Click=true;
            }
        });
        cc.eventManager.addListener(listener, this);
        this.CreateBg();
        create_layer1=new CreateLayer();
        this.addChild(create_layer1,10);
        var layer=new loadLayer(game_type);
        this.addChild(layer,12);
        read_layer=new readLayer();
        this.addChild(read_layer,10);
        read_layer.setVisible(false);
        go_layer=new goLayer();
        this.addChild(go_layer,10);
        go_layer.setVisible(false);
        this.scheduleUpdate();
    },
    update: function (dt) {
        if(Record_end&&game_satrt){
            startRecord_time+=dt;
            if(startRecord_time>=60){
                Record_end=false;
            }
        }
        if(!Is_wait){
            time+=dt;
            create_layer1.removeFromParent();
            read_layer.setVisible(true);
            if(time>1&&time<2){
                read_layer.removeFromParent();
                go_layer.setVisible(true)
            }else if(time>2&&time<3){
                go_layer.removeFromParent();
                var layer=new NetLayer();
                this.addChild(layer);
                Is_wait=true;
                game_satrt=true;
            }
        }
        if(game_satrt){
            clock_time+=dt;
            if(clock_timef1==0&&clock_timef==1&&clock_timeh==0){
                if( guiderGuider){
                    guiderGuider=false;
                    this.CreateGuider();
                }
            }
            if(clock_time>=1) {
                clock_time = 0;
                if (clock_timef1 > 0) {
                    clock_timef1--;
                } else {
                    if(clock_timef>0){
                        clock_timef--;
                        clock_timef1=9
                    }else{
                        if(clock_timeh>0){
                            clock_timeh--;
                            clock_timef=5;
                            clock_timef1=9
                        }else{
                            if(Is_game_over){
                                Is_game_over=false;
                                this.NetGame_ver();
                            }
                        }
                    }
                }
                Record_f1.setString(clock_timef1);
                Record_f.setString(clock_timef);
                Record_h.setString(clock_timeh);
            }
            if(net_Check&&!net_move&&!net_fall){
                for(var col=0;col<5;col++) {
                    for (var row = 0; row < 5; row++) {
                        if(net_All_Array[row*Max_row+col]){
                            if(!net_click_Check&&net_Check){
                                net_All_Array[row*Max_row+col].bool=false;
                                GetSprite(net_All_Array[row*Max_row+col],net_GetSprite_Array,net_All_Array,2,2);
                            }
                        }
                    }
                }
            }
            for(var a=0;a<net_All_Array.length;a++){
                if(net_All_Array[a]&&net_All_Array[a].move){
                    var posx=net_All_Block_Postion[net_All_Array[a].run_Array[net_All_Array[a].sp_length].row*Max_row+net_All_Array[a].run_Array[net_All_Array[a].sp_length].col].x
                    var posy=net_All_Block_Postion[net_All_Array[a].run_Array[net_All_Array[a].sp_length].row*Max_row+net_All_Array[a].run_Array[net_All_Array[a].sp_length].col].y
                    SpMove(net_All_Array[a],posx,posy,2);
                }
            }
            if(net_move||net_fall){
                net_Click=false
            }else if(!net_move&&!net_fall){
                net_Click=true
            }
            if(net_angin){//复活之后需要将生命填满，并且将钻石的数量减少
                net_angin=false;
                for(var a=0;a<net_Life_Array.length;a++){
                    net_Life_Array[a].setVisible(true)
                }
                net_life_num=5
            }
            if(net_overOrplay){
                cc.eventManager.removeAllListeners();
                net_Click=false;
            }
        }
    },
    NetGame_ver: function () {
        if(AnginLayer_layer){
            AnginLayer_layer.removeFromParent();
        }
        this.unscheduleUpdate();
        Net_layer.unscheduleAllCallbacks();
        cc.eventManager.removeAllListeners();
        if( record_Score!=window.get.interActionPlayGameLog.typeScore){
            record_Score=window.get.interActionPlayGameLog.typeScore;
        }
        record_Score_num.setString(record_Score);
        var winSize=cc.winSize;
        var sp=new cc.Sprite("res/Time_end.png");
        sp.x=winSize.width/2;
        sp.y=winSize.height;
        this.addChild(sp,10);
        var time_fun= cc.callFunc(function () {
            //var transition=new cc.TransitionProgressRadialCW(1,new endScene());
            var transition=new cc.TransitionRotoZoom(0.8,new endScene(gametype));
            cc.director.runScene(transition);
        });
        var dely=cc.delayTime(1.5);
        var move=new cc.moveTo(0.6,winSize.width/2,winSize.height/2);
        sp.runAction(cc.sequence(move.easing(cc.easeBackOut()),dely,time_fun));
    },
    CreateBg: function () {
        var size=cc.director.getVisibleSize();
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl=window.select.userProfile.headUrl
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,80,size.height-50,80,self);
        });
        //cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
        //    var head_bg=new cc.Sprite(record_headUrl);
        //    var scalx=80/head_bg.getBoundingBox().width;
        //    var scaly=80/head_bg.getBoundingBox().height
        //    head_bg.setPosition(80,size.height-50)
        //    head_bg.setScale(scalx,scaly)
        //    self.addChild(head_bg);
        //});
        //var name_str="吾问无为谓我";
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var record_name = new cc.LabelTTF(name_str, "Arial", 38);
        record_name.x = 130;
        record_name.y =size.height-50;
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);
        var self=this;
        var sp_height= net_y + 4 * (Min_width + net_xy)+66;
        for(var a=0;a<5;a++){
            var s2= new cc.Sprite(res.Nets2);
            s2.setPosition(450+a*60,sp_height+s2.getBoundingBox().height/2+20);
            this.addChild(s2,2);
        }
        for(var a=0;a<5;a++){
            var s1= new cc.Sprite(res.Nets1);
            //s1.setPosition(130+a*130,sp.y-sp.getBoundingBox().height-(s1.getBoundingBox().height/2));
            s1.setPosition(450+a*60,sp_height+s1.getBoundingBox().height/2+20);
            this.addChild(s1,3);
            net_Life_Array.push(s1);
        }
        var getScore="得 分:";
        var getScore = new cc.LabelTTF(getScore, "Arial", 45);
        getScore.x = 180;
        getScore.y = size.height-150;
        getScore.setColor(cc.color(88,195,224));
        this.addChild(getScore, 5);
        Score_num = new cc.LabelTTF("0", "Arial", 50);
        Score_num.setAnchorPoint(0,0.5);
        Score_num.x = 250;
        Score_num.y = getScore.y;
        Score_num.setColor(cc.color(88,195,224));
        this.addChild(Score_num, 5);
        var clock=new cc.Sprite(res.Netbg1);
        clock.setPosition(170,size.height-250);
        clock.setAnchorPoint(1,0.5);
        this.addChild(clock);
        var Record_x = new cc.LabelTTF("0", "Arial", 50);
        Record_x.setAnchorPoint(0,0.5);
        Record_x.x = 190;
        Record_x.y = size.height-250;
        Record_x.setColor(cc.color(252,152,76));
        this.addChild(Record_x);
        Record_h = new cc.LabelTTF("1", "Arial", 50);
        Record_h.setAnchorPoint(0,0.5);
        Record_h.x = 215;
        Record_h.y = size.height-250;
        Record_h.setColor(cc.color(252,152,76));
        this.addChild(Record_h);
        Record_str = new cc.LabelTTF(":", "Arial", 50);
        Record_str.setAnchorPoint(0,0.5);
        Record_str.x = 245;
        Record_str.y = size.height-250;
        Record_str.setColor(cc.color(252,152,76));
        this.addChild(Record_str);
        Record_f = new cc.LabelTTF("0", "Arial", 50);
        Record_f.setAnchorPoint(0,0.5);
        Record_f.x = 265;
        Record_f.y = size.height-250;
        Record_f.setColor(cc.color(252,152,76));
        this.addChild(Record_f);
        Record_f1 = new cc.LabelTTF("0", "Arial", 50);
        Record_f1.setAnchorPoint(0,0.5);
        Record_f1.x = 295;
        Record_f1.y = size.height-250;
        Record_f1.setColor(cc.color(252,152,76));
        this.addChild(Record_f1);
    },
    AddSprite: function () {
        for(var a=0;a<RecordData.length;a++){
            var sp_x = net_x + RecordData[a].col * (Min_width + net_xy);
            var sp_y = net_y + RecordData[a].row * (Min_width + net_xy);
            if(Record_end){
                Record_Array.push({
                    row:RecordData[a].row,
                    col:RecordData[a].col,
                    tag:RecordData[a].tag
                })
            }
            CreateSprite(RecordData[a].tag, sp_x, sp_y, RecordData[a].row ,RecordData[a].col,net_GetSprite_Array,net_All_Array,net_All_Block_Postion,Net_Main_layer,2);
        }

    },
    CreatemodelSprite:function (m,x,y) {
        var num=m%9;
        if(num==0){
            num=9
        }
        var sp;
        var num=1;
        sp=new cc.Sprite(g_resources[num-1+9]);
        sp.setPosition(x,y);
        sp.row=row;
        sp.col=col;
        sp.bool=true;//判断是否是点击点
        sp.move=false;//判断是否可以开始运动
        sp.fall=false;//判断对象是否处于下落状态
        sp.run=false;//判断是否处于运动状态
        sp.role_sp_row=null;//对对象进行计数
        sp.role_sp_col=null;//对对象进行计数
        sp.sp_length=0;//判断最远的对象距目标对象的长度
        sp.run_Array=[];//用于存储各个对象的运动路径
        sp.open=[];
        sp.close=[];
        sp.af=null
        this.addChild(sp);

    },
    CreateGuider: function () {
        var winSize=cc.winSize;
        var sp=new cc.Sprite("res/guider.png");
        sp.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(sp,20);
        var gudier_str="剩余时间: 10s";
        var gudier = new cc.LabelTTF(gudier_str, "Arial", 45);
        gudier.x = winSize.width/2;
        gudier.y = 43;
        sp.addChild(gudier);
        var fd = new cc.scaleTo(0.15,1);
        var sx = new cc.scaleTo(0.25,0);
        var dely=cc.delayTime(0.8);
        var destory = new cc.RemoveSelf(true);
        sp.runAction(cc.sequence(fd,dely,sx,destory))
    }
});
record_All_Array=[];
record_life_num=5;
record_move_num=0;
record_move_num1=0;
record_remove_num=0;
record_Click=true;
record_click_Check=false;//判断某个数值方块是否是点击状态
record_Check=false;//是否能够进行检测
record_fall=false;//是否处于下落状态
record_move=false;//是否处于移动状态
speed=0.08;
record_Score=0;
record_left_Array=[];
Record_All_Array=[];
Record_All_Block_Postion=[];
Record_GetSprite_Array=[];
record_x=null;
record_y=null;
record_xy=null;
record_Score_num=null;
record_index=0;
record_index_index=0;
record_clickindex=0;
record_start=null;

var NetLayer=cc.Layer.extend({
    bg:null,
    ctor: function () {
        this._super();
        Net_layer=this;
        speed=0.08;
        Max_row=5;
        Record_All_Block_Postion=[];
        record_x=null;
        record_y=null;
        record_xy=null;
        record_Score_num=null;
        record_index=0;
        record_index_index=0;
        record_clickindex=0;
        record_left_Array=[];
        Record_All_Array=[];
        Record_GetSprite_Array=[];
        record_life_num=5;
        record_move_num=0;
        record_move_num1=0;
        record_remove_num=0;
        record_Click=true;
        record_click_Check=false;//判断某个数值方块是否是点击状态
        record_fall=false;//是否处于下落状态
        record_Check=false;//是否能够进行检测
        record_move=false;//是否处于移动状态
        record_Score=0;
        record_time=0;
        var size=cc.director.getVisibleSize();
        record_x=size.width-215;
        record_y=size.height-290;
        record_xy=2;
        record_width=40;
        this.CreateBg();
        for(var a=0;a<RecordData.length;a++){
            var sp_x =record_x  + RecordData[a].col * (40 + 2);
            var sp_y =record_y+ RecordData[a].row * (40 + 2);
           CreateSprite(RecordData[a].tag, sp_x, sp_y, RecordData[a].row ,RecordData[a].col,Record_GetSprite_Array,Record_All_Array,Record_All_Block_Postion,Net_layer,3);
        }
        record_start=true;
        this.scheduleUpdate();
    },
    CreateBg: function () {
        var size=cc.director.getVisibleSize();
        bg=new cc.Sprite(res.Netbg);
        bg.setPosition(size.width-10,size.height);
        bg.setAnchorPoint(1,1);
        this.addChild(bg);
        var bg1=new cc.Sprite(res.Netbg1);
        bg1.setPosition(bg.x+bg.getBoundingBox().width/2-65,bg.getBoundingBox().height-8);
        bg1.setAnchorPoint(1,1);
        this.addChild(bg1);
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl=window.get.userProfile.headUrl;
        var record_headUrl
        if(window.get.userProfile&&window.get.userProfile.headUrl){
            record_headUrl=window.get.userProfile.headUrl
        }else{
            record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        var posy=bg.getBoundingBox().height-30;
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,45,posy,40,bg);
        });
        //var self=this;
        //cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
        //    var head_bg=new cc.Sprite(record_headUrl);
        //    var scalx=40/head_bg.getBoundingBox().width;
        //    var scaly=40/head_bg.getBoundingBox().height
        //    head_bg.setPosition(45,bg.getBoundingBox().height-30)
        //    head_bg.setScale(scalx,scaly)
        //    bg.addChild(head_bg);
        //});
        //var name_str="李乾@1758"
        var name_str1=window.get.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var record_name = new cc.LabelTTF(name_str, "Arial", 20);
        record_name.x = 80;
        record_name.y =bg.getBoundingBox().height-30;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,112,143));
        bg.addChild(record_name, 5);

        var getScorestr="得分:";
        var getScore = new cc.LabelTTF(getScorestr, "Arial", 24);
        getScore.x = 50;
        getScore.y =bg.getBoundingBox().height-80;
        getScore.setColor(cc.color(88,195,224));
        bg.addChild(getScore, 5);

        record_Score_num=new cc.LabelTTF("0", "Arial", 24);
        record_Score_num.x = 80;
        record_Score_num.y = bg.getBoundingBox().height-80;
        record_Score_num.setAnchorPoint(0,0.5);
        record_Score_num.setColor(cc.color(88,195,224));
        bg.addChild(record_Score_num, 5);
        record_Score_num.setString(0);
    },
    update: function (dt) {
        record_time+=dt;
        if(RecordClickData[record_clickindex]&&RecordClickData[record_clickindex].time&&record_time>=RecordClickData[record_clickindex].time&&record_clickindex<=RecordClickData.length-1){
                if(record_Score>=window.get.interActionPlayGameLog.typeScore){
                    record_Score=window.get.interActionPlayGameLog.typeScore;
                    record_Score_num.setString(record_Score);
                }
                record_Click=false;
                record_remove_num=0;
                record_click_Check=true;
                var tag=RecordClickData[record_clickindex].tag;
                var sp_col=RecordClickData[record_clickindex].col;
                var sp_row=RecordClickData[record_clickindex].row;
                var sp_x =record_x+sp_col * (40 + 2);
                var sp_y =record_y+sp_row* (40 + 2);
                Record_All_Array[sp_row*Max_row+sp_col].removeFromParent();
                Record_All_Array[sp_row*Max_row+sp_col]=null;

                record_clickindex++;

                CreateSprite(tag,sp_x,sp_y,sp_row,sp_col,Record_GetSprite_Array,Record_All_Array,Record_All_Block_Postion,Net_layer,3);

        }
        for(var a=0;a<Record_All_Array.length;a++){
            if(Record_All_Array[a]&&Record_All_Array[a].move){
                var posx=Record_All_Block_Postion[Record_All_Array[a].run_Array[Record_All_Array[a].sp_length].row*Max_row+Record_All_Array[a].run_Array[Record_All_Array[a].sp_length].col].x
                var posy=Record_All_Block_Postion[Record_All_Array[a].run_Array[Record_All_Array[a].sp_length].row*Max_row+Record_All_Array[a].run_Array[Record_All_Array[a].sp_length].col].y
                SpMove(Record_All_Array[a],posx,posy,3);
            }
        }
        if(record_move||record_fall){
            record_Click=false
        }else if(!record_move&&!record_fall){
            record_Click=true
        }
    }
});
var loadLayer=cc.Layer.extend({
    frist_time:null,
    end_time:null,
    Game_revelType:null,
    ctor: function (gametype) {
        this._super();
        time=0;
        frist_time=15;
        var alldata=[];
        Game_revelType=gametype;
        if(window.get&&window.get.interActionPlayGameLog&&window.get.interActionPlayGameLog.gameLog){
            alldata=JSON.parse(window.get.interActionPlayGameLog.gameLog);
            var PK_str;
            var FC_str;

            switch (gametype){
                case 0:
                     PK_str="我在游戏中"+net_Score+"分，这游戏太刺激，只有1%的人能玩到50000分？";
                    break;
                case 1:
                    FC_str="我在游戏中"+net_Score+"分，这游戏太刺激，只有1%的人能玩到50000分？";
                    break;
                case 2:
                    PK_str="我在游戏中"+net_Score+"分，这游戏太刺激，只有1%的人能玩到50000分？";
                    break;
                case 3:
                    PK_str="我在游戏中"+net_Score+"分，这游戏太刺激，只有1%的人能玩到50000分？";
                    break;
            }
            var share_str;
            var link;
            if(PK_str){
                share_str=PK_str;
                 link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=pk_dwjy_share";
            }else{
                share_str=FC_str;
                link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=fc_dwjy_share";
            }

            share_object={
                title:share_str,
                link:link
            }
            HLMY_PK.setShare(share_object);
            RecordData=[];
            RecordfallData=[];
            RecordClickData=[];
            RecordData=alldata[0];
            RecordfallData=alldata[1];
            RecordClickData=alldata[2];
            RecordremovespData=alldata[3];
            RecordScoreData=alldata[4];
            RecordClickData[0].time+=0.0000000000001;
            Net_Main_layer.AddSprite();
            cc.log("alldata",alldata);
            //end_time=12;
            if(window.get.interActionPlayGameLog.waitTime){
                end_time=frist_time-window.get.interActionPlayGameLog.waitTime;//需要从服务端获取的等待时间
            }else{
                end_time=6;//需要从服务端获取的等待时间
            }
            if(gametype!=0){
                Is_wait=false;
                return
            }
        }else{
            end_time=0;
            if(gametype!=0){
                var str;
                str="数据加载失败！";
                var layer=new  blackLayer(str,"确定", function () {
                    cc.director.runScene(new GameStartScene())
                },1);
                Net_Main_layer.addChild(layer,20)
            }
        }
        var winSize=cc.winSize;
        var bg=new cc.Sprite("res/load/load_bg.png");
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);

        var str="匹配中";
        var load_str=new cc.LabelTTF(str, "Arial", 50);
        load_str.x = winSize.width / 2;
        load_str.y = winSize.height/2+80;
        //load_str.setColor(cc.hexToColor("#24aaf2"));
        load_str.setColor(cc.color(255,255,255));
        this.addChild(load_str);
        var str1="预计:";
        var load_str1=new cc.LabelTTF(str1, "Arial", 30);
        load_str1.x = winSize.width / 2-10;
        load_str1.y = winSize.height/2;
        //load_str.setColor(cc.hexToColor("#24aaf2"));
        load_str1.setColor(cc.color(255,255,255));
        this.addChild(load_str1);
        load_time=new cc.LabelTTF(frist_time, "Arial", 30);
        load_time.x = winSize.width / 2+40;
        load_time.y = winSize.height/2;
        //load_str.setColor(cc.hexToColor("#24aaf2"));
        load_str.setColor(cc.color(255,255,255));
        this.addChild(load_time);
        CreateBtn("res/load/load_btn.png", function () {
            cc.director.runScene(new GameStartScene())
        },winSize.width/2,winSize.height/2-60,0.5,0.5,this);
        var bg1=new cc.Sprite("res/load/load_bg1.png");
        bg1.setPosition(winSize.width/2,winSize.height/2-60);
        this.addChild(bg1);
        bg1.runAction(cc.repeatForever(cc.rotateBy(0.1,20)));
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;
            }
        });
        cc.eventManager.addListener(listener, this);
        this.scheduleUpdate();
        return true;
    },
    update: function (dt) {
        time+=dt;
        if(time>1){
            time=0;
            if(frist_time==end_time){
                if(end_time>0){
                    Is_wait=false;
                    this.removeFromParent();
                }else{
                    this.removeFromParent();
                   var layer=new  blackLayer("暂无可匹配玩家,请重新匹配","确定", function () {
                       cc.director.runScene(new GameStartScene())
                   },1);
                    Net_Main_layer.addChild(layer,20)
                }
            }
            frist_time--;
            load_time.setString(frist_time);
        }
    }
})
var goLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        var winSize=cc.winSize;
        var bg1=new cc.Sprite("res/load/load_bg.png");
        bg1.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg1);
        var bg=new cc.Sprite("res/load/GO.png");
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;
            }
        });
        cc.eventManager.addListener(listener, this);

        return true;
    }
});
var readLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        var winSize=cc.winSize;
        var bg1=new cc.Sprite("res/load/load_bg.png");
        bg1.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg1);
        var bg=new cc.Sprite("res/load/READY.png");
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;
            }
        });
        cc.eventManager.addListener(listener, this);

        return true;
    }
});
var CreateLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        var winSize=cc.winSize;
        for(var row=0;row<5;row++){
            for(col=0;col<5;col++){
                var sp_x = net_x + row * (Min_width + net_xy);
                var sp_y = net_y + col* (Min_width + net_xy);
                this.CreatemodelSprite(1, sp_x, sp_y)
            }
        }

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;
            }
        });
        cc.eventManager.addListener(listener, this);

        return true;
    },
    CreatemodelSprite:function (m,x,y) {
        var num=m%9;
        if(num==0){
            num=9
        }
        var sp;
        var num=1;
        sp=new cc.Sprite("res/hui.png");
        sp.setPosition(x,y);
        sp.setColor(cc.hexToColor("#d6d6d6"))
        this.addChild(sp);
    }

});
var NetWorkScene=cc.Scene.extend({
    ctor: function (num) {
        this._super();
        var layer=new NetWorkLayer(num);
        cc.log("game_type:",num);
        this.addChild(layer);
    }
})