var CreateBtn=function(url,fun,x,y,Anchorx,Anchory,father,scale,tag){
    var btn = new cc.MenuItemImage(
        url,
        url,
        function () {
            fun()

        }, this
    )
    if(scale){
        btn.setScaleX(scale)
    }

    btn.setPosition(x,y);
    btn.setAnchorPoint(Anchorx,Anchory);
    var menu = new cc.Menu(btn);
    if(tag){
        menu.tag=tag
    }
    menu.x = 0;
    menu.y = 0;
    father.addChild(menu,5);
    //7580
};
var GuiderData= [
    [
        {tag:6,row:0,col:0},
        {tag:4,row:0,col:1},
        {tag:6,row:0,col:2},
        {tag:2,row:1,col:0},
        {tag:1,row:1,col:1},
        {tag:2,row:1,col:2}
    ],
    [
        {tag:6,row:0,col:0},
        {tag:4,row:0,col:1},
        {tag:6,row:0,col:2},
        {tag:4,row:1,col:0},
        {tag:3,row:1,col:1},
        {tag:5,row:1,col:2}
    ],
    [
        {tag:6,row:0,col:0},
        {tag:5,row:0,col:1},
        {tag:6,row:0,col:2},
        {tag:6,row:1,col:0},
        {tag:2,row:1,col:1},
        {tag:5,row:1,col:2}
    ],
];
var LevelData=[
    {row:1,col:1},
    {row:1,col:1},
];
var level=1;
var step=1;
var RecordData=[];
var RecordfallData=[];
var RecordClickData=[];
var RecordremovespData=[];
var RecordScoreData=[];
var Is_canGetAngin=true;
var CreateSprite=function (m,x,y,row ,col,Array,Array1,Array3,father,sex,num) {
    var num=m%9;
    if(num==0){
        num=9;
    }
    var sp;
    if(sex!=3){
        sp=new cc.Sprite(g_resources[num-1]);
    }else{
        sp=new cc.Sprite(g_resources[num-1+9]);
    }
    sp.setPosition(x,y);
    sp.row=row;
    sp.col=col;
    sp.tag=m;
    sp.bool=true;//判断是否是点击点
    sp.move=false;//判断是否可以开始运动
    sp.fall=false;//判断对象是否处于下落状态
    sp.run=false;//判断是否处于运动状态
    sp.role_sp_row=null;//对对象进行计数
    sp.role_sp_col=null;//对对象进行计数
    sp.sp_length=0;//判断最远的对象距目标对象的长度
    sp.run_Array=[];//用于存储各个对象的运动路径
    sp.open=[];
    sp.close=[];
    sp.af=null;
    father.addChild(sp);
    var sp_num= new cc.LabelAtlas(m, "res/num.png", 55, 71,"0");
    sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
    sp_num.setAnchorPoint(0.5,0.5);
    if(sex==3){
        sp_num.setScale(0.5)
    }
    sp.addChild(sp_num);
    Array1[sp.row*Max_row+sp.col]=sp;
    Array3[sp.row*Max_row+sp.col]={
        x:x,
        y:y,
        row:sp.row,
        col:sp.col
    };

    if(sex==1){
        if(Is_click_Check){
            sp.bool=false;
            Is_Check=false;
           GetSprite(Array1[sp.row*Max_row+sp.col],Array,Array1,sex);
        }
    }else if(sex==2){
        if(net_click_Check){
            sp.bool=false;
            net_Check=false;
            GetSprite(Array1[sp.row*Max_row+sp.col],Array,Array1,sex,1);
        }
    }else if(sex==3){
        if(record_click_Check){
            sp.bool=false;
            record_Check=false;
            GetSprite(Array1[sp.row*Max_row+sp.col],Array,Array1,sex);
        }
    }
};
var GetSprite=function (sp,Array,Array1,sex,num) {
    var record_sp=[];
    for(var a=0;a<Array.length;a++){
        Array.splice(a,1);
        a--;
    }
    if(Array.indexOf(sp)==-1){
        Array.push(sp);
    }
    if(sex==3){
        for(var a=0;a<Array1.length;a++){
            for(var b=0;b<RecordremovespData[record_clickindex-1].length;b++){
                if(Array1[a]&&Array1[a].row==RecordremovespData[record_clickindex-1][b].row&&Array1[a].col==RecordremovespData[record_clickindex-1][b].col){
                    if(Array.indexOf(Array1[a])==-1){
                        Array.push(Array1[a]);
                    }
                }
            }
        }
    }else {
        for(var a=0;a<Array.length;a++){
            CheckSprite(Array[a],Array,Array1);
        }
    }
    if(Array.length>=3){
        if(sex==1){
            Is_click_Check=false;
            sp_remove_num++;
            for(var a=1;a<sp_remove_num;a++){
                life_num++;
                if(life_num>5){
                    life_num=5
                }else{
                    Life_Array[life_num-1].setVisible(true);
                }
            }
        }else if(sex==2){
            for(var a=0;a<Array.length;a++){
                if(Record_end){
                    //if(GetSprite_Array[a].row!=sp.row&&GetSprite_Array[a].col!=sp.col){
                    record_sp.push({
                        row:Array[a].row,
                        col:Array[a].col,
                        tag:Array[a].tag
                    });
                    //}
                }
            }
            if(Record_end){
                Record_remove.push(record_sp);
            }
            if(Record_end&&num==2){
                Recordclick.push({
                    time:startRecord_time,
                    tag:sp.tag,
                    row:sp.row,
                    col:sp.col
                })
            }
            net_click_Check=false;
            net_remove_num++;
            for(var a=1;a<net_remove_num;a++){
                net_life_num++;
                if(net_life_num>5){
                    net_life_num=5
                }else{
                    net_Life_Array[net_life_num-1].setVisible(true);
                }
            }
           net_index_index=0
        }else if(sex==3){
            //record_click_Check=false
            //record_remove_num++
            //for(var a=1;a<record_remove_num;a++){
            //    record_life_num++
            //    if(record_life_num>5){
            //        record_life_num=5
            //    }else{
            //        Record_Life_Array[record_life_num-1].setVisible(true);
            //    }
            //}
            record_index_index=0
        }
        RemoveSprite(Array,Array1,sex);
    }else if(Array.length<3) {
        sp.bool=true;
        if(sex==1){
            if(Is_click_Check) {
                if(life_num<=1){
                    //gameOver 游戏结束
                    life_num--;
                    Life_Array[life_num].setVisible(false);
                    //判断是否复活
                    var layer=new AnginLayer(sex);
                    Main_layer.addChild(layer,10);
                }else{
                    Is_click_Check=false;
                    life_num--;
                    Life_Array[life_num].setVisible(false);
                }
            }
        }else if(sex==2){
            if(num==1){
                for(var a=0;a<Array.length;a++){
                    if(Record_end){
                        //if(GetSprite_Array[a].row!=sp.row&&GetSprite_Array[a].col!=sp.col){
                        record_sp.push({
                            row:Array[a].row,
                            col:Array[a].col,
                            tag:Array[a].tag
                        })
                        //}
                    }
                }
                if(Record_end){
                    Record_remove.push(record_sp);

                }
            }
            if(net_click_Check) {
                if(net_life_num<=1){
                    //gameOver 游戏结束
                    net_life_num--;
                    net_Life_Array[net_life_num].setVisible(false);
                    //判断是否复活
                    var angin_num=cc.sys.localStorage.getItem("dwjyAngin");
                    if(angin_num<=3){
                        var layer=new AnginLayer(sex);
                        Net_Main_layer.addChild(layer,10);
                    }else{
                        net_overOrplay=true
                    }
                }else{
                    net_click_Check=false;
                    net_life_num--;
                    net_Life_Array[net_life_num].setVisible(false);
                }
            }
        }else if(sex==3){
            //if(record_click_Check) {
            //    if(record_life_num<=1){
            //        //gameOver 游戏结束
            //        record_life_num--;
            //        record_left_Array[record_life_num].setVisible(false);
            //        //判断是否复活
            //        var layer=new AnginLayer();
            //        Main_layer.addChild(layer,10);
            //    }else{
            //        record_click_Check=false
            //        record_life_num--;
            //        record_left_Array[record_life_num].setVisible(false);
            //    }
            //}
        }
    }
};
var CheckSprite=function (sp,get_Array,All_Array) {
    if(sp.col+1<5) {
        if(All_Array[sp.row*Max_row+sp.col+1]){
            if(All_Array[sp.row*Max_row+sp.col+1].tag==sp.tag){
                if(get_Array.indexOf(All_Array[sp.row*Max_row+sp.col+1])==-1){
                    get_Array.push(All_Array[sp.row*Max_row+sp.col+1])
                }
            }
        }
    }
    if(sp.col-1>=0){
        if(All_Array[sp.row*Max_row+sp.col-1]){
            if(All_Array[sp.row*Max_row+sp.col-1].tag==sp.tag){
                if(get_Array.indexOf(All_Array[sp.row*Max_row+sp.col-1])==-1){
                    get_Array.push(All_Array[sp.row*Max_row+sp.col-1])
                }
            }
        }
    }
    if(sp.row+1<5){
        if(All_Array[(sp.row+1)*Max_row+sp.col]){
            if(All_Array[(sp.row+1)*Max_row+sp.col].tag==sp.tag){
                if(get_Array.indexOf(All_Array[(sp.row+1)*Max_row+sp.col])==-1){
                    get_Array.push(All_Array[(sp.row+1)*Max_row+sp.col])
                }
            }
        }
    }
    if(sp.row-1>-1){
        if(All_Array[(sp.row-1)*Max_row+sp.col]){
            if(All_Array[(sp.row-1)*Max_row+sp.col].tag==sp.tag){
                if(get_Array.indexOf(All_Array[(sp.row-1)*Max_row+sp.col])==-1){
                    get_Array.push(All_Array[(sp.row-1)*Max_row+sp.col])
                }
            }
        }
    }
};//找寻符合条件的要删除方块
var RemoveSprite=function (Array,All_Array1,sex) {
    if(sex==1){
        sp_move_num=Array.length;
        sp_move_num1=0;
        Is_Check=false;
        Is_move=true;
        Score+=Array.length*Array[0].tag*10;
        Score_num.setString(Score);
    }else if(sex==2){
        net_move_num=Array.length;
        net_move_num1=0;
        net_Check=false;
        net_move=true;
        net_Score+=Array.length*Array[0].tag*10;
        Record_Score.push({
            Score:net_Score,
            time:startRecord_time
        });
        Score_num.setString(net_Score);
    }else if(sex==3){
        record_move_num=Array.length;
        record_move_num1=0;
        record_Check=false;
        record_move=true;
        record_Score+=Array.length*Array[0].tag*10;
        //record_Score=RecordScoreData[Score_idnex].Score;
        //Score_idnex++;
        //cc.log("2880",window.get.interActionPlayGameLog.typeScore)
        if( record_Score>window.get.interActionPlayGameLog.typeScore){
            record_Score=window.get.interActionPlayGameLog.typeScore;
            record_Score_num.setString(record_Score);
        }
        record_Score_num.setString(record_Score);
    }
    var sp_x;
    var sp_y;
    var sp_col;
    var sp_row;
    var record_sp=[];
    for(var a=0;a<Array.length;a++){
        Array[a].run=true;
        All_Array1[Array[a].row*Max_row+Array[a].col].run=true;
        if(!Array[a].bool){
            tag=Array[a].tag+1;
            sp_x=Array[a].x;
            sp_y=Array[a].y;
            sp_col=Array[a].col;
            sp_row=Array[a].row;
            Array[a].setLocalZOrder(1);
        }
    }
    for(var a=0;a<Array.length;a++){
        Array[a].run_Array=FindWays(Array[a],All_Array1[sp_row*Max_row+sp_col],All_Array1);
        Array[a].move=true;
        Array[a].sp_length=Array[a].run_Array.theIndex-1;
    }
};//提供各个目标方块的路径
var SpMove=function (object,posx,posy,sex) {
    object.move=false;
    var speed1;
    if(object.run_Array.theIndex==1){
        speed1=1.5
    }else if(object.run_Array.theIndex==2){
        speed1=0.8
    }else if(object.run_Array.theIndex==3){
        speed1=0.5
    }else if(object.run_Array.theIndex==4){
        speed1=0.3
    }else if(object.run_Array.theIndex==5){
        speed1=0.2
    }else if(object.run_Array.theIndex==6){
        speed1=0.2
    }else{
        speed1=0.2
    }
    var move=new cc.moveTo(speed*speed1*object.run_Array.theIndex,posx,posy)
    var fun1=cc.callFunc(function (){
        object.sp_length--;
        if(object.sp_length<0){
            if(sex==1){
                sp_move_num1++;
                if(sp_move_num==sp_move_num1){
                    Is_move=false;
                    object.move=false;
                    for(var j=0;j<All_Block.length;j++){
                        if(All_Block[j]&&All_Block[j].run){
                            All_Block[All_Block[j].row*Max_row+All_Block[j].col].removeFromParent();
                            All_Block[All_Block[j].row*Max_row+All_Block[j].col]=null;
                        }
                    }
                    var tag=object.tag+1;
                    var sp_row=object.run_Array[0].row;
                    var sp_col=object.run_Array[0].col
                    CreateSprite(tag,posx,posy,sp_row,sp_col,GetSprite_Array,All_Block,All_Block_Postion,Main_layer,1);
                    SpriteFall();
                }
            }else if(sex==2){
                net_move_num1++;
                if(net_move_num==net_move_num1){
                    net_move=false;
                    object.move=false;
                    for(var j=0;j<net_All_Array.length;j++){
                        if(net_All_Array[j]&&net_All_Array[j].run){
                            net_All_Array[net_All_Array[j].row*Max_row+net_All_Array[j].col].removeFromParent();
                            net_All_Array[net_All_Array[j].row*Max_row+net_All_Array[j].col]=null;
                        }
                    }
                    var tag=object.tag+1;
                    var sp_row=object.run_Array[0].row;
                    var sp_col=object.run_Array[0].col;
                    CreateSprite(tag,posx,posy,sp_row,sp_col,net_GetSprite_Array,net_All_Array,net_All_Block_Postion,Net_Main_layer,2);
                    SpriteFall(sex);
                }

            }else if(sex==3){
                record_move_num1++;
                if(record_move_num==record_move_num1){
                    record_move=false;
                    object.move=false;
                    for(var j=0;j<Record_All_Array.length;j++){
                        if(Record_All_Array[j]&&Record_All_Array[j].run){
                            Record_All_Array[Record_All_Array[j].row*Max_row+Record_All_Array[j].col].removeFromParent();
                            Record_All_Array[Record_All_Array[j].row*Max_row+Record_All_Array[j].col]=null;
                        }
                    }
                    var tag=object.tag+1;
                    var sp_row=object.run_Array[0].row;
                    var sp_col=object.run_Array[0].col;
                    CreateSprite(tag,posx,posy,sp_row,sp_col,Record_GetSprite_Array,Record_All_Array,Record_All_Block_Postion,Net_layer,3);
                    SpriteFall(sex);
                }
            }
        }
        else{
            object.move=true;
        }
    })
    object.runAction(new cc.sequence(move,fun1));
};//遍历移动路径使其运动
var SpriteFall=function (sex) {
    var size=cc.winSize;
    for(var col=0;col<5;col++){
        var sprow = 0;
        for(var row=0 ;row<5;row++){
            if(sex==1){
                var roleSprite = All_Block[row * Max_row + col];
                if(roleSprite==null){
                    sprow ++;
                }else{
                    if(sprow>0){
                        var newRow = row - sprow;
                        All_Block[newRow * Max_row + col] = roleSprite;
                        All_Block[row * Max_row + col] = null;
                        var startPosition = roleSprite.getPosition();
                        var endPosition =PointGetEnd(newRow,col,sex);
                        var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                        var speed=(startPosition.y - endPosition.y)/speed1
                        roleSprite.stopAllActions();
                        Is_fall=true;
                        roleSprite.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y)));
                        roleSprite.row = newRow;
                    }
                }
            }else if(sex==2){
                var roleSprite = net_All_Array[row * Max_row + col];
                if(roleSprite==null){
                    sprow ++;
                }else{
                    if(sprow>0){
                        var newRow = row - sprow;
                        net_All_Array[newRow * Max_row + col] = roleSprite;
                        net_All_Array[row * Max_row + col] = null;
                        var startPosition = roleSprite.getPosition();
                        var endPosition =PointGetEnd(newRow,col,sex);
                        var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                        var speed=(startPosition.y - endPosition.y)/speed1
                        roleSprite.stopAllActions();
                        net_fall=true;
                        roleSprite.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y)));
                        roleSprite.row = newRow;
                    }
                }
            }else if(sex==3){
                var roleSprite = Record_All_Array[row * Max_row + col];
                if(roleSprite==null){
                    sprow ++;
                }else{
                    if(sprow>0){
                        var newRow = row - sprow;
                        Record_All_Array[newRow * Max_row + col] = roleSprite;
                        Record_All_Array[row * Max_row + col] = null;
                        var startPosition = roleSprite.getPosition();
                        var endPosition =PointGetEnd(newRow,col,sex);
                        var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                        var speed=(startPosition.y - endPosition.y)/speed1;
                        roleSprite.stopAllActions();
                        record_fall=true;
                        roleSprite.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y)));
                        roleSprite.row = newRow;
                    }
                }
            }
        }
    }
    var s_num=0;
    var s_num1=0;
    if(sex==1){
        for(var a=0 in All_Block){
            if(All_Block[a]==null){
                s_num++;
            }
        }
    }else if(sex==2){
        for(var a=0 in net_All_Array){
            if(net_All_Array[a]==null){
                s_num++
            }
        }
    }else if(sex==3){
        for(var a=0 in Record_All_Array){
            if(Record_All_Array[a]==null){
                s_num++
            }
        }
    }
    var Record_down=[];
    for(var col=0;col<5;col++){
        for(var row =0;row <5;row++){
            if(sex==1){
                if(All_Block[row*Max_row+col]==null){
                    var num=Math.floor(Math.random()*5+1);
                    var sp=new cc.Sprite(g_resources[num-1]);
                    sp.setPosition(All_Block_Postion[(row)*Max_row+col].x,All_Block_Postion[20].y+132);
                    sp.row=row;
                    sp.col=col;
                    sp.tag=num;
                    sp.bool=true;//判断是否是点击点
                    sp.move=false;//判断是否可以开始运动
                    sp.fall=false;//判断对象是否处于下落状态
                    sp.run=false;//判断是否处于运动状态
                    sp.role_sp_row=null;//对对象进行计数
                    sp.role_sp_col=null;//对对象进行计数
                    sp.sp_length=0;//判断最远的对象距目标对象的长度
                    sp.run_Array=[];//用于存储各个对象的运动路径
                    sp.open=[];
                    sp.close=[];
                    sp.af=null;
                    Main_layer.addChild(sp);
                    var sp_num= new cc.LabelAtlas(num, "res/num.png", 55, 71,"0");
                    sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
                    sp_num.setAnchorPoint(0.5,0.5);
                    sp.addChild(sp_num);
                    All_Block[sp.row*Max_row+sp.col]=sp;

                    var startPosition = sp.getPosition();
                    var endPosition = PointGetEnd(row,col,sex);
                    var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                    var speed=(startPosition.y - endPosition.y)/speed1;
                    var fun=cc.callFunc(function () {
                        s_num1++;
                        if(s_num==s_num1){
                            Is_fall=false;
                            Is_click_Check=false
                            Is_Check=true;
                        }
                    })
                    var dely=cc.delayTime(0.01);
                    sp.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y),dely,fun));
                }
            }
            else if(sex==2){
                if(net_All_Array[row*Max_row+col]==null){
                    var num=Math.floor(Math.random()*5+1)
                    var net_num=0
                    for(var j=0;j<RecordfallData.length;j++ ){
                        if(RecordfallData[j].a==record_index){
                            net_num++
                        }
                    }
                    for(var j=0;j<RecordfallData.length;j++){
                        if(RecordfallData[j].a==net_index){
                            if(row==RecordfallData[j].row&&col==RecordfallData[j].col){
                                num=RecordfallData[j].tag
                                net_index_index++
                                if(net_index_index==net_num){
                                    net_index++
                                    net_index_index=0
                                    break
                                }
                            }
                        }
                    }

                    var sp=new cc.Sprite(g_resources[num-1]);
                    sp.setPosition(net_All_Block_Postion[(row)*Max_row+col].x,net_All_Block_Postion[20].y+132);
                    sp.row=row;
                    sp.col=col;
                    sp.tag=num;
                    sp.bool=true;//判断是否是点击点
                    sp.move=false;//判断是否可以开始运动
                    sp.fall=false;//判断对象是否处于下落状态
                    sp.run=false;//判断是否处于运动状态
                    sp.role_sp_row=null;//对对象进行计数
                    sp.role_sp_col=null;//对对象进行计数
                    sp.sp_length=0;//判断最远的对象距目标对象的长度
                    sp.run_Array=[];//用于存储各个对象的运动路径
                    sp.open=[];
                    sp.close=[];
                    sp.af=null
                    Net_Main_layer.addChild(sp);
                    var sp_num= new cc.LabelAtlas(num, "res/num.png", 55, 71,"0");
                    sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
                    sp_num.setAnchorPoint(0.5,0.5);
                    sp.addChild(sp_num);
                    net_All_Array[sp.row*Max_row+sp.col]=sp;

                    Record_down.push({
                        row:row,
                        col:col,
                        tag:num,
                    })

                    var startPosition = sp.getPosition();
                    var endPosition = PointGetEnd(row,col,sex);
                    var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                    var speed=(startPosition.y - endPosition.y)/speed1;
                    var fun=cc.callFunc(function () {
                        s_num1++;
                        if(s_num==s_num1){
                            net_fall=false;
                            net_click_Check=false
                            net_Check=true;
                        }
                    })
                    var dely=cc.delayTime(0.01);
                    sp.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y),dely,fun));
                }

            }
            else if(sex==3){
                if(Record_All_Array[row*Max_row+col]==null){
                    //var num=Math.floor(Math.random()*5+1)
                    var num;
                    for(var j=0;j<RecordfallData[record_index].length;j++ ){
                        if(row==RecordfallData[record_index][j].row&&col==RecordfallData[record_index][j].col){
                            num=RecordfallData[record_index][j].tag;
                            record_index_index++;
                            if(record_index_index==RecordfallData[record_index].length){
                                record_index++
                                record_index_index=0;
                                break
                            }
                        }
                    }
                    //cc.log("wwww",num,row,col)
                    var sp=new cc.Sprite(g_resources[num-1+9]);
                    sp.setPosition(Record_All_Block_Postion[(row)*Max_row+col].x,Record_All_Block_Postion[20].y+132);
                    sp.row=row;
                    sp.col=col;
                    sp.tag=num;
                    sp.bool=true;//判断是否是点击点
                    sp.move=false;//判断是否可以开始运动
                    sp.fall=false;//判断对象是否处于下落状态
                    sp.run=false;//判断是否处于运动状态
                    sp.role_sp_row=null;//对对象进行计数
                    sp.role_sp_col=null;//对对象进行计数
                    sp.sp_length=0;//判断最远的对象距目标对象的长度
                    sp.run_Array=[];//用于存储各个对象的运动路径
                    sp.open=[];
                    sp.close=[];
                    sp.af=null
                    Net_layer.addChild(sp);
                    var sp_num= new cc.LabelAtlas(num, "res/num.png", 55, 71,"0");
                    sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
                    sp_num.setAnchorPoint(0.5,0.5);
                    sp_num.setScale(0.5)
                    sp.addChild(sp_num);
                    Record_All_Array[sp.row*Max_row+sp.col]=sp;
                    var startPosition = sp.getPosition();
                    var endPosition = PointGetEnd(row,col,sex);
                    var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                    var speed=(startPosition.y - endPosition.y)/speed1;
                    var fun=cc.callFunc(function () {
                        s_num1++;
                        if(s_num==s_num1){
                            record_fall=false;
                            record_click_Check=false;
                            record_Check=true;
                        }
                    })
                    var dely=cc.delayTime(0.01);
                    sp.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y),dely,fun));
                }
            }
        }
    }
    if(sex==2){
        Record_fall_Array[Record_fall_Array.length]=Record_down;
    }



};//物块的下落
var PointGetEnd = function(row , col,sex){
    if(sex==1){
        this.endX =  Min_x+col*(Min_width+Min_xy);
        this.endY =  Min_y+row*(Min_width+Min_xy);
        var point = cc.p(this.endX,this.endY);
        return point;
    }else if(sex==2){
        this.endX =  net_x+col*(Min_width+net_xy);
        this.endY =  net_y+row*(Min_width+net_xy);
        var point = cc.p(this.endX,this.endY);
        return point;
    }else if(sex==3){
        this.endX =  record_x+col*(record_width+record_xy);
        this.endY =  record_y+row*(record_width+record_xy);
        var point = cc.p(this.endX,this.endY);
        return point;
    }

};//获取目标点坐标
var FindWays= function (startPoint,endPoint,All_Array) {
    var openList  = [];                      //初始化开启列表
    var closeList = [];                      //初始化关闭列表
    var path_Array=[];
    startPoint.ag = 0;
    startPoint.ah = 0;
    startPoint.af = startPoint.ag + startPoint.ah;   //起点的G,H,F值为0
    openList.push(startPoint);               //起点加入开启列表
    var findTheWay = false;
    do{
        var centerNode = findMinNode(openList);  //寻找F值最低的节点
        openList.remove(centerNode);         //将此节点从开启列表中删除，为了下次遍历开启列表的时候不再出现此节点
        closeList.push(centerNode);          //并将此节点加入到关闭列表
        for(var i=0;i<4;i++)                 //遍历此节点周围的节点，并给这些节点加入坐标属性和G值
        {
            var aroundNode = {};
            if (Math.abs(centerNode.row-endPoint.row)==0 && Math.abs(centerNode.col-endPoint.col)==0)  //如果节点和终点的值相近，那么A*算法结束，得到路径
            {
                findTheWay = true;
                var pathArry=[]
                var aroundNode = {
                    row:centerNode.row,
                    col:centerNode.col
                };
                pathArry.push(aroundNode);
                pathArry.theIndex = pathArry.length;
                return pathArry;
                //this.unschedule(this.thePathSelector);
                //this.schedule(this.thePathSelector,null,pathArry.length-1);
                break;           //找到最短路径并跳出循环
            }
            switch (i){
                case 0:
                    aroundNode.row = centerNode.row+1;                //坐标属性
                    aroundNode.col = centerNode.col;
                    break;
                case 1:
                    aroundNode.row = centerNode.row-1;                //坐标属性
                    aroundNode.col = centerNode.col;
                    break;
                case 2:
                    aroundNode.row = centerNode.row;                //坐标属性
                    aroundNode.col = centerNode.col+1;
                    break;
                case 3:
                    aroundNode.row = centerNode.row;                //坐标属性
                    aroundNode.col = centerNode.col-1;
                    break;

            }

            if(All_Array[aroundNode.row*Max_row+aroundNode.col]){
                if(!All_Array[aroundNode.row*Max_row+aroundNode.col].run){
                    aroundNode.isOb = false;   //判断当前节点是否是不是要删除的节点
                }else{
                    aroundNode.isOb = true
                }
            }else{
                if(aroundNode.row<5&&aroundNode.row>=0&&aroundNode.col<5&&aroundNode.col>=0){
                    aroundNode.isOb = true
                }else{
                    aroundNode.isOb = false
                }
            }
            if (!aroundNode.isOb){                                       //如果是障碍物，跳过

            }else if(closeList.hasObject(aroundNode)){                         //如果在关闭列表里，跳过

            }else if(!openList.hasObject(aroundNode)){                          //如果不在开启列表里，加入到开启列表
                aroundNode.parentPath = centerNode;
                if (Math.abs(aroundNode.row-endPoint.row)==0 && Math.abs(aroundNode.col-endPoint.col)==0)  //如果节点和终点的值相近，那么A*算法结束，得到路径
                {
                    findTheWay = true;
                    var pathArry = [];
                    gettingAStarPath(aroundNode,pathArry);//寻找路径
                    pathArry.splice(0,0,{row:endPoint.row,col:endPoint.col});   //加终点到数组头部
                    pathArry.splice(pathArry.length-1,1);                                 //删一项数组底部的起点数据，此时的数组是最终的路径数组
                    path_Array = pathArry;
                    pathArry.theIndex = path_Array.length;
                    return pathArry;
                    //this.unschedule(this.thePathSelector);
                    //this.schedule(this.thePathSelector,null,pathArry.length-1);
                    break;           //找到最短路径并跳出循环
                }
                if (aroundNode.row!=centerNode.row && aroundNode.col!=centerNode.col)   //确定中心节点和周围节点形成的角度，正交G值消耗10*像素，斜角G值消耗14*像素
                    aroundNode.ag = centerNode.ag ;
                else
                    aroundNode.ag = centerNode.ag;
                aroundNode.af = getAF(aroundNode,endPoint);
                openList.push(aroundNode);
            }
            else if(openList.hasObject(aroundNode)){                            //如果在开启列表里
                //if (centerNode.af < aroundNode.af){                //如果新的g值小于周围节点本身的g值，那么周围节点的父节点改为当前中心节点，并重新计算其F值
                //    aroundNode.parentPath = centerNode;
                //    aroundNode.ag = centerNode.ag;
                //    aroundNode.af = this.getAF(aroundNode,endPoint);
                //}
            }
        }
    }while(!findTheWay)

};
var findMinNode=function(openListArray){//找到最小af
    var minNode = openListArray[0];
    for (var i=0;i<openListArray.length;i++)
    {
        if (minNode.af>openListArray[i].af) minNode=openListArray[i];
    }
    return minNode;
};//找最小的af的节点
var getAF=function(thisNode,endNode){
    var aHExpend = (Math.abs(thisNode.row-endNode.row) + Math.abs(thisNode.col-endNode.col));
    return aHExpend+thisNode.ag;
}; //获取af
var gettingAStarPath=function(laseNode,array){
    if(laseNode.parentPath != null) {
        array.push({row:laseNode.parentPath.row,col:laseNode.parentPath.col});
        gettingAStarPath(laseNode.parentPath,array);
    }
};//根据父节点找到每一个节点的父类从而找到各个节点的路径
//这里给Array数组添加3个实例方法
Array.prototype.aStarIndexOf = function(val) {        //通过对象寻找index值
    for (var i = 0; i < this.length; i++) {
        if (this[i].row==val.row && this[i].col==val.col) return i;
    }
    return -1;
};
Array.prototype.remove = function(val) {         //删除相应的对象
    var index = this.aStarIndexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Array.prototype.hasObject = function(val){       //判断是否是同一个对象
    for (var i = 0; i < this.length; i++){
        if (this[i].row==val.row && this[i].col==val.col)
            return true;
    }
    return false;
};
var CreateXml= function (headurl,fun,array) {
    var xhr = cc.loader.getXMLHttpRequest();
    var  url=headurl;
    xhr.open("POST", url,true);
    xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
    var self=this;
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4){
            if(xhr.status >= 200 && xhr.status <= 207){
                var resw = xhr.responseText;
                var jsondata = JSON.parse(resw);
                array=[];
                array= jsondata["data"]
                fun()
            }
        }
    }
    xhr.send();
};
var  CreateFun=function (width,hight,radius) {
    var sp_Array=[];
    var radius=radius;
    var num=100;
    var start_x=0
    var start_y=0
    //左下角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=sp_x-width/2;
        var jiaodu=Math.acos((radius-sp_x)/radius)
        var y=radius-(Math.sin(jiaodu)*radius)-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }

    //右下角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=sp_x+width-radius-width/2;
        var jiaodu=Math.asin((sp_x)/radius)
        var y=radius-(Math.cos(jiaodu)*radius)-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    //右上角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=width-sp_x-width/2;
        var jiaodu=Math.acos((radius-sp_x)/radius)
        var y=Math.sin(jiaodu)*radius+width-radius-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    //左上角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=radius-sp_x-width/2;
        var jiaodu=Math.asin((sp_x)/radius)
        var y=Math.cos(jiaodu)*radius+width-radius-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    return sp_Array;
};
var CreateClipperImage= function (url,x,y,radius,father,tag) {
    var shape =new cc.DrawNode();
    var vertices=CreateFun(radius,radius,radius/2);
    shape.drawPoly(vertices, cc.color(0, 255, 255, 80), 2, cc.color(255, 0, 0, 255));
    var clipper = new cc.ClippingNode();
    clipper.x = x;
    clipper.y = y;
    clipper.stencil = shape;  // 把刚刚创建的圆形模板放入
    if(tag){
        clipper.tag=tag;
    }
    father.addChild(clipper,10);
    var sp  = new cc.Sprite(url);
    var SceleX=radius/sp.getContentSize().width;
    var sceleY=radius/sp.getBoundingBox().height;
    sp.setScale(SceleX,sceleY);
    sp.setAnchorPoint(0.5,0.5);
    clipper.addChild(sp);   // 在这个clippingnode中只显示圆形模板的部分.
};
var CreateXhr= function (url,fun,fun1) {
    var url=url;
    var xhr = cc.loader.getXMLHttpRequest();
    xhr.open("POST", url,true);
    xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4){
            if(xhr.status >= 200 && xhr.status <= 207){
                var resw = xhr.responseText;
                var xhr_Array=[];
                xhr_Array = JSON.parse(resw);
                fun(xhr_Array);
            }else{
                fun1();
            }
        }
    };
    xhr.send();
};
var appId=114558;
var scoreId=198;
var getBytesLength= function (str,len) {
    // 在GBK编码里，除了ASCII字符，其它都占两个字符宽
    //return str.replace(/[^\x00-\xff]/g, 'xx').length;
    var str_length = 0;
    var str_len = 0;
    var  str_cut = new String();
    str_len = str.length;
    for(var i = 0;i<str_len;i++)
    {
        a = str.charAt(i);
        str_length++;
        if(escape(a).length > 4) {
            //中文字符的长度经编码之后大于4
            str_length++;
        }
        str_cut = str_cut.concat(a);
        if(str_length>=len) {
            str_cut = str_cut.concat("");
            return str_cut;
        }
    }
    //如果给定字符串小于指定长度，则返回源字符串；
    if(str_length<len){
        return  str;
    }
};
var Gamedata1= [];
var Gamedata2= [];
var Gamedata3=[];
var Gamedata4= [];
var Gamedata5=[];
var Gamedata6=[];
var index_xhr="/openapi/game/interactionGame/index.json?aid="+appId;//初始化信息接口
var first_jifen="/openapi/game/interactionGame/addUserPoint.json?aid="+appId+"&type="+1;//首次玩送玩家积分接口；
var pk_xhr="/openapi/game/interactionGame/matchGameLog.json?aid="+appId+"&rivalType=0&gameType=0";//匹配玩家接口；
var over_xhr="/openapi/game/interactionGame/index.json?aid="+appId;//经典游戏结束接口

var end_xhr="";//PK游戏结束接口
var PK_friendRnking="/openapi/game/interactionGame/getInteractionGameTop.json?aid="+appId+"&topType=0";//pk 好友榜；
var PK_zhouRnking="/openapi/game/interactionGame/getInteractionGameTop.json?aid="+appId+"&topType=1";//pk 周榜；
var PK_zongRnking="/openapi/game/interactionGame/getInteractionGameTop.json?aid="+appId+"&topType=2";//pk 总榜；
var friendRnking="/openapi/rank/queryMyGameRankingList.json?aid="+appId+" &scoreId="+scoreId;//经典 好友榜；
var zhouRnking="/openapi/rank/queryWeeklyGameRankingList.json?aid="+appId+" &scoreId="+scoreId;//经典 周榜
var zongRnking="/openapi/rank/queryTotalGameRankingList.json?aid="+appId+"&scoreId="+scoreId;//经典 总榜

//var PK_friendRnking="./a.json";//pk 好友榜；
//var PK_zhouRnking="./b.json";//pk 周榜；
//var PK_zongRnking="./c.json";//pk 总榜；
//var friendRnking="./d.json";//经典 好友榜；
//var zhouRnking="./e.json";//经典 周榜
//var zongRnking="./f.json";//经典 总榜

var shareData={
    openAppId:"107693",
    title:"这游戏太刺激，只有1%的人能玩到50000分？",
    link:"http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=dwjy_share",
    desc:"这游戏太刺激，只有1%的人能玩到50000分？",
    imgUrl:"http://images.1758.com/article/image/2016/10/20/97171476962315917.jpg"
};





















