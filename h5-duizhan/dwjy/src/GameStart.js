Is_jifen=null;
Game_BestScore=null;
Start_layer=null;
Is_pipei=true;
start_PK_name=null;
var GameStartLayer=cc.Layer.extend({
    getScore:null,
    ctor: function (game_type) {
        this._super();
        HLMY_PK.shareInit(shareData);
        Is_rank=true;
        time=0;
        Start_layer=this;
        Is_jifen=false;
        var size=cc.winSize;
        this.CreateBg(game_type);
        //game_type=1;
        if(game_type==1){
            var layer=new fuchouLayer();
            this.addChild(layer,20);
        }else{
            if(window.select.maliceRefreshRivalUserName){
                var rivel_type= JSON.parse(window.select.maliceRefreshRivalType);
                cc.log("rivel_type",rivel_type);
                var layer=new shuaifenLayer(rivel_type);
                this.addChild(layer,20);
            }
        }
    },
    CreateBg: function (game_type) {
        var winSize=cc.winSize;
        var bg = new cc.Sprite(res.start_bg);
        bg.setPosition(winSize.width / 2, winSize.height / 2);
        this.addChild(bg);
        var bg1 = new cc.Sprite(res.start_bg1);
        bg1.setPosition(winSize.width / 2, winSize.height / 2+250);
        this.addChild(bg1);
        var point_num=window.select.newUserAwardPoint;
        //var point_num=10;
        var Is_NewUser=window.select.isNewUser;
        //var Is_NewUser=false;
        if(Is_NewUser){
            window.select.isNewUser=false;
            var layer=new  fristLayer();
            this.addChild(layer,10)
        }
        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-70);
        start_score.setAnchorPoint(1,0.5);
        this.addChild(start_score);
        var record_headUrl;
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }

        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        //var name_str="李乾@1758";
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);
        gameNum1=window.select.newUserAwardPoint;
        gameNum2=window.select.userPoint;
        //gameNum1=5;
        //gameNum2=5;
        Game_BestScore=gameNum1+gameNum2;//积分总数
        getScore = new cc.LabelTTF("0", "Arial", 30);
        getScore.x = winSize.width-70;
        getScore.y =winSize.height-70;
        getScore.setAnchorPoint(0.5,0.5);
        getScore.setColor(cc.color(254,202,75));
        getScore.setString(gameNum2);
        this.addChild(getScore, 1);
        var start_bestScore;
        if(window.select.maxVaScore){
             start_bestScore=window.select.maxVaScore;
        }else{
             start_bestScore=0;
        }
        //var start_bestScore=100;
        if(start_bestScore>0){
        }else{
            start_bestScore=0;
        }
        var start_bestScore_str="最高分: "+start_bestScore;
        var start_bestScore_name = new cc.LabelTTF(start_bestScore_str, "Arial", 30);
        start_bestScore_name.x = winSize.width/2;
        start_bestScore_name.y =500;
        start_bestScore_name.setAnchorPoint(0.5,0.5);
        start_bestScore_name.setColor(cc.color(0,0,0));
        this.addChild(start_bestScore_name, 5);
        var start_PKbestScore=window.select.maxVsScore;
        //var start_PKbestScore=100;
        if(start_PKbestScore>0){
        }else{
            start_PKbestScore=0;
        }
        var start_PKbestScore_str="最高分: "+start_PKbestScore;
        var start_PKbestScore_name = new cc.LabelTTF(start_PKbestScore_str, "Arial", 30);
        start_PKbestScore_name.x = winSize.width/2;
        start_PKbestScore_name.y =300;
        start_PKbestScore_name.setAnchorPoint(0.5,0.5);
        start_PKbestScore_name.setColor(cc.color(0,0,0));
        this.addChild(start_PKbestScore_name, 5);
        var start_PKnum=window.select.userMedals;
        //var start_PKnum=100;
        if(!start_PKnum){
            start_PKnum=0;
        }
        var start_PK_str="奖牌数: "+start_PKnum;
        start_PK_name = new cc.LabelTTF(start_PK_str, "Arial", 30);
        start_PK_name.x = winSize.width/2;
        start_PK_name.y =250;
        start_PK_name.setAnchorPoint(0.5,0.5);
        start_PK_name.setColor(cc.color(0,0,0));
        this.addChild(start_PK_name, 5);
        //经典坐标：winSize.width/2,490 对战坐标;winSize.width/2,320
        CreateBtn(res.start_btn1, function () {
            //经典模式
            cc.director.runScene(new GameMainScene())
        },winSize.width/2,600,0.5,0.5,this);
        var self=this;
        CreateBtn(res.start_btn2, function () {
            //匹配模式
            var layer=new NetLoadLayer();
            Start_layer .addChild(layer,10);
            var start_time=new Date().getTime();
            var url=pk_xhr;
            var xhr_fun= function (xhr_data) {
                var end_time=new Date().getTime();
                if(end_time-start_time<=10*1000){
                    cc.log("start_time:",start_time,"end_time:",end_time,"end_time-start_time:",end_time-start_time);
                    window.get=[];
                    window.get= xhr_data["data"];
                    cc.log("window.get",window.get);
                    cc.director.runScene(new NetWorkScene(0));
                }
            };
            var error_fun= function () {
                self.removeFromParent();
                var layer=new TishiLayer("网络异常...");
                Start_layer .addChild(layer,10);
            };
            CreateXhr(url,xhr_fun,error_fun);
        },winSize.width/2,400,0.5,0.5,this);
        CreateBtn(res.follow, function () {
            //关注
            //showEWM();
            HLMY_PK.ewm();
        },100,150,0.5,0.5,this);
        CreateBtn(res.start_ranklist, function () {
            //排行榜
            //document.body.style.backgroundColor = '#9c9b9b';
            //document.documentElement.style.opacity = '0.5';
            cc.log('aaa');
                var layer=new NetLoadLayer();
                Start_layer .addChild(layer,10);
                var url=PK_zhouRnking;
                var fun= function (Array) {
                    if(Net_load_layer){
                        Net_load_layer.removeFromParent();
                    }
                    Gamedata2=[];
                    Gamedata2 = Array["data"];
                    var layer = new RankList(1);
                    layer.setScale(0.1);
                    Start_layer.addChild(layer,10);
                    var scal=new cc.ScaleTo(0.3,1);
                    layer.runAction(scal);
                };
                var error_fun= function () {
                    if(Net_load_layer){
                        Net_load_layer.removeFromParent();
                    }
                    var layer=new TishiLayer("网络异常...");
                    Start_layer .addChild(layer,10);
                };
                CreateXhr(url,fun,error_fun);
        },winSize.width/2-92,165,0.5,0.5,this);
        CreateBtn(res.moregame, function () {
            //更多游戏
            top.window.location.href ="http://wx.1758.com/play/gamebox/123?ex1758=1&tp=full&yn=小游戏&title=小游戏";
        },winSize.width/2+92,150,0.5,0.5,this);
        CreateBtn(res.invite, function () {
            //邀请
            cc.log("邀请");
            //var share = document.getElementById('share-square');
            //share.style.display = 'block';
            HLMY_PK.shareTip();
        },winSize.width-100,150,0.5,0.5,this);
        var startlogo=new cc.Sprite("res/start/start_logo.png");
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
        this.scheduleUpdate();
        cc.log("版本4")
    },
    update: function (dt) {
        if(Is_jifen){
            time+=dt;
            if(time>dt){
                time=0;
                gameNum2++;
                if(gameNum2>=Game_BestScore){
                    gameNum2=Game_BestScore
                }
                getScore.setString(gameNum2);
                if(gameNum2>=10000&&gameNum2<1000000){
                    getScore.setScale(0.8)
                }else if(gameNum2>=1000000){
                    getScore.setScale(0.6)
                }
            }
        }
    }
});
fuchou_Layer=null;
var fuchouLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        fuchou_Layer=this;
        var winSize=cc.winSize;
        var sp=new cc.Sprite("res/load/load_bg.png");
        sp.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(sp);
        //var name_str1="飞花飘絮";
        var name_str1=window.select.rivalUserProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var  fu_name1 = new cc.LabelTTF(name_str, "Arial", 30);
        fu_name1.setAnchorPoint(0,0.5);
        fu_name1.setColor(cc.color(255,246,0));
        this.addChild(fu_name1, 1);
        var fu_str="您败给了";
        var  fu_name = new cc.LabelTTF(fu_str, "Arial", 25);
        fu_name.setAnchorPoint(0,0.5);
        fu_name.setColor(cc.color(254,254,254));
        this.addChild(fu_name, 1);
        var sp_width=fu_name.getBoundingBox().width+fu_name1.getBoundingBox().width;
        fu_name.x = winSize.width/2-sp_width/2;
        fu_name.y =winSize.height/2+90;
        fu_name1.x = fu_name.x+fu_name.getBoundingBox().width;
        fu_name1.y =winSize.height/2+90;
        var fu_str2="是否进行复仇";
        var  fu_name2 = new cc.LabelTTF(fu_str2, "Arial", 25);
        fu_name2.x = winSize.width/2;
        fu_name2.y =winSize.height/2;
        fu_name2.setAnchorPoint(0.5,0.5);
        fu_name2.setColor(cc.color(254,254,254));
        this.addChild(fu_name2, 1);
        var fu_str3="复仇胜利可获得两块奖牌";
        var  fu_name3 = new cc.LabelTTF(fu_str3, "Arial", 25);
        fu_name3.x = winSize.width/2;
        fu_name3.y =winSize.height/2+45;
        fu_name3.setAnchorPoint(0.5,0.5);
        fu_name3.setColor(cc.color(254,254,254));
        this.addChild(fu_name3, 1);
        var Is_can=true;
        var btn = new cc.MenuItemImage(
            "res/start/fristbtn1.png",
            "res/start/fristbtn1.png",
            function () {
                if(Is_can){
                    Is_can=false;
                    var layer=new NetLoadLayer();
                    Start_layer .addChild(layer,10);
                    var start_time=new Date().getTime();
                    var userId=window.select.rivalUserProfile.userId;
                    var url="/openapi/game/interactionGame/matchGameLog.json?rivalUid="+userId+"&aid="+appId+"&rivalType=1&gameType=0";
                    var xhr_fun= function (xhr_data) {
                        var end_time=new Date().getTime();
                        if(end_time-start_time<=10*1000){
                            cc.log("start_time:",start_time,"end_time:",end_time,"end_time-start_time:",end_time-start_time);
                            window.get=[];
                            window.get= xhr_data["data"];
                            cc.log("window.get",window.get);
                            if(window.get.interActionPlayGameLog){
                                cc.director.runScene(new NetWorkScene(1));
                            }else{
                                if(fuchou_Layer){
                                    fuchou_Layer.removeFromParent();
                                }
                                if(Net_load_layer){
                                    Net_load_layer.removeFromParent();
                                }
                                var layer=new TishiLayer("您已经复仇过了，放过他吧");
                                Start_layer .addChild(layer,10);
                            }
                        }
                    };
                    var error_fun= function () {
                        if(fuchou_Layer){
                            fuchou_Layer.removeFromParent();
                        }
                        if(Net_load_layer){
                            Net_load_layer.removeFromParent();
                        }
                        var layer=new TishiLayer("网络异常...",2);
                        Start_layer .addChild(layer,10);
                    };
                    CreateXhr(url,xhr_fun,error_fun);
                }
            }, this
        );
        btn.setPosition(winSize.width/2-100,winSize.height/2-80);
        btn.setAnchorPoint(0.5,0.5);
        btn.setColor(cc.color(255,255,255));
        var self=this;
        var btn1 = new cc.MenuItemImage(
            "res/start/fristbtn1.png",
            "res/start/fristbtn1.png",
            function () {
                self.removeFromParent();
            }, this
        );
        btn1.setPosition(winSize.width/2+100,winSize.height/2-80);
        btn1.setAnchorPoint(0.5,0.5);
        var menu = new cc.Menu(btn,btn1);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu,5);
        var queding="是";
        var  queding_name = new cc.LabelTTF(queding, "Arial", 30);
        queding_name.x = winSize.width/2-100;
        queding_name.y =winSize.height/2-80;
        this.addChild(queding_name, 5);
        var quxiao="否";
        var  quxiao_name = new cc.LabelTTF(quxiao, "Arial", 30);
        quxiao_name.x = winSize.width/2+100;
        quxiao_name.y =winSize.height/2-80;
        this.addChild(quxiao_name, 5);

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {

                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
});
var shuaifenLayer=cc.Layer.extend({
    ctor: function (rival_Type) {
        this._super();
        fuchou_Layer=this;
        var winSize=cc.winSize;
        var name_str1=window.select.maliceRefreshRivalUserName;
        var nikname= getBytesLength(name_str1,12);
        var sun_num=Math.abs(window.select.maliceRefreshMedals);

        var rival_str;
        switch (rival_Type){
            case 0:
                rival_str="您在和 "+nikname+" PK中逃跑了";
                break;
            case 1:
                rival_str="您在对"+nikname+"复仇中逃跑了";
                break;
            case 2:
                rival_str="您在对"+nikname+"挑战中逃跑了";
                break ;
            case 3:
                rival_str="您在对"+nikname+"挑战中逃跑了";
                break;
        }
        var sp=new cc.Sprite("res/load/load_bg.png");
        sp.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(sp);
        cc.log("rival_str",rival_str);
        var  fu_name = new cc.LabelTTF(rival_str, "Arial", 30);
        fu_name.setAnchorPoint(0.5,0.5);
        fu_name.x = winSize.width/2;
        fu_name.y =winSize.height/2+90;
        fu_name.setColor(cc.color(254,254,254));
        this.addChild(fu_name, 1);

        var fu_str3="系统将自动扣除一块奖牌";
        var  fu_name3 = new cc.LabelTTF(fu_str3, "Arial", 30);
        fu_name3.x = winSize.width/2;
        fu_name3.y =winSize.height/2+30;
        fu_name3.setAnchorPoint(0.5,0.5);
        fu_name3.setColor(cc.color(254,254,254));
        this.addChild(fu_name3, 1);
        var self=this;
        var btn = new cc.MenuItemImage(
            "res/start/fristbtn1.png",
            "res/start/fristbtn1.png",
            function () {
                var availabity=0;
                var game_score=0;
                var game_type=0;
                var type_score=0;
                var player_GameLogId=1;
                var rivalUid=window.select.maliceRefreshRivalUid;
                var rivalType=-2;
                var rivalTypeScore=1;
                var  url="/openapi/game/interactionGame/postGameLog.json?rivalTypeScore="+rivalTypeScore +"&availabity="+availabity+"&aid="+appId+"&rivalUid="+rivalUid+"&rivalType="+rivalType+"&gameScore="+game_score+"&gameLog="+null+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+player_GameLogId+"&scoreId="+scoreId
                var xhr_fun= function (xhr_data) {

                };
                var error_fun= function () {
                };
                CreateXhr(url,xhr_fun,error_fun);
                var point_num=window.select.userMedals-Math.abs(window.select.maliceRefreshMedals);
                if(point_num<=0){
                    point_num=0
                }
                var start_PK_str="奖牌数: "+point_num;
                start_PK_name.setString(start_PK_str);
                self.removeFromParent()

            }, this
        );
        btn.setPosition(winSize.width/2,winSize.height/2-50);
        btn.setAnchorPoint(0.5,0.5);
        btn.setColor(cc.color(255,255,255));
        var menu = new cc.Menu(btn);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu,5);
        var queding="知道了";
        var  queding_name = new cc.LabelTTF(queding, "Arial", 30);
        queding_name.x = winSize.width/2;
        queding_name.y =winSize.height/2-50;
        this.addChild(queding_name, 5);


        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {

                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
});
var GameStartScene=cc.Scene.extend({
    ctor: function (num) {
        this._super();
        cc.log(num);
        var layer=new GameStartLayer(num);
        this.addChild(layer);
    }
})