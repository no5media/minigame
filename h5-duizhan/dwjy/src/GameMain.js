Score=0;
money_num=0;
Is_Angin=false;
Is_overOrplay=false;
Layer_object=null;
Select_Array=[];
Select_end_Array=[];
Select_end_num=0;
Select_end=false;
Is_Watch=false;
Is_share=false;
Main_layer=null;
typescore=0;
Record_end=null;
var GameMainLayer = cc.LayerColor.extend({
    getScore:null,
    ctor:function () {
        this._super(cc.color(255,255,255,255));
        Main_layer=this;
        money_num=0;
        Score=0;
        typescore=0;
        money_num=0;
        Is_Angin=false;
        Is_overOrplay=false;
        Layer_object=null;
        Select_Array=[];
        Select_end_Array=[];
        Select_end_num=0;
        Select_end=false;
        Is_Watch=false;
        Is_share=false;
        Life_Array=[];
        All_Block=[];
        All_Block_Postion=[];
        GetSprite_Array=[];
        Record_num=0;
        Record_num1=0;
        Is_record=false;
        Record_end=true;
        Record_Array=[];//存储初始化这个地图的数据，（用于对战时上传时用）
        Record_fall_Array=[];//存储下落时的数字方块，（用于对战时上传时用）
        Record_click=[];//存储玩家点击的数字方块，（用于对战时上传时用）
        Record_remove=[];
        Record_Score=[];
        sp_Money=null;
        Record_time=0;
        Min_x=48+132/2;//最左下角的的方块的坐标x值
        Min_y=42+132;//最左下角的的方块的坐标y值
        Min_xy=3;//设置方块方块的间隔
        Min_width=132;
        Max_row=5;
        life_num=5;
        time=0;
        speed=0.08;
        sp_move_num=0;
        sp_move_num1=0;
        sp_remove_num=0;
        Is_Click=true;
        Layer_object=this;
        Is_click_Check=false;//判断某个数值方块是否是点击状态
        Is_Check=false;//是否能够进行检测
        Is_fall=false;//是否处于下落状态
        Is_move=false;//是否处于移动状态
        Is_bianli=true;
         Is_create=true;
        Is_sanshimiao=true;
        typescore=0;
        this.CreateBg();
        this.AddSprite();
        var self=this;
        var winSize=cc.director.getVisibleSize();
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect= cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                if(Is_Click){
                    Is_Click=false;
                    for(var a=0;a<All_Block.length;a++){
                        if(All_Block[a]&&cc.rectIntersectsRect(All_Block[a].getBoundingBox(),rect)){
                            Is_record=true;
                            sp_remove_num=0;
                            Is_click_Check=true;
                            var tag=All_Block[a].tag+1;
                            var sp_x=All_Block[a].x;
                            var sp_y=All_Block[a].y;
                            var sp_col=All_Block[a].col;
                            var sp_row=All_Block[a].row;
                            All_Block[All_Block[a].row*Max_row+All_Block[a].col].removeFromParent();
                            All_Block[All_Block[a].row*Max_row+All_Block[a].col]=null;
                            Record_num++;
                            if(Record_end){
                                Record_click.push({
                                    time:Record_time,
                                    tag:tag,
                                    row:sp_row,
                                    col:sp_col
                                });
                                //cc.log(Record_click)
                            }
                            self.CreateSprite(tag,sp_x,sp_y,sp_row,sp_col);
                        }
                    }
                }
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;
            }
        });
        cc.eventManager.addListener(listener, this);
        this.scheduleUpdate();
        //this.getWxConfig();

        return true;
    },
    update: function (dt) {
        if(Record_end&&Is_record){
            Record_time+=dt;
            if(Record_time>=60&&Is_sanshimiao){
                Is_sanshimiao=false;
                typescore=Score;
                Record_end=false;
            }
        }
        if(Is_Check&&!Is_move&&!Is_fall){
            for(var col=0;col<5;col++) {
                for (var row = 0; row < 5; row++) {
                    if(All_Block[row*Max_row+col]){
                        if(!Is_click_Check&&Is_Check){
                            All_Block[row*Max_row+col].bool=false;
                            this.GetSprite(All_Block[row*Max_row+col],2);
                        }
                    }
                }
            }
        }
        for(var a=0;a<All_Block.length;a++){
            if(All_Block[a]&&All_Block[a].move&&!Is_fall){
                //var posx=All_Block[a].run_Array[All_Block[a].sp_length].x
                //var posy=All_Block[a].run_Array[All_Block[a].sp_length].y
                var posx=All_Block_Postion[All_Block[a].run_Array[All_Block[a].sp_length].row*Max_row+All_Block[a].run_Array[All_Block[a].sp_length].col].x
                var posy=All_Block_Postion[All_Block[a].run_Array[All_Block[a].sp_length].row*Max_row+All_Block[a].run_Array[All_Block[a].sp_length].col].y
                this.SpMove(All_Block[a],posx,posy);
            }
        }
        if(Is_move||Is_fall){
            Is_Click=false
        }else if(!Is_move&&!Is_fall){
            Is_Click=true
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false;
            getScore.setString(window.select.userPoint);
            for(var a=0;a<Life_Array.length;a++){
                Life_Array[a].setVisible(true)
            }
            life_num=5
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            Is_Click=false;
            this.overFinTag()
        }
        if(Select_end){
            Select_end=false;
            sp_Money.setString(money_num);
        }
    },
    CreateBg: function () {
        var size=cc.director.getVisibleSize();
        var size=cc.winSize;
        var winSize=cc.winSize;
        var self=this;
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
             record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,80,size.height-40,80,self);
        });
        //var name_str="李乾@1758";
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =size.height-20;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);

        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-20);
        start_score.setAnchorPoint(1,0.5);
        this.addChild(start_score);

        //var get_score=0;
        get_score=window.select.userPoint;
        getScore = new cc.LabelTTF(get_score, "Arial", 30);
        getScore.x = size.width-70;
        getScore.y =size.height-23;
        getScore.setColor(cc.color(88,195,224));
        getScore.setString(get_score);
        this.addChild(getScore, 1);
        if(gameNum2>=10000&&gameNum2<1000000){
            getScore.setScale(0.8);
        }else if(gameNum2>=1000000){
            getScore.setScale(0.6);
        }
        //CreateBtn("res/task/money.png",function(){
        //    //任务按钮
        //    var layer=new TaskLayer();
        //    self.addChild(layer,10);
        //},winSize.width-150,winSize.height-50,0.5,0.5,this);

        var str=new cc.MenuItemFont("游戏规则", function () {
            var layer=new ShuomingLayer();
            layer.setPosition(0,-1000);
            layer.runAction(new cc.moveBy(0.2,0,1000));
            self.addChild(layer,10)
        },this);
        str.setPosition(size.width/2,50);
        str.setColor(cc.color(88,195,224));
        var menu = new cc.Menu(str);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu);
        Score_num = new cc.LabelAtlas("0", "res/num1.png", 47,54,"0");
        Score_num.setAnchorPoint(0.5,0.5);
        Score_num.x = size.width / 2;
        Score_num.y = size.height-100;
        Score_num.setScale(1,1.2);
        this.addChild(Score_num, 5);
        for(var a=0;a<5;a++){
            var s2= new cc.Sprite(res.s2);
            s2.setPosition(130+a*130,848+66);
            this.addChild(s2,2);
        }
        for(var a=0;a<5;a++){
            var s1= new cc.Sprite(res.s1);
            s1.setPosition(130+a*130,848+66);
            this.addChild(s1,3);
            Life_Array.push(s1);
        }
    },
    AddSprite: function () {
        for(var a=0;a<5;a++){
            for(var b=0;b<5;b++){
                var num_Array1=[];
                for(var i=1;i<8;i++){
                    num_Array1.push(i)
                }
                var num_Array=[];
                if(All_Block[a*Max_row+b+1]){
                    num_Array.push(All_Block[a*Max_row+b+1].tag)
                }
                if(All_Block[a*Max_row+b-1]){
                    num_Array.push(All_Block[a*Max_row+b-1].tag)
                }
                if(All_Block[(a+1)*Max_row+b]){
                    num_Array.push(All_Block[(a+1)*Max_row+b].tag)
                }
                if(All_Block[(a-1)*Max_row+b]){
                    num_Array.push(All_Block[(a-1)*Max_row+b].tag)
                }
                for(var i=0;i<num_Array.length;i++){
                    for(var j=0 in num_Array1){
                        if(num_Array1[j]==num_Array[i]){
                            num_Array1.splice(j,1);
                            j--;
                        }
                    }
                }
                var num=Math.floor(Math.random()*(num_Array1.length-1));
                var m=num_Array1[num];
                for(var i=0;i<num_Array.length;i++){
                    num_Array.splice(i,1);
                    i--;
                }
                for(var i=0;i<num_Array1.length;i++){
                    num_Array1.splice(i,1);
                    i--;
                }
                var sp_x=Min_x+b*(Min_width+Min_xy);
                var sp_y=Min_y+a*(Min_width+Min_xy);
                this.CreateSprite(m,sp_x,sp_y,a,b);
                if(Record_end){
                    Record_Array.push({
                        row:a,
                        col:b,
                        tag:m
                    })
                }
            }
        }
    },
    CreateSprite: function (m,x,y,row ,col) {
        var num=m%9;
        if(num==0){
            num=9
        }
        var sp=new cc.Sprite(g_resources[num-1]);
        sp.setPosition(x,y);
        sp.row=row;
        sp.col=col;
        sp.tag=m;
        sp.bool=true;//判断是否是点击点
        sp.move=false;//判断是否可以开始运动
        sp.fall=false;//判断对象是否处于下落状态
        sp.run=false;//判断是否处于运动状态
        sp.role_sp_row=null;//对对象进行计数
        sp.role_sp_col=null;//对对象进行计数
        sp.sp_length=0;//判断最远的对象距目标对象的长度
        sp.run_Array=[];//用于存储各个对象的运动路径
        sp.open=[];
        sp.close=[];
        sp.af=null;
        this.addChild(sp);
        var sp_num= new cc.LabelAtlas(m, "res/num.png", 55, 71,"0");
        sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
        sp_num.setAnchorPoint(0.5,0.5);
        sp.addChild(sp_num);
        All_Block[sp.row*Max_row+sp.col]=sp;
        All_Block_Postion[sp.row*Max_row+sp.col]={
            x:sp.x,
            y:sp.y,
            row:sp.row,
            col:sp.col
        }
        if(Is_click_Check){
            sp.bool=false;
            Is_Check=false;
            this.GetSprite(All_Block[sp.row*Max_row+sp.col],1);
        }
    },
    GetSprite: function (sp,num) {
        var record_sp=[];
        for(var a=0;a<GetSprite_Array.length;a++){
            GetSprite_Array.splice(a,1);
            a--;
        }
        if(GetSprite_Array.indexOf(sp)==-1){
            GetSprite_Array.push(sp);
        }
        for(var a=0;a<GetSprite_Array.length;a++){
            this.CheckSprite(GetSprite_Array[a]);
        }
        if(GetSprite_Array.length>=3){
            for(var a=0;a<GetSprite_Array.length;a++){
                if(Record_end){
                    //if(GetSprite_Array[a].row!=sp.row&&GetSprite_Array[a].col!=sp.col){
                        record_sp.push({
                            row:GetSprite_Array[a].row,
                            col:GetSprite_Array[a].col,
                            tag:GetSprite_Array[a].tag
                        });
                    //}
                }
            }
            if(Record_end){
                Record_remove.push(record_sp);
            }
            if(Record_end&&num==2){
                Record_click.push({
                    time:Record_time,
                    tag:sp.tag,
                    row:sp.row,
                    col:sp.col
                })
            }
            Is_click_Check=false;
            this.RemoveSprite(GetSprite_Array);
            sp_remove_num++;
            for(var a=1;a<sp_remove_num;a++){
                life_num++;
                if(life_num>5){
                    life_num=5
                }else{
                    Life_Array[life_num-1].setVisible(true);
                }
            }
        }else{
            if(num==1){
                for(var a=0;a<GetSprite_Array.length;a++){
                    if(Record_end){
                        //if(GetSprite_Array[a].row!=sp.row&&GetSprite_Array[a].col!=sp.col){
                            record_sp.push({
                                row:GetSprite_Array[a].row,
                                col:GetSprite_Array[a].col,
                                tag:GetSprite_Array[a].tag
                            });
                        //}
                    }
                }
                if(Record_end){
                    Record_remove.push(record_sp);

                }
            }
            sp.bool=true;
            if(Is_click_Check) {
                if(life_num<=1){
                    //gameOver 游戏结束
                    life_num--;
                    Life_Array[life_num].setVisible(false);
                    //判断是否复活
                    var angin_num=cc.sys.localStorage.getItem("dwjyAngin");
                    if(angin_num<=3){
                        var layer=new AnginLayer(1);
                        this.addChild(layer,10);
                    }else{
                        this.unscheduleAllCallbacks();
                        cc.eventManager.removeAllListeners();
                        Is_Click=false;
                        this.overFinTag()
                    }
                }else{
                    Is_click_Check=false;
                    life_num--;
                    Life_Array[life_num].setVisible(false);
                }
            }
        }
    },
    CheckSprite: function (sp) {
        if(sp.col+1<5) {
            if(All_Block[sp.row*Max_row+sp.col+1]){
                if(All_Block[sp.row*Max_row+sp.col+1].tag==sp.tag){
                    if(GetSprite_Array.indexOf(All_Block[sp.row*Max_row+sp.col+1])==-1){
                        GetSprite_Array.push(All_Block[sp.row*Max_row+sp.col+1])
                    }
                }
            }
        }
        if(sp.col-1>=0){
            if(All_Block[sp.row*Max_row+sp.col-1]){
                if(All_Block[sp.row*Max_row+sp.col-1].tag==sp.tag){
                    if(GetSprite_Array.indexOf(All_Block[sp.row*Max_row+sp.col-1])==-1){
                        GetSprite_Array.push(All_Block[sp.row*Max_row+sp.col-1])
                    }
                }
            }
        }
        if(sp.row+1<5){
            if(All_Block[(sp.row+1)*Max_row+sp.col]){
                if(All_Block[(sp.row+1)*Max_row+sp.col].tag==sp.tag){
                    if(GetSprite_Array.indexOf(All_Block[(sp.row+1)*Max_row+sp.col])==-1){
                        GetSprite_Array.push(All_Block[(sp.row+1)*Max_row+sp.col])
                    }
                }
            }
        }
        if(sp.row-1>-1){
            if(All_Block[(sp.row-1)*Max_row+sp.col]){
                if(All_Block[(sp.row-1)*Max_row+sp.col].tag==sp.tag){
                    if(GetSprite_Array.indexOf(All_Block[(sp.row-1)*Max_row+sp.col])==-1){
                        GetSprite_Array.push(All_Block[(sp.row-1)*Max_row+sp.col])
                    }
                }
            }
        }
    },//找寻符合条件的要删除方块
    RemoveSprite: function (Array) {
        sp_move_num=Array.length;
        sp_move_num1=0;
        Is_Check=false;
        Is_move=true;
        var sp_x;
        var sp_y;
        var sp_col;
        var sp_row;
        //Score+=Array.length*Array[0].tag*10;
        Score+=Array.length*Array[0].tag*10;
        Score_num.setString(Score);
        Record_Score.push({
            Score:Score,
            time:Record_time
        });
        var record_sp=[];
        for(var a=0;a<Array.length;a++){
            Array[a].run=true;
            All_Block[Array[a].row*Max_row+Array[a].col].run=true;
            if(!Array[a].bool){
                tag=Array[a].tag+1;
                sp_x=Array[a].x;
                sp_y=Array[a].y;
                sp_col=Array[a].col;
                sp_row=Array[a].row;
                Array[a].setLocalZOrder(1);
            }else{
                record_sp[record_sp.length]={
                    row:Array[a].row,
                    col:Array[a].col,
                    tag:Array[a].tag
                }
            }
        }
        for(var a=0;a<Array.length;a++){
            Array[a].run_Array=this.FindWays(Array[a],All_Block[sp_row*Max_row+sp_col]);
            Array[a].move=true;
            Array[a].sp_length=Array[a].run_Array.theIndex-1;
        }
    }, //提供各个目标方块的路径
    SpMove: function (object,posx,posy) {
        object.move=false;
        var speed1;
        if(object.run_Array.theIndex==1){
            speed1=1.5
        }else if(object.run_Array.theIndex==2){
            speed1=0.8
        }else if(object.run_Array.theIndex==3){
            speed1=0.5
        }else if(object.run_Array.theIndex==4){
            speed1=0.3
        }else if(object.run_Array.theIndex==5){
            speed1=0.2
        }else if(object.run_Array.theIndex==6){
            speed1=0.2
        }else{
            speed1=0.2
        }
        var move=new cc.moveTo(speed*speed1*object.run_Array.theIndex,posx,posy)
        var self=this
        var fun1=cc.callFunc(function (){
            object.sp_length--;
            if(object.sp_length<0){
                sp_move_num1++;
                if(sp_move_num==sp_move_num1){
                    Is_move=false;
                    object.move=false;
                    for(var j=0;j<All_Block.length;j++){
                        if(All_Block[j].run){
                            All_Block[All_Block[j].row*Max_row+All_Block[j].col].removeFromParent();
                            All_Block[All_Block[j].row*Max_row+All_Block[j].col]=null;
                        }
                    }
                    var tag=object.tag+1;
                    var sp_x;
                    var sp_y;
                    var sp_row=object.run_Array[0].row
                    var sp_col=object.run_Array[0].col
                    self.CreateSprite(tag,posx,posy,sp_row,sp_col);
                    self.SpriteFall();
                }
            }
            else{
                object.move=true;
            }
        })

        object.runAction(new cc.sequence(move,fun1));
    },//遍历移动路径使其运动
    SpriteFall: function () {
        //Record_num1++;
        var size=cc.winSize;
        for(var col=0;col<5;col++){
            var sprow = 0;
            for(var row=0 ;row<5;row++){
                var roleSprite = All_Block[row * Max_row + col];
                if(roleSprite==null){
                    sprow ++;
                }else{
                    if(sprow>0){
                        var newRow = row - sprow;
                        All_Block[newRow * Max_row + col] = roleSprite;
                        All_Block[row * Max_row + col] = null;
                        var startPosition = roleSprite.getPosition();
                        var endPosition = this.PointGetEnd(newRow,col);
                        var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                        var speed=(startPosition.y - endPosition.y)/speed1;
                        roleSprite.stopAllActions();
                        Is_fall=true;
                        var fun=cc.callFunc(function () {

                        })
                        roleSprite.runAction(new cc.sequence(fun,new cc.moveTo(speed,endPosition.x,endPosition.y)));
                        roleSprite.row = newRow;
                    }
                }
            }
        }
        var s_num=0;
        var s_num1=0;
        for(var a=0 in All_Block){
            if(All_Block[a]==null){
                s_num++
            }
        }
        var Record_down=[];
        for(var col=0;col<5;col++){
            for(var row =0;row <5;row++){
                if(All_Block[row*Max_row+col]==null){
                    var num=Math.floor(Math.random()*5+1)
                    var sp=new cc.Sprite(g_resources[num-1]);
                    sp.setPosition(All_Block_Postion[(row)*Max_row+col].x,All_Block_Postion[20].y+132);
                    sp.row=row;
                    sp.col=col;
                    sp.tag=num;
                    sp.bool=true;//判断是否是点击点
                    sp.move=false;//判断是否可以开始运动
                    sp.fall=false;//判断对象是否处于下落状态
                    sp.run=false;//判断是否处于运动状态
                    sp.role_sp_row=null;//对对象进行计数
                    sp.role_sp_col=null;//对对象进行计数
                    sp.sp_length=0;//判断最远的对象距目标对象的长度
                    sp.run_Array=[];//用于存储各个对象的运动路径
                    sp.open=[];
                    sp.close=[];
                    sp.af=null
                    this.addChild(sp);
                    Record_down.push({
                        row:row,
                        col:col,
                        tag:num,
                    })

                    var sp_num= new cc.LabelAtlas(num, "res/num.png", 55, 71,"0");
                    sp_num.setPosition(sp.getContentSize().width/2,sp.getContentSize().height/2);
                    sp_num.setAnchorPoint(0.5,0.5);
                    sp.addChild(sp_num);
                    All_Block[sp.row*Max_row+sp.col]=sp;
                    var startPosition = sp.getPosition();
                    var endPosition = this.PointGetEnd(row,col);
                    var speed1 = (startPosition.y - endPosition.y)/(size.height/5000);
                    var speed=(startPosition.y - endPosition.y)/speed1;
                    var fun=cc.callFunc(function () {
                        s_num1++;
                        if(s_num==s_num1){
                            Is_fall=false;
                            Is_click_Check=false;
                            Is_Check=true;

                        }
                    })
                    var dely=cc.delayTime(0.01);
                    sp.runAction(new cc.sequence(new cc.moveTo(speed,endPosition.x,endPosition.y),dely,fun));
                }
            }
        }
        Record_fall_Array[Record_fall_Array.length]=Record_down;

    },//物块的下落
    PointGetEnd : function(row , col){
        this.endX =  Min_x+col*(Min_width+Min_xy);
        this.endY =  Min_y+row*(Min_width+Min_xy);
        var point = cc.p(this.endX,this.endY);

        return point;
    },//获取目标点坐标
    FindWays: function (startPoint,endPoint) {
        var openList  = [];                      //初始化开启列表
        var closeList = [];                      //初始化关闭列表
        var path_Array=[];
        startPoint.ag = 0;
        startPoint.ah = 0;
        startPoint.af = startPoint.ag + startPoint.ah;   //起点的G,H,F值为0
        openList.push(startPoint);               //起点加入开启列表
        var findTheWay = false;
        do{
            var centerNode = this.findMinNode(openList);  //寻找F值最低的节点
            openList.remove(centerNode);         //将此节点从开启列表中删除，为了下次遍历开启列表的时候不再出现此节点
            closeList.push(centerNode);          //并将此节点加入到关闭列表
            for(var i=0;i<4;i++)                 //遍历此节点周围的节点，并给这些节点加入坐标属性和G值
            {
                var aroundNode = {};
                if (Math.abs(centerNode.row-endPoint.row)==0 && Math.abs(centerNode.col-endPoint.col)==0)  //如果节点和终点的值相近，那么A*算法结束，得到路径
                {
                    findTheWay = true;
                    var pathArry=[]
                    var aroundNode = {
                        row:centerNode.row,
                        col:centerNode.col
                    };
                    pathArry.push(aroundNode)
                    pathArry.theIndex = pathArry.length;
                    return pathArry;
                    //this.unschedule(this.thePathSelector);
                    //this.schedule(this.thePathSelector,null,pathArry.length-1);
                    break;           //找到最短路径并跳出循环
                }
                switch (i){
                case 0:
                    aroundNode.row = centerNode.row+1;                //坐标属性
                    aroundNode.col = centerNode.col;
                    break;
                case 1:
                    aroundNode.row = centerNode.row-1;                //坐标属性
                    aroundNode.col = centerNode.col;
                    break;
                case 2:
                    aroundNode.row = centerNode.row;                //坐标属性
                    aroundNode.col = centerNode.col+1;
                    break;
                case 3:
                    aroundNode.row = centerNode.row;                //坐标属性
                    aroundNode.col = centerNode.col-1;
                    break;

            }

                if(All_Block[aroundNode.row*Max_row+aroundNode.col]){
                    if(!All_Block[aroundNode.row*Max_row+aroundNode.col].run){
                        aroundNode.isOb = false;   //判断当前节点是否是不是要删除的节点
                    }else{
                        aroundNode.isOb = true
                    }
                }else{
                    if(aroundNode.row<5&&aroundNode.row>=0&&aroundNode.col<5&&aroundNode.col>=0){
                        aroundNode.isOb = true
                    }else{
                        aroundNode.isOb = false
                    }
                }
                if (!aroundNode.isOb){                                       //如果是障碍物，跳过

                }else if(closeList.hasObject(aroundNode)){                         //如果在关闭列表里，跳过

                }else if(!openList.hasObject(aroundNode)){                          //如果不在开启列表里，加入到开启列表
                    aroundNode.parentPath = centerNode;
                    if (Math.abs(aroundNode.row-endPoint.row)==0 && Math.abs(aroundNode.col-endPoint.col)==0)  //如果节点和终点的值相近，那么A*算法结束，得到路径
                    {

                        findTheWay = true;
                        var pathArry = [];
                        this.gettingAStarPath(aroundNode,pathArry);//寻找路径
                        pathArry.splice(0,0,{row:endPoint.row,col:endPoint.col});   //加终点到数组头部
                        pathArry.splice(pathArry.length-1,1);                                 //删一项数组底部的起点数据，此时的数组是最终的路径数组
                        path_Array = pathArry;
                        pathArry.theIndex = path_Array.length;
                        return pathArry;
                        //this.unschedule(this.thePathSelector);
                        //this.schedule(this.thePathSelector,null,pathArry.length-1);
                        break;           //找到最短路径并跳出循环
                    }
                    if (aroundNode.row!=centerNode.row && aroundNode.col!=centerNode.col)   //确定中心节点和周围节点形成的角度，正交G值消耗10*像素，斜角G值消耗14*像素
                        aroundNode.ag = centerNode.ag ;
                    else
                    aroundNode.ag = centerNode.ag;
                    aroundNode.af = this.getAF(aroundNode,endPoint);
                    openList.push(aroundNode);
                }
                else if(openList.hasObject(aroundNode)){                            //如果在开启列表里
                    //if (centerNode.af < aroundNode.af){                //如果新的g值小于周围节点本身的g值，那么周围节点的父节点改为当前中心节点，并重新计算其F值
                    //    aroundNode.parentPath = centerNode;
                    //    aroundNode.ag = centerNode.ag;
                    //    aroundNode.af = this.getAF(aroundNode,endPoint);
                    //}
                }
            }


        }while(!findTheWay)

    },
    findMinNode:function(openListArray){//找到最小af
        var minNode = openListArray[0];
        for (var i=0;i<openListArray.length;i++)
        {
            if (minNode.af>openListArray[i].af) minNode=openListArray[i];
        }
        return minNode;
    },//找最小的af的节点
    getAF:function(thisNode,endNode){
        var aHExpend = (Math.abs(thisNode.row-endNode.row) + Math.abs(thisNode.col-endNode.col));
        return aHExpend+thisNode.ag;
    }, //获取af
    gettingAStarPath:function(laseNode,array){
        if(laseNode.parentPath != null) {
            array.push({row:laseNode.parentPath.row,col:laseNode.parentPath.col});
            this.gettingAStarPath(laseNode.parentPath,array);
        }
    },//根据父节点找到每一个节点的父类从而找到各个节点的路径
    overFinTag: function () {
        var tag_Array=[];
        for(var a=0;a<All_Block.length;a++){
            if(tag_Array.indexOf(All_Block[a].tag)==-1){
                tag_Array.push(All_Block[a].tag);
            }
        }
        var compare = function (x, y) {//比较函数
            if (x < y) {
                return -1;
            } else if (x > y) {
                return 1;
            } else {
                return 0;
            }
        }
        tag_Array.sort(compare);
        var self=this;
        var tag_Index=0;
       this.schedule(function () {
           if(Is_create&&tag_Index<tag_Array.length){
               for(var a=0;a<All_Block.length;a++){
                   if(All_Block[a].tag==tag_Array[tag_Index]){
                       Score+=All_Block[a].tag*10;
                       Score_num.setString(Score);
                       self.OverAction(All_Block[a])
                   }
               }
               tag_Index++
           }else{

               self.unscheduleAllCallbacks();
               var dely=cc.delayTime(0.5);
               var fun=new cc.callFunc(function () {
                   var layer=new GameOverLayer(tag_Array[tag_Array.length-1]);
                   self.addChild(layer,10);
               })
               self.runAction(new cc.sequence(dely,fun))
           }
       },0.5)
    },
    OverAction: function (object) {
        var scale1=new cc.ScaleTo(0.3,-1,1);
        var scale2=new cc.ScaleTo(0.3,1,1);
        var fun=new cc.callFunc(function () {

        })
        object.runAction(new cc.sequence(scale1,scale2))
        this.CreateNum(object,object.x,object.y)
    },
    CreateNum: function (object,x,y) {
        var num=object.tag*10
        var sp_num= new cc.LabelAtlas(num, "res/num.png", 55, 71,"0");
        sp_num.setPosition(x,y);
        sp_num.setAnchorPoint(0.5,0.5);
        sp_num.setOpacity(200);
        this.addChild(sp_num);
        var move=new cc.MoveBy(0.8,0,66);
        var destory= new cc.RemoveSelf(true)
        sp_num.runAction(new cc.sequence(move,destory));

    },
});
var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
});
//这里给Array数组添加3个实例方法
Array.prototype.aStarIndexOf = function(val) {        //通过对象寻找index值
    for (var i = 0; i < this.length; i++) {
        if (this[i].row==val.row && this[i].col==val.col) return i;
    }
    return -1;
};
Array.prototype.remove = function(val) {         //删除相应的对象
    var index = this.aStarIndexOf(val);
    if (index > -1) {
        this.splice(index, 1);
    }
};
Array.prototype.hasObject = function(val){       //判断是否是同一个对象
    for (var i = 0; i < this.length; i++){
        if (this[i].row==val.row && this[i].col==val.col)
            return true;
    }
    return false;
};
