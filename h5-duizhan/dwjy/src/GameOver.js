Over_layer=null;
var GameOverLayer=cc.LayerColor.extend({
    ctor: function (num) {
        this._super(cc.color(255,255,255,255));
        Over_layer=this;
        var winSize=cc.winSize;
        var bg=new cc.Sprite(res.start_bg);
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);
        cc.sys.localStorage.setItem("dwjyAngin",0);
        //SetShare(Score,0);
        var PK_str="我得了"+Score+"分，这游戏太刺激，只有1%的人能玩到50000分？";
        var link="http://"+document.domain+"/hlmy/interaction/clickMath/index2.html?chn=dwjy_share";
        share_object={
            title:PK_str,
            link:link
        };
        HLMY_PK.setShare(share_object);
        var allData=[];
        allData.push(Record_Array);
        allData.push(Record_fall_Array);
        allData.push(Record_click);
        allData.push(Record_remove);
        allData.push(Record_Score);
        //var jsondata1 = JSON.stringify(allData);
        var type_score=typescore;
        var game_score=Score;
        var game_type=0;
        var availabity=1;
        var rivalUid=0;
        var rivalType=-1;
        var min_Score=0;
        var min=0;
        for(var a=0;a<Record_Score.length;a++){
            if(Record_Score[a].time>=60){
                min_Score=Record_Score[a].Score;
                min=a;
                break;
            }
        }
        if(min==0){
        }else{
            if(Record_Score[min-1].Score==min_Score){
                type_score=min_Score;
            }else{
                type_score=Record_Score[min-1].Score
            }
        }
        allData[allData.length]=type_score;
        allData[allData.length]=Score;
        var jsondata1 = JSON.stringify(allData);
        //cc.sys.localStorage.setItem("allData1",jsondata1);
        //var  alldata=cc.sys.localStorage.getItem("allData1");
        //cc.log("alldata1",alldata);
        if(!Record_end&&Record_click.length>30){
            availabity=1
        }else{
            availabity=0
        }
        var  url="/openapi/game/interactionGame/postGameLog.json?gameConfigId=5&availabity="+availabity+"&aid="+appId+"&gameScore="+game_score+"&gameType="+game_type+"&typeScore="+type_score+"&rivalUid="+rivalUid+"&rivalType="+rivalType+"&playerGameLogId="+""+"&scoreId="+scoreId;
        var xhr = cc.loader.getXMLHttpRequest();
        var  url=url;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.send(jsondata1);

        //var bg1 = new cc.Sprite(res.start_bg1);
        //bg1.setPosition(winSize.width / 2, winSize.height / 2+350);
        //this.addChild(bg1);
        //玩家的结束信息
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl;
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        //var name_str="李乾@1758"
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12)
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-72;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,112,143));
        this.addChild(record_name, 5);
        var score_str="本局得分:";
        var score_str_name = new cc.LabelTTF(score_str, "Arial", 30);
        score_str_name.x = winSize.width/2;
        score_str_name.y =winSize.height-226;
        score_str_name.setAnchorPoint(0.5,0.5);
        score_str_name.setColor(cc.hexToColor("#ff8448"));
        this.addChild(score_str_name, 5);
        var game_Score=new cc.LabelTTF("0", "Arial", 50);
        game_Score.x = winSize.width / 2;
        game_Score.y = winSize.height-326;
        game_Score.setColor(cc.hexToColor("#fe5417"));
        game_Score.setScale(2.5);
        game_Score.setString(Score);

        if(Score>100000&&Score<100000){
            game_Score.setScale(0.7)
        }else if(Score>=100000){
            game_Score.setScale(0.6)
        }
        this.addChild(game_Score, 10);
        var self=this;
        var Best_Score_str;
        if(!window.select.maxVaScore){
            Best_Score_str=0
        }else{
            Best_Score_str=window.select.maxVaScore;
        }
        var best_str="最高分:";
        var best_str_name = new cc.LabelTTF(best_str, "Arial", 30);
        best_str_name.x = winSize.width/2;
        best_str_name.y =winSize.height-426;
        best_str_name.setAnchorPoint(0.5,0.5);
        best_str_name.setColor(cc.color(170,170,170));
        this.addChild(best_str_name, 5);
        var Best_score= new cc.LabelTTF("0", "Arial", 35);
        Best_score.x = winSize.width/2;
        Best_score.y = winSize.height-484;
        Best_score.setAnchorPoint(0.5,0.5);
        Best_score.setColor(cc.color(41,175,235));
        Best_score.setString(Best_Score_str);
        this.addChild(Best_score,10);

        var start_score=new cc.Sprite("res/start/Score.png");
        start_score.setPosition(winSize.width,winSize.height-72);
        start_score.setAnchorPoint(1,0.5);
        this.addChild(start_score);


        var url=over_xhr;
        var xhr_fun= function (data) {
            window.select=[];
            window.select = data["data"];
            gameNum2=window.select.userPoint;//积分总数
            getScore = new cc.LabelTTF("0", "Arial", 30);
            getScore.x = winSize.width-70;
            getScore.y =winSize.height-72;
            getScore.setAnchorPoint(0.5,0.5);
            getScore.setColor(cc.color(254,202,75));
            getScore.setString(gameNum2);
            self.addChild(getScore, 1);
            if(gameNum2>=10000&&gameNum2<1000000){
                getScore.setScale(0.8)
            }else if(gameNum2>=1000000){
                getScore.setScale(0.6)
            }
        };
        var error_fun= function () {
            var layer=new TishiLayer("网络异常...");
            Over_layer .addChild(layer,10);
        };
        CreateXhr(url,xhr_fun,error_fun);
        //var xhr = cc.loader.getXMLHttpRequest();
        //var  url="/openapi/game/interactionGame/index.json?aid="+appId;
        //xhr.open("POST", url,true);
        //xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        //xhr.onreadystatechange = function () {
        //    if (xhr.readyState == 4){
        //        if(xhr.status >= 200 && xhr.status <= 207){
        //            var resw = xhr.responseText;
        //            var jsondata = JSON.parse(resw);
        //            var data = jsondata["data"];
        //            gameNum2=data.userPoint;//积分总数
        //            getScore = new cc.LabelTTF("0", "Arial", 30);
        //            getScore.x = winSize.width-70;
        //            getScore.y =winSize.height-72;
        //            getScore.setAnchorPoint(0.5,0.5)
        //            getScore.setColor(cc.color(254,202,75));
        //            getScore.setString(gameNum2);
        //            self.addChild(getScore, 1);
        //            if(gameNum2>=10000&&gameNum2<1000000){
        //                getScore.setScale(0.8)
        //            }else if(gameNum2>=1000000){
        //                getScore.setScale(0.6)
        //            }
        //        }
        //    }
        //}
        //xhr.send();
        CreateBtn("res/start/show.png", function () {
            //var share = document.getElementById('share-square');
            //share.style.display = 'block';
            HLMY_PK.shareTip();
        },winSize.width/2,490,0.5,0.5,this);

        CreateBtn("res/start/angin.png", function () {
            Score=0;
            cc.director.runScene(new GameMainScene());
        },winSize.width/2,320,0.5,0.5,this);
        CreateBtn(res.start_ranklist, function () {
            //排行榜
            //document.body.style.backgroundColor = '#9c9b9b';
            //document.documentElement.style.opacity = '0.5';
            var layer=new NetLoadLayer();
            Over_layer .addChild(layer,10);
            var url=PK_zhouRnking;
            var fun= function (Array) {
                if(Net_load_layer){
                    Net_load_layer.removeFromParent();
                }
                Gamedata2=[];
                Gamedata2 = Array["data"];
                cc.log("Gamedata1",Gamedata2);
                var layer = new RankList(1);
                layer.setScale(0.1);
                Over_layer.addChild(layer,10);
                var scal=new cc.ScaleTo(0.3,1);
                layer.runAction(scal);
            };
            var error_fun= function () {
                if(Net_load_layer){
                    Net_load_layer.removeFromParent();
                }
                var layer=new TishiLayer("网络异常...");
                Over_layer .addChild(layer,10);
            };
            CreateXhr(url,fun,error_fun);

        },winSize.width/2-92,165,0.5,0.5,this);
        CreateBtn("res/start/gohome.png", function () {
            var url1=index_xhr;
            var xhr_fun= function (data) {
                window.select=[];
                window.select = data["data"];
                var transition=new cc.TransitionShrinkGrow(1,new GameStartScene());
                cc.director.runScene(transition);
            };
            var error_fun= function () {
            };
            CreateXhr(url1,xhr_fun,error_fun);
        },winSize.width/2+92,150,0.5,0.5,this);

        CreateBtn("res/start/follow.png", function () {
            //关注
            HLMY_PK.ewm();
        },100,150,0.5,0.5,this);
        CreateBtn("res/start/invite.png", function () {
            //邀请
            HLMY_PK.shareTip();
        },winSize.width-100,150,0.5,0.5,this);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;

            }
        });
        cc.eventManager.addListener(listener, this);
        var startlogo=new cc.Sprite("res/start/start_logo.png");
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
    }
})
