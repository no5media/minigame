Net_load_layer=null;
var NetLoadLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        Net_load_layer=this;
        var winSize=cc.director.getVisibleSize();
        var self=this;
        var bg=new cc.Sprite("res/load/load_bg.png");
        bg.x=winSize.width/2;
        bg.y=winSize.height/2;
        bg.setScaleX(0.8);
        this.addChild(bg);
        var sp=new cc.Sprite("res/load/bg.png");
        sp.x=winSize.width/2;
        sp.y=winSize.height/2;
        this.addChild(sp);
        sp.runAction(cc.repeatForever(cc.rotateBy(0.1,40)));
        CreateBtn("res/load/load_btn.png", function () {
            self.removeFromParent();
        },winSize.width/2,winSize.height/2,0.5,0.5,this);

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
})
