Ranklist_layer=null;
tableView=null;
var CustomTableViewCell =cc.TableViewCell.extend({
    ctor:function() {
        this._super();
        cc.associateWithNative( this, cc.TableViewCell );
    },
    draw: function (ctx) {
        this._super(ctx);
    }
});
var RankList = cc.Layer.extend({
    tableView:null,
    title_name:null,
    drawNode:null,
    sp_xian:null,
    ctor: function (num) {
        this._super();
        Ranklist_layer=this;
        Cell_Array=[];
        Xhr_num=[];
        Xhr_num.push(2);
        RankList_Data=[];
        var winSize=cc.winSize;
        ranking_click=2;//表示此时显示的是哪个按钮
        sp_xian1 = new cc.DrawNode();
        this.addChild(sp_xian1,5);
        document.body.style.backgroundColor = '#9c9b9b';
        var layer= new cc.LayerColor(cc.color(0,0,0,100));
        this.addChild(layer);
        document.documentElement.style.backgroundColor = '#9c9b9b';
        //document.body.style.backgroundColor = '#9c9b9b';


        CreateBtn("res/ranking/btn.png", function () {
            //退出排行榜
            Gamedata1=[];
            Gamedata2=[];
            Gamedata3=[];
            Gamedata4=[];
            Gamedata5=[];
            Gamedata6=[];
            document.documentElement.style.backgroundColor = '#ffffff';
            document.body.style.backgroundColor = '#ffffff';
            Ranklist_layer.removeFromParent();
        },winSize.width-60,1065,0.5,0.5,this);
        CreateBtn(res.downbg, function () {

        },winSize.width/2-3,winSize.height/2-390,0.5,0,this);

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            }
        });
        RankList_sp_str= "注：挑战周榜成功可获得三块奖牌";
        RankList_sp_name = new cc.LabelTTF(RankList_sp_str, "Arial", 20);
        RankList_sp_name.setAnchorPoint(0.5,0.5);
        RankList_sp_name.setColor(cc.color(255,0,0));
        RankList_sp_name.x=winSize.width/2;
        RankList_sp_name.y=winSize.height-230;
        this.addChild(RankList_sp_name, 20);

        cc.eventManager.addListener(listener, this);
        Is_PK=true;//默认显示PK榜
        if(Gamedata2.gameRankList&&Gamedata2.gameRankList.length>0){
            RankList_Data=Gamedata2;//获取进入排行榜单时的数据
            cell_num=RankList_Data.gameRankList.length;//表示cell的个数
            this.CreateTableView();
        }
        this.CreateXian();
        this.CreateRankingBtn();
        this.CreateBg(num);
        this.CreateDownBtn();
    },

    CreateXian: function () {
        var winSize=cc.winSize;
        sp_xian1.drawRect(
            cc.p(winSize.width/2-285,winSize.height/2+350), // 起点
            cc.p(winSize.width/2-175,winSize.height/2+355), // 起点的对角点
            cc.hexToColor("#ff8362"), // 填充颜色
            1, // 线粗
            cc.hexToColor("#ff8362") // 线颜色
        );
        sp_xian1.setVisible(false);
        sp_xian2= new cc.DrawNode();
        this.addChild(sp_xian2,5);
        sp_xian2.drawRect(
            cc.p(winSize.width/2-55,winSize.height/2+350), // 起点
            cc.p(winSize.width/2+55,winSize.height/2+355), // 起点的对角点
            cc.hexToColor("#ff8362"), // 填充颜色
            1, // 线粗
            cc.hexToColor("#ff8362") // 线颜色
        );
        sp_xian2.setVisible(true);
        sp_xian3= new cc.DrawNode();
        this.addChild(sp_xian3,5);
        sp_xian3.drawRect(
            cc.p(winSize.width/2+175,winSize.height/2+350), // 起点
            cc.p(winSize.width/2+285,winSize.height/2+355), // 起点的对角点
            cc.hexToColor("#ff8362"), // 填充颜色
            1, // 线粗
            cc.hexToColor("#ff8362") // 线颜色
        );
        sp_xian3.setVisible(false);
    },
    CreateRankingBtn: function () {
        var winSize=cc.winSize;
        var self=this;
        RankList_btn = new cc.MenuItemFont(
            "好友榜",
            function () {
                if(ranking_click!=1){
                    RankList_sp_str= "注：挑战好友成功可获得两块奖牌";
                    RankList_sp_name.setString(RankList_sp_str);
                    ranking_click=1;
                    RankList_btn.setColor(cc.color(255,131,98));
                    RankList_btn1.setColor(cc.color(165,165,165));
                    RankList_btn2.setColor(cc.color(165,165,165));
                    sp_xian1.setVisible(true);
                    sp_xian2.setVisible(false);
                    sp_xian3.setVisible(false);
                    if(tableView){
                        tableView.removeFromParent();
                    }
                    if(Is_PK){
                        //PK榜下的好友榜
                        var url=PK_friendRnking;
                        self.DataRnking(Gamedata1,url,1);
                    }else{
                        //经典榜下的好友榜
                        var url=friendRnking;
                        self.DataRnking(Gamedata4,url,4);
                    }
                }
            }, this
        );
        RankList_btn.setPosition(winSize.width/2-230,winSize.height/2+380);
        RankList_btn.setColor(cc.color(165,165,165));
        RankList_btn.setScale(1.2);
        RankList_btn1 = new cc.MenuItemFont(
            "周榜",
            function () {
                if(ranking_click!=2){
                    RankList_sp_str= "注：挑战周榜成功可获得三块奖牌";
                    RankList_sp_name.setString(RankList_sp_str);
                    ranking_click=2;
                    RankList_btn1.setColor(cc.color(255,131,98));
                    RankList_btn.setColor(cc.color(165,165,165));
                    RankList_btn2.setColor(cc.color(165,165,165));
                    sp_xian2.setVisible(true);
                    sp_xian1.setVisible(false);
                    sp_xian3.setVisible(false);
                    if(tableView){
                        tableView.removeFromParent();
                    }
                    if(Is_PK){
                        var url=PK_zhouRnking;
                        self.DataRnking(Gamedata2,url,2);
                    }else{
                        //经典榜下的好友榜
                        var url=zhouRnking;
                        self.DataRnking(Gamedata5,url,5);
                    }
                }
            }, this
        );
        RankList_btn1.setPosition(winSize.width/2,winSize.height/2+380);
        RankList_btn1.setColor(cc.color(255,131,98));
        RankList_btn1.setScale(1.2);
        RankList_btn2 = new cc.MenuItemFont(
            "总榜",
            function () {
                if(ranking_click!=3){
                    ranking_click=3;
                    RankList_sp_str= "注：挑战总榜成功可获得三块奖牌";
                    RankList_sp_name.setString(RankList_sp_str);
                    RankList_btn2.setColor(cc.color(255,131,98));
                    RankList_btn.setColor(cc.color(165,165,165));
                    RankList_btn1.setColor(cc.color(165,165,165));
                    sp_xian3.setVisible(true);
                    sp_xian2.setVisible(false);
                    sp_xian1.setVisible(false);
                    if(tableView){
                        tableView.removeFromParent();
                    }
                    if(Is_PK){
                        //PK榜下的好友榜
                        var url=PK_zongRnking;
                        self.DataRnking(Gamedata3,url,3);
                    }else{
                        //经典榜下的好友榜

                        var url=zongRnking;
                        self.DataRnking(Gamedata6,url,6);
                    }
                }
            }, this
        );
        RankList_btn2.setPosition(winSize.width/2+230,winSize.height/2+380);
        RankList_btn2.setColor(cc.color(165,165,165));
        RankList_btn2.setScale(1.2);
        var menu = new cc.Menu(RankList_btn,RankList_btn1,RankList_btn2);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu,10);
    },
    DataRnking: function (DataArray,data_url,num) {
       var bool=true;
       var bool_num=0;
       for(var a=0;a<Xhr_num.length;a++){
           if(Xhr_num.indexOf(num)==-1){
               bool_num++;
           }
       }
       if(bool_num==0){
           bool=true;
       }else{
           bool=false;
           Xhr_num.push(num);
       }
       if(!bool&&(!DataArray||DataArray.length==0)){
           var url=data_url;
           var self=this;
           var layer=new NetLoadLayer();
           Ranklist_layer .addChild(layer,20);
           var fun= function (Array){
               if(Net_load_layer){
                   Net_load_layer.removeFromParent();
               }
               DataArray= Array["data"];
               RankList_Data=DataArray;
               self.Peopleupdate(DataArray,num);
               if(RankList_Data){
                   switch (num){
                       case 1:
                           Gamedata1=[];
                           Gamedata1=DataArray;
                           if(RankList_Data.gameRankList&&RankList_Data.gameRankList.length>0){
                               cell_num=RankList_Data.gameRankList.length;
                           }else{
                               cell_num=0;
                           }
                           break;
                       case 2:
                           Gamedata2=[];
                           Gamedata2=DataArray;

                           if(RankList_Data.gameRankList&&RankList_Data.gameRankList.length>0){
                               cell_num=RankList_Data.gameRankList.length;
                           }else{
                               cell_num=0;
                           }
                           break;
                       case 3:
                           Gamedata3=[];
                           Gamedata3=DataArray;
                           if(RankList_Data.gameRankList&&RankList_Data.gameRankList.length>0){
                               cell_num=RankList_Data.gameRankList.length;
                           }else{
                               cell_num=0;
                           }
                           break;
                       case 4:
                           Gamedata4=[];
                           Gamedata4=DataArray;
                           if(RankList_Data.gameRankList&&RankList_Data.gameRankList.length>0){
                               cell_num=RankList_Data.gameRankList.length;
                           }else{
                               cell_num=0;
                           }

                           break;
                       case 5:
                           Gamedata5=[];
                           Gamedata5=DataArray;
                           if(RankList_Data.gameRankList&&RankList_Data.gameRankList.length>0){
                               cell_num=RankList_Data.gameRankList.length;
                           }else{
                               cell_num=0;
                           }
                           break;
                       case 6:
                           Gamedata6=[];
                           Gamedata6=DataArray;
                           if(RankList_Data.gameRankList&&RankList_Data.gameRankList.length>0){
                               cell_num=RankList_Data.gameRankList.length;
                           }else{
                               cell_num=0;
                           }
                           break;
                   }
                   Ranklist_layer.CreateTableView();
               }else{
                   cell_num=0;
               }
           };
           var error_fun= function () {
               if(Net_load_layer){
                   Net_load_layer.removeFromParent();
               }
               var layer=new TishiLayer("网络异常...");
               Start_layer .addChild(layer,10);
           }
           CreateXhr(url,fun,error_fun);
       }else{
           this.Peopleupdate(DataArray,num);
           RankList_Data=DataArray;
           if(Is_PK){
               if (RankList_Data.gameRankList && RankList_Data.gameRankList.length > 0) {
                   cell_num = RankList_Data.gameRankList.length;
                   Ranklist_layer.CreateTableView();
               } else {
                   cell_num = 0;
               }
           }else {
               if (RankList_Data.gameRankList && RankList_Data.gameRankList.length > 0) {
                   cell_num = RankList_Data.gameRankList.length;
                   Ranklist_layer.CreateTableView();
               } else {
                   cell_num = 0;
               }
           }
       }
    },
    Peopleupdate: function (DataArray,num) {
       var paiming_num;
       var shuliang_num;
       if(num<=3){
           if(!DataArray.myTopStruct.rankNo){
               paiming_num=0;
           }else{
               paiming_num=DataArray.myTopStruct.rankNo;
           }
           if(!DataArray.myTopStruct.scoreValue){
               shuliang_num=0
           }else{
               shuliang_num=DataArray.myTopStruct.scoreValue;
           }
       }else{
           if(DataArray.relitiveGameRankList&&DataArray.relitiveGameRankList.length>0){
               for(var a=0;a<DataArray.relitiveGameRankList.length;a++){
                    if(DataArray.relitiveGameRankList[a].isHost){
                        paiming_num=DataArray.relitiveGameRankList[a].rankNo;
                        shuliang_num=DataArray.relitiveGameRankList[a].scoreValue;
                    }
               }
           }else if(DataArray.gameRankList&&DataArray.gameRankList.length>0){
               var gameRanklist_num=0;
               for(var a=0;a<DataArray.gameRankList.length;a++){
                   if(DataArray.gameRankList[a].isHost){
                       gameRanklist_num++;
                       paiming_num=DataArray.gameRankList[a].rankNo;
                       shuliang_num=DataArray.gameRankList[a].scoreValue;
                   }
               }
               if(gameRanklist_num==0){
                   paiming_num=0;
                   shuliang_num=0;
               }
           }else {
               paiming_num=0;
               shuliang_num=0;
           }
       }
       var paiming=paiming_num;//更改玩家的排名数
       var shuliang=shuliang_num;//更改玩家的奖牌数或者是得分数
       people_label.setString(paiming);
       people_Rankingnum.setString(shuliang);
   },
    CreateDownBtn: function () {
        var winSize=cc.winSize;
        var self=this;
        //color_str="#ddeaf0";
        leftFun= function () {
            //title_name.setColor(cc.color(0,0,0));
            if(!Is_PK){
                Is_PK=true;
                var title_str="PK榜";
                people_RankingScore.setString("奖牌数:");
                title_name.setString(title_str);
                drawNode.setVisible(false);
                drawNode1.setVisible(true);
                RankList_btn1.setColor(cc.color(255,131,98));
                RankList_btn.setColor(cc.color(165,165,165));
                RankList_btn2.setColor(cc.color(165,165,165));
                sp_xian2.setVisible(true);
                sp_xian1.setVisible(false);
                sp_xian3.setVisible(false);
                if(tableView){
                    tableView.removeFromParent();
                }
                var url=PK_zhouRnking;
                ranking_click=2;
                self.DataRnking(Gamedata2,url,2);
            }
        };
        rightFun= function () {
            if(Is_PK){
                Is_PK=false;
                var title_str="经典榜";
                people_RankingScore.setString("分数:");
                title_name.setString(title_str);
                drawNode1.setVisible(false);
                drawNode.setVisible(true);
                RankList_btn1.setColor(cc.color(255,131,98));
                RankList_btn.setColor(cc.color(165,165,165));
                RankList_btn2.setColor(cc.color(165,165,165));
                sp_xian2.setVisible(true);
                sp_xian1.setVisible(false);
                sp_xian3.setVisible(false);
                ranking_click=2;
                if(tableView){
                    tableView.removeFromParent();
                }
                var url=zhouRnking;
                self.DataRnking(Gamedata5,url,5);
            }
        };
        CreateBtn(res.left_btn1, leftFun,winSize.width/2-165,winSize.height/2-438,0.5,0.5,this);
        CreateBtn(res.right_btn1, rightFun,winSize.width/2+165,winSize.height/2-438,0.5,0.5,this);
    },
    CreateTableView: function () {
        //创建一个tableview
        for(var a=0;a<Cell_Array.length;a++){
            Cell_Array.splice(a,1);
            a--;
        }
        tableView = new cc.TableView(this, cc.size(632, 600));
        //设置tableview的滑动的方向
        //cc.SCROLLVIEW_DIRECTION_HORIZONTAL 水平
        //cc.SCROLLVIEW_DIRECTION_VERTICAL 竖直
        tableView.setDirection(cc.SCROLLVIEW_DIRECTION_VERTICAL);
        tableView.setAnchorPoint(0.5,0.5);
        tableView.x = 65;
        tableView.y = 290;
        // 设置委托
        tableView.setDelegate(this);
        //tableView填充方式  (cc.TABLEVIEW_FILL_BOTTOMUP)
        tableView.setVerticalFillOrder(cc.TABLEVIEW_FILL_TOPDOWN);
        this.addChild(tableView,3);
        //更新tableview
        tableView.reloadData();
    },
    CreateBg: function (num) {
        var winSize=cc.winSize;
        var bg=new cc.Sprite(res.bg);
        bg.setPosition(winSize.width/2,winSize.height/2+430);
        this.addChild(bg,1);
        var bg1=new cc.Sprite(res.bg1);
        bg1.setPosition(winSize.width/2,winSize.height/2-430);
        this.addChild(bg1,1);
        var winSize=cc.winSize;
        drawNode = new cc.DrawNode();
        this.addChild(drawNode,2);
        drawNode1 = new cc.DrawNode();
        this.addChild(drawNode1,2);
        var title_str;
        drawNode.drawRect(
            cc.p(winSize.width/2-340,winSize.height/2-390), // 起点
            cc.p(winSize.width/2+340,winSize.height/2+350), // 起点的对角点
            cc.hexToColor("#ede6d6"), // 填充颜色
            1, // 线粗
            cc.hexToColor("#ede6d6") // 线颜色
        );
        drawNode1.drawRect(
            cc.p(winSize.width/2-340,winSize.height/2-390), // 起点
            cc.p(winSize.width/2+340,winSize.height/2+350), // 起点的对角点
            cc.hexToColor("#ddeaf0"), // 填充颜色
            1, // 线粗
            cc.hexToColor("#ddeaf0") // 线颜色
        );
        if(Is_PK){
            title_str="PK榜";
            drawNode.setVisible(false);
        }else{
            title_str="经典榜";
            drawNode1.setVisible(false);
        }
        title_name = new cc.LabelTTF(title_str, "Arial", 40);
        title_name.x = winSize.width/2;
        title_name.y =winSize.height-100;
        title_name.setAnchorPoint(0.5,0.5);
        title_name.setColor(cc.color(0,0,0));
        this.addChild(title_name, 1);
        var self=this;
        var record_headUrl;
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }

        //record_headUrl="http://wx.qlogo.cn/mmopen/xwM5o6EmKsmEicmsaNAKgaTiaQgdKMbRgma5mMicT3vRQ6Em9bibX8icw6bXc88CT9ulTGbr0ZLEkMhHKiaOUX1S0icME1yWjgZibmE5/0"
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,220,winSize.height/2-340,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        //var name_str="飞花飘絮111111111";
        var people_name = new cc.LabelTTF(name_str, "Arial", 30);
        people_name.x =winSize.width/2-110;
        people_name.y =winSize.height/2-320;
        people_name.setAnchorPoint(0,0.5);
        people_name.setColor(cc.color(165, 165, 165));
        this.addChild(people_name, 6);

        people_RankingScore=new cc.LabelTTF("奖牌数:", "Arial", 20);
        people_RankingScore.x =winSize.width/2-103;
        people_RankingScore.y =winSize.height/2-360;
        people_RankingScore.setAnchorPoint(0,0.5);
        people_RankingScore.setColor(cc.color(165, 165, 165));
        this.addChild(people_RankingScore, 6);
        var people_num;
        if(Gamedata2.myTopStruct&&Gamedata2.myTopStruct.rankNo!=0){
            people_num=Gamedata2.myTopStruct.rankNo;
        }else{
            people_num=0;
        }
        people_label = new cc.LabelTTF("0", "Helvetica", 40);
        people_label.x = 120;
        people_label.y = winSize.height/2-340;
        people_label.color = cc.color(165, 165, 165);
        people_label.setString(people_num);
        this.addChild(people_label,6);
        if(people_num>1000&&people_num<999999){
            people_label.setScale(0.8)
        }else if(people_num>999999){
            people_label.setScale(0.6)
        }
        var people_Ranking_num;
        if(Gamedata2.myTopStruct&&Gamedata2.myTopStruct.scoreValue!=0){
            people_Ranking_num=Gamedata2.myTopStruct.scoreValue;
        }else{
            people_Ranking_num=0
        }
        people_Rankingnum= new cc.LabelTTF("0", "Helvetica", 25);
        people_Rankingnum.x = winSize.width/2-20;
        people_Rankingnum.y = winSize.height/2-360;
        people_Rankingnum.setAnchorPoint(0,0.5);
        people_Rankingnum.color = cc.color(165, 165, 165);
        people_Rankingnum.setString(people_Ranking_num);
        this.addChild(people_Rankingnum,6);
        if(people_Ranking_num>1000&&people_Ranking_num<999999){
            people_Rankingnum.setScale(0.8);
        }else if(people_num>999999){
            people_Rankingnum.setScale(0.6);
        }
    },
    //TableView继承ScrollView有这俩个方法，不需要添加任何内容
    //scrollViewDidScroll: function (view) {
    //
    //},
    //scrollViewDidZoom: function (view) {
    //},
    //设置点击cell后的回调函数
    tableCellTouched: function (table, cell) {
        //cc.log("cell touched at index: " + cell.getIdx());
        var homeLink=RankList_Data.gameRankList[cell.getIdx()].playerInfo.homeLink;
        top.window.location.href =homeLink;
    },
    //设置cell大小
    tableCellSizeForIndex: function (table, idx) {
        return cc.size(632, 110);
    },
    //添加Cell
    tableCellAtIndex: function (table, idx) {
        var self=this;
        var strValue = idx.toFixed(0);
        strValue++;
        //获得一个cell，滑动cell的时候会执行这个方法，把没有显示（没渲染）的cell拿过来，更改内容，为了减小内存的开销
        var cell = table.dequeueCell();
        var label;//排名
        var record_name;//玩家昵称
        var record_name1;//获取得分还是奖牌
        var Rangking_name;
            if (!cell) {
                cell = new CustomTableViewCell();
                cell.host=RankList_Data.gameRankList[strValue-1].host;
                cell.tag_tag=strValue-1;
                Cell_Array.push(cell);
                //添加图片
                var sprite = new cc.Sprite(res.cell);
                sprite.anchorX = 0;
                sprite.anchorY = 0;
                sprite.x = 0;
                sprite.y = 0;
                cell.addChild(sprite);
                // 添加文本
                label = new cc.LabelTTF(strValue, "Helvetica", 40);
                label.x = 50;
                label.y = 55;
                label.anchorX = 0.5;
                label.anchorY = 0.5;
                label.color = cc.color(165, 165, 165);
                label.tag = 120;
                cell.addChild(label);
                if(strValue==1){
                    this.CreateSan(cell,1)
                }else if(strValue==2){
                    this.CreateSan(cell,2)
                }else if(strValue==3){
                    this.CreateSan(cell,3)
                }
                var record_headUrl;
                var name_str;//
                var Rangking_str;//奖牌还是得分
                var Ranking_num;//奖牌还是得分的数量
                if(Is_PK){
                    var starFun= function () {
                        if(!RankList_Data.gameRankList[strValue-1].host){
                            var layer=new NetLoadLayer();
                            Ranklist_layer .addChild(layer,10);
                            var start_time=new Date().getTime();
                            var rivalType;
                            if(ranking_click==1){
                                rivalType=2;
                            }else{
                                rivalType=3;
                            }
                            var rivalUid=RankList_Data.gameRankList[cell.getIdx()].playerInfo.userId;
                            var url="/openapi/game/interactionGame/matchGameLog.json?aid="+appId+"&rivalUid="+rivalUid +"&rivalType="+rivalType+"&gameType="+0;
                            var xhr_fun= function (data) {
                                var end_time=new Date().getTime();
                                if(end_time-start_time<=10*1000){
                                    window.get=[];
                                    window.get= data["data"];
                                    if(window.get.isCanChallenge){
                                        if(window.get.interActionPlayGameLog){
                                            Gamedata1=[];
                                            Gamedata2=[];
                                            Gamedata3=[];
                                            Gamedata4=[];
                                            Gamedata5=[];
                                            Gamedata6=[];
                                            document.documentElement.style.backgroundColor = '#ffffff';
                                            document.body.style.backgroundColor = '#ffffff';
                                            cc.director.runScene(new NetWorkScene(rivalType));
                                        }else{
                                            if(Net_load_layer){
                                                Net_load_layer.removeFromParent();
                                            }
                                            var str;
                                            str="数据加载失败！";
                                            var layer=new  blackLayer(str,"确定", function () {
                                                block_layer.removeFromParent();
                                            },1);
                                            Ranklist_layer.addChild(layer,20)
                                        }

                                    }else{
                                        if(Net_load_layer){
                                            Net_load_layer.removeFromParent();
                                        }
                                        var layer=new TishiLayer("您已经挑战过!");
                                        Ranklist_layer .addChild(layer,10);
                                    }
                                }
                            };
                            var error_fun= function () {
                                self.removeFromParent();
                                if(Net_load_layer){
                                    Net_load_layer.removeFromParent();
                                }
                                var layer=new TishiLayer("网络异常...");
                                Ranklist_layer .addChild(layer,20);
                            };
                            CreateXhr(url,xhr_fun,error_fun);
                        }else{
                            if(Net_load_layer){
                                Net_load_layer.removeFromParent();
                            }
                            var layer=new TishiLayer("您不能挑战自己!");
                            Ranklist_layer .addChild(layer,20);
                        }
                        //cc.log("马上去挑战，别急...对手Id为strValue:",cell.getIdx()+1)
                    };
                    CreateBtn("res/ranking/cell_btn.png", starFun,560,55,0.5,0.5,cell,null,160);
                    if(RankList_Data.gameRankList[strValue-1].host){
                       var cell_sp=cell.getChildByTag(160);
                        cell_sp.removeFromParent();
                    }
                    Rangking_str="奖牌数:";
                }else{
                    Rangking_str="分数:";
                }
                record_headUrl=RankList_Data.gameRankList[strValue-1].playerInfo.headUrl;
                var name_str1=RankList_Data.gameRankList[strValue-1].playerInfo.nickname;
                name_str= getBytesLength(name_str1,12);
                if(strValue==7){
                    Ranking_num=RankList_Data.gameRankList[strValue-1].scoreValue;
                    if(!record_headUrl){
                        record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
                    }
                    cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
                        CreateClipperImage(record_headUrl,130,55,80,cell,121);
                    });
                }


                record_name = new cc.LabelTTF(name_str, "Arial", 30);
                record_name.x = 200;
                record_name.y =70;
                record_name.tag=122;
                record_name.setAnchorPoint(0,0.5);
                record_name.setColor(cc.color(255,112,143));
                cell.addChild(record_name, 5);

                record_name1= new cc.LabelTTF(Rangking_str, "Arial", 20);
                record_name1.x = 200;
                record_name1.y =30;
                record_name1.tag=123;
                record_name1.setAnchorPoint(0,0.5);
                record_name1.setColor(cc.color(255,112,143));
                cell.addChild(record_name1, 5);

                Rangking_name = new cc.LabelTTF(Ranking_num, "Arial", 20);
                Rangking_name.x = 280;
                Rangking_name.y =30;
                Rangking_name.tag=124;
                Rangking_name.setAnchorPoint(0,0.5);
                Rangking_name.setColor(cc.color(255,112,143));
                cell.addChild(Rangking_name, 5);

            } else {
                //更改文本信息
                //this.CreatePeople(cell,strValue);
                label = cell.getChildByTag(120);
                label.setString(strValue);

                if(strValue==1){
                    var labelsp = cell.getChildByTag(161);
                    if(labelsp){
                    }else{
                        this.CreateSan(cell,1)
                    }
                }else if(strValue==2){
                    var labelsp = cell.getChildByTag(162);
                    if(labelsp){
                    }else{
                        this.CreateSan(cell,2)
                    }
                }else if(strValue==3){
                    var labelsp = cell.getChildByTag(163);
                    if(labelsp){
                    }else{
                        this.CreateSan(cell,3)
                    }
                }else{
                    var labelsp = cell.getChildByTag(161);
                    if(labelsp){
                        labelsp.removeFromParent();
                    }
                    var labelsp1 = cell.getChildByTag(162);
                    if(labelsp1){
                        labelsp1.removeFromParent();
                    }
                    var labelsp2 = cell.getChildByTag(163);
                   if(labelsp2){
                       labelsp2.removeFromParent();
                   }
                }
                if(Is_PK){
                    var cell_sp=cell.getChildByTag(160);
                    if(cell_sp&&RankList_Data.gameRankList[strValue-1].host){
                        cell_sp.removeFromParent();
                    }else if(!cell_sp&&!RankList_Data.gameRankList[strValue-1].host){
                        var starFun= function () {
                            if(!RankList_Data.gameRankList[strValue-1].host){
                                var layer=new NetLoadLayer();
                                Ranklist_layer .addChild(layer,10);
                                var start_time=new Date().getTime();
                                var rivalType;
                                if(ranking_click==1){
                                    rivalType=2;
                                }else{
                                    rivalType=3;
                                }
                                var rivalUid=RankList_Data.gameRankList[cell.getIdx()].playerInfo.userId;
                                var url="/openapi/game/interactionGame/matchGameLog.json?aid="+appId+"&rivalUid="+rivalUid +"&rivalType="+rivalType+"&gameType="+0;
                                var xhr_fun= function (data) {
                                    var end_time=new Date().getTime();
                                    if(end_time-start_time<=10*1000){
                                        window.get=[];
                                        window.get= data["data"];
                                        if(window.get.isCanChallenge){
                                            if(window.get.interActionPlayGameLog){
                                                Gamedata1=[];
                                                Gamedata2=[];
                                                Gamedata3=[];
                                                Gamedata4=[];
                                                Gamedata5=[];
                                                Gamedata6=[];
                                                document.documentElement.style.backgroundColor = '#ffffff';
                                                document.body.style.backgroundColor = '#ffffff';
                                                cc.director.runScene(new NetWorkScene(rivalType));
                                            }else{
                                                if(Net_load_layer){
                                                    Net_load_layer.removeFromParent();
                                                }
                                                var str;
                                                str="数据加载失败！";
                                                var layer=new  blackLayer(str,"确定", function () {
                                                    block_layer.removeFromParent();
                                                },1);
                                                Ranklist_layer.addChild(layer,20);
                                            }
                                        }else{
                                            if(Net_load_layer){
                                                Net_load_layer.removeFromParent();
                                            }
                                            var layer=new TishiLayer("您已经挑战过!");
                                            Ranklist_layer .addChild(layer,10);
                                        }

                                    }
                                };
                                var error_fun= function () {
                                    self.removeFromParent();
                                    if(Net_load_layer){
                                        Net_load_layer.removeFromParent();
                                    }
                                    var layer=new TishiLayer("网络异常...");
                                    Ranklist_layer .addChild(layer,20);
                                };
                                CreateXhr(url,xhr_fun,error_fun);
                            }else{
                                if(Net_load_layer){
                                    Net_load_layer.removeFromParent();
                                }
                                var layer=new TishiLayer("您不能挑战自己!");
                                Ranklist_layer .addChild(layer,20);
                            }
                            //cc.log("马上去挑战，别急...对手Id为strValue:",cell.getIdx()+1)
                        };
                        CreateBtn("res/ranking/cell_btn.png", starFun,560,55,0.5,0.5,cell,null,160);
                    }

                }
              /*    表示玩家昵称    */
                record_name=cell.getChildByTag(122);
                var name_str1;
                name_str1=RankList_Data.gameRankList[strValue-1].playerInfo.nickname;
                var name_str= getBytesLength(name_str1,12);
                record_name.setString(name_str);
                /*   表示玩家奖牌数或者是得分   */
                var Rangking_str;
                if(Is_PK){
                    Rangking_str="奖牌数:";
                }else{
                    Rangking_str="分数:";
                }
                Ranking_num=RankList_Data.gameRankList[strValue-1].scoreValue;
                record_name1=cell.getChildByTag(123);
                record_name1.setString(Rangking_str);
                /*   表示奖牌或   */
                Rangking_name = cell.getChildByTag(124);
                Rangking_name.setString(Ranking_num);
                /*   表示玩家头像   */
                var sp=cell.getChildByTag(121);
                if(sp){
                    var IS_YES=true;
                    do{
                        sp=cell.getChildByTag(121);
                        if(!sp){
                            var record_headUrl=RankList_Data.gameRankList[strValue-1].playerInfo.headUrl;
                            if(!record_headUrl){
                                record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
                            }
                            cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
                                CreateClipperImage(record_headUrl,130,55,80,cell,121);
                            });
                            sp=cell.getChildByTag(121);
                            IS_YES=false;
                            break;
                        }else{
                            sp.removeFromParent();
                        }
                    }while(IS_YES);
                }else{
                    var record_headUrl=RankList_Data.gameRankList[strValue-1].playerInfo.headUrl;
                    if(!record_headUrl){
                        record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
                    }
                    cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
                        CreateClipperImage(record_headUrl,130,55,80,cell,121);
                    });
                }
            }
        return cell;
   },
   CreatePeople: function (cell,num) {
        var record_headUrl;
        record_headUrl=RankList_Data.gameRankList[num-1].playerInfo.headUrl;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,130,55,80,cell,121);
        });
    },
    CreateSan: function (cell,num) {
        var sp=new cc.Sprite("res/ranking/sp"+num+".png");
        sp.x=50;
        sp.y=50;
        sp.tag=160+num;
        cell.addChild(sp,2);
    },

    //设置cell个数
   numberOfCellsInTableView: function (table) {
        return cell_num;
   }
});
var RinkingScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new RankList();
        this.addChild(layer);
    }
});
