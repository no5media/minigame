/**
 * A brief explanation for "project.json":
 * Here is the content of project.json file, this is the global configuration for your game, you can modify it to customize some behavior.
 * The detail of each field is under it.
 {
    "project_type": "javascript",
    // "project_type" indicate the program language of your project, you can ignore this field

    "debugMode"     : 1,
    // "debugMode" possible values :
    //      0 - No message will be printed.
    //      1 - cc.error, cc.assert, cc.warn, cc.log will print in console.
    //      2 - cc.error, cc.assert, cc.warn will print in console.
    //      3 - cc.error, cc.assert will print in console.
    //      4 - cc.error, cc.assert, cc.warn, cc.log will print on canvas, available only on web.
    //      5 - cc.error, cc.assert, cc.warn will print on canvas, available only on web.
    //      6 - cc.error, cc.assert will print on canvas, available only on web.

    "showFPS"       : true,
    // Left bottom corner fps information will show when "showFPS" equals true, otherwise it will be hide.

    "frameRate"     : 60,
    // "frameRate" set the wanted frame rate for your game, but the real fps depends on your game implementation and the running environment.

    "noCache"       : false,
    // "noCache" set whether your resources will be loaded with a timestamp suffix in the url.
    // In this way, your resources will be force updated even if the browser holds a cache of it.
    // It's very useful for mobile browser debuging.

    "id"            : "gameCanvas",
    // "gameCanvas" sets the id of your canvas element on the web page, it's useful only on web.

    "renderMode"    : 0,
    // "renderMode" sets the renderer type, only useful on web :
    //      0 - Automatically chosen by engine
    //      1 - Forced to use canvas renderer
    //      2 - Forced to use WebGL renderer, but this will be ignored on mobile browsers

    "engineDir"     : "frameworks/cocos2d-html5/",
    // In debug mode, if you use the whole engine to develop your game, you should specify its relative path with "engineDir",
    // but if you are using a single engine file, you can ignore it.

    "modules"       : ["cocos2d"],
    // "modules" defines which modules you will need in your game, it's useful only on web,
    // using this can greatly reduce your game's resource size, and the cocos console tool can package your game with only the modules you set.
    // For details about modules definitions, you can refer to "../../frameworks/cocos2d-html5/modulesConfig.json".

    "jsList"        : [
    ]
    // "jsList" sets the list of js files in your game.
 }
 *
 */

cc.game.onStart = function(){
    if(!cc.sys.isNative && document.getElementById("cocosLoading")) //If referenced loading.js, please remove it
        document.body.removeChild(document.getElementById("cocosLoading"));
    cc.view.enableAutoFullScreen(false);
    // Pass true to enable retina display, on Android disabled by default to improve performance
    //cc.view.enableRetina(cc.sys.os === cc.sys.OS_IOS ? true : false);
    cc.view.enableRetina(true);
    // Adjust viewport meta
    cc.view.adjustViewPort(true);
    // Setup the resolution policy and design resolution size
    cc.view.setDesignResolutionSize(768,1130, cc.ResolutionPolicy.SHOW_ALL);
    // Instead of set design resolution, you can also set the real pixel resolution size
    // Uncomment the following line and delete the previous line.
    // cc.view.setRealPixelResolution(960, 640, cc.ResolutionPolicy.SHOW_ALL);
    // The game will be resized when browser size change
    cc.view.resizeWithBrowserSize(true);
    //load resources
    var xhr = cc.loader.getXMLHttpRequest();
    var self=this;
    function GetQueryString(name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
        var r = window.location.search.substr(1).match(reg);
        if (r!=null) return (r[2]); return null;
    }
    var rivalGid=GetQueryString("rivalGid");
    var rivalType=GetQueryString("rivalType");
    var gamePushId=GetQueryString("gamePushId");
    var  url;
    if(rivalGid&&rivalType){
          url="/openapi/game/interactionGame/index.json?gameConfigId=5&aid="+114558+"&rivalGid="+rivalGid+"&rivalType="+rivalType+"&gamePushId="+gamePushId ;
    }else{
          url="/openapi/game/interactionGame/index.json?gameConfigId=5&aid="+114558;
    }
    xhr.open("POST", url,true);
    xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4){
            if(xhr.status >= 200 && xhr.status <= 207){
                var resw = xhr.responseText;
                var jsondata = JSON.parse(resw);
                var data = jsondata["data"];
                if(jsondata.result==1&&data.code==100){
                    //cc.log(data.url);
                    top.window.location.href =data.url;
                }else{
                    cc.LoaderScene.preload(g_resources, function () {
                        cc.sys.localStorage.setItem("dwjyAngin",0);
                        var self=this;
                        window.select=[];
                        window.get=[];
                        window.select=data;
                        var game_type;
                        cc.log("rivalType",rivalType);
                        if(rivalType==1){
                            game_type=1;//复仇；
                            cc.director.runScene(new GameStartScene(game_type));
                        }else {
                            game_type=0;//经典匹配
                            if(window.select.isNewUser){
                                cc.director.runScene(new GuiderScene(game_type));
                            }else{
                                cc.director.runScene(new GameStartScene(game_type));
                            }
                        }

                    }, this);
                }
            }else{
                alert("服务器开小差了，请重试");
            }
        }
    };
    xhr.send();
    //cc.LoaderScene.preload(g_resources, function () {
    //    cc.sys.localStorage.setItem(appId,0);
    //    var self=this;
    //    window.document.title="这游戏太刺激，只有1%的人能玩到50000分!";
    //    //cc.director.runScene(new GameStartScene(1));
    //    cc.director.runScene(new HelloWorldScene());
    //
    //}, this);

};
cc.game.run();