Score=0
Main_layer=null;
Is_Angin=false;
Is_overOrplay=false
var GameMainLayer=cc.LayerColor.extend({
    Sprite_Array:null,
    ctor: function () {
        this._super(cc.color(231,237,239));
        Score=0;
        time=0;
        clock_time=40;
        Sprite_Array=[];
        Main_layer=this;
        Is_Angin=false;;
        Is_overOrplay=false;
        var self=this;
        Is_Click=true;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=new cc.Rect(touch.getLocation().x,touch.getLocation().y,1,1)
                if(cc.rectIntersectsRect(rect,self.leftSp1.getBoundingBox())){
                    self.leftSp1.setVisible(false);
                    self.leftSp2.setVisible(true);
                    self.Left_fun()
                }
                if(cc.rectIntersectsRect(rect,self.rightSp1.getBoundingBox())){
                    self.rightSp1.setVisible(false);
                    self.rightSp2.setVisible(true);
                    self.Right_fun()
                }
                return true;
            },
            onTouchEnded:function(touch,event){
                self.leftSp1.setVisible(true);
                self.leftSp2.setVisible(false);
                self.rightSp1.setVisible(true);
                self.rightSp2.setVisible(false);
            }
        });
        cc.eventManager.addListener(listener, this);
        this.CreateBg();
        this.CreatePeopleInformation();
        this.CreateSprite();
        this.DrawBg();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt) {
        time+=dt;
        if(time>=1){
            time=0;
            clock_time--;
            timeclock.setString(clock_time);
            if(clock_time==0){
                var angin_num=cc.sys.localStorage.getItem("zysAngin")
                cc.director.pause();
                if(angin_num<3){
                    var layer=new AnginLayer(1);
                    this.addChild(layer,10);
                }else{
                    cc.director.resume()
                    this.unscheduleAllCallbacks();
                    cc.eventManager.removeAllListeners();
                    this.GameOver();
                    return
                }
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false;
            clock_time=5;
            getspScore.setString( window.select.userPoint)
            timeclock.setString(clock_time);
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return;
        }
    },
    Left_fun: function () {
        if(Sprite_Array[0]&&Sprite_Array[0].tag==1){
            this.CreateAnginSprite();
            Is_Click=true;
            Score+=100;
            getScore.setString(Score)
        }else{
            Score-=150;
            if(Score<0){
                Score=0
            }
            getScore.setString(Score);
            this.CreateError(1);
            this.CreateAnginSprite();
            Is_Click=true;
        }

    },
    Right_fun: function () {
        if(Sprite_Array[0]&&Sprite_Array[0].tag==2){
            this.CreateAnginSprite();
            Score+=100;
            getScore.setString(Score);
            Is_Click=true;
        }else{
            Score-=150;
            if(Score<0){
                Score=0
            }
            getScore.setString(Score);
            this.CreateAnginSprite();
            this.CreateError(2);
            Is_Click=true
        }
    },
    CreateError: function (m) {
        var winSize=cc.winSize;
        var layer=new cc.LayerColor(cc.color(225,50,120,60));
        this.addChild(layer)
        var blink =new cc.Blink(0.3,2);//闪烁函数
        var sp=new cc.Sprite(res.error_btn);
        if(m==1){
            sp.setPosition(winSize.width/2-155,170)
        }else{
            sp.setPosition(winSize.width/2+155,170)
        }
        this.addChild(sp,10);
        var destory = new cc.RemoveSelf(true);
        var fun=cc.callFunc(function () {
            sp.removeFromParent()
        })
        layer.runAction(cc.repeatForever(cc.sequence(blink,blink,destory,fun)))
        //layer.runAction(cc.repeatForever(cc.sequence(blink)))

    },
    CreateAnginSprite: function () {
        Sprite_Array[0].removeFromParent()
        Sprite_Array.splice(0,1);
        this.CreateSprite()
    },
    DrawBg: function () {
        var winSize=cc.winSize;
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,5);
        drawNode.drawRect(
            cc.p(0,295), // 起点
            cc.p(winSize.width,350), // 起点的对角点
            cc.color(255,255, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(winSize.width,winSize.height), // 起点
            cc.p(0,winSize.height-160), // 起点的对角点
            cc.color(6,183, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(6,183, 255, 255) // 线颜色
        );

    },
    CreateSprite: function () {
        var winSize=cc.winSize;
        var num=Math.floor(Math.random()*14)
        var sp=new cc.Sprite(g_resources[num]);
        sp.setPosition(winSize.width/2,winSize.height/2+100);
        this.addChild(sp);
        var sp_rotation=Math.floor(Math.random()*4);
        sp.rotation=sp_rotation*90;
        if(num<7){
            sp.tag=1
        }else{
            sp.tag=2
        }
        Sprite_Array.push(sp);
    },
    CreateBg: function () {
        var winSize=cc.winSize;
        this.leftSp1=new cc.Sprite(res.left_btn1);
        this.leftSp1.setPosition(winSize.width/2-155,170);
        this.addChild(this.leftSp1,5);
        this.leftSp2=new cc.Sprite(res.left_btn2);
        this.leftSp2.setPosition(winSize.width/2-155,170);
        this.addChild(this.leftSp2,5);
        this.leftSp2.setVisible(false);
        this.rightSp1=new cc.Sprite(res.right_btn1);
        this.rightSp1.setPosition(winSize.width/2+155,170);
        this.addChild(this.rightSp1,5);
        this.rightSp2=new cc.Sprite(res.right_btn2);
        this.rightSp2.setPosition(winSize.width/2+155,170);
        this.addChild(this.rightSp2,5);
        this.rightSp2.setVisible(false);
        var tishi_str="提示 : "
        var tishi = new cc.LabelTTF(tishi_str, "Arial", 30);
        tishi.x = 25;
        tishi.y = 320;
        tishi.setAnchorPoint(0,0.5)
        tishi.setColor(cc.color(225,50,120))
        this.addChild(tishi, 6);
        var tishi_str1="这是左手还是右手呢？"
        var tishi1 = new cc.LabelTTF(tishi_str1, "Arial", 30);
        tishi1.x = 120;
        tishi1.y = 320;
        tishi1.setAnchorPoint(0,0.5)
        tishi1.setColor(cc.color(0,0,0))
        this.addChild(tishi1, 6);
        var time_str="倒计时:"
        var timeName = new cc.LabelTTF(time_str, "Arial", 20);
        timeName.x = 170;
        timeName.y = winSize.height-130;
        timeName.setColor(cc.color(255,255,255))
        this.addChild(timeName, 6);
        timeclock=new cc.LabelAtlas("40", "res/num.png", 20, 29,"0");
        timeclock.setPosition(210, winSize.height-140);
        this.addChild(timeclock,10);
        getScore=new cc.LabelAtlas("0", "res/num.png", 20, 29,"0");
        getScore.setPosition(winSize.width-80, winSize.height-130);
        getScore.setAnchorPoint(0.5,0.5)
        this.addChild(getScore,10);

    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-70)
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score,10);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname
        var name_str= getBytesLength(name_str1,12)
        //var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,255,255))
        this.addChild(record_name, 10);
         var get_score=window.select.userPoint
        //var get_score=10
         getspScore = new cc.LabelTTF("0", "Arial", 30);
        getspScore.x = winSize.width-70;
        getspScore.y =winSize.height-70;
        getspScore.setAnchorPoint(0.5,0.5)
        getspScore.setColor(cc.color(254,202,75));
        getspScore.setString(get_score)
        this.addChild(getspScore, 10);

    },
    GameOver: function () {
        var winSize=cc.winSize
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners()
        var dely=cc.delayTime(2.5);
        var sp=new cc.Sprite(res.bg);
        sp.setPosition(winSize.width/2,winSize.height/2);
        sp.setScale(0.1);
        var fun=cc.callFunc(function () {
            sp.removeFromParent()
            var transition=new cc.TransitionProgressRadialCW(1,new GameOverScene());
            cc.director.runScene(transition);
        })
        var fd = new cc.scaleTo(0.1,1);
        this.addChild(sp,12)
        sp.runAction(cc.sequence(fd,dely,fun));
    }
})

var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})