Score=0
Main_layer=null
Is_Angin=false;
Is_overOrplay=false
var GameMainLayer=cc.LayerColor.extend({
    sp_speed:null,
    ctor: function () {
        this._super(cc.color(255,255,255));
        Score=0;
        Main_layer=this;
        this.CreateBg();
        Sprite_Array=[];
        Is_clockwise=true;
        Is_click=false;
        Is_Angin=false;
        Is_overOrplay=false;
        sp_rotation=0;
        speed=3;
        this.CreatePeopleInformation();
        var index=Math.floor(Math.random()*2);
        var rota=0
        if(index==1){
            Is_clockwise=true;
            rota=0
        }else{
            Is_clockwise=false;
            rota=360
        }
        var num=this.CretaeNum(1)-1
        start_rotation=this.getMinRotation(num+1);
        end_rotation=this.getMaxRotation(num+1);
        this.CreteSprite(num,rota);
        Is_check=false;
        Is_check1=false;
        Is_Create=false;
        var self=this;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                Score+=1
                SpScore.setString(Score)
                Is_check=0
                Is_Create=true;
                if(Is_clockwise){
                    Is_clockwise=false;
                   self.CreateClick()
                }else{
                    Is_clockwise=true;
                    self.CreateClick()
                }
                return true;
            },
            onTouchEnded:function(touch,event){

            }
        });
        cc.eventManager.addListener(listener, this);
        this.scheduleUpdate()
        return true;
    },
    update: function () {
        if(Score>=5&&Score<10){
            speed=4
        }else if(Score>=10&&Score<20){
            speed=5
        }else if(Score>=20&&Score<30){
            speed=6
        }else if(Score>=30&&Score<40){
            speed=8
        }else if(Score>=40){
            speed=10
        }
        if(Is_clockwise){
            sp_speed=speed
        }else{
            sp_speed=-speed
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_check=0;
            Is_check1=0;
            Is_Create=true;
            if(Is_clockwise){
                Is_clockwise=false;
                this.CreateClick();
            }else{
                Is_clockwise=true;
                this.CreateClick();
            }
            Is_Angin=false;
            getScore.setString(window.select.userPoint)
            getSpScore.setString(window.select.userPoint)

        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return
        }
        if(Sprite_Array[0]){
            var rota=Sprite_Array[0].rotation
            if(Sprite_Array[0].tag==1){
                if((rota>=315&&rota<=360)||(rota>=0&&rota<=45)){
                    Is_check=1;
                    Is_check1=1;
                }else{
                    Is_check=0;
                }
            }else{
                if(Sprite_Array[0].rotation>end_rotation){
                    Is_check=0;
                }else if(Sprite_Array[0].rotation<=end_rotation&&Sprite_Array[0].rotation>=start_rotation){
                    Is_check=1;
                    Is_check1=1;
                }else if(Sprite_Array[0].rotation<start_rotation){
                    Is_check=0;
                }
            }
            if(Is_Create){
                Is_Create=false;
                Is_check1=0;
            }
            if(Is_check!=Is_check1){
                 cc.log("a",Is_check1,Is_check,Is_Create)
                this.AnginGame();
            }
            Sprite_Array[0].rotation+=sp_speed;
            if( Sprite_Array[0].rotation>=360){
                Sprite_Array[0].rotation=Sprite_Array[0].rotation-360;
            }else if(Sprite_Array[0].rotation<0){
                Sprite_Array[0].rotation=360+Sprite_Array[0].rotation;
            }
        }

    },
    getMinRotation: function (tag) {
        if(tag==1){
            var num=315;
           return num
        }else{
            var min=45+90*(tag-2);
            return min
        }
    },
    getMaxRotation: function (tag) {
        if(tag==1){
            var num=45;
            return num;
        }else{
            var max=45+90*(tag-2)+90
            return max
        }
    },
    CreateClick: function () {
        var bool=this.CheckSprite();
        if(bool==1){
            var num=this.CretaeNum(Sprite_Array[0].tag)-1
            start_rotation=this.getMinRotation(num+1);
            end_rotation=this.getMaxRotation(num+1);
            var rotation=Sprite_Array[0].rotation;
            Sprite_Array[0].removeFromParent();
            Sprite_Array.splice(0,1);
            this.CreteSprite(num,rotation)
        }else{
            if(Is_Angin){
                var rota=Sprite_Array[0].rotation;
                var sp_tag
                if((rota>315&&rota<=360)||(rota>=0&&rota<=45)){
                    sp_tag=1
                }else if(rota>45&&rota<=135){
                    sp_tag=2;
                }else if(rota>135&&rota<=225){
                    sp_tag=3;
                }else if(rota>225&&rota<=315){
                    sp_tag=4;
                }
                var num=this.CretaeNum1(sp_tag)-1;
                start_rotation=this.getMinRotation(num+1);
                end_rotation=this.getMaxRotation(num+1);
                var rotation=Sprite_Array[0].rotation;
                Sprite_Array[0].removeFromParent();
                Sprite_Array.splice(0,1);
                this.CreteSprite(num,rotation)
            }else{
                this.AnginGame();
            }
        }
    },
    CheckSprite: function () {
        if(Sprite_Array[0].tag==1){
            if((Sprite_Array[0].rotation>=0&&Sprite_Array[0].rotation<=45)||(Sprite_Array[0].rotation>=315&&Sprite_Array[0].rotation<=360)){
                return 1;
            }else{
                return 0;
            }
        }else{
            var min=45+90*(Sprite_Array[0].tag-2)
            var Max=min+90
            if(Sprite_Array[0].rotation>=min&&Sprite_Array[0].rotation<=Max){
                return 1;
            }else{
                return 0;
            }
        }
    },
    CretaeNum: function (num) {
        var num_array=[1,2,3,4];
        for(var a=0;a<num_array.length;a++){
            if(num_array[a]==num){
                num_array.splice(a,1);
            }
        }
        var m=Math.floor(Math.random()*3);
        return num_array[m];
    },
    CretaeNum1: function (num) {
        if(num==1){
            return 3;
        }else if(num==2){
            return 4;
        }else if(num==3){
            return 1;
        }else if(num==4){
            return 2;
        }

    },
    CreteSprite: function (num,rota) {
        var winSize=cc.winSize;
        var sp=new cc.Sprite(g_resources[num]);
        sp.setPosition(winSize.width/2,(winSize.height-250)/2);
        sp.setAnchorPoint(0.5,0.1);
        sp.rotation=rota
        Sprite_Array.push(sp);
        sp.tag=num+1
        this.addChild(sp);
    },
    CreateBg: function () {
        var winSize=cc.winSize;
        var bg=new cc.Sprite(res.bg);
        bg.setPosition(winSize.width/2,(winSize.height-250)/2);
        this.addChild(bg);
        SpScore = new cc.LabelTTF("0", "Arial", 80);
        SpScore.x = winSize.width/2;
        SpScore.y =winSize.height-250;
        SpScore.setAnchorPoint(0.5,0.5)
        SpScore.setColor(cc.color(254,202,75));
        SpScore.setString(Score)
        this.addChild(SpScore, 1);
    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-70)
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname
        var name_str= getBytesLength(name_str1,12)
        //var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);
        var get_score=window.select.userPoint
        //var get_score=10
         getSpScore = new cc.LabelTTF("0", "Arial", 30);
        getSpScore.x = winSize.width-70;
        getSpScore.y =winSize.height-70;
        getSpScore.setAnchorPoint(0.5,0.5)
        getSpScore.setColor(cc.color(254,202,75));
        getSpScore.setString(get_score)
        this.addChild(getSpScore, 1);

    },
    AnginGame: function () {
        var angin_num=cc.sys.localStorage.getItem("wxszAngin")
        if(angin_num<3){
            cc.director.pause();
            var layer=new AnginLayer(1);
            this.addChild(layer,10);
        }else{
            cc.director.resume();
            Is_overOrplay=true;
        }

    },
    GameOver: function () {
        var dely=cc.delayTime(0.07);
        var fun=cc.callFunc(function () {
            var transition=new cc.TransitionMoveInT(1,new GameOverScene());
            cc.director.runScene(transition);
        })
        var chandong = cc.sequence(cc.moveBy(0.01, 3, 0), cc.moveBy(0.01, 0, 3), cc.moveBy(0.02, -6, 0), cc.moveBy(0.01, 0, -6), cc.moveBy(0.01, 3, 0), cc.moveBy(0.01, 0, 3))
        this.runAction(cc.sequence(chandong,dely,fun));
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners()
    }
})

var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})