Is_jifen=null;
start_layer=null;
var GameStartLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        Is_jifen=false;
        start_layer=this
        this.Createbg();
        return true;
    },
    Createbg: function () {
        var point_num=window.select.newUserAwardPoint
        //var point_num=10
        var Is_NewUser=window.select.isNewUser
        //var Is_NewUser=false
        if(Is_NewUser){
            window.select.isNewUser=false
            var layer=new  fristLayer();
            this.addChild(layer,10)
        }
        var winSize=cc.winSize
        var  startbg1_x=winSize.width/2;
        var  startbg1_y=winSize.height-220;
        var bg=new cc.Sprite(res.start_bg);
        bg.setPosition(winSize.width/2,winSize.height/2)
        this.addChild(bg);
        var bg1 = new cc.Sprite(res.start_bg1);
        bg1.setPosition(winSize.width / 2, winSize.height / 2+250);
        this.addChild(bg1);
        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-70)
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score);
        var startbg1=new cc.Sprite(res.start_bg1)
        startbg1.setPosition(startbg1_x,startbg1_y);
        startbg1.setAnchorPoint(0.5,0.5);
        //this.addChild(startbg1);

        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }


        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname
        var name_str= getBytesLength(name_str1,12)
        //var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);
        gameNum1=window.select.newUserAwardPoint;
        gameNum2=window.select.userPoint;
        //gameNum1=5;
        //gameNum2=5;
        cc.log("aaa",gameNum1,gameNum2);
        Game_BestScore=gameNum1+gameNum2//积分总数
        getScore = new cc.LabelTTF("0", "Arial", 30);
        getScore.x = winSize.width-70;
        getScore.y =winSize.height-70;
        getScore.setAnchorPoint(0.5,0.5);
        getScore.setColor(cc.color(254,202,75));
        getScore.setString(gameNum2)
        this.addChild(getScore, 1);

        CreateBtn(res.start_btn1, function () {
            //经典模式
            cc.director.runScene(new GameMainScene())
        },winSize.width/2,490,0.5,0.5,this)
        CreateBtn(res.start_btn2, function () {
            //匹配模式
            var layer=new NetMainLayer();
            start_layer.addChild(layer,10);
            var url="/openapi/game/interactionGame/matchGameLog.json?aid="+appId+"&gameType=0"
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.open("POST", url,true);
            xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
            var self=this;
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4){
                    if(xhr.status >= 200 && xhr.status <= 207){
                        var resw = xhr.responseText;
                        var jsondata = JSON.parse(resw);
                        window.get=[];
                        window.get= jsondata["data"];
                        cc.director.runScene(new NetWorkScene());
                        cc.log("window.get",window.get)

                    }else{
                        alert("网络异常，请重试")
                    }
                }
            }
            //xhr.send();
        },winSize.width/2,320,0.5,0.5,this)
        CreateBtn(res.follow, function () {
            //关注
            showEWM()
        },82.5,150,0.5,0.5,this)
        //CreateBtn("res/start/start_ranklist.png", function () {
        //    //排行榜
        //},winSize.width/2-100,200,0.5,0.5,this)
        CreateBtn(res.moregame, function () {
            //更多游戏
            top.window.location.href ="http://wx.1758.com/play/gamebox/123?ex1758=1&tp=full&yn=小游戏&title=小游戏";
        },winSize.width/2,150,0.5,0.5,this)
        CreateBtn(res.invite, function () {
            //邀请
            cc.log("邀请")
            var share = document.getElementById('share-square');
            share.style.display = 'block';
        },winSize.width-82.5,150,0.5,0.5,this)
        var startlogo=new cc.Sprite("res/start_logo.png")
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
        this.scheduleUpdate();
    },
    update: function (dt) {
        if(Is_jifen){
            time+=dt;
            if(time>dt){
                time=0;
                gameNum2++;
                if(gameNum2>=Game_BestScore){
                    gameNum2=Game_BestScore
                }
                getScore.setString(gameNum2);
                if(gameNum2>=10000&&gameNum2<1000000){
                    getScore.setScale(0.8)
                }else if(gameNum2>=1000000){
                    getScore.setScale(0.6)
                }
            }
        }
    }
})
var GameStartScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameStartLayer();
        this.addChild(layer);
    }
})