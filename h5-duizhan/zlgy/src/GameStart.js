start_layer=null;
var GameStartLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        this._super();
        var size = cc.winSize;
        start_layer=this;
        //var bg=new cc.Sprite("res/start_bg.jpg");
        //bg.setPosition(size.width/2,size.height/2);
        //this.addChild(bg)
        //var btn1 = new cc.MenuItemImage(
        //    "res/start_btn.png",
        //    "res/start_btn.png",
        //    function () {
        //        var Is_first=cc.sys.localStorage.getItem("zzw");
        //        if(Is_first){
        //            cc.director.runScene(new GamplayScene());
        //        }else{
        //            cc.director.runScene(new NewGuiderScene());
        //        }
        //    }, this
        //)
        //var fadin=cc.fadeIn(0.5,200)
        //var fadout=cc.fadeOut(0.5,0)
        //btn1.runAction(cc.repeatForever(cc.sequence(fadout,fadin)))
        //btn1.setPosition(size.width/2,300)
        //var menu = new cc.Menu(btn1);
        //menu.x = 0;
        //menu.y = 0;
        //this.addChild(menu);
        Is_jifen=false;
        this.CreateBg()
        return true;
    },
    CreateBg: function () {
        var winSize=cc.winSize;
        var point_num=window.select.newUserAwardPoint
        var bg=new cc.Sprite(res.startbg);
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg)

        //var point_num=10
        var Is_NewUser=window.select.isNewUser
        //var Is_NewUser=false
        if(Is_NewUser){
            window.select.isNewUser=false
            var layer=new  fristLayer();
            this.addChild(layer,10)
        }

        var  startbg_x=winSize.width/2;
        var  startbg_y=winSize.height/2;
        var  startbg1_x=winSize.width/2;
        var  startbg1_y=winSize.height-220;

        var start_score=new cc.Sprite(res.Score)
        start_score.setPosition(winSize.width,winSize.height-70)
        start_score.setScale(0.8)
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score);
        var start_bg1=new cc.Sprite(res.start_bg1)
        start_bg1.setPosition(winSize.width/2,winSize.height-300)
        //start_bg1.setScale(0.8);
        this.addChild(start_bg1);

        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }


        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,40,winSize.height-70,40,self);
        });
        var name_str1=window.select.userProfile.nickname
        var name_str= getBytesLength(name_str1,12)
        //var name_str="李乾@1758"
        var record_name = new cc.LabelTTF(name_str, "Arial", 20);
        record_name.x = 70;
        record_name.y =winSize.height-70;
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);
        gameNum1=window.select.newUserAwardPoint;
        gameNum2=window.select.userPoint;
        //gameNum1=5;
        //gameNum2=5;
        cc.log("aaa",gameNum1,gameNum2)
        Game_BestScore=gameNum1+gameNum2//积分总数
        getScore = new cc.LabelTTF("0", "Arial", 26);
        getScore.x = winSize.width-60
        getScore.y =winSize.height-72;
        getScore.setAnchorPoint(0.5,0.5)
        getScore.setColor(cc.color(88,195,224));
        getScore.setString(gameNum2)
        this.addChild(getScore, 1);
        if(gameNum2>=10000&&gameNum2<1000000){
            getScore.setScale(0.8)
        }else if(gameNum2>=1000000){
            getScore.setScale(0.6)
        }
        CreateBtn(res.start_btn1,res.start_btn1, function () {
            //经典模式
            cc.director.runScene(new GamplayScene())
        },winSize.width/2,450,0.5,0.5,5,this,1)
        CreateBtn(res.start_btn2,res.start_btn2, function () {
            //匹配模式
            var layer=new NetMainLayer();
            start_layer.addChild(layer,10)
            //alert("敬请期待")
            var url="/openapi/game/interactionGame/matchGameLog.json?aid="+appId+"&gameType=0"
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.open("POST", url,true);
            xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
            var self=this;
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4){
                    if(xhr.status >= 200 && xhr.status <= 207){
                        var resw = xhr.responseText;
                        var jsondata = JSON.parse(resw);
                        window.get=[];
                        window.get= jsondata["data"]
                        cc.log("window.get",window.get)

                    }else{
                        alert("网络异常，请重试")
                    }
                }
            }
            //xhr.send();
        },winSize.width/2,300,0.5,0.5,5,this,1)
        CreateBtn(res.follow,res.follow, function () {
            //关注
            showEWM()
        },100,150,0.5,0.5,5,this,1)
        //CreateBtn("res/start/start_ranklist.png", function () {
        //    //排行榜
        //},winSize.width/2-100,200,0.5,0.5,this)
        CreateBtn(res.moregame,res.moregame, function () {
            //更多游戏
            top.window.location.href ="http://wx.1758.com/play/gamebox/123?ex1758=1&tp=full&yn=小游戏&title=小游戏";
        },winSize.width/2,150,0.5,0.5,5,this,1)
        CreateBtn(res.invite,res.invite, function () {
            //邀请
            cc.log("邀请")
            var share = document.getElementById('share-square');
            share.style.display = 'block';
        },winSize.width-100,150,0.5,0.5,5,this,1)
        var startlogo=new cc.Sprite("res/start/start_logo.png")
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
        this.scheduleUpdate();
    },
    update: function (dt) {
        if(Is_jifen){
            time+=dt;
            if(time>dt){
                time=0;
                gameNum2++;
                if(gameNum2>=Game_BestScore){
                    gameNum2=Game_BestScore
                }
                getScore.setString(gameNum2);
                if(gameNum2>=10000&&gameNum2<1000000){

                    getScore.setScale(0.8)
                }else if(gameNum2>=1000000){

                    getScore.setScale(0.6)
                }
            }
        }
    },
});

var GameStartScene = cc.Scene.extend({
    onEnter:function () {
        this._super();

        var layer = new GameStartLayer();
        this.addChild(layer);
    }
});
