/*

var GamePlayLayer = cc.Layer.extend({
    sprite:null,
    ctor:function () {
        this._super();
        var size = cc.winSize;
        cc.inputManager.setAccelerometerInterval(interval);
        cc.inputManager.setAccelerometerEnabled(true);
        cc.eventManager.addListener({
            event:cc.EventListener.ACCELERATION,
            callback: function (acelerometerInfo,event) {

            }
        },this);

        return true;
    }
});

var GamePlayScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new GamePlayLayer();
        this.addChild(layer);
    }
});
*/
Score=0;
Is_Angin=null
Is_overOrplay=null
Main_layer=null
var GamplayLayer = cc.Layer.extend({
    ball:null,
    game_score:null,
    sjxz:null,
    sp_height:null,
    u:null,
    ctor: function () {
        this._super();
        Block_Array=[];
        Block_down=false;
        Is_Angin=false
        Is_overOrplay=false
        Main_layer=this
        over_x=0
        over_y=0
        time=0;
        sp_T=0.5;
        sp_X=500;
        sp_min=100
        sp_a=(2*sp_X)/(sp_T*sp_T);
        sp_v2=0
        speed=sp_a*sp_T;
        sp_v1=sp_a*sp_T;
        sp_time= sp_time=(sp_v1-sp_v2)/sp_a;
        //var Draw= new cc.DrawNode();
        //this.addChild(Draw,10);
        //Draw.drawSegment(cc.p(0, 400),
        //    cc.p(640,400),
        //    1,
        //    cc.color(255,0, 255)
        //);

        var winSize = cc.director.getWinSize();
        var bg= new cc.Sprite("res/bg.jpg");
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg);

        //cc.spriteFrameCache.addSpriteFrames(res.sp_plist);
        //ball=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("sp1.png"));
        ////ball.setPosition(winSize.width/2+200,450);
        ////this.addChild(ball,3)
        //var frames2 = [];
        //for (var i = 1; i <3; i++) {
        //    var str = "s"+i+".png";
        //    var frame = cc.spriteFrameCache.getSpriteFrame(str);
        //    frames2.push(frame);
        //}
        //var animation = new cc.Animation(frames2, 1);
        //var action = new cc.Animate(animation);
        //ball.runAction(cc.repeatForever(action));

        ball = new cc.Sprite("res/sp1.png");
        ball.x = 50;
        ball.y = 100;
        ball.up=true;
        ball.num=0;
        this.addChild(ball,5);
        game_score = new cc.LabelTTF("0", "Arial", 45);
        // position the label on the center of the screen
        game_score.x = 100;
        game_score.y = winSize.height -100;
        this.addChild(game_score, 5);
        if('accelerometer' in cc.sys.capabilities){
            cc.inputManager.setAccelerometerInterval(1/30);        //重力感应器的频率
            cc.inputManager.setAccelerometerEnabled(true);        //是否允许重力感应器
            cc.eventManager.addListener({
                event:cc.EventListener.ACCELERATION,
                callback:function(accelerometerInfo,event){
                    var target = event.getCurrentTarget();
                    var pos = {
                        x:accelerometerInfo.x*30,
                        y:accelerometerInfo.y*30,
                        z:accelerometerInfo.z*30
                    };
                    ball.x += pos.x;
                    if(ball.x<=-20){
                        ball.x = 660;
                    }else if(ball.x>=680){
                        ball.x = -20;
                    }
                }
            },this);
        }

        for(var a=1;a<20;a++){
            this.CreateCloud(a*300)
        }
        this.scheduleUpdate();
        u = navigator.userAgent;
        if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
            cc.audioEngine.playMusic(res.bgmusic_ogg,true);
        } else  {
            cc.audioEngine.playEffect(res.bgmusic_mp3,true);
        }
        btn2 = new cc.MenuItemImage(
            "res/musicOn.png",
            "res/musicOn.png",
            function () {
                btn2.setVisible(false)
                btn3.setVisible(true);
                cc.audioEngine.stopMusic();
                cc.audioEngine.stopAllEffects()
            }, this
        )
        var move=cc.rotateBy(0.1,10)
        btn2.runAction(cc.repeatForever(move))
        btn2.setPosition(winSize.width-50,winSize.height-250)
        btn3 = new cc.MenuItemImage(
            "res/musicOff.png",
            "res/musicOff.png",
            function () {
                btn2.setVisible(true)
                btn3.setVisible(false);
                if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) {//安卓手机
                    cc.audioEngine.playMusic(res.bgmusic_ogg,true);
                    //cc.audioEngine.playEffect(res.run1_ogg,true);
                } else  {
                    cc.audioEngine.playEffect(res.bgmusic_mp3,true);
                }

            }, this
        )
        btn3.setPosition(winSize.width-50,winSize.height-250);
        btn3.setVisible(false)
        var menu = new cc.Menu(btn2,btn3);
        //var menu = new cc.Menu(btn2);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu,10);

    },
    update: function (dt) {
        time+=dt;
        if(ball.up){
            ball.y=sp_v1*time-time*time*sp_a/2+sp_min
        }else{
            ball.y=sp_height-time*time*sp_a/2
        }
        if((time>=sp_T&&ball.y<=560)&&ball.up){
            sp_v1=speed
            time=0;
            sp_height=ball.y
           ball.up=false
        }else if(time>=sp_T&&!ball.up){
            if(ball.num==0){
                time=0;
                ball.up=true
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false
            ball.x=over_x;
            ball.y=over_y;
            ball.up=true
            ball.num=1;
            time=sp_T-time;
            Block_down=true;
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            Is_overOrplay=false
            var transition=new cc.TransitionMoveInT(1,new GameOverScene());
            cc.director.runScene(transition);
            return
        }
      for(var a=0;a<Block_Array.length;a++){
          if(!ball.up&cc.rectIntersectsRect(Block_Array[a].getBoundingBox(),this.SpBoundingBox(ball))){
              if(!Block_Array[a].check){
                  Score+=10;
                  game_score.setString(Score);
                  ball.num=1;
                  time=sp_T-time;
                  Block_down=true;
                  ball.up=true
                  Block_Array[a].check=true;
                  over_x= Block_Array[a].x;
                  over_y= Block_Array[a].y+20;
              }else{
                  sp_min=ball.y
                  time=0;
                  ball.up=true
              }
          }
      }
        if(Block_down){
            Block_down=false;
            if(Score<400){
                for(var a=0;a<Block_Array.length;a++){
                    var move=new cc.moveBy(0.4,0,-300);
                    Block_Array[a].runAction(move);
                }
            }else if(Score>=400&&Score<800){
                for(var a=0;a<Block_Array.length;a++){
                    var move=new cc.moveBy(0.3,0,-600);
                    Block_Array[a].runAction(move);
                }
            }else if(Score>=800){
                for(var a=0;a<Block_Array.length;a++){
                    var move=new cc.moveBy(0.2,0,-900);
                    Block_Array[a].runAction(move);
                }
            }
        }
        if(Block_Array.length<=20){
            this.CreateCloud(Block_Array[Block_Array.length-1].y+300);
        }
        for(var a=0;a<Block_Array.length;a++){
            if(Block_Array[a].y<-70){
                Block_Array[a].removeFromParent();
                Block_Array.splice(a,1)
            }
        }
        for(var a=0;a<Block_Array.length;a++){
            if(Block_Array[a+1]){
                Block_Array[a+1].y=Block_Array[a].y+300;
            }
        }
        if(ball.y<=-50){
            //游戏结束

            //this.unscheduleUpdate();
            //ball.removeFromParent();
            //cc.eventManager.removeAllListeners()
            //var layer = new GameOverLayer();
            //this.addChild(layer,10);
            var angin_num=cc.sys.localStorage.getItem("cenbpAngin");
            if(angin_num<3){
                cc.director.pause()
                var layer = new AnginLayer(1);
                this.addChild(layer,10);
            }else{
                this.unscheduleAllCallbacks();
                cc.eventManager.removeAllListeners();
                var transition=new cc.TransitionMoveInT(1,new GameOverScene());
                cc.director.runScene(transition);
                return
            }

        }
    },
    CreateCloud: function (poy) {
        var sp=new cc.Sprite("res/cloud.png");
        var pox=Math.floor(Math.random()*540+40)
        sp.setPosition(pox,poy);
        sp.check=false;
        this.addChild(sp);
        Block_Array.push(sp);
    },
    SpriteRun: function () {
        var a=10;
        var v=10;
        var h;
        h=v*v/2*a;
    },
    SpBoundingBox: function (node) {
        var rect =cc.rect(node.x,node.y-60,40,20)
        return rect;
    }

});
var GamplayScene = cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer = new GamplayLayer();
        this.addChild(layer);
    }
});