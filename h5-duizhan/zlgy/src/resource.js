var res = {
    HelloWorld_png : "res/HelloWorld.png",
    gameoverbg:"res/gameoverbg.png",
    overangin:"res/overangin.png",
    overmore:"res/overmore.png",
    overwatch:"res/overwatch.png",
    bgmusic_mp3:"res/bgmusic01M.mp3",
    bgmusic_ogg:"res/bgmusic01M.ogg",
    startbg:"res/start/start_bg.jpg",
    Score: "res/start/Score.png",
    start_bg1:"res/start/start_bg1.png",
    start_btn1: "res/start/start_btn1.png",
    start_btn2:  "res/start/start_btn2.png",
    follow:"res/start/follow.png",
    moregame:  "res/start/moregame.png",
    invite:"res/start/invite.png"

};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
