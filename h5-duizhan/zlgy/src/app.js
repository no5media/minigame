
var HelloWorldLayer = cc.Layer.extend({
    sjxz:null,
    ctor:function () {
        this._super();
        var winSize=cc.winSize;
        sjxz=new cc.Sprite("res/sjxz.png")
        sjxz.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(sjxz);
        cc.view.setDesignResolutionSize(1131, 640, cc.ResolutionPolicy.SHOW_ALL);
        return true;
    },

});

var HelloWorldScene = cc.Scene.extend({
    onEnter:function () {
        this._super();
        var layer = new HelloWorldLayer();
        this.addChild(layer);
    }
});

