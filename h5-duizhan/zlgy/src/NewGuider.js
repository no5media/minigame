var NewGuiderLayer=cc.Layer.extend({
    ctor: function () {
        this._super()
        var size=cc.winSize;
        cc.sys.localStorage.setItem("zzw","100");
        var sp=new cc.Sprite("res/course.png");
        sp.setPosition(size.width/2,size.height/2);
        this.addChild(sp);

        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                cc.director.runScene(new GamplayScene())
                return true;
            }
        });
        cc.eventManager.addListener(this.touchListener,this);
    }
});
var NewGuiderScene=cc.Scene.extend({
    onEnter: function () {
        this._super()
        var layer=new NewGuiderLayer();
        this.addChild(layer);
    }
});