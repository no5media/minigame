Score=0;
Is_Angin=false;
Is_overOrplay=false;
Main_layer=null;
var GameMainLayer=cc.LayerColor.extend({
    first_sp:null,
    second_sp:null,
    end_sp:null,
    s2:null,
    s3:null,
    stiao1:null,
    getScore:null,
    ctor: function () {
        this._super(cc.color(255,255,255));
        Is_Angin=false;
        Is_overOrplay=false;
        first_num=0;
        second_num=0;
        end_num=0;
        correct_num=0;
        error_num=0;
        Score=0;
        speed=1.5;
        time=1;
        Main_layer=this;
        Start_Array=[];
        Is_start=false;
        this.CreateBg();

        var self=this;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=new cc.Rect(touch.getLocation().x,touch.getLocation().y,1,1);
                if(cc.rectIntersectsRect(rect,self.leftSp1.getBoundingBox())){
                    self.leftSp1.setVisible(false);
                    self.leftSp2.setVisible(true);
                    self.Left_fun();
                }
                if(cc.rectIntersectsRect(rect,self.rightSp1.getBoundingBox())){
                    self.rightSp1.setVisible(false);
                    self.rightSp2.setVisible(true);
                    self.Right_fun();
                }
                return true;
            },
            onTouchEnded:function(touch,event){
                self.leftSp1.setVisible(true);
                self.leftSp2.setVisible(false);
                self.rightSp1.setVisible(true);
                self.rightSp2.setVisible(false);
            }
        });
        cc.eventManager.addListener(listener, this);
        this.CreatePeopleInformation();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt) {
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
           for(var a=0;a<3;a++){
               this.CreateStart(a*70+48,890)
           }
            tiao1.scaleX=1;
            time=1;
            Is_Angin=false;
            PeopleScore.setString(window.select.userPoint)
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return
        }
        if(Score<5){
            speed=1.5
        }else if(Score>=5&&Score<=10){
            speed=1.2;
        }else if(Score>=10){
            speed=0.9;
        }
        if(Is_start&&tiao1){
            time-=dt/speed;
            tiao1.scaleX=time;
            if(time<=0){
                this.AnginGame();
            }
        }
    },
    Left_fun: function () {
        if(error_num==correct_num){
            Score++;
            getScore.setString(Score);
            tiao1.scaleX=1;
            time=1;
            this.CreateNum();
        }else{
            this.AnginGame();
        }
    },
    Right_fun: function () {
        if(error_num!=correct_num){
            Score++;
            getScore.setString(Score);
            this.CreateNum();
            tiao1.scaleX=1;
            time=1;
        }else{
            this.AnginGame();
        }
    },
    AnginGame: function () {
        var chandong = cc.sequence(cc.moveBy(0.01, 3, 0), cc.moveBy(0.01, 0, 3), cc.moveBy(0.02, -6, 0), cc.moveBy(0.01, 0, -6), cc.moveBy(0.01, 3, 0), cc.moveBy(0.01, 0, 3))
        this.runAction(cc.sequence(chandong));
        if(Start_Array[0]){
            Start_Array[Start_Array.length-1].removeFromParent();
            Start_Array.splice(Start_Array.length-1,1);
            tiao1.scaleX=1;
            time=1;
        }else{
            //是否复活
            tiao1.scaleX=0;
            var angin_num=cc.sys.localStorage.getItem("zlcsAngin")
            if(angin_num<3){
                cc.director.pause();
                var layer=new AnginLayer(1);
                this.addChild(layer)
            }else{
                Is_overOrplay=true;
            }

        }
    },
    CreateBg: function () {
        var winSize=cc.winSize;
        tiao2=new cc.Sprite("res/tiao2.png");
        tiao2.setPosition(winSize.width/2,950);
        this.addChild(tiao2)
        tiao1=new cc.Sprite("res/tiao1.png");
        tiao1.setPosition(winSize.width/2-544/2,950);
        tiao1.setAnchorPoint(0,0.5);
        this.addChild(tiao1)

        this.leftSp1=new cc.Sprite(res.left_btn1);
        this.leftSp1.setPosition(winSize.width/2-155,200);
        this.addChild(this.leftSp1);
        this.leftSp2=new cc.Sprite(res.left_btn2);
        this.leftSp2.setPosition(winSize.width/2-155,200);
        this.addChild(this.leftSp2);
        this.leftSp2.setVisible(false);
        this.rightSp1=new cc.Sprite(res.right_btn1);
        this.rightSp1.setPosition(winSize.width/2+155,200);
        this.addChild(this.rightSp1);
        this.rightSp2=new cc.Sprite(res.right_btn2);
        this.rightSp2.setPosition(winSize.width/2+155,200);
        this.addChild(this.rightSp2);
        this.rightSp2.setVisible(false);
        var posy1=500;
        var posy2=700;
        first_sp=new cc.LabelAtlas("3", "res/num.png", 91, 124,"0");
        first_sp.setPosition(winSize.width/2-100,posy2);
        first_sp.setAnchorPoint(0.5,0.5);
        this.addChild(first_sp);
        second_sp=new cc.LabelAtlas("2", "res/num.png", 91, 124,"0");
        second_sp.setPosition(winSize.width/2+100,posy2);
        second_sp.setAnchorPoint(0.5,0.5);
        this.addChild(second_sp);
        end_sp=new cc.LabelAtlas("5", "res/num.png", 91, 124,"0");
        end_sp.setPosition(winSize.width/2,posy1);
        end_sp.setAnchorPoint(0,0.5);
        this.addChild(end_sp);
        var s1=new cc.Sprite("res/s1.png");
        s1.setPosition(winSize.width/2-100,posy1);
        this.addChild(s1);
        s2=new cc.Sprite("res/s2.png");
        s2.setPosition(winSize.width/2,posy2);
        this.addChild(s2)
        s3=new cc.Sprite("res/s3.png");
        s3.setPosition(winSize.width/2,posy2);
        this.addChild(s3)
        s3.setVisible(false)
        getScore=new cc.LabelAtlas("0", "res/num.png", 91, 124,"0");
        getScore.setPosition(winSize.width-48,890);
        getScore.setAnchorPoint(1,0.5);
        getScore.setScale(0.3)
        this.addChild(getScore);
        this.CreateNum();
        for(var a=0;a<3;a++){
            this.CreateStart(a*70+48,890)
        }
    },
    CreateStart: function (x,y) {
        var sp=new cc.Sprite("res/xiaoxin.png");
        sp.setPosition(x,y);
        this.addChild(sp)
        sp.setAnchorPoint(0,0.5)
        Start_Array.push(sp)
    },
    CreateNum: function () {
        var winSize=cc.winSize;
        var jia_Or_jia=Math.floor(Math.random()*2);
        if(jia_Or_jia==1){
            s3.setVisible(false);
            s2.setVisible(true);
            var num1=Math.floor(Math.random()*9+1);
            var num2=Math.floor((Math.random()*9+1));
            correct_num=num1+num2;
            first_sp.setString(num1);
            second_sp.setString(num2);
            var error_array=[correct_num-1,correct_num,correct_num+1];
            var error_index=Math.floor(Math.random()*3);
            error_num=error_array[error_index];
            end_sp.setString(error_num)
            if(error_num>9){
                end_sp.x=winSize.width/2-50
            }else{
                end_sp.x=winSize.width/2
            }
            for(var a=0;a<error_array.length;a++){
                error_array.splice(a,1);
                a--;
            }
        }else{
            s2.setVisible(false);
            s3.setVisible(true);
            var num2=Math.floor(Math.random()*9+1);
            var num1_Array
            num1_Array=[1,2,3,4,5,6,7,8,9];
            for(var a=0;a<num1_Array.length;a++){
                if(num1_Array[a]<num2){
                    num1_Array.splice(a,1);
                    a--
                }
            }
            var num1_index=Math.floor((Math.random()*num1_Array.length));
            var num1=num1_Array[num1_index];
            correct_num=num1-num2;
            first_sp.setString(num1);
            second_sp.setString(num2);
            var error_array=[]
            if(correct_num<=0){
                error_array=[correct_num,correct_num+1,correct_num+1,correct_num];
            }else{
                error_array=[correct_num-1,correct_num,correct_num+1,correct_num];
            }
            var error_index=Math.floor(Math.random()*3);
            error_num=error_array[error_index];

            end_sp.setString(error_num);
            if(error_num>9){
                end_sp.x=winSize.width/2-50
            }else{
                end_sp.x=winSize.width/2
            }
            for(var a=0;a<num1_Array.length;a++){
                num1_Array.splice(a,1);
                a--;
            }
            for(var a=0;a<error_array.length;a++){
                error_array.splice(a,1);
                a--;
            }
        }

        tiao1.scaleX=1;
        Is_start=true;
    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        var start_score=new cc.Sprite(res.Score);
        start_score.setPosition(winSize.width,winSize.height-70)
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score);
        var record_headUrl
        //if(window.select.userProfile&&window.select.userProfile.headUrl){
        //    record_headUrl=window.select.userProfile.headUrl;
        //}else{
        //    record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        //}
        var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        //var name_str1=window.select.userProfile.nickname
        //var name_str= getBytesLength(name_str1,12)
        var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);
        var get_score=window.select.userPoint
        //var get_score=10
        PeopleScore = new cc.LabelTTF("0", "Arial", 30);
        PeopleScore.x = winSize.width-70;
        PeopleScore.y =winSize.height-70;
        PeopleScore.setAnchorPoint(0.5,0.5)
        PeopleScore.setColor(cc.color(254,202,75));
        PeopleScore.setString(get_score)
        this.addChild(PeopleScore, 1);

    },
    GameOver: function () {
        var transition=new cc.TransitionMoveInT(1,new GameOverScene());
        cc.director.runScene(transition);
    }
})
var GameMainScene=cc.Scene.extend({
    onEnter: function(){
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})
