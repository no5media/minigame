var res = {
    sp_png : "res/sp.png",
    game_time : "res/game_time.png",
    newgame : "res/newgame.png",
    score : "res/score.png",
    btn : "res/btn.png",
    btn1 : "res/btn1.png",
    btn2 : "res/btn2.png",
    over : "res/over.png",
    close : "res/close.png",
    guider : "res/guider.png",
    white : "res/white.png",
    gou : "res/gou.png",
    gou1 : "res/gou1.png",
    entergame : "res/entergame.png",
    startbtn1 : "res/startbtn.png",
    startbtn2 : "res/startbtn2.png",

    startbg:"res/start/start_bg.png",
    Score: "res/start/Score.png",
    start_bg1:"res/start/start_bg1.png",
    start_btn1: "res/start/start_btn1.png",
    start_btn2:  "res/start/start_btn2.png",
    follow:"res/start/follow.png",
    moregame:  "res/start/moregame.png",
    invite:"res/start/invite.png",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
