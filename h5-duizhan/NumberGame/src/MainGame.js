Score=0;
game_step=100;
Is_Guider=true;
Is_newGame=false;
Is_Angin=false;
Is_overOrplay=false
Main_layer=null
var MainGameLayer=cc.LayerColor.extend({
    getScore:null,
    gettime:null,
    btn2:null,
    ctor: function () {
        this._super(cc.color(252,249,242,255*0.99));
        NUM = 0;
        NUM_X = 0;
        NUM_Y = 0;
        Sprite_Array=[];
        Color_Array=[];
        Is_Move=false;
        Is_Click=true;
        time=0;
        time1=0;
        step=100;
        new_game_time=60;
        Main_layer=this
        Is_Angin=false;
        Is_overOrplay=false
        var size=cc.winSize;
        for(var a=0;a<5;a++ ){
            for(var b=0;b<5;b++ ){
                var posX=90+146*a;
                var posY=200+146*b;
                this.CreateSprite(posX,posY,b,a)
            }
        }
        var game_time=new cc.Sprite(res.game_time);
        game_time.setAnchorPoint(0,0);
        game_time.setPosition(25,890);
        this.addChild(game_time,1);
        var self=this;
        btn1 = new cc.MenuItemImage(
            res.newgame,
            res.newgame,
            function () {
                if(new_game_time==60){
                    cc.director.runScene(new MainGameScene());
                    Is_newGame=true;
                }
            }, this
        )
        btn1.setPosition(600, 940);
        btn2 = new cc.MenuItemImage(
            res.btn,
            res.btn,
            function () {
                if(Is_Guider){
                    Is_Guider=false
                    cc.director.pause();
                    var layer=new NewGuiderLayer();
                    self.addChild(layer,10);

                }
            }, this
        )
        btn2.setPosition(size.width/2, 50);
        btn2.setAnchorPoint(0.5,0.5)

        btn2.game_new=new cc.LayerColor(cc.color(0,0,0,120));
        btn2.game_new.setContentSize(289,88);
        btn2.game_new.setAnchorPoint(0,0.5);
        btn2.game_new.setPosition(145,865)
        btn2.game_new.setLocalZOrder(2);
        btn2.addChild( btn2.game_new);
        if(Is_newGame){
            btn2.game_new.setVisible(true);
        }else {
            btn2.game_new.setVisible(false);
        }

        //btn2.setColor(cc.color(0,0,0))
        var menu = new cc.Menu(btn1,btn2);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu,10);
        var game_score=new cc.Sprite(res.score);
        game_score.setPosition(600,1040);
        this.addChild(game_score,1);
        getScore=new cc.LabelTTF("0","Arial","40")
        getScore.setPosition(600,1020);
        getScore.setAnchorPoint(0.5,0.5)
        getScore.setString(Score)
        //getScore.setColor(cc.color(0,0,0));
        this.addChild(getScore,1);
        gettime=new cc.LabelTTF(game_step,"Arial","99")
        gettime.setPosition(232,970);
        gettime.setAnchorPoint(0.5,0.5)
        //getScore.setColor(cc.color(0,0,0));
        this.addChild(gettime,1);

        var self=this
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                if(Is_Click){
                    var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                    for(var a=0;a<Sprite_Array.length;a++){
                        if(Sprite_Array[a]&&cc.rectIntersectsRect(rect,Sprite_Array[a].getBoundingBox())){
                            NUM=a;
                            NUM_X=Sprite_Array[a].x;
                            NUM_Y=Sprite_Array[a].y;
                            Is_Move=true;
                            Is_Click=false;
                        }
                    }
                    return true;
                }
            },
            onTouchMoved: function (touch,event) {
                if(Is_Move){
                    Sprite_Array[NUM].setLocalZOrder(2)
                    Sprite_Array[NUM].x=touch.getLocation().x;
                    Sprite_Array[NUM].y=touch.getLocation().y;
                    var m_num=0
                    for(var a=0;a<Sprite_Array.length;a++){
                        if(Sprite_Array[a]&&Color_Array[0]&&Math.sqrt((Color_Array[0].x-Sprite_Array[a].x)*(Color_Array[0].x-Sprite_Array[a].x)+(Color_Array[0].y-Sprite_Array[a].y)*(Color_Array[0].y-Sprite_Array[a].y))<=125/2-10){
                            m_num++
                        }
                    }
                    if(m_num==0){
                        if(Color_Array[0]){
                            Color_Array[0].removeFromParent();
                            Color_Array.splice(0,1)
                        }
                    }
                    for(var a=0;a<Sprite_Array.length;a++){
                        if(NUM!=a&&Sprite_Array[NUM]&&Sprite_Array[a]){
                            var Min=Math.sqrt((Sprite_Array[NUM].x-Sprite_Array[a].x)*(Sprite_Array[NUM].x-Sprite_Array[a].x)+(Sprite_Array[NUM].y-Sprite_Array[a].y)*(Sprite_Array[NUM].y-Sprite_Array[a].y));
                            if(Min<=125/2-10){
                                if(Color_Array[0]){
                                    if(Color_Array[0].x==Sprite_Array[a].x&&Color_Array[0].y==Sprite_Array[a].y){

                                    }else {
                                        Color_Array[0].removeFromParent();
                                        Color_Array.splice(0,1)
                                        if(Color_Array.length<1){
                                            self.CreateColor(Sprite_Array[a])
                                        }
                                    }
                                }else {
                                    if(Color_Array.length<1){
                                        self.CreateColor(Sprite_Array[a])
                                    }
                                }
                            }
                        }
                    }
                }
            },
            onTouchEnded: function (touch,event) {
                if(Is_Move){
                    if(!Color_Array[0]){
                        Is_Move=false;
                        var fun=cc.callFunc(function () {
                            Is_Click=true;
                        },this)
                        Sprite_Array[NUM].runAction(cc.sequence(cc.moveTo(0.3,NUM_X,NUM_Y),fun))
                    }else {
                        Is_Click=true;
                    }
                    for(var a=0;a<Sprite_Array.length;a++){
                        if(NUM!=a&&Sprite_Array[NUM]&&Sprite_Array[a]){
                            var Min=Math.sqrt((Sprite_Array[NUM].x-Sprite_Array[a].x)*(Sprite_Array[NUM].x-Sprite_Array[a].x)+(Sprite_Array[NUM].y-Sprite_Array[a].y)*(Sprite_Array[NUM].y-Sprite_Array[a].y));
                            if(Min<=125/2-10){
                                if(Sprite_Array[NUM].tag==Sprite_Array[a].tag){
                                    Score+=Sprite_Array[a].tag;
                                    game_step+=10;
                                    if(game_step>=100){
                                        game_step=100
                                    }
                                    gettime.setString(game_step);
                                    getScore.setString(Score);
                                    Sprite_Array[a].removeFromParent();
                                    Sprite_Array[NUM].removeFromParent();
                                    Sprite_Array[NUM]=null;
                                    Sprite_Array[a]=null;
                                    self.CreateNullSprite();
                                    Is_Move=false;
                                }else {
                                    var tag=Sprite_Array[NUM].tag+Sprite_Array[a].tag;
                                    var posX=Sprite_Array[a].x
                                    var posY=Sprite_Array[a].y
                                    var row=Sprite_Array[a].row
                                    var col=Sprite_Array[a].col;
                                    Sprite_Array[a].removeFromParent();
                                    Sprite_Array[NUM].removeFromParent();
                                    Sprite_Array[NUM]=null;
                                    Sprite_Array[a]=null;
                                    if(Color_Array[0]){
                                        Color_Array[0].removeFromParent();
                                        Color_Array.splice(0,1)
                                    }
                                    self.CreateAddingSprite(posX,posY,row,col,tag)
                                    Is_Move=false;
                                }
                            }
                        }
                    }
                }
            }
        });
        cc.eventManager.addListener(this.touchListener,this);
        this.scheduleUpdate();

    },
    update: function (dt) {
        if(Is_newGame){
            time1+=dt;
            if(time1>=1){
                new_game_time--;
                time1=0;
                btn2.game_new.setScale(new_game_time/60,1);
                if(new_game_time<=1){
                    Is_newGame=false
                    new_game_time=60;
                    time1=0;
                }
            }
        }
        time+=dt;
        if(time>=1){
            time=0;
            game_step--;
            gettime.setString(game_step);
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false
            //加步数
            game_step=10;
            gettime.setString(game_step);
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            Is_overOrplay=false
            var transition=new cc.TransitionMoveInT(1,new GameOverScene());
            cc.director.runScene(transition);
            return
        }
        if(game_step==0){
            var angin_num=cc.sys.localStorage.getItem("jfllkAngin")
            if(angin_num<3){
                cc.director.pause();
                var layer=new AnginLayer(1);
                this.addChild(layer,10)
            }else{
                Is_overOrplay=true
            }

        }


    },
    CreateSprite: function (posX,posY,row,col) {
        var sp=new cc.Sprite(res.sp_png);
        sp.setPosition(posX,posY);
        var sp_m;
        var tag_Array=[];
        var tag_Array1=[];
        for(var a=0;a<Sprite_Array.length;a++){
            if(Sprite_Array[a]){
                tag_Array.push(Sprite_Array[a].tag);
            }
        }
        for(var a=1;a<100;a++){
            tag_Array1.push(a)
        }
        for(var a=0;a<tag_Array.length-1;a++){
            for(var b=0;b<tag_Array1.length-1;b++){
                if(tag_Array1[b]==tag_Array[a]){
                   tag_Array1.splice(b,1);
                }
            }
        }
        sp_m=Math.floor(Math.random() * tag_Array1.length );
        var sp_num=new cc.LabelTTF(tag_Array1[sp_m],'Arial',40)
        sp_num.setPosition(64,64);
        //sp_num.setColor(0,0,0,50)
        sp_num.setAnchorPoint(0.5,0.5);
        sp.setTag(tag_Array1[sp_m]);
        sp.row=row;
        sp.col=col
        sp.addChild(sp_num);
        Sprite_Array[sp.row*5+sp.col]=sp;
        this.addChild(sp);
    },
    CreateAddingSprite: function (posX,posY,row,col,tag) {
        var sp=new cc.Sprite(res.sp_png);
        sp.setPosition(posX,posY);
        var sp_num=new cc.LabelTTF(tag,'Arial',40)
        sp_num.setPosition(64,64);
        //sp_num.setColor(0,0,0,50)
        sp_num.setAnchorPoint(0.5,0.5);
        sp.setTag(tag);
        sp.row=row;
        sp.col=col
        sp.addChild(sp_num);
        Sprite_Array[sp.row*5+sp.col]=sp;
        this.addChild(sp);
    },
    CreateNullSprite: function () {
        for(var a=0;a<5;a++ ){
            for(var b=0;b<5;b++ ){
                if(!Sprite_Array[5*a+b]){
                    var posX=90+146*b;
                    var posY=200+146*a;
                    this.CreateSprite(posX,posY,a,b)
                }
            }
        }
    },
    CreateColor: function (sp) {
        var layer = new cc.LayerColor(cc.color(0, 0, 0, 50));
        layer.setContentSize(125, 125)
        //layer.setScale(0.1588,0.119)
        sp.addChild(layer)
        Color_Array.push(layer);
    }
});
var MainGameScene=cc.Scene.extend({
    onEnter: function () {
     this._super();
        var layer=new MainGameLayer();
        this.addChild(layer);
    }
})