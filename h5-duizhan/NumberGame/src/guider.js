level=1;
var guiderLayer=cc.LayerColor.extend({
    ctor: function () {
        GuiderSprite_Array=[];
        GuiderSprite_Array1=[];
        num_Array=[];
        Is_Click=true;
        Is_left=true;
        Is_right=false;
        Is_Create=true;
        this._super(cc.color(252,249,242,255*0.99));
        var size=cc.winSize
        for(var a=6;a<99;a++){
            num_Array.push(a);
        }
        var leylabel = new cc.Sprite("res/guider1.png");
        leylabel.x = size.width/2;
        leylabel.setAnchorPoint(0.5,0.5);
        leylabel.y = 1050;
        this.addChild(leylabel,10);
        var bg_layer=new cc.LayerColor(cc.color(0,0,0,200));
        this.addChild(bg_layer,1);
        for(var a=1;a<6;a++ ){
            for(var b=1;b<6;b++ ){
                if(b==3){
                    var posX=90+146*(a-1);
                    //var posY=size.height/2;
                    var posY=300+146*2;
                    var sp=new cc.Sprite(res.sp_png);
                    sp.setPosition(posX,posY);
                    var sp_num=new cc.LabelTTF(a,'Arial',40)
                    sp_num.setPosition(64,64);
                    sp_num.setAnchorPoint(0.5,0.5);
                    sp.setTag(a);
                    sp.addChild(sp_num);
                    if(a==1){
                        sp.setLocalZOrder(11);
                        GuiderSprite_Array1.push(sp);

                    }else if(a==3) {
                        GuiderSprite_Array.push(sp);
                    }else {
                        GuiderSprite_Array.push(sp);
                    }
                    this.addChild(sp);
                }else {
                    var posX=90+146*(a-1);
                    var posY=300+146*(b-1);
                    var sp=new cc.Sprite(res.sp_png);
                    sp.setPosition(posX,posY);
                    var m1=Math.floor(Math.random() * num_Array.length );
                    var m=num_Array[m1];
                    num_Array.splice(m1,1);
                    var sp_num=new cc.LabelTTF(m,'Arial',40)
                    sp_num.setPosition(64,64);
                    sp_num.setAnchorPoint(0.5,0.5);
                    sp.addChild(sp_num);
                    this.addChild(sp);
                }
            }
        }
        var Rubber= "提示：\n\n"+"请点击屏幕亮色的方块";
        this.le = new cc.LabelTTF(Rubber,"Arial",30);
        this.le.x = 400;
        this.le.y = 150;
        //this.le1.setColor(cc.color(0,0,0));
        this.addChild(this.le,10);

        var self=this
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                if(Is_Click){
                    if(level==1){
                        var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                        for(var a=0;a<GuiderSprite_Array1.length;a++){
                            if(cc.rectIntersectsRect(rect,GuiderSprite_Array1[a].getBoundingBox())){
                                self.le.removeFromParent();
                                GuiderSprite_Array[1].setLocalZOrder(10);
                                var Rubber= "提示：\n\n"+"玩家将亮起的方块拖至闪亮方块的上方，可叠加方块数字；";
                                self.le1 = new cc.LabelTTF(Rubber,"Arial",25);
                                self.le1.x = 400;
                                self.le1.y = 150;
                                self.addChild(self.le1,10);
                                self.SpriteBink(GuiderSprite_Array[1])
                                level++
                            }
                        }

                    }
                    if(level==2||level==3){
                        var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                        for(var a=0;a<GuiderSprite_Array1.length;a++){
                            if(cc.rectIntersectsRect(rect,GuiderSprite_Array1[a].getBoundingBox())){
                                NUM_X = GuiderSprite_Array1[0].x;
                                NUM_Y = GuiderSprite_Array1[0].y;
                                Is_Click=false
                                return true;
                            }
                        }
                    }
                }
            },
            onTouchMoved: function (touch,event) {
                if(level==1){
                }else {
                    GuiderSprite_Array1[0].stopAllActions();
                    GuiderSprite_Array1[0].setOpacity(250);
                    GuiderSprite_Array1[0].x=touch.getLocation().x;
                    GuiderSprite_Array1[0].y=touch.getLocation().y;
                }

            },
            onTouchEnded: function (touch,event) {
                if(level==1){
                    level++;
                    Is_Click=true;
                }
                if(level==2){
                    var Min=Math.sqrt((GuiderSprite_Array1[0].x-GuiderSprite_Array[1].x)*(GuiderSprite_Array1[0].x-GuiderSprite_Array[1].x)+(GuiderSprite_Array1[0].y-GuiderSprite_Array[1].y)*(GuiderSprite_Array1[0].y-GuiderSprite_Array[1].y));
                    if(Min<=69){
                        var posX=GuiderSprite_Array[1].x;
                        var posY=GuiderSprite_Array[1].y;
                        var tag=GuiderSprite_Array[1].tag+GuiderSprite_Array1[0].tag;
                        GuiderSprite_Array[1].removeFromParent();
                        GuiderSprite_Array1[0].removeFromParent();
                        GuiderSprite_Array.splice(1,1)
                        GuiderSprite_Array1.splice(0,1)
                        self.CreateSprite(posX,posY,tag)
                        self.SpriteBink(GuiderSprite_Array[1])
                        //self.SpriteBink(GuiderSprite_Array1[0])

                        GuiderSprite_Array[1].setLocalZOrder(10);
                        level++
                        self.le1.removeFromParent();
                        var Rubber= "提示：\n\n"+"将亮起的方块拖至闪亮方块，带有相同数字的两\n"+"个方块可消除，并可获得该方块所示分值。";
                        self.le2 = new cc.LabelTTF(Rubber,"Arial",30);
                        self.le2.x = 400;
                        self.le2.y = 150;
                        //self.le2.setColor(cc.color(0,0,0));
                        self.addChild(self.le2,10);
                        Is_Click=true;
                    }else {
                        var fun=cc.callFunc(function () {
                            Is_Click=true;
                            //self.SpriteBink(GuiderSprite_Array1[0])
                        },this)
                        GuiderSprite_Array1[0].runAction(cc.sequence(cc.moveTo(0.3,NUM_X,NUM_Y),fun))
                    }
                }else  if(level==3){
                    var Min=Math.sqrt((GuiderSprite_Array1[0].x-GuiderSprite_Array[1].x)*(GuiderSprite_Array1[0].x-GuiderSprite_Array[1].x)+(GuiderSprite_Array1[0].y-GuiderSprite_Array[1].y)*(GuiderSprite_Array1[0].y-GuiderSprite_Array[1].y));
                    if(Min<=69){
                        var layer=new Guider_guiderlayer();
                        self.addChild(layer,15);

                    }else {
                        var fun=cc.callFunc(function () {
                            Is_Click=true;
                        },this)
                        GuiderSprite_Array1[0].runAction(cc.sequence(cc.moveTo(0.3,NUM_X,NUM_Y),fun))
                    }
                }
            }
        })
        cc.eventManager.addListener(this.touchListener,this);
    },
    CreateSprite: function (posX,posY,tag) {
        var sp=new cc.Sprite(res.sp_png);
        sp.setPosition(posX,posY);
        var sp_num=new cc.LabelTTF(tag,'Arial',40)
        sp_num.setPosition(64,64);
        sp_num.setAnchorPoint(0.5,0.5);
        sp.setTag(tag);
        sp.setLocalZOrder(11)
        sp.addChild(sp_num);
        this.addChild(sp);
        GuiderSprite_Array1.push(sp);
    },
    SpriteBink: function (object) {
        var fadein1=new cc.fadeIn(0.3,180);
        var fadein2=new cc.fadeOut(0.3,0);
        object.runAction(cc.repeatForever(cc.sequence(fadein1,fadein2)));

    }
});
var Guider_guiderlayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        Is_visble=true;
        var size=cc.winSize;
        var bg_sp=new cc.Sprite(res.white);
        bg_sp.setPosition(size.width/2,size.height/2);
        this.addChild(bg_sp);
        this.gou=new cc.Sprite(res.gou );
        this.gou.setPosition(150,390);
        this.addChild( this.gou,2);
        this.gou1=new cc.Sprite(res.gou1);
        this.gou1.setPosition(150,390);
        this.addChild( this.gou1,1);
        var btn= new cc.MenuItemImage(
            res.entergame,
            res.entergame,
            function () {
                if(Is_visble){
                    cc.sys.localStorage.setItem("zzw","50");
                }
                cc.director.runScene(new MainGameScene())

            }, this
        )
        btn.setPosition(size.width/2,size.height/2-50)
        var menu = new cc.Menu(btn);
        menu.x = 0;
        menu.y = 0;
        this.addChild(menu);

        var self=this
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: false,
            onTouchBegan: function (touch, event) {
                var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1)
                if(cc.rectIntersectsRect(rect,self.gou.getBoundingBox())){
                    if(Is_visble){
                        self.gou.setLocalZOrder(10);
                        self.gou1.setLocalZOrder(11);
                        Is_visble=false
                    }else {
                        self.gou.setLocalZOrder(11);
                        self.gou1.setLocalZOrder(10);
                        Is_visble=true
                    }
                }
                return true;
            }
        })
        cc.eventManager.addListener(this.touchListener,this);
    }

})


var guiderScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new guiderLayer();
        this.addChild(layer);



    }
});