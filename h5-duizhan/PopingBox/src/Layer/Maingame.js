Score1=0;
number=3;
Is_Angin=false;
Is_overOrplay=false
over_Score=0
Main_layer=null
var MaingameLayer  = cc.Layer.extend({
    ctor: function () {
        this._super();
        this.block1=[];
        this.block2=[];
        this.Moveblock=[];
        this.MovePengblock=[];
        this.bujuSize = 54;
        this.size = cc.winSize;
        M_wight=5;
        M_height =5;
        role_GAP =1;//x轴间隔
        role_GAP1=-4;//y轴间隔
        data=0;
        IsLeft=false;
        IsRight=false;
        IsDown=false;
        Isrun=false;
        IsMove=false;
        IsPeng=false;
        IsPeng1=false;
        IsCreate=false;
        firstPoint=0;
        endPoint=0;
        this.number1=0;
        Is_Angin=false;
        Is_overOrplay=false
        Main_layer=this
        var bg=flax.assetsManager.createDisplay(res.All_plist, "bg", {parent: this, x: 240, y: 400});
        bg.setAnchorPoint(0.5,0.5);
        this.CreateBlock();
        this.Goal= new cc.LabelAtlas(gameData[gameLevel-1].FenShu, "res/1.png", 23, 33,"0");
        this. Goal.x=250;
        this.Goal.y=695;
        //this.Goal.setScale(0.8);
        this.Goal.setAnchorPoint(0.5,0.5);
        this.addChild(this.Goal);

        this.Score= new cc.LabelAtlas("0", "res/1.png", 23, 33,"0");
        this. Score.x=240;
        this.Score.y=740;
        //this.Score.setScale(0.8);
        this.Score.setAnchorPoint(0.5,0.5);
        this.Score.setString(Score1);
        this.addChild(this.Score);

        this.num= new cc.LabelAtlas(number, "res/1.png", 23, 33,"0");
        this. num.x=410;
        this.num.y=700;
        this.num.setScale(0.8);
        this.num.setAnchorPoint(0.5,0.5);
        this.addChild(this.num);
        //this.num.setString(number);
        this.hero=flax.assetsManager.createDisplay(res.action_plist, "stand", {parent: this, x: 420, y: 50});
        this.hero.setAnchorPoint(0.5,0.5);
        this.hero.setLocalZOrder(3);
        this.hero.play();
        this.addBlock=flax.assetsManager.createDisplay(res.All_plist, "b5", {parent: this, x: 380, y: 50});
        this.addBlock.setAnchorPoint(0.5,0.5);
        this.addBlock.setZOrder(2);
        this.Moveblock.push(this.addBlock)
        for(var b=1;b<3;b++){
            var rect1=this.addBlock['rect'+b]
            rect1.setVisible(false);
            rect1.tag=5;
            rect1.setVisible(false);
            this.MovePengblock.push(rect1);
        }
        this.tishi=flax.assetsManager.createDisplay(res.All_plist, "tishi",{parent:this,x:240,y:500});
        this.tishi.setVisible(false);
        var self=this;
        this.touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                firstPoint=touch.getLocationY();
                var rect=cc.rect(touch.getLocation().x,touch.getLocation().y,1,1);
                for(var a=0;a<self.Moveblock.length;a++){
                    if(cc.rectIntersectsRect(self.Moveblock[a].getBoundingBox(),rect)){
                        if(!Isrun){
                            IsPeng1=true;
                            IsLeft=true;
                        }
                    }
                }
                return true;
            },
            onTouchMoved: function (touch, event) {
                endPoint=touch.getLocationY()
                if(!IsMove){
                    if(endPoint>=firstPoint+50){
                        firstPoint=endPoint;
                        if(self.hero.y>=628){
                            self.hero.y=648;
                            self.Moveblock[0].y=648;
                            Isrun=true;
                        }else{
                            self.hero.y+=50;
                            self.Moveblock[0].y+=50;
                            Isrun=true;
                        }
                    } else if(endPoint<=firstPoint-50){
                        firstPoint=endPoint;
                        if(self.hero.y<=70){
                            self.hero.y=50;
                            self.Moveblock[0].y=50;
                            Isrun=true;
                        }else{
                            self.hero.y-=50;
                            self.Moveblock[0].y-=50;
                            Isrun=true;
                        }
                    }
                }
            },
            onTouchEnded: function (touch, event) {
                Isrun=false;
            }
        });
        cc.eventManager.addListener(this.touchListener,this);


        this.initMatrix();
        for(var a=0;a<this.block2.length;a++){
            for(var b=1;b<3;b++){
                if(this.block2[a]){
                    this.block2[a]['rect'+b].setVisible(false);
                }
            }
        }

        this.scheduleUpdate();


        return true;
    },
    update: function () {
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false
            flax.replaceScene("Maingame");
            number=3
            Score1=over_Score
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            Is_overOrplay=false
            var transition=new cc.TransitionMoveInT(1,new GameOverScene());
            cc.director.runScene(transition);
            return
        }
        if(Score1>=gameData[gameLevel-1].FenShu){
            this.unscheduleAllCallbacks();
            this.hero.removeFromParent();
            this.Moveblock[0].removeFromParent();
            over_Score=Score1
            var layer = new GameLevelLayer();
            this.addChild(layer);
        }
        for(var a=0;a<this.block2.length;a++){
            for(var b=1;b<3;b++){
                if(this.block2[a]){
                    this.block2[a]['rect'+b].setVisible(false);
                }
            }
        }
        if(this.block2.length==0&&Score1<gameData[gameLevel-1].FenShu){
            var angin_num=cc.sys.localStorage.getItem("sjxcAngin")
            if(angin_num<3){
                cc.director.pause()
                var layer = new AnginLayer(1);
                this.addChild(layer,100);
            }else{
                this.unscheduleAllCallbacks();
                cc.eventManager.removeAllListeners();
                Is_overOrplay=false
                var transition=new cc.TransitionMoveInT(1,new GameOverScene());
                cc.director.runScene(transition);
                return
            }



            //this.unscheduleAllCallbacks();
            //this.hero.removeFromParent();
            //this.Moveblock[0].removeFromParent();
            //var layer = new GameoverLayer();
            //this.addChild(layer);
        }
//物块向左移动
        if(IsLeft){
            IsMove=true;
            for(var a=0;a<this.Moveblock.length;a++){
                this.Moveblock[a].x-=8;
                if(this.Moveblock[a].x<=50){
                    this.Moveblock[a].x=50;
                    IsLeft=false;
                    IsDown=true;
                }
            }
        }
//物块向下移动
        if(IsDown){
            IsMove=true;
            for(var a=0;a<this.Moveblock.length;a++){
                if(this.Moveblock[a].y<=48){
                    IsDown=false;
                    IsPeng1=false;
                    this.Moveblock[a].runAction(cc.moveTo(0.3,this.hero.x-40,this.hero.y));
                }else{
                    this.Moveblock[a].y-=8;
                }
            }
        }
//和砖块的碰撞
        for(var a=0;a<this.block1.length;a++){
            for(var b=0;b<this.Moveblock.length;b++){
                //var point = this.MovePengblock[b].parent.convertToWorldSpace(this.MovePengblock[b].getPosition());
                if(cc.rectIntersectsRect(this.block1[a].getBoundingBox(),this.MyBoundingBox(this.Moveblock[b]))){
                    IsLeft=false;
                    IsDown=true;
                }
            }
        }
 //和物块的碰撞
        if(!IsPeng1){
            this.fillVacancies();
        }
        if(IsPeng1){
            for(var a=0;a<this.block2.length;a++){
                for(var b=0;b<this.MovePengblock.length;b++){
                    var point = this.MovePengblock[b].parent.convertToWorldSpace(this.MovePengblock[b].getPosition());
                    if(this.block2[a]&&cc.rectIntersectsRect(this.block2[a].getBoundingBox(),this.MyBoundingBoxA(point,this.MovePengblock[b]))){
                       //符合条件的碰撞
                        if(this.MovePengblock[b].tag==5){
                            IsCreate=true;
                            IsPeng=true;
                            this.number1++;
                            cc.log(IsDown,IsLeft,this.block2[a].m_row,this.block2[a].m_col)

                            if(IsLeft){
                                var check_num=0;
                                for(var m=0;m<5;m++){
                                    if(this.block2[ this.block2[a].m_row * M_wight + m]&&this.block2[a].tag!=this.block2[ this.block2[a].m_row * M_wight + m].tag){
                                        check_num++;
                                    }
                                }
                                if(check_num==0){}else {
                                    for(var z=0;z<2;z++){
                                        this.MovePengblock[z].tag=this.block2[a].tag;
                                    }
                                }
                            }else  if(IsDown){
                                var check_num=0;
                                for(var m=0;m<5;m++){
                                    if(this.block2[ m * M_wight + this.block2[a].m_col]&&this.block2[a].tag!=this.block2[ m * M_wight + this.block2[a].m_col].tag){
                                        check_num++;
                                    }
                                }
                                if(check_num==0){

                                }else {
                                    for(var z=0;z<2;z++){
                                        this.MovePengblock[z].tag=this.block2[a].tag;
                                    }
                                }
                            }

                            //for(var z=0;z<2;z++){
                            //    this.MovePengblock[z].tag=this.block2[a].tag;
                            //}

                            var boom=flax.assetsManager.createDisplay(res.action_plist, "a"+this.block2[a].tag, {parent: this, x: this.block2[a].x, y: this.block2[a].y});
                            boom.gotoAndPlay(0);
                            boom.setLocalZOrder(3);
                            boom.autoDestroyWhenOver=true;
                            this.block2[a].removeFromParent();
                            this.block2[ this.block2[a].m_row * M_wight +  this.block2[a].m_col] = null;
                        }else{
                            if(this.block2[a].tag==this.MovePengblock[b].tag){
                                IsCreate=true;
                                var boom=flax.assetsManager.createDisplay(res.action_plist, "a"+this.block2[a].tag, {parent: this, x: this.block2[a].x, y: this.block2[a].y});
                                boom.gotoAndPlay(0);
                                boom.setLocalZOrder(3);
                                boom.autoDestroyWhenOver=true;
                                this.number1++;
                                this.block2[a].removeFromParent();
                                this.block2[ this.block2[a].m_row * M_wight +  this.block2[a].m_col] = null;
                                IsPeng=true;
                            }else if(IsPeng){
                                if(IsLeft){
                                    IsLeft=false;
                                    IsMove=true
                                    IsPeng1=false;
                                    if(IsCreate){
                                        IsCreate=false;
                                        Score1+=(this.number1*this.number1)*5+5*this.number1;
                                        this.Score.setString(Score1);
                                        this.number1==0
                                    }
                                    var tag1=this.block2[a].tag;
                                    var tag2=this.MovePengblock[b].tag;
                                    var i=this.block2[a].x;
                                    var j=this.block2[a].y
                                    var m=this.block2[a].m_row;
                                    var n= this.block2[a].m_col;
                                    var x1=this.Moveblock[0].x;
                                    var y1=this.Moveblock[0].y;
                                    var x2=this.block2[a].x;
                                    var y2=this.block2[a].y;
                                    for(var z=0;z<this.MovePengblock.length;z++){
                                        this.MovePengblock[z].removeFromParent();
                                        this.MovePengblock.splice(z,1);
                                        z--;
                                    }
                                    this.Moveblock[0].removeFromParent();
                                    this.Moveblock.splice(0,1);
                                    this.block2[a].removeFromParent();
                                    this.block2[this.block2[a].m_row * M_wight + this.block2[a].m_col] = null;

                                    var str1 =flax.assetsManager.createDisplay(res.All_plist, "b"+(tag1), {parent:this,x:x1,y:y1});
                                    str1.setLocalZOrder(2);
                                    //str1.setTag(tag1)
                                    this.Moveblock.push(str1);
                                    for(var v=1;v<3;v++){
                                        var rect1=str1['rect'+v]
                                        rect1.setVisible(false);
                                        rect1.tag=tag1;
                                        this.MovePengblock.push(rect1);
                                    }
                                    var str2=flax.assetsManager.createDisplay(res.All_plist, "b"+tag2, {parent:this,x:x2,y:y2});
                                    str2.setTag(tag2);
                                    str2.m_row=m;
                                    str2.m_col=n;
                                    this.block2[m*M_wight+n]=str2;
                                    this.Moveblock[0].runAction(cc.moveTo(0.3,this.hero.x-40,this.hero.y));
                                }else if(IsDown){
                                    IsDown=false;
                                    IsMove=true
                                    IsPeng1=false;
                                    if(IsCreate){
                                        IsCreate=false;
                                        Score1+=(this.number1*this.number1)*5+5*this.number1;
                                        this.Score.setString(Score1);
                                        this.number1==0
                                    }
                                    var tag1=this.block2[a].tag;
                                    var tag2=this.MovePengblock[b].tag;
                                    var i=this.block2[a].x;
                                    var j=this.block2[a].y
                                    var m=this.block2[a].m_row;
                                    var n= this.block2[a].m_col;
                                    var x1=this.Moveblock[0].x;
                                    var y1=this.Moveblock[0].y;
                                    var x2=this.block2[a].x;
                                    var y2=this.block2[a].y;
                                    for(var z=0;z<this.MovePengblock.length;z++){
                                        this.MovePengblock[z].removeFromParent();
                                        this.MovePengblock.splice(z,1);
                                        z--;
                                    }
                                    this.Moveblock[0].removeFromParent();
                                    this.Moveblock.splice(0,1);
                                    this.block2[a].removeFromParent();
                                    this.block2[this.block2[a].m_row * M_wight + this.block2[a].m_col] = null;

                                    var str1 =flax.assetsManager.createDisplay(res.All_plist, "b"+(tag1), {parent:this,x:x1,y:y1});
                                    str1.setLocalZOrder(2);
                                    str1.setTag(tag1);
                                    this.Moveblock.push(str1);
                                    for(var v=1;v<3;v++){
                                        var rect1=str1['rect'+v]
                                        rect1.setVisible(false);
                                        rect1.tag=tag1;
                                        this.MovePengblock.push(rect1);
                                    }
                                    var str2=flax.assetsManager.createDisplay(res.All_plist, "b"+tag2, {parent:this,x:x2,y:y2});
                                    str2.setTag(tag2);
                                    str2.m_row=m;
                                    str2.m_col=n;
                                    this.block2[m*M_wight+n]=str2;
                                    this.Moveblock[0].runAction(cc.moveTo(0.3,this.hero.x-40,this.hero.y));
                                }
                            }else if(!IsPeng){
                                if(IsLeft){
                                    IsLeft=false;
                                }else if(IsDown){
                                    IsDown=false;
                                }
                                this.SpriteCheck();
                                if(IsCreate){
                                    IsCreate=false;
                                    Score1+=(this.number1*this.number1)*5+5*this.number1;
                                    this.Score.setString(Score1);
                                    this.number1==0
                                }
                                IsMove=true;
                                IsPeng1=false;
                                this.Moveblock[0].runAction(cc.moveTo(0.3,this.hero.x-40,this.hero.y));
                            }
                        }
                    }
                }
            }
        }

//物块和英雄的碰撞
        for(var a=0;a<this.Moveblock.length;a++){
            if(cc.rectIntersectsRect(this.Moveblock[a].getBoundingBox(),this.hero.getBoundingBox())){
                IsMove=false;
                IsPeng=false;
                this.number1=0;
                this.SpriteCheck();
            }
        }
    },
    SpriteCheck: function () {
        var SpriteCheck=[];
        for(var i=0;i<5;i++){
            var num=0;
            for(var j=0;j<5;j++){
                if( this.block2[i * M_wight + j]){
                    if(this.block2[i * M_wight + j].m_col>=num){
                        num=this.block2[i * M_wight + j].m_col
                    }
                }
            }

            SpriteCheck.push(this.block2[i * M_wight + num])
        }

        for(var j=0;j<5;j++){
            var num=0;
            for(var i=0;i<5;i++){
                if( this.block2[i * M_wight + j]){
                    if(this.block2[i * M_wight + j].m_row>=num){
                        num=this.block2[i * M_wight + j].m_row
                    }
                }
            }
            if(gameLevel==2||gameLevel==6||gameLevel==10){
                if(j==3){}else{
                    if(SpriteCheck.indexOf(this.block2[num * M_wight + j])==-1){
                        SpriteCheck.push(this.block2[num * M_wight + j])
                    }
                }
            }
            else if(gameLevel==3||gameLevel==7||gameLevel==11){
                if(j==1){}else{
                    if(SpriteCheck.indexOf(this.block2[num * M_wight + j])==-1){
                        SpriteCheck.push(this.block2[num * M_wight + j])
                    }
                }
            }
            else if(gameLevel==4||gameLevel==8||gameLevel==12){
                if((j==3)||(j==1)){}else{
                    if(SpriteCheck.indexOf(this.block2[num * M_wight + j])==-1){
                        SpriteCheck.push(this.block2[num * M_wight + j])
                    }
                }
            }else {
                if(SpriteCheck.indexOf(this.block2[num * M_wight + j])==-1){
                    SpriteCheck.push(this.block2[num * M_wight + j])
                }
            }
        }
        var i=0
        for(var a=0;a<SpriteCheck.length;a++){
            if(this.MovePengblock[0]&&SpriteCheck[a]&&(this.MovePengblock[0].tag==SpriteCheck[a].tag||this.MovePengblock[0].tag==5)){
                i++;
            }
        }
        if(i==0){
            number--;
            if(number<=0){
                this.num.setString(number);

                cc.director.pause()
                var layer = new AnginLayer(1);
                this.addChild(layer,100);

                //this.unscheduleAllCallbacks();
                //this.hero.removeFromParent();
                //this.Moveblock[0].removeFromParent();
                //var layer = new GameoverLayer();
                //this.addChild(layer,100);
            }else{
                this.num.setString(number);
            }
            this.tishi.setVisible(true);
            var fadeIn=new cc.fadeIn(0.2);
            var fadeout = new  cc.fadeOut(0.3);
            this.tishi.runAction(cc.sequence(fadeIn,fadeout));
            var x=this.Moveblock[0].x;
            var y=this.Moveblock[0].y;
            this.Moveblock[0].removeFromParent();
            this.Moveblock.splice(0,1);
            for(var z=0;z<this.MovePengblock.length;z++){
                this.MovePengblock[z].removeFromParent();
                this.MovePengblock.splice(z,1);
                z--;
            }


            var str1 =flax.assetsManager.createDisplay(res.All_plist, "b"+5, {parent:this,x:this.hero.x-40,y:this.hero.y});
            str1.setLocalZOrder(2);
            str1.setTag(5);
            this.Moveblock.push(str1);
            for(var v=1;v<3;v++){
                var rect1=str1['rect'+v]
                rect1.setVisible(false);
                rect1.tag=5;
                this.MovePengblock.push(rect1);
            }
        }
    },
    fillVacancies : function(){
        var colEmptyInfo = [];
        for(var col = 0;col < M_wight ; col++){
            var removedRoleOfCol = 0;
            for(var row = 0 ; row < M_height ; row ++){
                var roleSprite = this.block2[row * M_wight + col];
                if(roleSprite == null){
                    removedRoleOfCol ++;
                }else{
                    if(removedRoleOfCol > 0){
                        var newRow = row - removedRoleOfCol;
                        this.block2[newRow * M_wight + col] = roleSprite;
                        this.block2[row * M_wight + col] = null;
                        var startPosition = roleSprite.getPosition();
                        var endPosition = this.PointGetEnd(newRow,col);
                        var speed = (startPosition.y - endPosition.y)/this.size.height;
                        roleSprite.stopAllActions();
                        roleSprite.runAction(new cc.moveTo(speed,endPosition));
                        roleSprite.m_row = newRow;
                    }
                }
            }
            colEmptyInfo[col] = removedRoleOfCol;
        }

    },
    PointGetEnd : function(row , col){
        this.endX = this.m_arrayLeftspriteX + (this.bujuSize + role_GAP) * col + this.bujuSize/2-80;
        this.endY = this.m_arrayLeftspriteY + (this.bujuSize + role_GAP1) * row + this.bujuSize/2-89;
        var point = cc.p(this.endX,this.endY);
        return point;
    },
    CreateBlock: function () {
        for(var i=0;i<BlockData[gameLevel-1].length;i++) {
            for(var j=0;j<BlockData[gameLevel-1][i].length;j++){
                var block=flax.assetsManager.createDisplay(res.All_plist, "b6", {parent: this});
                block.x = 50 + 56*BlockData[gameLevel-1][i][j][0];
                block.y = 648 + 50*BlockData[gameLevel-1][i][j][1];
                block.setAnchorPoint(0.5,0.5);
                this.block1.push(block);
            }
        }
    },
    CreateSprite: function (row , col) {
        var MM = Math.floor(cc.rand()%4)+1;
        var str =flax.assetsManager.createDisplay(res.All_plist, "b"+MM);
        str.m_row = row;
        str.m_col = col;
        str.setPosition(cc.p(row,col));
        str.setTag(MM);
        return str;
    },
    CreateAnginSprite: function (m,x,y) {
        var str =flax.assetsManager.createDisplay(res.All_plist, "b"+m, {parent:x,y:y});
        str.setTag(m);
    },
    initMatrix : function(){
        for(var row = 0 ; row < M_height ; row ++ ) {
            for(var col = 0 ; col <  M_wight; col ++) {
                this.addRoleSprite(row , col);
            }
        }
    },
    addRoleSprite : function(row , col){

        this.roleSprite = this.CreateSprite(row,col);
        //精灵开始出现的位置
        this.m_arrayLeftspriteX = (this.size.width - this.bujuSize * M_wight - role_GAP * (M_wight-1))/2;
        this.m_arrayLeftspriteY = (this.size.height*2/3 - this.bujuSize * M_height - role_GAP1 *(M_height-1) )/2.5;
        //精灵下落到终点的位置
        var x = (this.m_arrayLeftspriteX + (this.bujuSize + role_GAP) * col + this.bujuSize/2)-80;
        var y = (this.m_arrayLeftspriteY + (this.bujuSize + role_GAP1) * row + this.bujuSize/2)-89;
        var endPosition = cc.p(x,y);
        this.roleSprite.setAnchorPoint(0.5,0.5);
        this.StartPositinon = cc.p(x , y + this.size.height / 2);
        this.roleSprite.attr({
            x : this.StartPositinon.x,
            y : this.StartPositinon.y
        });
        this.addChild(this.roleSprite,1);
        this.speed = this.StartPositinon.y / (this.size.height*2)*1.5;
        var moveAc = new cc.moveTo(this.speed/2,endPosition);
        this.roleSprite.runAction(moveAc);
        this.block2[row * M_wight + col] = this.roleSprite;

    },

    MyBoundingBoxA :function( node,object)
    {
        var rect = cc.rect(node.x,node.y,object.getBoundingBox().width,object.getBoundingBox().height);
        return rect;
    },
    MyBoundingBox :function( node)
    {
        var rect = cc.rect(node.x-33,node.y-10,30,20);
        return rect;
    }
});
