var NewGuiderLayer = cc.LayerColor.extend({
    newGuider:null,
    ctor:function(){
        this._super(cc.color(10,10,10,50));
        var touchListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            }
        });

        cc.eventManager.addListener(touchListener,this);
        cc.director.pause();
        this.newGuider = flax.assetsManager.createDisplay(res.newGuider_plist, "newGuider", {parent: this, x:240 , y:400});
        this.newGuider.setAnchorPoint(0.5,0.5);
        flax.inputManager.addListener(this.newGuider.btn, this.close, InputType.click,this);
        return true;
    },
    close:function(){
        this.removeFromParent()
        cc.director.resume();
        //var start = new StartLayer();
        //GameNode.addChild(start,50);
    }
});