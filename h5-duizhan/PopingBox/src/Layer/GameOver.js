var GameoverLayer = cc.Layer.extend({
    ctor: function () {
        this._super();
        var winSize=cc.winSize;
        var bg=new cc.Sprite(res.startbg);
        bg.setPosition(winSize.width/2,winSize.height/2);
        this.addChild(bg)
        var start_bg1=new cc.Sprite(res.start_bg1)
        start_bg1.setPosition(winSize.width/2,winSize.height-150)
        start_bg1.setScale(0.8);
        this.addChild(start_bg1);
        hlmySetShare(score,'jingdian')
        var type_score=score;
        var game_score=score;
        var game_type=0;
        var availabity=0;
        var url="/openapi/game/interactionGame/postGameLog.json?availabity="+availabity+"&aid="+appId+"&gameScore="+game_score+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+""+"&scoreId="+scoreId
        var xhr = cc.loader.getXMLHttpRequest();
        var url=url;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.send()
        var start_score=new cc.Sprite(res.Score)
        start_score.setPosition(winSize.width-80,winSize.height-40)
        start_score.setScale(0.8)
        this.addChild(start_score);
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,40,winSize.height-40,40,self);
        });
        //var name_str="李乾@1758"
        var name_str1=window.select.userProfile.nickname
        var name_str= getBytesLength(name_str1,12)
        var record_name = new cc.LabelTTF(name_str, "Arial", 20);
        record_name.x = 70;
        record_name.y =winSize.height-40;
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);


        var score_str="本局得分:"
        var score_str_name = new cc.LabelTTF(score_str, "Arial", 30);
        score_str_name.x = winSize.width/2;
        score_str_name.y =winSize.height-252;
        score_str_name.setAnchorPoint(0.5,0.5);
        score_str_name.setColor(cc.hexToColor("#ff8448"))
        this.addChild(score_str_name, 5);
        score=Score1
        var game_Score=new cc.LabelTTF("0", "Arial", 40);
        game_Score.x = winSize.width / 2;
        game_Score.y = winSize.height-302;
        game_Score.setColor(cc.hexToColor("#fe5417"));
        game_Score.setScale(1.6)
        game_Score.setString(score)

        if(score>100000&&score<100000){
            game_Score.setScale(0.7)
        }else if(score>=100000){
            game_Score.setScale(0.6)
        }
        this.addChild(game_Score, 10);
        var self=this
        var Best_Score_str= cc.sys.localStorage.getItem("sjxcBestScore");
        if(Best_Score_str){
            if(Best_Score_str<score){
                Best_Score_str=score
            }
        }else{
            Best_Score_str=score
        }
        cc.sys.localStorage.setItem("sjxcBestScore",Best_Score_str);
        cc.sys.localStorage.setItem("sjxcAngin",0);
        var best_str="最高分:"
        var best_str_name = new cc.LabelTTF(best_str, "Arial", 25);
        best_str_name.x = winSize.width/2;
        best_str_name.y =winSize.height-362;
        best_str_name.setAnchorPoint(0.5,0.5);
        best_str_name.setColor(cc.color(170,170,170))
        this.addChild(best_str_name, 5);
        var Best_score= new cc.LabelTTF("0", "Arial", 30);
        Best_score.x = winSize.width/2;
        Best_score.y = winSize.height-402;
        Best_score.setAnchorPoint(0.5,0.5)
        Best_score.setColor(cc.color(41,175,235));
        Best_score.setString(Best_Score_str)
        this.addChild(Best_score,10);

        var xhr = cc.loader.getXMLHttpRequest();
        var  url="/openapi/game/interactionGame/index.json?aid="+appId;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status <= 207){
                    var resw = xhr.responseText;
                    var jsondata = JSON.parse(resw);
                    var data = jsondata["data"];
                    gameNum2=data.userPoint;//积分总数
                    getScore = new cc.LabelTTF("0", "Arial", 20);
                    getScore.x = winSize.width-60
                    getScore.y =winSize.height-42;
                    getScore.setColor(cc.color(88,195,224));
                    getScore.setString(gameNum2);
                    self.addChild(getScore, 1);
                    if(gameNum2>=10000&&gameNum2<1000000){
                        getScore.setScale(0.8)
                    }else if(gameNum2>=1000000){
                        getScore.setScale(0.6)
                    }
                }
            }
        }
        xhr.send();
        CreateBtn("res/start/show.png","res/start/show.png", function () {
            var share = document.getElementById('share-square');
            share.style.display = 'block';

        },winSize.width/2,320,0.5,0.5,5,this,1)

        CreateBtn("res/start/angin.png","res/start/angin.png", function () {
            score=0;
            hlmySetShare(0,'jingdian')
            flax.replaceScene("Maingame")
            gameLevel=1
            number=3
            over_Score=0
        },winSize.width/2,220,0.5,0.5,5,this,1)
        CreateBtn("res/start/gohome.png","res/start/gohome.png", function () {
            //cc.director.runScene(new StartScene());
            flax.replaceScene("startGame")
        },winSize.width/2,120,0.5,0.5,5,this,1)
        CreateBtn("res/start/follow.png","res/start/follow.png", function () {
            //关注
            showEWM()
        },70,120,0.5,0.5,5,this,1)
        CreateBtn("res/start/invite.png", "res/start/invite.png", function () {
            //邀请
            var share = document.getElementById('share-square');
            share.style.display = 'block';
        },winSize.width-70,120,0.5,0.5,5,this,1)
        var startlogo=new cc.Sprite("res/start/start_logo.png")
        startlogo.setPosition(winSize.width/2,50);
        startlogo.setAnchorPoint(0.5,0.5);
        this.addChild(startlogo);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;

            }
        });
        cc.eventManager.addListener(listener, this);


        return true;

    }
});
var GameOverScene=cc.Scene.extend({
    onEnter: function () {
        this._super()
        var layer=new GameoverLayer();
        this.addChild(layer)
    }
})