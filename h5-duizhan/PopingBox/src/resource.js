var res = {
    All_plist:"res/"+ g.language+"/All.plist",
    All_png:"res/"+ g.language+"/All.png",
    //bg_plist:"res/bg.plist",
    //bg_png:"res/bg.png",
    action_plist:"res/action.plist",
    action_png:"res/action.png",
    newGuider_plist:"res/"+ g.language+"/newGuider.plist",
    newGuider_png:"res/"+ g.language+"/newGuider.png",
    end_plist:"res/"+ g.language+"/end.plist",
    end_png:"res/"+ g.language+"/end.png",
    start_plist:"res/"+ g.language+"/start.plist",
    start_png:"res/"+ g.language+"/start.png",
    level_plist:"res/"+ g.language+"/level.plist",
    level_png:"res/"+ g.language+"/level.png",
    //l1:"res/"+ g.language+"/L1.png"


    startbg:"res/start/start_bg.jpg",
    Score: "res/start/Score.png",
    start_bg1:"res/start/start_bg1.png",
    start_btn1: "res/start/start_btn1.png",
    start_btn2:  "res/start/start_btn2.png",
    follow:"res/start/follow.png",
    moregame:  "res/start/moregame.png",
    invite:"res/start/invite.png"
};
var res_start = [
    res.start_plist,
    res.start_png,
    res.start_png,
    res.startbg,
    res.Score,
    res.start_bg1,
    res.start_btn1,
    res.start_btn2,
    res.follow,
    res.moregame,
    res.invite

];
var res_Maingame = [
    res.All_plist,
    res.All_png,
    //res.bg_plist,
    //res.bg_png,
    res.end_plist,
    res.end_png,
    res.level_plist,
    res.level_png,
    res.newGuider_plist,
    res.newGuider_png,
    res.action_plist,
    res.action_png,
    //res.l1
];

