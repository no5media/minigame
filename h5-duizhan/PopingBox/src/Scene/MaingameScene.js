var MiangameScene  = cc.Scene.extend({
    onEnter:function(){
        this._super();

        var layer = new MaingameLayer();
        this.addChild(layer);


        flax.fetchUserData();
        if(!flax.userData.isFirstPlay){
            flax.userData.isFirstPlay = true;
            var newlayer = new NewGuiderLayer();
            this.addChild(newlayer,1);
            flax.saveUserData();
        }

    }
});