var gameData = [
    {level:1,FenShu:220},
    {level:2,FenShu:440},
    {level:3,FenShu:660},
    {level:4,FenShu:920},
    {level:5,FenShu:1220},
    {level:6,FenShu:1520},
    {level:7,FenShu:1820},
    {level:8,FenShu:2220},
    {level:9,FenShu:2720},
    {level:10,FenShu:3320},
    {level:11,FenShu:4020},
    {level:12,FenShu:5320}
];

var BlockData = [
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1],[2,-1]],  [[0,-2],[1,-2]],   [[0,-3]]],//1
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1]],         [[0,-2]],          [[0,-3]]],//2
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1],[2,-1]],  [[0,-2],[1,-2]],           ],//3
    [                   [[3,-1]],                                   [[1,-3]]],         //4

    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1],[2,-1]],  [[0,-2],[1,-2]],   [[0,-3]]],//5
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1]],         [[0,-2]],          [[0,-3]]],//6
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1],[2,-1]],  [[0,-2],[1,-2]],           ],//7
    [                   [[3,-1]],                                   [[1,-3]]         ],//8

    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1],[2,-1]],  [[0,-2],[1,-2]],   [[0,-3]]],//9
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1]],         [[0,-2]],          [[0,-3]]],//10
    [[[0,0],[1,0],[2,0],[3,0]],  [[0,-1],[1,-1],[2,-1]],  [[0,-2],[1,-2]],           ],//11
    [                   [[3,-1]],                                   [[1,-3]]         ] //12
];
var gameLevel =1;