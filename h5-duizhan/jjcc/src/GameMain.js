Score=0;
Main_layer=null;
Is_Angin=false;
Is_overOrplay=false;
first_num=0;
second_num=0;
Is_correct=false;
var GameMainLayer=cc.LayerColor.extend({
    first_Sp:null,
    second_Sp:null,
    end_Sp:null,
    ctor: function () {
        this._super(cc.color(231,237,239));
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        Score=0;
        time=0;
        clock_time=40;
        first_num=0;
        second_num=0;
        end_num=0;
        correct_num=0;
        //error_num=0;
        Main_layer=this;
        Is_Angin=false;
        Is_overOrplay=false;
        Is_correct=false;
        this.CreatePeopleInformation();
        this.DrawBg();
        this.CreateClock();
        this.CreateBg();
        this.CreateNum();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt){
        time+=dt;
        if(time>=1){
            time=0;
            clock_time--;
            timeclock.setString(clock_time);
            if(clock_time==0){
                if(angin_num<3){
                    cc.director.pause();
                    var layer=new AnginLayer(1);
                    this.addChild(layer,12);
                }else{
                    Is_overOrplay=true
                }
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false;
            clock_time+=5;
            timeclock.setString(clock_time);
            PeppleScore.setString(window.select.userPoint);
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return true;
        }
        if(Is_correct){
            Is_correct=false;
            this.CreateNum();
        }
    },
    CreateBg: function () {
        var winSize=cc.winSize;
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_bg1.png"), function () {
            var num=first_num+second_num;
            if(num==correct_num){
                Is_correct=true;
                Score+=96
                SpgetScore.setString(Score)
            }else{
                Score-=150;
                if(Score<0){
                    Score=0
                }
                SpgetScore.setString(Score)
                Main_layer.CreateError(165 ,411)
            }
        },165 ,411,0.5,0.5,this);
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_bg1.png"), function () {
            var num=first_num-second_num;
            if(num==correct_num){
                Is_correct=true;
                Score+=96
                SpgetScore.setString(Score)
            }else{
                Score-=150
                if(Score<0){
                    Score=0
                }
                SpgetScore.setString(Score)
                Main_layer.CreateError(475,411)
            }
        },475,411,0.5,0.5,this) ;
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_bg1.png"), function () {

            var num=first_num*second_num;
            if(num==correct_num){
                Is_correct=true;
                Score+=96
                SpgetScore.setString(Score)
            }else{
                Score-=150
                if(Score<0){
                    Score=0
                }
                SpgetScore.setString(Score)
                Main_layer.CreateError(165,214);
            }
        },165,214,0.5,0.5,this);
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_bg1.png"), function () {
            var num=first_num/second_num;
            if(num==correct_num){
                Is_correct=true;
                Score+=96
                SpgetScore.setString(Score)
            }else{
                Score-=150;
                if(Score<0){
                    Score=0
                }
                SpgetScore.setString(Score)
                Main_layer.CreateError(475,214);
            }
        },475,214,0.5,0.5,this);
        var num=1;
        for(var a=0;a<2;a++){
            for(var b=0;b<2;b++){
                var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_btn"+num+".png"))
                sp.x=165+a*310;
                sp.y=416-b*197;
                this.addChild(sp,5);
                num++;
            }
        }
        first_Sp=new cc.LabelTTF("0", "Arial", 100);
        first_Sp.x=90;
        first_Sp.y=winSize.height-400;
        first_Sp.setColor(cc.color(7,182,255));
        first_Sp.setString(first_num)
        this.addChild(first_Sp,10);
        second_Sp=new cc.LabelTTF("0", "Arial", 100);
        second_Sp.x=winSize.width/2+15;
        second_Sp.y=winSize.height-400;
        second_Sp.setColor(cc.color(7,182,255));
        second_Sp.setString(second_num)
        this.addChild(second_Sp,10);
        end_Sp=new cc.LabelTTF("0", "Arial", 100);
        end_Sp.x=winSize.width-90;
        end_Sp.y=winSize.height-400;
        end_Sp.setColor(cc.color(7,182,255));
        end_Sp.setString(end_num)
        this.addChild(end_Sp,10);
        game_s1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_s1.png"));
        game_s1.x=220;
        game_s1.y=winSize.height-360;
        this.addChild(game_s1,10);
        game_s2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_s2.png"));
        game_s2.x=450;
        game_s2.y=winSize.height-400;
        this.addChild(game_s2,10);

    },
    CreateNum: function () {
        var winSize=cc.winSize;
        var num=Math.floor(Math.random()*4+1);
        //var num=2
        switch(num)
        {
            case 1:
                this.Create_Jia_Num();
                break;
            case 2:
                this.Create_Jian_Num();
                break;
            case 3:
                this.Create_Chen_Num();
                break;
            case 4:
                this.Create_Chu_Num();
                break;
        }
    },
    Create_Jia_Num: function () {
        var Is_can=false;
        var sp_i=0
        do{
            first_num=Math.floor(Math.random()*19+1);
            second_num=Math.floor(Math.random()*19+1);
            sp_i++
            var correct1=first_num+second_num;
            var correct2=first_num-second_num;
            var correct3=first_num*second_num;
            var correct4=first_num/second_num;
            if(correct1==correct2){
                Is_can=true
            }else if(correct1==correct3){
                Is_can=true
            }else if(correct1==correct4){
                Is_can=true
            }else{
                Is_can=false;

                correct_num=correct1;
                first_Sp.setString(first_num);
                second_Sp.setString(second_num);
                end_Sp.setString(correct_num);
                break
            }

        }while(!Is_can);
    },
    Create_Jian_Num: function () {
        var Is_can=true;
        do{
            first_num=Math.floor(Math.random()*19+1);
            second_num=Math.floor(Math.random()*first_num);
            var correct1=first_num-second_num;
            var correct2=first_num+second_num;
            var correct3=first_num*second_num;
            var correct4=first_num/second_num;
            if(correct1==correct2){
                Is_can=true
            }else if(correct1==correct3){
                Is_can=true
            }else if(correct1==correct4){
                Is_can=true
            }else{
                cc.log("-")
                Is_can=false;
                correct_num=correct1;
                first_Sp.setString(first_num);
                second_Sp.setString(second_num);
                end_Sp.setString(correct_num);
                break
            }
        }while(Is_can);
    },
    Create_Chen_Num: function () {
        var Is_can=true;
        do{
            first_num=Math.floor(Math.random()*10);
            second_num=Math.floor(Math.random()*10);
            var correct1=first_num*second_num;
            var correct2=first_num-second_num;
            var correct3=first_num+second_num;
            var correct4=first_num/second_num;
            if(correct1==correct2){
                Is_can=true
            }else if(correct1==correct3){
                Is_can=true
            }else if(correct1==correct4){
                Is_can=true
            }else{
                Is_can=false;
                correct_num=correct1;
                first_Sp.setString(first_num);
                second_Sp.setString(second_num);
                end_Sp.setString(correct_num);
                break
            }

        }while(Is_can);
    },
    Create_Chu_Num: function () {
        var Is_can=true;
        do{
            first_num=Math.floor(Math.random()*10);
            second_num=Math.floor(Math.random()*9+1);
            first_num=first_num*second_num;
            var correct1=first_num/second_num;
            var correct2=first_num-second_num;
            var correct3=first_num*second_num;
            var correct4=first_num+second_num;
            if(correct1==correct2){
                Is_can=true;
            }else if(correct1==correct3){
                Is_can=true;
            }else if(correct1==correct4){
                Is_can=true;
            }else{
                Is_can=false;
                correct_num=correct1;
                first_Sp.setString(first_num);
                second_Sp.setString(second_num);
                end_Sp.setString(correct_num);
                break
            }
        }while(Is_can);
    },
    CreateClock: function () {
        var winSize=cc.winSize;
        var time_str="倒计时:";
        var timeName = new cc.LabelTTF(time_str, "Arial", 20);
        timeName.x = 170;
        timeName.y = winSize.height-130;
        timeName.setColor(cc.color(255,255,255));
        this.addChild(timeName, 6);
        timeclock=new cc.LabelAtlas("40", "res/num.png", 20, 29,"0");
        timeclock.setPosition(210, winSize.height-140);
        this.addChild(timeclock,10);
        SpgetScore=new cc.LabelAtlas("0", "res/num.png", 20, 29,"0");
        SpgetScore.setPosition(winSize.width-80, winSize.height-130);
        SpgetScore.setAnchorPoint(0.5,0.5);
        this.addChild(SpgetScore,10);
    },
    CreateError: function (x,y) {
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        var winSize=cc.winSize;
        var layer=new cc.LayerColor(cc.color(225,50,120,60));
        this.addChild(layer);
        var blink =new cc.Blink(0.3,2);//闪烁函数
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg2.png"));
        sp.setAnchorPoint(0.5,0.5);
        sp.setPosition(x,y);
        this.addChild(sp,10);
        var no_sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_s3.png"));
        no_sp.setPosition(sp.getBoundingBox().width/2,sp.getBoundingBox().height/2);
        sp.addChild(no_sp,10);
        var destory = new cc.RemoveSelf(true);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
        });
        layer.runAction(cc.repeatForever(cc.sequence(blink,fun,destory)))
    },
    DrawBg: function () {
        var winSize=cc.winSize;
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,5);
        drawNode.drawRect(
            cc.p(0,0), // 起点
            cc.p(winSize.width,100), // 起点的对角点
            cc.color(0,0, 0, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(winSize.width,winSize.height), // 起点
            cc.p(0,winSize.height-160), // 起点的对角点
            cc.color(6,183, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(6,183, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(15,winSize.height-200), // 起点
            cc.p(winSize.width-15,winSize.height-600), // 起点的对角点
            cc.color(255,255, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        cc.spriteFrameCache.getSpriteFrame("Score.png");
        var start_score=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("Score.png"));
        start_score.setPosition(winSize.width,winSize.height-70);
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score,10);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12);
        //var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70;
        record_name.setAnchorPoint(0,0.5);
        record_name.setColor(cc.color(255,255,255));
        this.addChild(record_name, 10);
        var get_score=window.select.userPoint;
        //var get_score=10
        PeppleScore = new cc.LabelTTF("0", "Arial", 30);
        PeppleScore.x = winSize.width-70;
        PeppleScore.y =winSize.height-70;
        PeppleScore.setAnchorPoint(0.5,0.5);
        PeppleScore.setColor(cc.color(254,202,75));
        PeppleScore.setString(get_score);
        this.addChild(PeppleScore, 10);

    },
    GameOver: function () {
        var winSize=cc.winSize;
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners();
        var dely=cc.delayTime(1.5);
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("end_bg.png"));
        sp.setPosition(winSize.width/2,winSize.height/2);
        sp.setScale(0.1);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
            var transition=new cc.TransitionProgressRadialCW(1,new GameOverScene());
            cc.director.runScene(transition);
        });
        var fd = new cc.scaleTo(0.1,1);
        this.addChild(sp,12)
        sp.runAction(cc.sequence(fd,dely,fun));
    }
});
var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})
