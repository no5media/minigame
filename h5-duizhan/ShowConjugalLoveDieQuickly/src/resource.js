var res = {
    main_block1:"res/main/block1.png",
    main_block2:"res/main/block2.png",
    HelloWorld_png : "res/HelloWorld.png",
    start_bg:"res/start/bg.png",
    start_btn1:"res/start/btn1.png",
    start_btn2:"res/start/btn2.png",
    main_bg1:"res/main/bg.png",
    main_bg2:"res/main/bg2.png",
    main_hero:"res/main/hero.png",
    main_love_hero:"res/main/love_hero.png",
    over_bg:"res/over/bg.png",
    over_left:"res/over/left.png",
    over_left_btn:"res/over/left_btn.png",
    over_right:"res/over/right.png",
    over_right_btn:"res/over/right_btn.png",

    Score: "res/start/Score.png",
    start_bg1:"res/start/start_bg1.png",
    start_btn1: "res/start/start_btn1.png",
    start_btn2:  "res/start/start_btn2.png",
    follow:"res/start/follow.png",
    moregame:  "res/start/moregame.png",
    invite:"res/start/invite.png",
};

var g_resources = [];
for (var i in res) {
    g_resources.push(res[i]);
}
