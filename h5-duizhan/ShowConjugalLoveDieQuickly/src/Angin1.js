
var AnginLayer1=cc.Layer.extend({
    ctor: function (num) {
        this._super();
        var winSize=cc.director.getVisibleSize();
        var angin_num=cc.sys.localStorage.getItem("dsgnzkAngin")
        if(angin_num){
            angin_num++
        }else{
            angin_num=1
        }
        cc.sys.localStorage.setItem("dsgnzkAngin",angin_num);
        cc.log("angin_num",angin_num)
        var _str="网络异常，请重试!"
        var _name = new cc.LabelTTF(_str, "Arial", 24);
        _name.x = winSize.width/2;
        _name.y =winSize.height/2+80;
        this.addChild(_name, 5);
        _name.setColor(cc.color(255,0,0))
        var anginNum;
        if(angin_num<=3){
            anginNum=20*angin_num
        }else if(angin_num>3&&angin_num<=4){
            anginNum=100
        }else{
            anginNum=200
        }
        var angin_str="消耗"+anginNum+"个积分继续挑战？"
        var angin_name = new cc.LabelTTF(angin_str, "Arial", 24);
        angin_name.x = winSize.width/2;
        angin_name.y =winSize.height/2+40;
        this.addChild(angin_name, 5);

        var shengyu_num="剩余积分: "+window.select.userPoint
        //var shengyu_num="剩余积分: "+10
        var shengyu_str = new cc.LabelTTF(shengyu_num, "Arial", 24);
        shengyu_str.x = winSize.width/2;
        shengyu_str.y =winSize.height/2-20;
        this.addChild(shengyu_str, 5);
        var bg=new cc.Sprite("res/start/fristBg.png");
        bg.setPosition(winSize.width/2,winSize.height/2);
        bg.setScale(0.8)
        this.addChild(bg);
        var self=this;
        CreateBtn("res/start/fristbtn1.png","res/start/fristbtn1.png", function () {
            var angin_type=2+angin_num-1
            var xhr = cc.loader.getXMLHttpRequest();
            var  url="/openapi/game/interactionGame/reduceUserPoint.json?aid="+appId+"&type="+angin_type;
            xhr.open("POST", url,true);
            xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4 ){
                    if(xhr.status >= 200 && xhr.status <= 207) {
                        var resw = xhr.responseText;
                        var jsondata = JSON.parse(resw);
                        var data = jsondata["data"];
                        if(data.code==1){
                            window.select.userPoint=data.userPoint
                            if(num==1){
                                Is_Angin=true
                                cc.director.resume();
                            }else{
                                //net_angin=true
                            }
                            self.removeFromParent();
                        }else if(data.code==0){
                            if(num==1){
                                var layer=new  blackLayer("您当前积分不足","确定", function () {
                                    if(num==1){
                                        Is_overOrplay=true
                                        cc.director.resume();
                                    }else{
                                        net_overOrplay=true
                                    }
                                    block_layer.removeFromParent()
                                });
                                self.removeFromParent()
                                Main_layer.addChild(layer,10)
                            }else if(num==2){
                                //var layer=new  blackLayer("积分不足","确定", function () {
                                //    if(num==1){
                                //        Is_overOrplay=true
                                //    }else{
                                //        net_overOrplay=true
                                //    }
                                //    block_layer.removeFromParent()
                                //});
                                //self.removeFromParent()
                                //Net_Main_layer.addChild(layer,10)
                            }
                        }else if(data.code==-1){
                            if(num==1){
                                self.removeFromParent()
                                var layer=new  AnginLayer1()
                                Main_layer.addChild(layer,10)
                            }else  if(num==2){
                                //self.removeFromParent()
                                //var layer=new  AnginLayer1()
                                //Net_Main_layer.addChild(layer,10)
                            }
                        }
                    }
                }else{

                }
            }
            xhr.send();

        },winSize.width/2-80,winSize.height/2-80,0.5,0.5,5,this,1)
        CreateBtn("res/start/fristbtn1.png","res/start/fristbtn1.png", function () {
            if(num==1){
                Is_overOrplay=true
                Is_Click=false
                cc.director.resume();
            }else{
                net_overOrplay=true
                net_Click=false
            }
            self.removeFromParent();
        },winSize.width/2+80,winSize.height/2-80,0.5,0.5,5,this,1)
        var quxiao="取消"
        var  quxiao_name = new cc.LabelTTF(quxiao, "Arial", 30);
        quxiao_name.x = winSize.width/2+80;
        quxiao_name.y =winSize.height/2-80;
        this.addChild(quxiao_name, 5);
        var buman="重试"
        var  buman_name = new cc.LabelTTF(buman, "Arial", 30);
        buman_name.x = winSize.width/2-80;
        buman_name.y =winSize.height/2-80;
        this.addChild(buman_name, 5);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {

                return true;
            }
        });
        cc.eventManager.addListener(listener, this);
    }
})