Score=0
Main_layer=null
Is_overOrplay=null
Is_Angin=null
var GameMainLayer=cc.LayerColor.extend({
    bg1:null,
    bg2:null,
    left_lover:null,
    right_lover:null,
    winSize:null,
    Score_name:null,
    ctor: function () {
        Main_layer=this
        Block_Array=[];
        posX_Array=[];
        Count_Array=[];
        Is_Angin=false
        Is_overOrplay=false
        Is_Click=false;
        Is_Schedule=true
        Is_true=false
        Count_num=0;
        speed=10
        speed1=6;
        schedule_time1=0.7;
        PosXY=40
        time=0;
        Is_Angin_time=0
        time1=0;
        this._super(cc.color(255,255,255));
        winSize=cc.director.getVisibleSize();
        for(var a=0;a<5;a++){
            var posx=40+a*40;
            posX_Array.push(posx);
        }
        posX_Array.push(215);
        for(var a=0;a<1;a++){
            for(var b=0;b<posX_Array.length;b++){
                Count_Array.push(posX_Array[b]);
            }
        }
        Count_Array.shuffle();
        Count_num=Math.floor(Math.random()*Count_Array.length-1);
        this.CreateSprite();
        var self=this;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                Is_Click=true;
                love_hero.setVisible(false);
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=false;
            }
        });
        cc.eventManager.addListener(listener, this);
        this.scheduleUpdate();
    },
    update: function (dt) {
        time+=dt;
        time1+=dt;
        if(time>=1){
            Score++;
            Score_name.setString(Score)
            time=0;
        }
        if(Is_Angin){
            Is_Angin_time+=dt;
            if(Is_Angin_time>1){
                Is_Angin=false;
                Is_Angin_time=0
            }
        }
        if(Is_overOrplay){
            Is_overOrplay=false
            this.overFun()
            return
        }
        if(Score>10){
            speed=7
            speed1=7
            schedule_time1=0.45
        }else{
            speed=6
            speed1=7
            schedule_time1=0.6
        }
        if(time1>=schedule_time1){
            time1=0;
            this["BlockAction" +1]();
        }
       for(var a=0;a<Block_Array.length;a++){
           if(Block_Array[a]){
               if(Block_Array[a].tag==1){
                   Block_Array[a].y-=speed;
               }else if(Block_Array[a].tag==2){
                   Block_Array[a].y-=speed+1.5;
               }
               if(Block_Array[a].y<=-20){
                   Block_Array[a].removeFromParent();
                   Block_Array.splice(a,1);
                   a--
               }
           }
       }
        if(Is_Click){
            left_lover.setVisible(true)
            right_lover.setVisible(true)
            left_lover.x-=speed1
            right_lover.x+=speed1
            if(left_lover.x<=40){
                left_lover.x=40
            }
            if(right_lover.x>=440){
                right_lover.x=440
            }
        }else{
            left_lover.x+=speed1
            right_lover.x-=speed1
            if(left_lover.x>=winSize.width/2-22){
                left_lover.x=winSize.width/2-22
                left_lover.setVisible(false)
                love_hero.setVisible(true);
            }
            if(right_lover.x<=winSize.width/2+22){
                right_lover.x=winSize.width/2+22
                right_lover.setVisible(false)
                love_hero.setVisible(true);
            }
        }
        for(var a=0;a<Block_Array.length;a++){
            if(!Is_Angin&&Block_Array[a]&&cc.pDistance(Block_Array[a].getPosition(),left_lover.getPosition())<=PosXY){
                cc.director.pause()
                var angin_num=cc.sys.localStorage.getItem("dsgnzkAngin")
                if(angin_num<3){
                    var layer=new AnginLayer(1);
                    this.addChild(layer,10);
                }else{
                    cc.director.resume();
                    this.overFun()
                    return
                }
                break

            } else if(!Is_Angin&&Block_Array[a]&&cc.pDistance(Block_Array[a].getPosition(),right_lover.getPosition())<=PosXY){
                cc.director.pause()
                var angin_num=cc.sys.localStorage.getItem("dsgnzkAngin")
                if(angin_num<3){
                    var layer=new AnginLayer(1);
                    this.addChild(layer,10);
                }else{
                    cc.director.resume();
                    this.overFun()
                    return
                }
                break
            }
        }
    },
    CreateSprite: function () {
        bg1=new cc.Sprite(res.main_bg1);
        bg1.setPosition(winSize.width/2,winSize.height/2-150);
        this.addChild(bg1);
        //var bg4=new cc.Sprite("res/sp1.png");
        //bg4.setPosition(0,0);
        //bg4.setAnchorPoint(0,0);
        //this.addChild(bg4);
        bg2=new cc.Sprite(res.main_bg2);
        bg2.setPosition(winSize.width/2,winSize.height/2-150);
        this.addChild(bg2);
        left_lover=new cc.Sprite(res.main_hero);
        left_lover.setPosition(winSize.width/2-22,winSize.height/2-150);
        this.addChild(left_lover);
        left_lover.setVisible(false)
        right_lover=new cc.Sprite(res.main_hero);
        right_lover.setPosition(winSize.width/2+22,winSize.height/2-150);
        this.addChild(right_lover);
        right_lover.setVisible(false);
        love_hero=new cc.Sprite(res.main_love_hero);
        love_hero.setPosition(winSize.width/2,winSize.height/2-140);
        this.addChild(love_hero);
        Score_name = new cc.LabelTTF("0", "Arial", 38);
        Score_name.x = winSize.width-40;
        Score_name.y = winSize.height-50;
        Score_name.setColor(cc.color(0,0,0));
        Score_name.setAnchorPoint(1,0.5);
        this.addChild(Score_name, 5);
        var Sp = new cc.LabelTTF("秒", "Arial", 38);
        Sp.x = winSize.width -20;
        Sp.y = winSize.height-50;
        Sp.setColor(cc.color(0,0,0));
        this.addChild(Sp, 5);
    },
    CreateBlock: function (m,x) {
        var sp=new cc.Sprite(g_resources[m-1]);
        sp.setPosition(x,winSize.height+100);
        sp.setTag(m);
        this.addChild(sp);
        Block_Array.push(sp);
    },
    BlockAction1: function () {
        Count_num++;
        if(Count_num>=Count_Array.length){
            Count_num=0;
            Count_Array.shuffle();
        }
        var sp_tag=1;
        if(Score>=8){
            var sp_tag1=Math.floor(Math.random()*3);
            if(sp_tag1==2){
                var sp_index=Math.floor(Math.random()*posX_Array.length);
                var pos=posX_Array[sp_index];
                this.BlockAction2(pos);
            }
        }
        var  posx=Count_Array[Count_num];
        var  posx1=480-Count_Array[Count_num];
        this.CreateBlock(sp_tag,posx);
        this.CreateBlock(sp_tag,posx1);

    },
    BlockAction2: function (posx) {
        var sp_tag=2
        var  posx1=480-posx;
        this.CreateBlock(sp_tag,posx);
        this.CreateBlock(sp_tag,posx1);

    },
    overFun: function () {
       this.unscheduleAllCallbacks();
        cc.eventManager.removeAllListeners();
        cc.log("11111")
        var transition=new cc.TransitionMoveInT(1,new GameEndScene());
        cc.director.runScene(transition);
        //cc.director.runScene(new GameOverScene());
        //var dely=cc.delayTime(0.6);
        //var fun=cc.callFunc(function () {
        //    var transition=new cc.TransitionMoveInT(1,new GameOverScene());
        //    //cc.director.runScene(new  GameMainScene())
        //    cc.director.runScene(transition);
        //})
        //var chandong = cc.sequence(cc.moveBy(0.01, 3, 0), cc.moveBy(0.01, 0, 3), cc.moveBy(0.02, -6, 0), cc.moveBy(0.02, 0, -6), cc.moveBy(0.01, 3, 0), cc.moveBy(0.01, 0, 3))
        //this.runAction(cc.sequence(chandong,dely,fun));
    }
});
var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})

Array.prototype.shuffle = function() {
    var input = this;
    for (var i = input.length-1; i >=0; i--) {
        var randomIndex = Math.floor(Math.random()*(i+1));
        var itemAtIndex = input[randomIndex];
        input[randomIndex] = input[i];
        input[i] = itemAtIndex;
    }
    return input;
}