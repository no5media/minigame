var GameEndLayer=cc.LayerColor.extend({
    ctor: function (num) {
        this._super(cc.color(255,255,255,255));
        var winSize=cc.winSize;
        cc.sys.localStorage.setItem("dsgnzkAngin",0);
        var type_score=Score;
        var game_score=Score;
        var game_type=0;
        var availabity=1;
        //if(Record_end&&Record_click.length>30){
        //    availabity=0
        //}else{
        //    availabity=1
        //}
        var  url="/openapi/game/interactionGame/postGameLog.json?availabity="+availabity+"&aid="+appId+"&gameScore="+game_score+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+""+"&scoreId="+scoreId
        var xhr = cc.loader.getXMLHttpRequest();
        var  url=url;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.send()
        hlmySetShare(Score,1)
        //玩家的结束信息
        //var record_headUrl="http://wx.qlogo.cn/mmopen/Q3auHgzwzM6ZnXkuxs711rhESCuoWr3RQ3s52rib7OQTwrlSwnSHkIa8B3qg7lqrPQ3qbh9uBZAjQfg4Et8lDTw/0"
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl=" http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,40,winSize.height-40,40,self);
        });
        //var name_str="李乾@1758"
        var name_str1=window.select.userProfile.nickname
        var name_str= getBytesLength(name_str1,12)
        var record_name = new cc.LabelTTF(name_str, "Arial", 20);
        record_name.x = 70;
        record_name.y =winSize.height-40;
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,112,143))
        this.addChild(record_name, 5);
        var str_Array=["注定孤独","强拆达人","情比金坚","死了都要爱","性福七夕","比翼双飞","开挂大神"]
        var str="";
        var str_index=str_Array[parseInt(Score/10)];
        if(str_index>=6){
            str_index=6
        }
        var name_str1 = new cc.LabelTTF(str_index, "Arial", 30);
        name_str1.x = winSize.width/2+80;
        name_str1.y = winSize.height / 2 + 225;
        name_str1.setColor(cc.hexToColor("#dcb22d"))
        this.addChild(name_str1, 5);

        var name_str2 = new cc.LabelTTF("获得成就:", "Arial", 30);
        name_str2.x = winSize.width/2-130;
        name_str2.y = winSize.height / 2 + 225
        name_str2.setAnchorPoint(0,0.5)
        name_str2.setColor(cc.color(0,0,0))
        this.addChild(name_str2, 5);
        var Best_Score_str= cc.sys.localStorage.getItem("dsgnzc");
        if(Best_Score_str){
            if(Best_Score_str<Score){
                Best_Score_str=Score
                cc.sys.localStorage.setItem("dsgnzc",Best_Score_str);
            }
        }else{
            Best_Score_str=Score
            cc.sys.localStorage.setItem("dsgnzc",Best_Score_str);
        }
        var best_str="最长一次为 "+Best_Score_str+" 秒"
        var best = new cc.LabelTTF(best_str, "Arial", 30);
        best.setColor(cc.color(0,0,0))
        best.x = winSize.width/2;
        best.y = winSize.height/2 + 150;
        this.addChild(best, 5);

        var Score_str="坚持了 "+Score+" 秒"
        var Score_str = new cc.LabelTTF(Score_str, "Arial", 30);
        Score_str.setColor(cc.color(0,0,0));
        Score_str.x = winSize.width/2;
        Score_str.y = winSize.height/2 + 300;
        this.addChild(Score_str, 5);
        var bg=new cc.Sprite("res/over/bg.png");
        bg.setPosition(winSize.width/2,winSize.height/2+20);
        this.addChild(bg);


        var start_score=new cc.Sprite("res/start/Score.png");
        start_score.setPosition(winSize.width-80,winSize.height-40);
        start_score.setScale(0.5);
        this.addChild(start_score);
        var xhr = cc.loader.getXMLHttpRequest();
        var  url="/openapi/game/interactionGame/index.json?aid="+appId;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4){
                if(xhr.status >= 200 && xhr.status <= 207){
                    var resw = xhr.responseText;
                    var jsondata = JSON.parse(resw);
                    var data = jsondata["data"];
                    gameNum2=data.userPoint;//积分总数
                    getScore = new cc.LabelTTF("0", "Arial", 20);
                    getScore.x = winSize.width-70;
                    getScore.y =winSize.height-42;
                    getScore.setColor(cc.color(88,195,224));
                    getScore.setString(gameNum2);
                    self.addChild(getScore, 1);
                    if(gameNum2>=10000&&gameNum2<1000000){
                        getScore.setScale(0.8)
                    }else if(gameNum2>=1000000){
                        getScore.setScale(0.6);
                    }
                }
            }
        }
        xhr.send();
        CreateBtn("res/start/show.png","res/start/show.png", function () {
            var share = document.getElementById('share-square');
            share.style.display = 'block';

        },winSize.width/2,300,0.5,0.5,5,this,1)

        CreateBtn("res/start/angin.png","res/start/angin.png", function () {
            Score=0;
            money_num=0;
            Is_Angin=false;
            Is_overOrplay=false
            cc.director.runScene(new GameMainScene());
        },winSize.width/2,200,0.5,0.5,5,this,1)
        CreateBtn("res/start/gohome.png","res/start/gohome.png", function () {
            cc.director.runScene(new GameStartScene());
        },winSize.width/2,100,0.5,0.5,5,this,1)
        CreateBtn("res/start/follow.png","res/start/follow.png", function () {
            //关注
            showEWM()
        },100,100,0.5,0.5,5,this,1)
        CreateBtn("res/start/invite.png", "res/start/invite.png", function () {
            //邀请
            var share = document.getElementById('share-square');
            share.style.display = 'block';
        },winSize.width-100,100,0.5,0.5,5,this,1)

        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchEnded:function(touch,event){
                Is_Click=true;

            }
        });
        cc.eventManager.addListener(listener, this);
        return true

    }
})
var GameEndScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameEndLayer();
        this.addChild(layer);
    }
})