var GameOverLayer=cc.LayerColor.extend({
    ctor: function () {
        this._super(cc.color(255,255,255))
        var winSize=cc.director.getVisibleSize();
        var bg=new cc.Sprite(res.over_bg);
        bg.setPosition(winSize.width/2,winSize.height/2)
        this.addChild(bg)
        hlmySetShare(Score,'jingdian')
        var type_score=Score;
        var game_score=Score;
        var game_type=0;
        var availabity=0;
        var url="/openapi/game/interactionGame/postGameLog.json?availabity="+availabity+"&aid="+appId+"&gameScore="+game_score+"&gameType="+game_type+"&typeScore="+type_score+"&playerGameLogId="+""+"&scoreId="+scoreId
        var xhr = cc.loader.getXMLHttpRequest();
        var url=url;
        xhr.open("POST", url,true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");
        xhr.send()
        var str_Array=["注定孤独","强拆达人","情比金坚","死了都要爱","性福七夕","比翼双飞","开挂大神"]
        var str="";
        var str_index=str_Array[parseInt(Score/10)];
        if(str_index>=6){
            str_index=6
        }
        var name_str1 = new cc.LabelTTF(str_index, "Arial", 30);
        name_str1.x = winSize.width/2+80;
        name_str1.y = winSize.height / 2 + 225;
        name_str1.setColor(cc.hexToColor("#dcb22d"))
        this.addChild(name_str1, 5);

        var name_str2 = new cc.LabelTTF("获得成就:", "Arial", 30);
        name_str2.x = winSize.width/2-130;
        name_str2.y = winSize.height / 2 + 225
        name_str2.setAnchorPoint(0,0.5)
        name_str2.setColor(cc.color(0,0,0))
        this.addChild(name_str2, 5);
        var Best_Score_str= cc.sys.localStorage.getItem("dsgnzc");
        if(Best_Score_str){
            if(Best_Score_str<Score){
                Best_Score_str=Score
                cc.sys.localStorage.setItem("dsgnzc",Best_Score_str);
            }
        }else{
            Best_Score_str=Score
            cc.sys.localStorage.setItem("dsgnzc",Best_Score_str);
        }
        var best_str="最长一次为 "+Best_Score_str+" 秒"
        var best = new cc.LabelTTF(best_str, "Arial", 30);
        best.setColor(cc.color(0,0,0))
        best.x = winSize.width/2;
        best.y = winSize.height/2 + 150;
        this.addChild(best, 5);

        var Score_str="坚持了 "+Score+" 秒"
        var Score_str = new cc.LabelTTF(Score_str, "Arial", 30);
        Score_str.setColor(cc.color(0,0,0))
        Score_str.x = winSize.width/2;
        Score_str.y = winSize.height/2 + 300;
        this.addChild(Score_str, 5);
        CreateBtn(res.over_left_btn,res.over_left, function () {
            //再来一局
            cc.director.runScene(new GameMainScene)
            Score=0;
        },winSize.width/2-100,200,0.5,0.5,2,this)
        CreateBtn(res.over_right_btn,res.over_right, function () {
            //召唤好友
            top.window.location.href ="http://wx.1758.com/play/gamebox/123?ex1758=1&tp=full&yn=%E5%B0%8F%E6%B8%B8%E6%88%8F&title=%E5%B0%8F%E6%B8%B8%E6%88%8F";

        },winSize.width/2+100,200,0.5,0.5,2,this)

    }
});
var GameOverScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameOverLayer();
        this.addChild(layer);
    }
})