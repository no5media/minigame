Score=0;
Main_layer=null;
Is_Angin=false;
Is_overOrplay=false;
Num_Array=[];
sp_num=0;
Is_Start=false;
SpgetScore=null;
var GameMainLayer=cc.LayerColor.extend({
    timeclock:null,
    PeppleScore:null,
    ctor: function () {
        this._super(cc.color(231,237,239));
        //cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        Score=0;
        time=0;
        sp_num=0;
        clock_time=40;
        Main_layer=this;
        Is_Angin=false;
        Is_Start=false;
        Is_overOrplay=false;
        Num_Array=[];
        this.CreatePeopleInformation();
        this.DrawBg();
        this.CreateClock();
        var layer=new selectLayer();
        this.addChild(layer,10)
        this.scheduleUpdate();
        return true;
    },
    update: function (dt){
        if(Is_Start){
            time+=dt;
            if(time>=1){
                time=0;
                clock_time--;
                timeclock.setString(clock_time);
                if(clock_time==0){
                    if(angin_num<3){
                        cc.director.pause();
                        var layer=new AnginLayer(1);
                        this.addChild(layer,12);
                    }else{
                        Is_overOrplay=true
                    }
                }
            }
        }
        if(Is_Angin){//复活之后需要将生命填满，并且将钻石的数量减少
            Is_Angin=false;
            clock_time+=5;
            timeclock.setString(clock_time);
            PeppleScore.setString(window.select.userPoint);
        }
        if(Is_overOrplay){
            this.unscheduleAllCallbacks();
            cc.eventManager.removeAllListeners();
            this.GameOver();
            return true;
        }
    },
    CreateClock: function () {
        var winSize=cc.winSize;
        var time_str="倒计时:";
        var timeName = new cc.LabelTTF(time_str, "Arial", 20);
        timeName.x = 170;
        timeName.y = winSize.height-130;
        timeName.setColor(cc.color(255,255,255));
        this.addChild(timeName, 6);
        timeclock=new cc.LabelAtlas("40", "res/num.png", 20, 29,"0");
        timeclock.setPosition(210, winSize.height-140);
        this.addChild(timeclock,10);
        SpgetScore=new cc.LabelAtlas("0", "res/num.png", 20, 29,"0");
        SpgetScore.setPosition(winSize.width-80, winSize.height-130);
        SpgetScore.setAnchorPoint(0.5,0.5);
        this.addChild(SpgetScore,10);
    },
    CreateError: function (x,y) {
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        var winSize=cc.winSize;
        var layer=new cc.LayerColor(cc.color(225,50,120,60));
        this.addChild(layer);
        var blink =new cc.Blink(0.3,2);//闪烁函数
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg1.png"));
        sp.setAnchorPoint(0.5,0.5);
        sp.setPosition(x,y);
        this.addChild(sp,10);
        var no_sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_s3.png"));
        no_sp.setPosition(sp.getBoundingBox().width/2,sp.getBoundingBox().height/2);
        sp.addChild(no_sp,10);
        var destory = new cc.RemoveSelf(true);
        var fun=cc.callFunc(function () {
            sp.removeFromParent();
        });
        layer.runAction(cc.repeatForever(cc.sequence(blink,fun,destory)))
    },
    DrawBg: function () {
        var winSize=cc.winSize;
        var drawNode = new cc.DrawNode();
        this.addChild(drawNode,5);
        drawNode.drawRect(
            cc.p(0,0), // 起点
            cc.p(winSize.width,100), // 起点的对角点
            cc.color(0,0, 0, 255), // 填充颜色
            1, // 线粗
            cc.color(255,255, 255, 255) // 线颜色
        );
        drawNode.drawRect(
            cc.p(winSize.width,winSize.height), // 起点
            cc.p(0,winSize.height-160), // 起点的对角点
            cc.color(6,183, 255, 255), // 填充颜色
            1, // 线粗
            cc.color(6,183, 255, 255) // 线颜色
        );
    },
    CreatePeopleInformation: function () {
        var winSize=cc.winSize
        cc.spriteFrameCache.getSpriteFrame("Score.png");
        var start_score=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("Score.png"));
        start_score.setPosition(winSize.width,winSize.height-70);
        start_score.setAnchorPoint(1,0.5)
        this.addChild(start_score,10);
        var record_headUrl
        if(window.select.userProfile&&window.select.userProfile.headUrl){
            record_headUrl=window.select.userProfile.headUrl;
        }else{
            record_headUrl="http://wx.qlogo.cn/mmopen/fATicXdQ1ibcHw03pBk5FCXDcM8lobNy1ibSPRtAx46TGwDGtqibmiadHibI4UFVuiaCiatd63DC8CFvljySQ6pFcoGYtBkWU3ngYekE/0"
        }
        //var record_headUrl="http://wx.qlogo.cn/mmopen/HxAmhVc1HOMyibVia77X1x5Yiaf876ubNhxicNUiajEeMjNBaVejtA54u3okIQMepvP7Z6X5cq63aIMc7YFJvH6nLmKDEDqpcqiafX/0"
        var self=this;
        cc.loader.loadImg(record_headUrl, {isCrossOrigin : false}, function(err,img){
            CreateClipperImage(record_headUrl,65,winSize.height-70,80,self);
        });
        var name_str1=window.select.userProfile.nickname;
        var name_str= getBytesLength(name_str1,12)
        //var name_str="飞花飘絮"
        var record_name = new cc.LabelTTF(name_str, "Arial", 30);
        record_name.x = 130;
        record_name.y =winSize.height-70
        record_name.setAnchorPoint(0,0.5)
        record_name.setColor(cc.color(255,255,255))
        this.addChild(record_name, 10);
        var get_score=window.select.userPoint;
        //var get_score=10
        PeppleScore = new cc.LabelTTF("0", "Arial", 30);
        PeppleScore.x = winSize.width-70;
        PeppleScore.y =winSize.height-70;
        PeppleScore.setAnchorPoint(0.5,0.5)
        PeppleScore.setColor(cc.color(254,202,75));
        PeppleScore.setString(get_score)
        this.addChild(PeppleScore, 10);

    },
    GameOver: function () {
        var winSize=cc.winSize;
        this.unscheduleUpdate();
        cc.eventManager.removeAllListeners();
        var dely=cc.delayTime(1.5);
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("end_bg.png"));
        sp.setPosition(winSize.width/2,winSize.height/2);
        sp.setScale(0.1);
        var fun=cc.callFunc(function () {
            sp.removeFromParent()
            var transition=new cc.TransitionProgressRadialCW(1,new GameOverScene());
            cc.director.runScene(transition);
        })
        var fd = new cc.scaleTo(0.1,1);
        this.addChild(sp,12)
        sp.runAction(cc.sequence(fd,dely,fun));
    }
});
var selectLayer=cc.Layer.extend({
    SpriteNum:null,
    ctor: function () {
        this._super();
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        var winSize=cc.winSize;
        var self=this;
        CreateBtn(cc.spriteFrameCache.getSpriteFrame("game_btn.png"), function () {
            //经典模式
            Is_Start=true;
            self.removeFromParent();
            var layer=new BtnLayer();
            Main_layer.addChild(layer,10);
        },winSize.width/2,470,0.5,0.5,this);
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var rect=new cc.Rect(touch.getLocation().x,touch.getLocation().y,1,1);
                //cc.rectIntersectsRect
                return true;
            },
            onTouchEnded:function(touch,event){

            }
        });
        cc.eventManager.addListener(listener, this);
        this.CreateBg();
        this.CtrateNum();
        return true;
    },
    CreateBg: function (num) {
        var winSize=cc.winSize;
        var bg=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg2.png"));
        bg.setPosition(winSize.width/2,winSize.height/2+150);
        this.addChild(bg);
        SpriteNum = new cc.LabelTTF("0", "Arial", 80);
        SpriteNum.x = winSize.width/2;
        SpriteNum.y =winSize.height/2+150;
        SpriteNum.setAnchorPoint(0.5,0.5);
        SpriteNum.setColor(cc.color(7,182,255));
        this.addChild(SpriteNum, 10);
    },
    CtrateNum: function () {
        sp_num=0;
        if(level1>2){
            level++;
            level1=0;
        }
        var Max=level+5;
        for(var a=0;a<Num_Array.length;a++){
            Num_Array.splice(a,1);
            a--;
        }
        for(var a=0;a<Max;a++){
            var sp
            if(a==0){
                 sp=1;
            }else{
                 sp=Math.floor(Math.random()*10);
            }
            Num_Array.push(sp);
            sp_num=(Math.pow(10,Max-a-1))*sp+sp_num
        }
        level1++;
        SpriteNum.setString(sp_num);
    }
});
var BtnLayer=cc.Layer.extend({
    ctor: function () {
        this._super();
        cc.spriteFrameCache.addSpriteFrames(res.game_plist,res.game_png);
        Block_Array=[];
        Cue_Array=[];
        var winSize=cc.winSize;
        index=0;
        Is_startClick=true;//为创建提示进行条件限制
        Is_CreateCue=false;//控制创建提示
        click_time=0;//判断经过多少秒经行提示
        btn_Click=false;
        Is_HaveCue=false;
        sp_number=0;
        var bg=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg2.png"));
        bg.setPosition(winSize.width/2,winSize.height-200);
        bg.setAnchorPoint(0.5,1);
        this.addChild(bg);
        SpriteNum = new cc.LabelTTF("0", "Arial", 80);
        SpriteNum.x = winSize.width/2;
        SpriteNum.y =winSize.height-300;
        SpriteNum.setAnchorPoint(0.5,0.5);
        SpriteNum.setColor(cc.color(7,182,255));
        this.addChild(SpriteNum, 10);
        var self=this;
        var listener=cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                btn_Click=true;
                var rect=new cc.Rect(touch.getLocation().x,touch.getLocation().y,1,1);
                for(var a=0;a<Block_Array.length;a++){
                    if(Block_Array[a]&&cc.pDistance(Block_Array[a].getPosition(),touch.getLocation())<=65){
                        if(Block_Array[a].tag-1==Num_Array[index]){
                            sp_number=0;
                            for(var b=0;b<=index;b++){
                                sp_number=Num_Array[b]*Math.pow(10,index-b)+sp_number;
                            }
                            SpriteNum.setString(sp_number);
                            index++;
                            for(var i=0;i<Cue_Array.length;i++){
                                Cue_Array[0].removeFromParent();
                                Cue_Array.splice(i,1);
                                i--;
                            }
                            Is_HaveCue=false;
                        }else{
                            Score-=150;
                            if(Score<0){
                                Score=0
                            }
                            SpgetScore.setString(Score);
                            Main_layer. CreateError(Block_Array[a].x,Block_Array[a].y);
                        }

                        if(index==Num_Array.length){
                            Score+=(30+(index-5)*10)*index;
                            SpgetScore.setString(Score);
                            var dely=cc.delayTime(0.3);
                            var fun=new cc.callFunc(function () {
                                self.removeFromParent();
                                var layer=new selectLayer();
                                Main_layer.addChild(layer,10)
                            });
                            self.runAction(cc.sequence(dely,fun));
                        }
                    }
                }
                return true;
            },
            onTouchEnded:function(touch,event){
            }
        });
        cc.eventManager.addListener(listener, this);
        var num=0;
        for(var a=0;a<3;a++){
            for(var b=0;b<3;b++){
                var posx=180+b*150;
                var posy=640-a*150;
                num++;
                this.CreateBg(posx,posy,num);
            }
        }
        this.AddSprite();
        this.scheduleUpdate();
        return true;
    },
    update: function (dt) {
        if(Is_startClick){
            click_time+=dt;
            if(click_time>=2){
                if(btn_Click){
                    click_time=0;
                    btn_Click=false;
                    Is_CreateCue=false;
                }else{
                    Is_CreateCue=true;
                }
            }
        }
        if(Is_CreateCue&&!Is_HaveCue){
            Is_CreateCue=false;
            for(var a=0;a<Block_Array.length;a++){
                if(Block_Array[a].tag-1==Num_Array[index]){
                    this.CreateCue(Block_Array[a].x,Block_Array[a].y);
                    Is_HaveCue=true;
                }
            }
        }

    },
    CreateCue: function (x,y) {
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg3.png"));
        sp.x=x,
        sp.y=y
        var blink =new cc.Blink(0.6,2);//闪烁函数
        sp.runAction(cc.repeatForever(blink));
        this.addChild(sp,8);
        Cue_Array.push(sp);
    },
    CreateBg: function (x,y,m) {
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg.png"));
        sp.x=x;
        sp.y=y;
        sp.tag=m+1;
        this.addChild(sp);
        Block_Array.push(sp);
        var sp_num=new cc.LabelAtlas(m, "res/num.png", 20, 29,"0");
        sp_num.setScale(2);
        sp_num.x=x;
        sp_num.y=y;
        sp_num.setAnchorPoint(0.5,0.5);
        this.addChild(sp_num,10);
    },
    AddSprite: function () {
        var sp=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg.png"));
        sp.x=180+1*150;
        sp.y=640-3*150;
        sp.tag=1;
        this.addChild(sp);
        Block_Array.push(sp);
        var sp_num=new cc.LabelAtlas(0, "res/num.png", 20, 29,"0");
        sp_num.setScale(2);
        sp_num.x=180+1*150;
        sp_num.y=640-3*150;
        sp_num.setAnchorPoint(0.5,0.5);
        this.addChild(sp_num,10);
        var sp1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg.png"));
        sp1.x=180;
        sp1.y=640-3*150;
        this.addChild(sp1);
        var sp_num1=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_s1.png"));
        sp_num1.x=180;
        sp_num1.y=640-3*150;
        sp_num1.setScale(1.5);
        this.addChild(sp_num1);
        var sp2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_bg.png"));
        sp2.x=180+2*150;
        sp2.y=640-3*150;
        this.addChild(sp2);
        var sp_num2=new cc.Sprite(cc.spriteFrameCache.getSpriteFrame("game_s2.png"));
        sp_num2.x=180+2*150;
        sp_num2.y=640-3*150;
        sp_num2.setScale(1.5);
        sp_num2.setAnchorPoint(0.5,0.5);
        this.addChild(sp_num2,10);
    }
})
var GameMainScene=cc.Scene.extend({
    onEnter: function () {
        this._super();
        var layer=new GameMainLayer();
        this.addChild(layer);
    }
})
