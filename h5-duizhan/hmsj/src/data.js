var CreateBtn=function(url1,fun,x,y,Anchorx,Anchory,father,scale){
    var btn = new cc.MenuItemImage(
        url1,
        url1,
        function () {
            fun()

        }, this
    )
    if(scale){
        btn.setScale(scale);
    }
    btn.setPosition(x,y);
    btn.setAnchorPoint(Anchorx,Anchory);
    var menu = new cc.Menu(btn);
    menu.x = 0;
    menu.y = 0;
    father.addChild(menu)
}
appId=115717 ;
scoreId=221;
angin_num=0;
level=0;
level1=1;
var CreateClipperImage= function (url,x,y,radius,father) {
    var shape =new cc.DrawNode();
    var vertices=CreateFun(radius,radius,radius/2);
    shape.drawPoly(vertices, cc.color(0, 255, 255, 80), 2, cc.color(255, 0, 0, 255));
    var clipper = new cc.ClippingNode();
    clipper.x = x;
    clipper.y = y;
    clipper.stencil = shape;  // 把刚刚创建的圆形模板放入
    father.addChild(clipper,10);
    var sp  = new cc.Sprite(url);
    var SceleX=radius/sp.getContentSize().width;
    var sceleY=radius/sp.getBoundingBox().height;
    sp.setScale(SceleX,sceleY);
    sp.setAnchorPoint(0.5,0.5);
    clipper.addChild(sp);   // 在这个clippingnode中只显示圆形模板的部分.
};
var CreateFun=function (width,hight,radius) {
    var sp_Array=[];
    var radius=radius;
    var num=100;
    var start_x=0
    var start_y=0
    //左下角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num;
        var x=sp_x-width/2;
        var jiaodu=Math.acos((radius-sp_x)/radius)
        var y=radius-(Math.sin(jiaodu)*radius)-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }

    //右下角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=sp_x+width-radius-width/2;
        var jiaodu=Math.asin((sp_x)/radius)
        var y=radius-(Math.cos(jiaodu)*radius)-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    //右上角
    var sp_x=0
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=width-sp_x-width/2;
        var jiaodu=Math.acos((radius-sp_x)/radius)
        var y=Math.sin(jiaodu)*radius+width-radius-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    //左上角
    var sp_x=0;
    for(var a=0;a<num;a++){
        sp_x=sp_x+radius/num
        var x=radius-sp_x-width/2;
        var jiaodu=Math.asin((sp_x)/radius)
        var y=Math.cos(jiaodu)*radius+width-radius-hight/2
        sp_Array.push(cc.p(x+start_x,y+start_y))
    }
    return sp_Array;
};
var getBytesLength= function (str,len) {
    // 在GBK编码里，除了ASCII字符，其它都占两个字符宽
    //return str.replace(/[^\x00-\xff]/g, 'xx').length;
    var str_length = 0;
    var str_len = 0;
    var  str_cut = new String();
    str_len = str.length;
    for(var i = 0;i<str_len;i++)
    {
        a = str.charAt(i);
        str_length++;
        if(escape(a).length > 4) {
            //中文字符的长度经编码之后大于4
            str_length++;
        }
        str_cut = str_cut.concat(a);
        if(str_length>=len) {
            str_cut = str_cut.concat("");
            return str_cut;
        }
    }
    //如果给定字符串小于指定长度，则返回源字符串；
    if(str_length<len){
        return  str;
    }
}