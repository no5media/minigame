//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        isModal: false,
        gameIcon: 'http://images.1758.com/image/20180606/open_104818_cf4839bad3baa022b6c3cd0ba0a62c1f.gif',
        gameName: '猫来了H5',
        gameDesc: '相亲相爱，不如来互相伤害！',
        bigImg: 'http://images.1758.com/image/20180607/open_104818_b9edabe1352fc4b3c3aeaa1e22f3ab58.jpg',
        moreGames: [{
            icon: "http://images.1758.com/article/image/2018/05/18/37931526638430041.gif",
            name: "真机抓娃娃"
        }, {
            icon: "http://images.1758.com/article/image/2017/09/06/92111504679276847.gif",
            name: "疯狂动物雨"
        }, {
            icon: "http://images.1758.com/article/image/2016/07/15/78471468572845919.gif",
            name: "错嫁太子妃"
        }]
    },
    onLoad: function() {},
    moreGame: function() {
        this.setData({
            isModal: true
        })
    },
    closeModal(evt) {
        this.setData({
            isModal: false
        })
    }
})