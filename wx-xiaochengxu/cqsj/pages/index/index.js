//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        isModal: false,
        gameIcon: 'http://images.1758.com/article/image/2016/06/28/52431467099755147.png',
        gameName: '传奇世界',
        gameDesc: '经典正版，行会攻城，等你来战！快来领取屠龙刀',
        bigImg: 'http://images.1758.com/article/image/2017/03/03/6071488509536057.jpg',
        moreGames: [{
            icon: "http://images.1758.com/article/image/2017/09/20/96261505874414043.png",
            name: "传奇来了"
        }, {
            icon: "http://images.1758.com/article/image/2017/11/09/30101510219976857.png",
            name: "决战沙城"
        }, {
            icon: "http://images.1758.com/article/image/2018/02/06/95741517908139719.png",
            name: "雷霆战神"
        }]
    },
    onLoad: function() {},
    moreGame: function() {
        this.setData({
            isModal: true
        })
    },
    closeModal(evt) {
        this.setData({
            isModal: false
        })
    }
})