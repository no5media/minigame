//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        isModal: false,
        gameIcon: 'http://images.1758.com/image/20180606/open_104818_bef9be10181970c8cba5a32fd9ddcafb.png',
        gameName: '海盗来了H5',
        gameDesc: '跟好友一起互相伤害！',
        bigImg: 'http://images.1758.com/image/20180607/open_104818_354685ae05bbb21275969c64d1452120.jpg',
        moreGames: [{
            icon: "http://images.1758.com/article/image/2018/05/18/37931526638430041.gif",
            name: "真机抓娃娃"
        }, {
            icon: "http://images.1758.com/article/image/2017/09/06/92111504679276847.gif",
            name: "疯狂动物雨"
        }, {
            icon: "http://images.1758.com/article/image/2016/07/15/78471468572845919.gif",
            name: "错嫁太子妃"
        }]
    },
    onLoad: function() {},
    moreGame: function() {
        this.setData({
            isModal: true
        })
    },
    closeModal(evt) {
        this.setData({
            isModal: false
        })
    }
})