//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        isModal: false,
        gameIcon: 'http://images.1758.com/article/image/2017/11/09/30101510219976857.png',
        gameName: '决战沙城',
        gameDesc: '传奇世界正版授权，全民抢BOSS，橙装人人有',
        bigImg: 'http://images.1758.com/article/image/2016/10/25/62491477393761490.jpg',
        moreGames: [{
            icon: "http://images.1758.com/article/image/2017/09/20/96261505874414043.png",
            name: "传奇来了"
        }, {
            icon: "http://images.1758.com/article/image/2018/02/06/95741517908139719.png",
            name: "雷霆战神"
        }, {
            icon: "http://images.1758.com/article/image/2016/06/28/52431467099755147.png",
            name: "传奇世界"
        }]
    },
    onLoad: function() {},
    moreGame: function() {
        this.setData({
            isModal: true
        })
    },
    closeModal(evt) {
        this.setData({
            isModal: false
        })
    }
})