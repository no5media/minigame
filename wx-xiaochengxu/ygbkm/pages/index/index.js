//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    isModal: false,
    gameIcon: 'http://images.1758.com/article/image/2017/03/16/28321489647397776.png',
    gameName: '妖怪宝可萌',
    gameDesc: '抓宠挂机回合制，上线就送VIP！',
    bigImg: 'http://images.1758.com/article/image/2017/03/16/49751489647366406.jpg',
    moreGames: [{
      icon: "http://images.1758.com/article/image/2016/06/28/83061467100106072.png",
      name: "口袋妖怪联盟"
    }, {
      icon: "http://images.1758.com/article/image/2017/09/06/92111504679276847.gif",
      name: "疯狂动物雨"
    }, {
        icon: "http://images.1758.com/article/image/2016/06/28/52431467099755147.png",
        name: "传奇世界"
    }]
  },
  onLoad: function () {
  },
  moreGame: function () {
    this.setData({
      isModal: true
    })
  },
  closeModal(evt) {
    this.setData({
      isModal: false
    })
  }
})
