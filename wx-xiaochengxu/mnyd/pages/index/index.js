//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    isModal:false,
    gameIcon:'http://images.1758.com/article/image/2017/06/30/51361498791141481.jpg',
    gameName:'美男有毒',
    gameDesc:'咦……被穿越，你是否能逆袭做王妃？',
    bigImg:'http://images.1758.com/article/image/2017/06/30/52391498791407695.gif',
    moreGames: [{
      icon: "http://images.1758.com/article/image/2018/05/18/37931526638430041.gif",
      name: "真机抓娃娃"
    }, {
      icon: "http://images.1758.com/article/image/2018/05/03/80341525332863086.jpg",
      name: "嫡女心计"
    }, {
      icon: "http://images.1758.com/article/image/2018/03/22/14801521709026771.jpg",
      name: "锦绣缘"
    }]
  },
  onLoad: function () {
  },
  moreGame: function(){
    this.setData({
      isModal: true
    })
  },
  closeModal(evt){
    this.setData({
      isModal: false
    })
  }
})
