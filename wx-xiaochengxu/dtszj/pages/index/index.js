//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        isModal: false,
        gameIcon: 'http://images.1758.com/article/image/2017/10/30/20671509354646455.gif',
        gameName: '大天使之剑',
        gameDesc: '正版奇迹MU授权，新颖玩法让经典继续',
        bigImg: 'http://images.1758.com/article/image/2017/09/29/23131506652853422.jpg',
        moreGames: [{
            icon: "http://images.1758.com/article/image/2017/09/20/96261505874414043.png",
            name: "传奇来了"
        }, {
            icon: "http://images.1758.com/article/image/2017/11/09/30101510219976857.png",
            name: "决战沙城"
        }, {
            icon: "http://images.1758.com/article/image/2016/06/28/52431467099755147.png",
            name: "传奇世界"
        }]
    },
    onLoad: function() {},
    moreGame: function() {
        this.setData({
            isModal: true
        })
    },
    closeModal(evt) {
        this.setData({
            isModal: false
        })
    }
})