// pages/userInfo/userInfo.js
var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;
var isOpenHome = true;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    chance: 0,/* 剩余次数 */
    challengeIimes: 0,/* 累计挑战 */
    canGetNum: 0,
    isShowtousuBtn: true,
    isHaveMoreGame: false,
    moreGame: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })



  },
  addajax() {
    var url = _url.summary;
    var _data = {};
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var resData = res.data;

      var Enable = _globalData.summary.data.summary.config.complaintEnable/* 是否显示投诉按钮 */
      if (Enable == 0 || !Enable) {
        that.setData({
          isShowtousuBtn: false
        })
      }

      if (resData.result == 1) {

        that.setData({
          chance: resData.data.summary.chance,/* 剩余次数 */
          challengeIimes: resData.data.summary.playTimes,/* 累计挑战 */
          canGetNum: resData.data.summary.credit
        })
      }
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  canGet() {

    if (this.data.canGetNum > 0) {
      wx.redirectTo({
        url: '../shop/shop',
      })
    }
  },
  gohome() {

    if (this.data.chance > 0) {
      wx.navigateTo({
        url: '../start/start',
      })
    }
  },
  goHomeCf() {
    isOpenHome = false;
    wx.navigateBack({
      // url: '../start/start',
    })
    // wx.redirectTo({
    //   url: '../start/start',
    //   success: function (res) {

    //   },
    //   fail: function (res) {
    //     console.log(res)
    //     isOpenHome = true;
    //   },
    // })
    // }
  },
  goTousu() {
    wx.navigateTo({
      url: '../tousu/tousu',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })
    this.tanping = this.selectComponent("#tanping");

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.addajax();
    var _moreGame = _globalData.summary.data.summary.moreGames;
    if (_moreGame.length > 0) {

      this.setData({
        isHaveMoreGame: true,
        moreGame: _moreGame
      })
    } else {

      this.setData({
        isHaveMoreGame: false,
        moreGame: ""
      })
    }


  },
  openMoregame(evt) {
    var wxAppid = this.data.moreGame[evt.currentTarget.id].wxAppid;
    wx.navigateToMiniProgram({
      appId: wxAppid,
      path: 'pages/index/index',
      extarData: {
        open: 'happy'
      },
      envVersion: 'release',
      success(res) {
        // 打开成功  
      }
    })
  },
  /**
   * 生命周期函数--监听页面隐藏
   */

  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */

  onShareAppMessage: function (res) {
    return util.onShareAppMessage(this, this.tanping);;
  }
})