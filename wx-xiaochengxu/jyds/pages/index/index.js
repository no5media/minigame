// pages/head/head.js
var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow1: false,
    isShow2: false,
    isShow3: false,
    isShow4: false,
    isShow5: false,
    isShowShouQuan: false
  },

  auth(data) {
    var that = this;
    var url = _url.auth;
    var _data = data;
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var str = JSON.stringify(res);
      if (res.data.result == 1) {
        _url.cookie = "wy_user=" + res.data.data.hlmy_token + ";hlmy_token=" + res.data.data.hlmy_token;
        wx.redirectTo({
          url: '../start/start',
        })
      } else {
        console.log("请求auth，登录失败")
      }
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  getUserInfo(code) {
    var loginStatus;
    var that = this;
    wx.getUserInfo({
      success(res) {
        console.log("s",res)
        var data = {
          encryptedData: res.encryptedData,
          iv: res.iv,
          nickName: res.userInfo.nickName,
          avatarUrl: res.userInfo.avatarUrl,
          city: res.userInfo.city,
          country: res.userInfo.country,
          province: res.userInfo.province,
          gender: res.userInfo.gender,
          code: code
        }
        that.auth(data);
      },
      fail(err) {
        if (err.errMsg == "getUserInfo:fail auth deny"){
          that.setData({
            isShowShouQuan: true
          })
        }
        console.log("err请求getUserInfo", err);
      }
    })
  },
  _cancelEvent() {
    this.setData({
      isShowShouQuan:false
    })
  },
  _confirmEvent() {
    var that = this;
    wx.openSetting({
      success: function (data) {
        if (data) {
          if (data.authSetting["scope.userInfo"] == true) {
            that.setData({
              isShowShouQuan: false
            })
          } else {
            console.log("取消授权")
            that.setData({
              isShowShouQuan: true
            })
          }
        }
      },
      fail: function () {
        console.info("设置失败返回数据");
      }
    });
  },
  checkLogin() {
    var that = this;
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log(res.code)
        that.getUserInfo(res.code);
      }, fail: res => {
        console.log("获取code失败", res);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
 
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.dialog = this.selectComponent("#dialog");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
    var that = this;

    // setTimeout(function () {
    console.log(!this.data.isShowShouQuan)
    if (!this.data.isShowShouQuan) {
      that.checkLogin();
      that.setData({
        isShowShouQuan:false
      })
    }

    // }, 500)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    if (this.data.isShowShouQuan) {
      console.log("onhide")
      this.setData({
        isShowShouQuan: false
      })
    }


  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})