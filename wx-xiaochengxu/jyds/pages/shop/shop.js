// pages/shop/shop.js
var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    isleft: true,
    isNot: false,
    isClick: true,
    isCanGet: true,
    dsc: "",
    "tab_style1": "tab_current1",
    "tab_style2": "tab_current2",
    giftList: [],/* 存储礼品数据 */
    isGifthavedata: 'false',
    dataList: '',/* 存储兑换记录数据 */
    isListhavedata: 'false'
  },
  /* 礼物列表 */
  getRecordGift() {
    if (!this.data.isleft) {
      this.setData({
        "tab_style1": "tab_current1",
        "tab_style2": "tab_current2",
        isleft: true,
      })
    }
  },
  /* 兑换记录 */
  getRecord() {
    this.moreExchanges();
    if (this.data.isleft) {

      this.setData({
        "tab_style1": "tab_current3",
        "tab_style2": "tab_current4",
        isleft: false,
      })
    }

  },
  moreExchanges() {
    var url = _url.moreExchanges;
    var _data = {
      pageNo: 1,
      pageSize: 20
    };
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var reqdata = res.data;
      if (reqdata.result == 1) {
        if (reqdata.data.fallloadResult.dataList.length > 0) {
          that.setData({
            dataList: reqdata.data.fallloadResult.dataList,
            isListhavedata: true
          })
        } else {
          that.setData({           
            isListhavedata: false
          })
        }

      }


    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  submitExchange(_data) {
    /*提交兑换请求  */
    var url = _url.submitExchange;
    var _data = _data;
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      if (res.data.result == 1) {
        that.setData({
          dsc: "礼品兑换成功，兑换资格" + res.data.data.operateResult.addCredit
        })
        that.scene.showDialog();
      }
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);

  },
  getGift(evt) {
    if (this.data.isClick) {

      if (_globalData.summary.data.summary.credit > 0) {
        var that = this;
        var giftId = evt.currentTarget.dataset.id;
        /* 提交成功拉去收件人地址接口 */
        wx.chooseAddress({
          success: function (res) {
            var str = res.provinceName + res.cityName + res.countyName + res.detailInfo
            var Data = {
              prosId: giftId,
              postMobile: res.telNumber,
              postName: res.userName,
              postAddress: str,
              postCode: res.postalCode
            }

            that.submitExchange(Data)
          }, fail: function (err) {
            wx.getSetting({
              success: (res) => {
                if (!res.authSetting['scope.address']) {
                  wx.authorize({
                    scope: 'scope.address',
                    success(res) {
                      //用户授权后执行方法  
                    },
                    fail(res) {
                      //用户拒绝授权后执行  
                    }
                  })
                }
              }
            })
          }
        })

      } else {

        /*没有可领取奖品次数 */
        this.setData({
          isNot: true,
          isClick: false
        })
        setTimeout(() => {
          this.setData({
            isNot: false,
            isClick: true
          })
        }, 3000);
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })


  },
  getList() {
    /* 礼品列表请求 */
    var url = _url.morePros;
    var _data = {
      pageNo: 1,
      pageSize: 20
    };
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      /* 判断礼品数组是否有数据 */
      var _list = res.data.data.fallloadResult.dataList;
      if (_list.length > 0) {
        that.setData({
          giftList: _list,
          isGifthavedata: 'true',
        })
      } else {
        that.setData({
          isGifthavedata: 'false',
        })

      }

    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.scene = this.selectComponent("#scene");
    this.tanping = this.selectComponent("#tanping");

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList();
  },
  scneeQuxiao() {
    this.scene.hideDialog()
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */

  onShareAppMessage: function (res) {
    return util.onShareAppMessage(this, this.tanping);;
  }
})