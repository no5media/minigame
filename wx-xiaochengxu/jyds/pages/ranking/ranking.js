// pages/ranking/ranking.js
var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;
var pageClick = false;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 页面排行榜
    pageRank: [],
    // 我的排行榜
    myRank: {},
    rankTitle: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // wx.setNavigationBarColor({
    //   frontColor: '#ffffff',
    //   backgroundColor: '#774599',
    // })
    // wx.setNavigationBarTitle({
    //   title: _url.gameTitle,
    // })

  },
  getRankResult() {
    var url = _url.getRankResult;
    var data = {
      pageNo: 1,
      pageSize: 20
    }
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var resData = res.data;

      if (resData.result == 1 && resData.data.rankResult) {
        var _rankTitle = ''
        if (resData.data.rankResult.rankTitle) {
          _rankTitle = resData.data.rankResult.rankTitle;
        }

        that.setData({
          myRank: resData.data.rankResult.myRankInfo,
          pageRank: resData.data.rankResult.pagingData.dataList,
          rankTitle: _rankTitle
        })
      } else {
        console.log("打开排行榜失败");
      }
    }
    util.requestUrl(url, data, _cookie, "POST", requestcall);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.tab = this.selectComponent("#tab");
    this.tanping = this.selectComponent("#tanping");
  },
  goHome() {
    var data = getCurrentPages();
    console.log("data", data)
    wx.navigateBack({
      delta: data.length
      // url: '../start/start',
    })
    //  wx.reLaunch({
    //    url: '../start/start',
    //  })
    // wx.navigateTo({

    // })
  },
  openRanking() {
    if (pageClick) {
      wx.redirectTo({
        url: '../ranking/ranking',

      })
    }
  },
  openShop() {
    wx.redirectTo({
      url: '../shop/shop',
    })
  },
  openUserInfo() {
    wx.redirectTo({
      url: '../userInfo/userInfo',
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    pageClick = true;
    this.getRankResult();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */

  onShareAppMessage: function (res) {
    return util.onShareAppMessage(this, this.tanping);;
  }
})