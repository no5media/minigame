var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;
var tousuText = null;
Page({
  data: {
    isClick: true,
    items: [
      { value: '色情', checked: 'true' },
      { value: '骚扰' },
      { value: '欺诈' },
      { value: '传播谣言' },
      { value: '内容不符' },
      { value: '恶意收集信息' },
      { value: '侵权' },
    ]
  },
  submitComplaint() {
    if (tousuText) {
      var url = _url.submitComplaint;
      var _data = { content: tousuText };
      var _cookie = _url.cookie;
      var that = this;
      var requestcall = function (res) {

        if (res.data.result == 1) {
          util.PromptPanel(that, that.tanping, "投诉成功");
          setTimeout(function () {
            wx.navigateTo({
              url: '../userInfo/userInfo',
            })
          }, 1500)
        }
      }
      util.requestUrl(url, _data, _cookie, "POST", requestcall);
    }
  },
  radioChange: function (e) {
    tousuText = this.data.items[e.detail.value].value;
    console.log('radio发生change事件，携带value值为：', e.detail.value, tousuText)
  },
  onReady: function () {
    this.tanping = this.selectComponent("#tanping");
  },
  onLoad: function (options) {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })
  },
  onShow: function () {
    var list = _globalData.summary.data.summary.config.complaints;
    if (list.length > 1) {
      var item = [];
      for (var a = 0; a < list.length; a++) {
        var _obj;
        if (a == 0) {
          _obj = {
            id: a,
            value: list[a],
            checked: 'true'
          }
        } else {
          _obj = {
            id: a,
            value: list[a],
          }
        }
        item.push(_obj)
      }
      this.setData({
        items: item
      })
    }
  }
})