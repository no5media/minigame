// pages/game/game.js
var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;
var _gameData = null;
var gameData = [];
var gameIndex = 0;
var progressNum = 0;
var timer;
Page({
  /**
   * 页面的初始数据
   */
  data: {
    progress: 0,
    questionNumber: gameIndex + 1,
    num1: null,
    num2: null,
    symbol: null,
    answer: null,
    score: 0,
    isSuccess: 0,
    isClick: true,
    isRanking: true,
    isAddscene: false,
    // 是否显示排行榜
    rankShow: false,
    // 我的排行榜
    myRank: {},
    // 排行榜信息
    pageRank: [],
    isResult: false,
    isshowResult: false,
  },
  nextQuestion() {
    progressNum = 0
    var Alltime = gameData[gameIndex].timeMillis
    var that = this;
    var _progressNum = 100 / (Alltime / 10);
    this.timer = setInterval(function () {
      progressNum += _progressNum;
      //当进度条为100时清除定时任务
      if (progressNum >= 100) {
        that.gameover();
      } else {
        that.setData({
          progress: progressNum
        })
      }
      //并且把当前的进度值设置到progress中
    }, 10);

  },
  leftBtnCf(evt) {
    if (this.data.isClick) {
      if (gameData[gameIndex]["isRight"] == 1) {
        /* 下一题 */
        clearInterval(this.timer);
        this.NextQuestion();
      } else {
        /* 失败 */
        this.gameover()
      }
    }
  },
  rightBtnCf(evt) {
    if (this.data.isClick) {
      if (gameData[gameIndex]["isRight"] == 0) {
        /* 下一题 */
        clearInterval(this.timer);
        this.NextQuestion();
      } else {
        /* 失败 */
        this.gameover()
      }
    }
  },
  NextQuestion() {
    progressNum = 0;
    this.setData({
      progress: progressNum
    })
    gameIndex++;
    if (gameIndex < gameData.length) {
      this.setData({
        progress: progressNum,
        questionNumber: gameIndex + 1,
        num1: gameData[gameIndex]["num1"],
        num2: gameData[gameIndex]["num2"],
        symbol: gameData[gameIndex]["symbol"],
        answer: gameData[gameIndex]["answer"],
      })
      this.nextQuestion();
    } else {
      this.setData({
        score: 40,
        isSuccess: 1,
        isshowResult: true
      })
      // this.result.isSuccess();
      // this.result.showDialog();
      this.submitPlayResult();
      console.log("挑战成功");
    }
  },

  submitPlayResult() {
    var _data = {
      score: this.data.score
    }
    var url = _url.submitPlayResult;
    var _data = _data;
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  //确认事件
  /* 打开结果页中的排行榜 */
  openRank() {
    var that = this;
    wx.navigateTo({
      url: '../ranking/ranking',
      success(res) {

      }
    })
  },
  goHome() {
    wx.navigateBack({
      url: '../start/start',
    })
  },
  /* 继续挑战 */
  gochallenge() {
    if (_globalData.ChallengeNum > 0) {
      this.setData({
        isshowResult: false
      })
      // this.result.hideDialog();
      this.resitGame();
    } else {
      this.setData({
        isshowResult: false
      })
      // this.result.hideDialog();
      this.share.showDialog();
      /* 弹出挑战次数不够 */

    }
  },
  resitGame() {
    progressNum = 0;
    gameIndex = 0;
    this.addUrl();
  },
  addUrl() {
    var url = _url.startPlay;
    var _data = {};
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var resData = res.data;
      if (resData.result == 1) {
        progressNum = 0;
        gameIndex = 0;
        var _globalData1 = resData.data.operateResult.questionList;
        _globalData.ChallengeNum = resData.data.operateResult.chance;
        gameData = [];
        for (var i in _globalData1) {
          gameData.push(_globalData1[i]);
        }
        gameIndex = 0;
        that.setData({
          isClick: true,
          progress: 0,
          score: 0,
          questionNumber: 1,
          num1: gameData[gameIndex]["num1"],
          num2: gameData[gameIndex]["num2"],
          symbol: gameData[gameIndex]["symbol"],
          answer: gameData[gameIndex]["answer"],
          isSuccess: 0,
          isClick: true,
          isRanking: true,
          isAddscene: false,
          // 是否显示排行榜
          rankShow: false,
          // 我的排行榜
          myRank: {},
          // 排行榜信息
          pageRank: []
        })
        that.nextQuestion();
      } else {
      }
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  /* 返回首页 */

  gohome() {
    wx.navigateBack({
      url: '../start/start',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("onload")
    gameData = [];
    gameIndex = 0;
    var _data = JSON.parse(options.data)
    _gameData = _data.data;
    for (var i in _gameData) {
      gameData.push(_gameData[i]);
    }
    this.setData({
      isshowResult: false,// 是否显示排行榜
      num1: gameData[gameIndex]["num1"],
      num2: gameData[gameIndex]["num2"],
      symbol: gameData[gameIndex]["symbol"],
      answer: gameData[gameIndex]["answer"],
      score: 0,
      progress: 0,
      questionNumber: gameIndex + 1,
      isSuccess: 0,
      isClick: true,
      isRanking: true,
      isAddscene: false,
      rankShow: false,
      // 我的排行榜
      myRank: {},
      // 排行榜信息
      pageRank: []
    })
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })
    this.nextQuestion();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  gameover() {
    clearInterval(this.timer);
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.setData({
      isClick: false,
      score: gameIndex,
      isSuccess: 0,
      isshowResult: true
    })
    this.submitPlayResult();
    console.log("gameover1", this.data.isshowResult);

  },
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    console.log("onHide",  this.data.isshowResult)
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log("onUnload", this.data.isshowResult)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  quxiaoBtn() {

    this.share.hideDialog();
    wx.redirectTo({
      url: '../start/start',
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.result = this.selectComponent("#result");
    this.share = this.selectComponent("#share");
    this.rank = this.selectComponent("#rank");
    this.tanping = this.selectComponent("#tanping");
  },
  onShareAppMessage: function (res) {
    if (res.target && res.target.id == "gameShare") {
      var that = this;
      var nextFun = function (result) {
        if (result.res == 1 && result.data > 0) {
          that.share.hideDialog();
          that.resitGame();
        }
      }
      return util.onShareAppMessage(this, this.tanping, nextFun);
    } else {
      return util.onShareAppMessage(this, this.tanping, null);
    }
  },




})