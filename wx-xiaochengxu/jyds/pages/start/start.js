// pages/start/start.js
var util = require("../../utils/util.js");
var _url = require("../../utils/url.js");
var _app = getApp();
var _globalData = _app.globalData;
var isOpenRank = true;
var isOpenshop = true;
var isOpenUser = true;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    Isanimation: "pwd-wrapper-animation",/* 表示的是跑马灯的动画执行 */
    islottory: true,
    ChallengeNum: 0,
    ChallengEDnum: 1,
    isAddscene: false,
    onlineUserNum: 584,
    userInfo: [],
    playingUsers: '',
    ceshiNum: 1,
    shareDesc: "",/* 分享描述 */
    lottoryList: ""
  },
  /**
   * 请求   summary  借口
   */
  summary() {
    var url = _url.summary;
    var _data = {};
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var resData = res.data.data;
      if (res.data.result == 1) {
        _globalData.summary = [];
        _globalData.summary = res.data;
        _globalData.ChallengeNum = resData.summary.chance;
        var _lottoryList;
        var _len;

        if (_globalData.summary.data.summary.lottoryList.length > 0) {
          _lottoryList = _globalData.summary.data.summary.lottoryList;
          _lottoryList = _lottoryList.concat(_lottoryList)
          _len = _globalData.summary.data.summary.lottoryList;
        } else {
          _len = 0
          _lottoryList = [];
        }
        that.setData({
          Isanimation: "pwd-wrapper-animation",
          islottory: true,
          lottoryList: _lottoryList,
          lottoryListLen: _len,
          userInfo: resData.summary,
          ChallengeNum: _globalData.ChallengeNum
        })
      } else {
        console.log("请求summary失败")
      }
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  onLoad: function (options) {

    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })


  },
  /* 打开游戏介绍 */
  clickIntro() {
    this.intro.showDialog();
  },
  /* 点击开始按钮 */
  startGame() {
    var num = _globalData.ChallengeNum;

    if (num > 0) {
      this.addUrl();
    } else {

      this.share.showDialog();
    }
  },
  /* 请求startPlay 这个借口，获取游戏具体数据 */
  clickintro() {
    this.intro.showDialog();
  },
  addUrl() {
    var url = _url.startPlay;
    var _data = {};
    var _cookie = _url.cookie;
    var that = this;
    var requestcall = function (res) {
      var resData = res.data;
      if (resData.result == 1) {
        var _data = resData.data.operateResult.questionList;
        var data={
          data: _data,
          isStart:false
        }
        var _data1 = JSON.stringify(data);
        _globalData.ChallengeNum = resData.data.operateResult.chance;
        wx.navigateTo({
          url: '../game/game?data=' + _data1,
        });
      } else {

        util.PromptPanel(that, that.tanping, res.data.message);
      }
    }
    util.requestUrl(url, _data, _cookie, "POST", requestcall);
  },
  /* 打开排行榜 */
  openRanking() {
    if (isOpenRank) {
      isOpenRank = false;
      wx.navigateTo({
        url: '../ranking/ranking',
        success: function (res) { },
        fail: function (res) {
          isOpenRank = true;
        },
      })
    }

  },
  /* 商品兑换 */
  openShop() {
    if (isOpenshop) {
      isOpenshop = false;
      wx.navigateTo({
        url: '../shop/shop',
        success: function (res) { },
        fail: function (res) {
          isOpenshop = true;
        },
      })
    }

  },
  /* 打开个人信息 */
  openUserInfo() {
    if (isOpenUser) {
      isOpenUser = false;
      wx.navigateTo({
        url: '../userInfo/userInfo',
        success: function (res) { },
        fail: function (res) {
          isOpenUser = true;
        },
      })
    }
  },


  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    wx.setNavigationBarColor({
      frontColor: '#ffffff',
      backgroundColor: '#774599',
    })
    wx.setNavigationBarTitle({
      title: _url.gameTitle,
    })
    this.summary();
    isOpenRank = true;
    isOpenshop = true;
    isOpenUser = true;

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    this.setData({
      Isanimation: ''
    })
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    this.setData({
      Isanimation: ""
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */

  quxiaoBtn() {
    this.share.hideDialog();
  },
  scneeQuxiao() {
    this.scene.hideDialog()
  },
  /**
     * 生命周期函数--监听页面初次渲染完成
     */
  onReady: function () {
    this.share = this.selectComponent("#share");
    this.tab = this.selectComponent("#tab");
    this.intro = this.selectComponent("#intro");
    this.scene = this.selectComponent("#scene");
    this.tanping = this.selectComponent("#tanping");

  },
  onShareAppMessage: function (res) {
    if (res.target && res.target.id == "shareId") {
      var that = this;
      var nextFun = function (data) {
        console.log("reer");
        that.share.hideDialog();
      }
      return util.onShareAppMessage(this, this.tanping, nextFun);
    } else {
      return util.onShareAppMessage(this, this.tanping, null);
    }

  },

})