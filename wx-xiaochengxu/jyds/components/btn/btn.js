// components/btn/btn.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    btnIcon: {
      type: String,
      default: ''
    },
    btnText: {
      type: String,
      default: '开始挑战'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    _confirmEvent() {
      //触发成功回调
      console.log("btngoHome")
      this.triggerEvent("confirmEvent");
    },
  }
})
