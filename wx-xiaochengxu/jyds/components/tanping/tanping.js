// components/tanping/tanping.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    shareDesc: {
      type: String,
      default: '开始挑战'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isShow:false

  },

  /**
   * 组件的方法列表
   */
  methods: {
    hideDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    //展示弹框
    showDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    }
  }
})
