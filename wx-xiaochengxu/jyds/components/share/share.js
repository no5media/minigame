// components/taping/sence.js
Component({
  /**
   * 组件的属性列表
   */

  properties: {
    title: {
      type: String,
      default: ''
    },
    dsc: {
      type: String,
      default: ''
    },
    shareId: {
      type: String,
      default: ''
    }

  },


  /**
   * 组件的初始数据
   */
  data: {
    isShow: false,
    imageSize: {}
  },

  /**
   * 组件的方法列表
   */
  methods: {
    hideDialog() {
      console.log("ss", this.data.isShow)
      this.setData({
        isShow: !this.data.isShow
      })
    },
    //展示弹框
    showDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    _confirmEvent() {
      //触发成功回调
      this.triggerEvent("confirmEvent");
    },
    quxiaoBtn() {
      this.triggerEvent("quxiaoBtn");
    },
    
  }
})
