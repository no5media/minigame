// components/rank/rank.js
var gamedata = [
  {
    imgsrc: "http://thirdqq.qlogo.cn/qqapp/101026391/5E2F719FD1E804DB5252EDFB2964E659/100",
    name: "周志伟",
  }
]

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    rankTitle:{
      type: String,
      default: "每周一凌晨刷新",
    },
    pageRank: {
      default: [],
      type: Array
    },
    myRank: {
      type: Object,
      default: {
        rankIndex: 0,
        userProfile: {
          profile: {
            headUrl: "",
            nickname: ""
          }
        }
      }
    }
  },
  attached() {
  },
  ready() {
  },
  /**
   * 组件的初始数据
   */
  data: {
    isShow: false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    dataInit() {
    },
    hideDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    //展示弹框
    showDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    _confirmEvent() {
      //触发成功回调
      this.triggerEvent("confirmEvent");
    },
    goHome() {
      console.log("rankgoHome")
      this.triggerEvent("confirmEvent");
    }
  }
})
