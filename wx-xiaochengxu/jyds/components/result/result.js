// components/rank/rank.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    isshowResult: {
      type: Boolean,
      default: ''
    },
    isSuccessResult: {
      type: Boolean,
      default: ''
    },
    isSuccess: {
      type: Boolean,
      default: ''
    },
    score: {
      type: String,
      default: '0'
    },
    playerName: {
      type: String,
      default: '周志伟'
    },
    time: {
      type: String,
      default: '30'
    },
  },
  /**
   * 组件的初始数据
   */
  data: {
    isShow: false,
    isOpen: true
  },

  /**
   * 组件的方法列表
   */
  methods: {
    hideDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    //展示弹框
    isFail() {
      console.log("fail")
      this.setData({
        isOpen: false
      })
      this.showDialog();
    },
    isSuccess() {
      console.log("isSuccess")
      this.setData({
        isOpen: true
      })
      this.showDialog();
    },
    showDialog() {
      this.setData({
        isShow: !this.data.isShow
      })
    },
    _confirmEvent() {
      //触发成功回调
      console.log("触发成功回调")
      this.triggerEvent("confirmEvent");
    },
    gohome() {
      this.triggerEvent("gohome");
    },
    gochallenge() {
      this.triggerEvent("gochallenge");

    }

  }
})
