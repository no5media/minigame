var ua = window.navigator.userAgent.toLowerCase();
var userAgent = getUserAgent();
export default {
    userAgent,
    // 获取userAgent
    getUserAgent,
    // 获取url参数
    getUrlParams,
    // 时间格式化
    dateFormat,
    // 经验值转换为图片icon
    expToIcon,
    // 复制内容
    copyContent
}

/**
 * 获取用户的ua信息
 * return 
 * ```
 * {
 *      plat: 'android | ios | pc',
 *      client: 'client | wx | qq | others'
 * }
 * ```
 */
function getUserAgent() {
    var plat = ''; //安卓 ios pc
    var client = ''; //微信 qq 客户端 其他
    if (ua.indexOf("android") > -1) {
        plat = "android";
    } else if (ua.indexOf("iphone") > -1 || ua.indexOf("ipad") > -1 || ua.indexOf("ipod") > -1) {
        plat = "ios";
    } else {
        plat = "pc";
    }

    if (/dwjia/i.test(ua)) {
        client = 'client';
    } else if (/MicroMessenger/i.test(ua)) {
        client = 'wx';
    } else if (/QQ\//i.test(ua)) {
        client = 'qq';
    } else {
        client = 'others'
    }
    return {
        plat: plat,
        client: client
    }
}

/**
 * 
 * 获取url上的参数
 * 
 * @param {String} name     要获取的参数
 * @param {String} url      要使用的url 如果不穿参数 则会使用当前url
 * @returns 
 */
function getUrlParams(name, url) {
    if (name === undefined) {
        return;
    }
    if (url === undefined) {
        url = window.location.href;
    }
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = url.substr(url.indexOf('?')).substr(1).match(reg);
    if (r != null) {
        return decodeURIComponent(r[2]);
    }
    return '';
}

/**
 * 时间格式转换
 * @param {String|Date} date 
 * @param {String} fmt 
 */
function dateFormat(date, fmt) {
    if (typeof(fmt) == 'undefined') {
        fmt = 'yyyy-MM-dd hh:mm:ss'
    }
    return (Format((new Date(date)), fmt));
}

/**
 * 格式化日期
 * @param {*} date 
 * @param {*} fmt 
 */
function Format(date, fmt) {
    var o = {
        "M+": date.getMonth() + 1, //月份 
        "d+": date.getDate(), //日 
        "h+": date.getHours(), //小时 
        "m+": date.getMinutes(), //分 
        "s+": date.getSeconds(), //秒 
        "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
        "S": date.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) {
        fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    }
    for (var k in o) {
        if (new RegExp("(" + k + ")").test(fmt)) {
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        }
    }
    return fmt;
}

/**
 * 经验数值转换为icon图片
 * @param {Number} level 
 */
function expToIcon(level) {
    var lev, levToFour, levArr,
        i, len, n,
        testHtml, strHtml = '',
        imgurl;
    lev = parseInt(level);
    if (!lev) {
        return '';
    }
    levToFour = lev.toString(4);
    levArr = levToFour.split('').reverse();
    for (i = 0, len = levArr.length; i < len; i++) {
        switch (i) {
            case 0:
                //星星
                imgurl = '<img src="http://images.1758.com/game/m/hdt.png" alt="星星">';
                break;
            case 1:
                //月亮
                imgurl = '<img src="http://images.1758.com/game/m/hds.png" alt="月亮">';
                break;
            case 2:
                //太阳
                imgurl = '<img src="http://images.1758.com/game/m/hdu.png" alt="太阳">';
                break;
            case 3:
                //皇冠
                imgurl = '<img src="http://images.1758.com/game/m/hdr.png" alt="皇冠">';
                break;
        }
        testHtml = '';
        n = parseInt(levArr[i]);
        while (n > 0) {
            testHtml += imgurl;
            n--;
        }
        if (strHtml == '') {
            strHtml = testHtml;
        } else {
            strHtml = testHtml + strHtml;
        }
    }
    return strHtml;
}

// 复制内容
function copyContent(s) {
    if (typeof document.execCommand === 'function') {
        var oInput = document.createElement('input');
        oInput.value = s;
        oInput.style.position = 'fixed';
        oInput.style.zIndex = -222;
        oInput.className = 'oInput';
        document.body.appendChild(oInput);
        oInput.select(); // 选择对象
        var bl = document.execCommand("Copy"); // 执行浏览器复制命令
        oInput.remove();
        return bl;
    } else {
        return false;
    }
}