export default {
    /**
     * 设置html页面的font-size
     */
    browerUa: function() {
        if (this.isPc()) {
            document.getElementsByTagName('html')[0].style.fontSize = 50 + 'px';
            return;
        }
        var Dpr = 1,
            uAgent = window.navigator.userAgent;
        var isIOS = uAgent.match(/iphone/i);

        function resizeRoot() {
            var wWidth = (screen.width > 0) ? (window.innerWidth >= screen.width || window.innerWidth == 0) ? screen.width :
                window.innerWidth : window.innerWidth,
                wDpr, wFsize;
            var wHeight = (screen.height > 0) ? (window.innerHeight >= screen.height || window.innerHeight == 0) ?
                screen.height : window.innerHeight : window.innerHeight;
            if (window.devicePixelRatio) {
                wDpr = window.devicePixelRatio;
            } else {
                wDpr = isIOS ? wWidth > 818 ? 3 : wWidth > 480 ? 2 : 1 : 1;
            }
            if (isIOS) {
                wWidth = screen.width;
                wHeight = screen.height;
            }
            if (wWidth > wHeight) {
                wWidth = wHeight;
            }
            wFsize = wWidth > 1080 ? 144 : wWidth / 7.5;
            wFsize = wFsize > 32 ? wFsize : 32;
            window.screenWidth_ = wWidth;
            document.getElementsByTagName('html')[0].style.fontSize = wFsize + 'px';
        }
        resizeRoot();
    },
    // 判断是否是pc，返回true或false
    isPc: function() {
        if (/AppleWebKit.*Mobile/i.test(navigator.userAgent)) {
            try {
                if (/Android|Windows Phone|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
                    // mobile
                    // window.location.href = "http://3g.163.com";
                    return false;
                }
            } catch (e) {}
        }
        return true;
    }
}